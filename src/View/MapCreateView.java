package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import Controller.MapCreateViewController;
import Model.CampaignModel;
import Model.CharacterModel;
import Model.ItemModel;
import Model.MapModel;
import metadatas.GridCellMetadata;
import metadatas.ListMetadata;
import utilities.Grid;
import utilities.GridCell;
import utilities.ListPanelUtility;
import utilities.ListUtility;
import javax.swing.JComboBox;

/**
 * This Class forms the core of Creating a New Map or Editing an Existing Map.
 * Lays out the Grid os user required size to design a new Map.
 * It implements Listener classes to capture all the User inputs.
 * It calls the MapViewController Class to process the user inputs and proceed further to Model.
 * 
 * @author Vijay Karan Singh
 * @see MapCreateViewController
 * @see ItemModel
 * @see CharacterModel
 * @see MapModel
 * @see Grid
 * 
 */

public class MapCreateView extends JPanel implements ActionListener,PropertyChangeListener,ItemListener{
	
	/**
	 * Jpanels for Outer and Inner panel
	 */
	private JPanel mainpanel;
	private JPanel map_panel;
	
	/**
	 * Array of ToggleButtons for Map Items
	 */
	private JToggleButton tglbtns[] = new JToggleButton[14];
	
	/**
	 * Separator for Spacing
	 */
	private JSeparator separator;
	
	/**
	 * JButtons for Map Operations
	 */
	private JButton btnSaveMap;
	private JButton btnCancel;
	private JButton btnDeleteItem;
	
	/**
	 * Map name textfield
	 */
	private JTextField txtEnterMapName;
	
	/**
	 * Go to Maps List Window
	 */
	private JButton btnMapsList;
	
	/**
	 * Labels for Fields
	 */
	private JLabel lblMmxm;
	private JLabel lblMapTag;
	private JLabel lblSelectedList;
	
	/**
	 * New Buttons
	 */
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnNewButton_3;
	
	/**
	 * Vertical Strut components to add spacing between components
	 */
	private Component verticalStrut;
	private Component verticalStrut_1;
	
	/**
	 * Map Size Drop Down List
	 */
	private JComboBox comboBox;
	
	/**
	 * List Area Map Window
	 */
	private JTextArea listArea;
	
	/**
	 * Controller Object for handling MapCreateView
	 */
	MapCreateViewController controller;
	
	/**
	 * Models Associated with Map Create View
	 */
	CharacterModel characterModel;
	ItemModel itemModel;
	Grid grid;
	GridCell gridCell_matrix[][];
	
	/**
	 * Variables to Store the Value and Type of cell to save. 
	 */
	String value;
	String type;
	
	/**
	 * ListPanelUtilities to act as containers for different item lists.
	 */
	public static ListPanelUtility listPanel;
	public static ListPanelUtility listPanel2;
	public static ListPanelUtility listPanel3;
	public static ListPanelUtility listPanel4, listPanel5, listPanel6, listPanel7, listPanel8, listPanel9;
	ListMetadata listMetadata, listMetadata2, listMetadata3, listMetadata4, listMetadata5, listMetadata6, listMetadata7,
	listMetadata8, listMetadata9;

	/**
	 * HashMap containing CharacterModel data
	 */
	public static HashMap<String, CharacterModel> characterListData;
	
	/**
	 * Default Map Size
	 */
	String map_size = "8";
	
	/**
	 * initializing the left Panel.
	 */
	JPanel left_container = new JPanel();
	
	
	/**
	 * Constructor to initialize the Create Map View 
	 * 
	 * @param controller
	 * @param grid
	 */
	public MapCreateView(MapCreateViewController controller, Grid grid, String init_map_name) {
		this.controller = controller;
		this.grid = grid;
		init_map(init_map_name);
	}	
	
	
	/**
	 * Called from MapCreateView Constructor.
	 * Core of Map Creation Initialization. 
	 */
	public void init_map(String init_map_name)
	{
		
		characterModel = new CharacterModel();
		
		itemModel = new ItemModel();
		
		mainpanel = this;
		mainpanel.setBounds(0, 0, 800, 800);
		mainpanel.setLayout(new BorderLayout());		
		
		left_container.setLayout(new BoxLayout(left_container, BoxLayout.Y_AXIS));
		
		listPanel = new ListPanelUtility(characterModel.getListMetadata());
		listPanel.setToolTipText("");
		listPanel.setVisible(false);
		listPanel.addPropertyChangeListener(this);
		listMetadata = listPanel.getSelectedObject();
	
		listPanel2 = new ListPanelUtility(characterModel.getListMetadata());
		listPanel2.setToolTipText("");
		listPanel2.setVisible(false);
		listPanel2.addPropertyChangeListener(this);
		listMetadata2 = listPanel2.getSelectedObject();
		
		listPanel3 = new ListPanelUtility(itemModel.getListMetadata("HELMET"));
		listPanel3.setToolTipText("");
		listPanel3.setVisible(false);
		listPanel3.addPropertyChangeListener(this);
		listMetadata3 = listPanel3.getSelectedObject();
		
		listPanel4 = new ListPanelUtility(itemModel.getListMetadata("SHIELD"));
		listPanel4.setToolTipText("");
		listPanel4.setVisible(false);
		listPanel4.addPropertyChangeListener(this);
		listMetadata4 = listPanel4.getSelectedObject();
		
		listPanel5 = new ListPanelUtility(itemModel.getListMetadata("BELT"));
		listPanel5.setToolTipText("");
		listPanel5.setVisible(false);
		listPanel5.addPropertyChangeListener(this);
		listMetadata5 = listPanel5.getSelectedObject();
		
		listPanel6 = new ListPanelUtility(itemModel.getListMetadata("ARMOR"));
		listPanel6.setToolTipText("");
		listPanel6.setVisible(false);
		listPanel6.addPropertyChangeListener(this);
		listMetadata6 = listPanel6.getSelectedObject();
		
		listPanel7 = new ListPanelUtility(itemModel.getListMetadata("RING"));
		listPanel7.setToolTipText("");
		listPanel7.setVisible(false);
		listPanel7.addPropertyChangeListener(this);
		listMetadata7 = listPanel7.getSelectedObject();
		
		listPanel8 = new ListPanelUtility(itemModel.getListMetadata("BOOTS"));
		listPanel8.setToolTipText("");
		listPanel8.setVisible(false);
		listPanel8.addPropertyChangeListener(this);
		listMetadata8 = listPanel8.getSelectedObject();
		
		listPanel9 = new ListPanelUtility(itemModel.getListMetadata("WEAPON"));
		listPanel9.setToolTipText("");
		listPanel9.setVisible(false);
		listPanel9.addPropertyChangeListener(this);
		listMetadata9 = listPanel9.getSelectedObject();
		
		// Items Panel 
		JPanel items_panel = new JPanel();
		items_panel.setBounds(new Rectangle(0, 0, 200, 500));
		items_panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		
		// List Panel
		JPanel list_panel = new JPanel();
		list_panel.setLayout(new BorderLayout());
		
		listArea = new JTextArea();
		listArea.setBounds(new Rectangle(0, 0, 0, 40));
		listArea.setBorder(new LineBorder(Color.LIGHT_GRAY, 2));
		
		tglbtns[0] = new JToggleButton("Entry");
		tglbtns[0].setPreferredSize(new Dimension(40, 10));
		tglbtns[0].setMargin(new Insets(4, 4, 4, 4));
		tglbtns[0].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[0].addItemListener(this);
		tglbtns[0].addActionListener(this);
		tglbtns[1] = new JToggleButton("Exit");
		tglbtns[1].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[1].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[1].addItemListener(this);
		tglbtns[1].addActionListener(this);
		tglbtns[2] = new JToggleButton("Monster");
		tglbtns[2].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[2].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[2].addItemListener(this);
		tglbtns[2].addActionListener(this);
		tglbtns[3] = new JToggleButton("Friend");
		tglbtns[3].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[3].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[3].addItemListener(this);
		tglbtns[3].addActionListener(this);
		tglbtns[4] = new JToggleButton("Wall");
		tglbtns[4].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[4].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[4].addItemListener(this);
		tglbtns[4].addActionListener(this);
		tglbtns[5] = new JToggleButton("Key");
		tglbtns[5].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[5].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[5].addItemListener(this);
		tglbtns[5].addActionListener(this);
		tglbtns[6] = new JToggleButton("Chest");
		tglbtns[6].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[6].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[6].addItemListener(this);
		tglbtns[6].addActionListener(this);
		tglbtns[7] = new JToggleButton("Weapon");
		tglbtns[7].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[7].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[7].addItemListener(this);
		tglbtns[7].addActionListener(this);
		tglbtns[8] = new JToggleButton("Armour");
		tglbtns[8].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[8].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[8].addItemListener(this);
		tglbtns[8].addActionListener(this);
		tglbtns[9] = new JToggleButton("Helmet");
		tglbtns[9].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[9].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[9].addItemListener(this);
		tglbtns[9].addActionListener(this);
		tglbtns[10] = new JToggleButton("Shield");
		tglbtns[10].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[10].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[10].addItemListener(this);
		tglbtns[10].addActionListener(this);
		tglbtns[11] = new JToggleButton("Ring");
		tglbtns[11].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[11].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[11].addItemListener(this);
		tglbtns[11].addActionListener(this);
		tglbtns[12] = new JToggleButton("Belt");
		tglbtns[12].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[12].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[12].addItemListener(this);
		tglbtns[12].addActionListener(this);
		tglbtns[13] = new JToggleButton("Boots");
		tglbtns[13].setMargin(new Insets(2, 4, 2, 4));
		tglbtns[13].setFont(new Font("Tahoma", Font.PLAIN, 16));
		tglbtns[13].addItemListener(this);
		tglbtns[13].addActionListener(this);
		
		items_panel.add(tglbtns[0]);
		items_panel.add(tglbtns[1]);
		items_panel.add(tglbtns[2]);
		items_panel.add(tglbtns[3]);
		items_panel.add(tglbtns[4]);
		items_panel.add(tglbtns[5]);
		items_panel.add(tglbtns[6]);
		items_panel.add(tglbtns[7]);
		items_panel.add(tglbtns[8]);
		items_panel.add(tglbtns[9]);
		items_panel.add(tglbtns[10]);
		items_panel.add(tglbtns[11]);
		items_panel.add(tglbtns[12]);
		items_panel.add(tglbtns[13]);
		
		// Map Area Panel
		map_panel = new JPanel();
		map_panel.setLayout(null);
				
		separator = new JSeparator();
		separator.setForeground(Color.GRAY);
		separator.setBackground(Color.GRAY);
		separator.setPreferredSize(new Dimension(0, 0));
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(5, 0, 5, 750);
		map_panel.add(separator);
		
		grid.setBounds(10, 50, 605, 605);
		map_panel.add(grid);
		
		// Add property change listener to Grid
		grid.addPropertyChangeListener(this);
		
		mainpanel.add(left_container, BorderLayout.WEST);
		left_container.add(items_panel);
		
		verticalStrut_1 = Box.createVerticalStrut(20);
		left_container.add(verticalStrut_1);
		
		lblSelectedList = new JLabel("Selected List");
		lblSelectedList.setBounds(new Rectangle(2, 10, 100, 70));

		lblSelectedList.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSelectedList.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		lblSelectedList.setAlignmentX(Component.CENTER_ALIGNMENT);
		left_container.add(lblSelectedList);
		
		verticalStrut = Box.createVerticalStrut(20);
		left_container.add(verticalStrut);
		left_container.add(listPanel);
		left_container.add(listPanel2);
		left_container.add(listPanel3);
		left_container.add(listPanel4);
		left_container.add(listPanel5);
		left_container.add(listPanel6);		
		left_container.add(listPanel7);
		left_container.add(listPanel8);
		left_container.add(listPanel9);
		
		btnNewButton = new JButton("New button1");
		btnNewButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		Component verticalStrut_2 = Box.createVerticalStrut(20);
		left_container.add(verticalStrut_2);
		//left_container.add(btnNewButton);
		
		Component verticalStrut_3 = Box.createVerticalStrut(20);
		//left_container.add(verticalStrut_3);
		
		btnNewButton_1 = new JButton("New button2");
		btnNewButton_1.setAlignmentX(Component.CENTER_ALIGNMENT);
		//left_container.add(btnNewButton_1);
		
		Component verticalStrut_4 = Box.createVerticalStrut(20);
		left_container.add(verticalStrut_4);
		
		btnNewButton_2 = new JButton("New button3");
		btnNewButton_2.setAlignmentX(Component.CENTER_ALIGNMENT);
		//left_container.add(btnNewButton_2);
		
		Component verticalStrut_5 = Box.createVerticalStrut(20);
		left_container.add(verticalStrut_5);
		
		btnNewButton_3 = new JButton("New button4");
		btnNewButton_3.setAlignmentX(Component.CENTER_ALIGNMENT);
		//left_container.add(btnNewButton_3);
		
		Component verticalStrut_6 = Box.createVerticalStrut(20);
		left_container.add(verticalStrut_6);
		mainpanel.add(map_panel, BorderLayout.CENTER);
		
		btnSaveMap = new JButton("Save Map");
		btnSaveMap.setBounds(22, 698, 97, 25);
		btnSaveMap.addActionListener(this);
		map_panel.add(btnSaveMap);
		
		btnCancel = new JButton("Cancel");
		btnCancel.setBounds(339, 698, 117, 25);
		btnCancel.addActionListener(this);
		map_panel.add(btnCancel);
		
		btnDeleteItem = new JButton("Delete Item");
		btnDeleteItem.setBounds(167, 698, 117, 25);
		btnDeleteItem.addActionListener(this);
		//map_panel.add(btnDeleteItem);
		
		btnMapsList = new JButton("Maps List");
		btnMapsList.setBounds(496, 698, 97, 25);
		btnMapsList.addActionListener(this);
		//map_panel.add(btnMapsList);
		
		txtEnterMapName = new JTextField();
		txtEnterMapName.setText(init_map_name);
		txtEnterMapName.setBounds(115, 12, 207, 25);
		map_panel.add(txtEnterMapName);
		txtEnterMapName.setColumns(10);
		
		lblMmxm = new JLabel("Map Name:");
		lblMmxm.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMmxm.setBounds(19, 12, 100, 25);
		map_panel.add(lblMmxm);
		
		lblMapTag = new JLabel("Map Size:");
		lblMapTag.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMapTag.setBounds(353, 12, 83, 25);
		map_panel.add(lblMapTag);
		
		comboBox = new JComboBox();
		comboBox.setBounds(448, 15, 100, 22);
		comboBox.addItem("8X8");
		comboBox.addItem("6X6");
		comboBox.addItem("7X7");
		comboBox.addItem("9X9");
		comboBox.addActionListener(this);
		map_panel.add(comboBox);
	}
	
	/* Handles the State Change Event for JToggleButtons
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	public void itemStateChanged(ItemEvent ev)
	{
		JToggleButton tbtn = (JToggleButton) ev.getItem();
		if(tbtn.getActionCommand() == "Entry" && tbtn.isSelected()){
			value = "Entry";
			type = "ENTRY";
		}
		else if(tbtn.getActionCommand() == "Entry" && !tbtn.isSelected()){
			value = "";
			type = "";
		}
		else if(tbtn.getActionCommand() == "Exit" && tbtn.isSelected()){
			value = "Exit";
			type = "EXIT";
		}
		else if(tbtn.getActionCommand() == "Exit" && !tbtn.isSelected()){
			value = "";
			type = "";
		}
		else if(tbtn.getActionCommand() == "Wall" && tbtn.isSelected()){
			value = "Wall";
			type = "WALL";
		}
		else if(tbtn.getActionCommand() == "Wall" && !tbtn.isSelected()){
			value = "";
			type = "";
		}
		else if(tbtn.getActionCommand() == "Key" && tbtn.isSelected()){
			value = "Key";
			type = "KEY";
		}
		else if(tbtn.getActionCommand() == "Key" && !tbtn.isSelected()){
			value = "";
			type = "";
		}
		else if(tbtn.getActionCommand() == "Monster" && tbtn.isSelected()){
			//value = "Monster";
			type = "MONSTER";
			MapCreateView.listPanel.setVisible(true);
			value = listMetadata.getItem_ID();
		}
		else if(tbtn.getActionCommand() == "Monster" && !tbtn.isSelected()){
			value = "";
			type = "";
			MapCreateView.listPanel.setVisible(false);
		}
		else if(tbtn.getActionCommand() == "Friend" && tbtn.isSelected()){
			//value = "Friend";
			type = "FRIEND";
			MapCreateView.listPanel2.setVisible(true);
			value = listMetadata2.getItem_ID();
		}
		else if(tbtn.getActionCommand() == "Friend" && !tbtn.isSelected()){
			value = "";
			type = "";
			MapCreateView.listPanel2.setVisible(false);
		}
		else if(tbtn.getActionCommand() == "Chest" && tbtn.isSelected()){
			value = "";
			type = "CHEST";
			
		}
		else if(tbtn.getActionCommand() == "Chest" && !tbtn.isSelected()){
			value = "";
			type = "";
		}
		else if(tbtn.getActionCommand() == "Weapon" && tbtn.isSelected()){
			//value = "Weapon";
			type = "WEAPON";
			MapCreateView.listPanel9.setVisible(true);
			value = listMetadata9.getItem_ID();
		}
		else if(tbtn.getActionCommand() == "Weapon" && !tbtn.isSelected()){
			value = "";
			type = "";
			MapCreateView.listPanel9.setVisible(false);
		}
		else if(tbtn.getActionCommand() == "Armour" && tbtn.isSelected()){
			//value = "Armour";
			type = "ARMOR";
			MapCreateView.listPanel6.setVisible(true);
			value = listMetadata6.getItem_ID();
		}
		else if(tbtn.getActionCommand() == "Armour" && !tbtn.isSelected()){
			value = "";
			type = "";
			MapCreateView.listPanel6.setVisible(false);
		}
		else if(tbtn.getActionCommand() == "Helmet" && tbtn.isSelected()){
			//value = "Helmet";
			type = "HELMET";
			MapCreateView.listPanel3.setVisible(true);
			value = listMetadata3.getItem_ID();
		}
		else if(tbtn.getActionCommand() == "Helmet" && !tbtn.isSelected()){
			value = "";
			type = "";
			MapCreateView.listPanel3.setVisible(false);
		}
		else if(tbtn.getActionCommand() == "Shield" && tbtn.isSelected()){
			//value = "Shield";
			type = "SHIELD";
			MapCreateView.listPanel4.setVisible(true);
			value = listMetadata4.getItem_ID();
		}
		else if(tbtn.getActionCommand() == "Shield" && !tbtn.isSelected()){
			value = "";
			type = "";
			MapCreateView.listPanel4.setVisible(false);
		}
		else if(tbtn.getActionCommand() == "Ring" && tbtn.isSelected()){
			//value = "Ring";
			type = "RING";
			MapCreateView.listPanel7.setVisible(true);
			value = listMetadata7.getItem_ID();
		}
		else if(tbtn.getActionCommand() == "Ring" && !tbtn.isSelected()){
			value = "";
			type = "";
			MapCreateView.listPanel7.setVisible(false);
		}
		else if(tbtn.getActionCommand() == "Belt" && tbtn.isSelected()){
			//value = "Belt";
			type = "BELT";
			MapCreateView.listPanel5.setVisible(true);
			value = listMetadata5.getItem_ID();
		}
		else if(tbtn.getActionCommand() == "Belt" && !tbtn.isSelected()){
			value = "";
			type = "";
			MapCreateView.listPanel5.setVisible(false);
		}
		else if(tbtn.getActionCommand() == "Boots" && tbtn.isSelected()){
			//value = "Boots";
			type = "BOOTS";
			MapCreateView.listPanel8.setVisible(true);
			value = listMetadata8.getItem_ID();
		}
		else if(tbtn.getActionCommand() == "Boots" && !tbtn.isSelected()){
			value = "";
			type = "";
			MapCreateView.listPanel8.setVisible(false);
		}		
	}
	
	
	/* Handles the Action Performed Event on Buttons
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) 
	{
		
		if(e.getActionCommand() == "Save Map"){
			if(txtEnterMapName.getText().isEmpty()){
				System.out.println("Please Enter Name for Map");
			}
			else{
				String map_name = txtEnterMapName.getText();
				String size = map_size;
				controller.saveMap(map_name, size);
			}
		}
		else if(e.getActionCommand() == "Cancel"){
			controller.cancelOperation();
		}
		
		for(JToggleButton jtb : tglbtns){
			if(!(jtb.getActionCommand() == e.getActionCommand())){
				jtb.setSelected(false);
				
			}
		}
		
		if(e.getSource() instanceof JComboBox){
			  JComboBox combo = (JComboBox) e.getSource();
			  Object selected = combo.getSelectedItem();
			  if(selected == "6X6")
			  {
				  grid.setNewSize(6);
				  map_size = "6";
				  map_panel.remove(grid);
				  map_panel.repaint();
				  map_panel.add(grid);
				  map_panel.revalidate();
			  }
			  else if(selected == "8X8")
			  {
				  grid.setNewSize(8);
				  map_size = "8";
				  map_panel.remove(grid);
				  map_panel.repaint();
				  map_panel.add(grid);
				  map_panel.revalidate();
			  }
			  else if(selected == "7X7")
			  {
				  grid.setNewSize(7);
				  map_size = "7";
				  map_panel.remove(grid);
				  map_panel.repaint();
				  map_panel.add(grid);
				  map_panel.revalidate();
			  }
			  else if(selected == "9X9")
			  {
				  grid.setNewSize(9);
				  map_size = "9";
				  map_panel.remove(grid);
				  map_panel.repaint();
				  map_panel.add(grid);
				  map_panel.revalidate();
			  }
			}
	}
	
	
	/* Handles the Property Change event on ListUtilities.
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) 
	{
		while(GridCell.isChanged)
		{
			GridCell.isChanged = false;
			GridCellMetadata gridCell = GridCell.responseCellData;
			int xPos = gridCell.getxPosition();
			int yPos = gridCell.getyPosition();
			int mapSize = Integer.parseInt(map_size);
			int allowedX = 0;
			int allowedY = 0;
			int allowed = 0;
			int keyAllowed = 0;
			boolean validEntryExit = true;
			
			if(type == "WALL"){
				
				for(int i=xPos+1; i<xPos+4 && i < mapSize; i++){
					if(grid.get_GridCellType(i, yPos) == "WALL"){
						allowedX++;
					}
				}
				for(int i=xPos-1; i>xPos-4 && i >= 0; i--){
					if(grid.get_GridCellType(i, yPos) == "WALL"){
						allowedX++;
					}
				}
				for(int i=yPos+1; i<yPos+4 && i < mapSize; i++){
					if(grid.get_GridCellType(xPos, i) == "WALL"){
						allowedY++;
					}
				}
				for(int i=yPos-1; i>yPos-4 && i >= 0; i--){
					if(grid.get_GridCellType(xPos, i) == "WALL"){
						allowedY++;
					}
				}
				for(int i=xPos+1; i<xPos+4 && i<mapSize; i++){
					for(int j=yPos+1; j<yPos+4 && j<mapSize; j++){
						
					}
				}
				for(int i=1;i<4;i++){
					if(xPos+i<mapSize && yPos-i>0 && grid.get_GridCellType(xPos+i, yPos-i) == "WALL"){
						allowedX++;
						allowedY++;
					}
					if(xPos+i<mapSize && yPos+i<mapSize && grid.get_GridCellType(xPos+i, yPos+i) == "WALL"){
						allowedX++;
						allowedY++;
					}
					if(xPos-i>0 && yPos-i>0 && grid.get_GridCellType(xPos-i, yPos-i) == "WALL"){
						allowedX++;
						allowedY++;
					}
					if(xPos-i>0 && yPos+i<mapSize && grid.get_GridCellType(xPos-i, yPos+i) == "WALL"){
						allowedX++;
						allowedY++;
					}
				}
			}
			
			else if(type == "ENTRY"){
				for(int i=0;i<mapSize;i++){
					for(int j=0;j<mapSize;j++){
						if(grid.get_GridCellType(i, j) == "ENTRY"){
							allowed = 3;
						}
					}
				}
				if(!isBoundary(xPos, yPos)){
					validEntryExit = false;
				}
			}
			
			else if(type == "EXIT"){
				for(int i=0;i<mapSize;i++){
					for(int j=0;j<mapSize;j++){
						if(grid.get_GridCellType(i, j) == "EXIT"){
							allowed = 3;
						}
					}
				}
				if(!isBoundary(xPos, yPos)){
					validEntryExit = false;
				}
			}
			else if(type == "KEY"){
				for(int i=0;i<mapSize;i++){
					for(int j=0;j<mapSize;j++){
						if(grid.get_GridCellType(i, j) == "KEY"){
							keyAllowed = 3;
						}
					}
				}
			}
			else if(type == "WEAPON" || type == "ARMOR" || type == "SHIELD" || type == "HELMET" || type == "RING" ||
					type == "BELT" || type == "BOOTS"){
				if(grid.get_GridCellType(xPos, yPos) == "CHEST"){
					String currentVal = grid.get_GridCellValue(xPos, yPos);
					String append = "#" + type + "_" + value;
					value = currentVal + append;
					type = "CHEST";
					
				}
			}
			
			
						
			if(allowedX >=3 || allowedY >=3){
				System.out.println("Cannot Place More than 3 Adjacent Walls");
			}
			else if(allowed == 3){
				System.out.println("No More than One Entry/Exit on the Map");
			}
			else if(keyAllowed == 3){
				System.out.println("Only One Key Allowed on the Map");
			}
			else if(validEntryExit == false){
				System.out.println("Entry/Exit Can only be Placed on Boundary");
			}
			else
				controller.updateGridCell(type, value, map_size);
			
			
			
			for (JToggleButton jToggleButton : tglbtns) {
	        jToggleButton.setSelected(false);
	        }			
		}
		
		while(ListUtility.isChanged)
		{	
			ListUtility.isChanged = false;
			value = ListUtility.responseMetadata.getSelected_id();
//			value = ListUtility.responseMetadata.getSelected_name();				
		}
	}
	
	/**
	 * This Method tests the Boundary Condition for Entry and Exit Positions on the Map.
	 * @param x x axis position of the cell being modified
	 * @param y y axis position of the cell being modified
	 * @return true if on the boundary, false otherwise.
	 */
	public boolean isBoundary(int x, int y)
	{
		int size = Integer.parseInt(map_size);
		int i = 0;
		int k = size-1;
		for(int j=0; j<size; j++){
			if((x==i || x==k) && y==j){
				return true;
			}
			else if((y==i || y==k) && x==j){
				return true;
			}
		}
		return false;
	}
}
