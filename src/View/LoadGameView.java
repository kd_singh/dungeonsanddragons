package View;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import Controller.CampaignMainMenuViewController;
import Controller.LoadGameViewController;
import Model.CampaignModel;
import Model.CharacterModel;
import Model.SessionModel;
import metadatas.CampaignMapsMetadata;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.ListMetadata;
import metadatas.SessionMetadata;
import utilities.ListPanelUtility;
import utilities.ListUtility;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * This Load Game View is the screen which appears when a user clicks load game. It is 
 * used to view and play previously saved Games
 * 
 *
 * @author Tarandeep Singh
 * @version 1.0.0
 * @since 10 April 2017
 * @see CampaignModel
 * @see ListMetadata
 * @see CampaignMainMenuViewController
 * @see ListPanelUtility
 * @see DefaultTableModel
 */

public class LoadGameView extends JPanel implements PropertyChangeListener, ActionListener {
	
	/**
	 * All swing objects used for the Load Game View
	 */
	private JPanel panel;
	private JPanel parentPanel;
	private JPanel panel_ListHeader;
	private JPanel panel_CampaignInfo;
	private JTable tableCampaignInfo;
	private JScrollPane tableCampaignInfoScroller;
	private JLabel lblSelectAGame;
	private JButton btnPlayGame;
	private JButton btnCancel;
	private JScrollPane scrollPane = new JScrollPane();

	/**
	 * DefaultTabelModel instantiated 
	 */

	DefaultTableModel tableModel;
	
	String[] tableHeader = {"Map Sequence","Map Name"};
	private CharacterModel characterModel;
	CampaignModel campaignModel;
	SessionModel sessionModel;
	ListMetadata listMetadata;
	ListMetadata listMetadataChar;
	ListPanelUtility listPanel;
	ListPanelUtility listPanelChar;
	String selectedID;
	String selectedIDChar;
	String actionType;
	
	CharacterMetadata characterMetadata;
	
	/**
	 * A HashMap that contains all the Campaigns inside this.
	 */
	public HashMap<String, SessionMetadata> sessionListData;	
	
	/**
	 * An object created for LoadGameViewController
	 */
	LoadGameViewController controller;

	/**
	 * Constructor to point to current object of controller and campaign model
	 * with calling Initialize method <code> init_() </code>.
	 * @param controller Controller for campaign main menu
	 * @param campaignModel Model for campaign
	 */
	public LoadGameView(LoadGameViewController controller, SessionModel sessionModel) {
		
		this.controller = controller;
		this.sessionModel = sessionModel;
		init_();
	}
	
	/**
	 * This method initializes the view for Load Game 
	 */
	
	public void init_() 
	{

		/**
		 * Returns the state of this class <code>CampaignModel</code> wrapped
		 * in the form of HashMap into campaignListData
		 * 
		 */
		sessionModel = new SessionModel();
		//characterModel = new CharacterModel();
		sessionListData = sessionModel.getSessionMap();
		//characterListData = characterModel.getCharacterMap();
		
		
		///////////////////BASIC LAYOUT OF THE VIEW BEGINS HERE//////////////////////
		parentPanel = this;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0};
		setLayout(gridBagLayout);
		parentPanel.setBounds(0, 0, 800, 800);
		//////////////////HEADING OR VIEW PURPOSE ENDS HERE//////////////////////////


		//////////////////HEADING OR VIEW PURPOSE BEGINS HERE////////////////////////
		lblSelectAGame = new JLabel("SAVED GAME SELECTION MENU");
		lblSelectAGame.setVerticalAlignment(SwingConstants.TOP);
		lblSelectAGame.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblSelectAGame = new GridBagConstraints();
		gbc_lblSelectAGame.insets = new Insets(0, 0, 5, 5);
		gbc_lblSelectAGame.gridx = 2;
		gbc_lblSelectAGame.gridy = 2;
		add(lblSelectAGame, gbc_lblSelectAGame);
		//////////////////HEADING OR VIEW PURPOSE ENDS HERE//////////////////////////
		
		
		/**
		 * Calling the ListPanelUtility to list the Campaign names present in Campaign file
		 * 
		 */
		listPanel = new ListPanelUtility(sessionModel.getListMetadata());
		
		///////////////////////CAMPAIGN LIST DISPLAYED HERE//////////////////////////		
		panel_ListHeader = new JPanel();
		panel_ListHeader.setBorder(new TitledBorder(null, "Saved Game List", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_header = new GridBagConstraints();
		gbc_panel_header.insets = new Insets(0, 0, 5, 5);
		gbc_panel_header.gridx = 2;
		gbc_panel_header.gridy = 4;
		add(panel_ListHeader, gbc_panel_header);
		GridBagLayout gbl_panel_header = new GridBagLayout();
		panel_ListHeader.setLayout(gbl_panel_header);
		GridBagConstraints gbc_listPanel = new GridBagConstraints();
		panel_ListHeader.add(listPanel, gbc_listPanel);
		listPanel.addPropertyChangeListener(this);
		listPanel.setBounds(0,0,100,100);	
		
		if(!(listPanel.getSelectedObject()==null))	
		{
			listMetadata = listPanel.getSelectedObject();
			selectedID = listMetadata.getItem_ID();
		}
		
		/////////////////////////// SETTING THE LAYOUT ///////////////////////////

		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 3;
		gbc_panel.gridy = 6;
		add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		/////////////////////////// SETTING THE LAYOUT ENDS ///////////////////////////
		
		tableModel = new DefaultTableModel(0,0);
		tableModel.setColumnIdentifiers(tableHeader);
		
		///////////////////////// PLAY BUTTON LAYOUT ////////////////////////////////
		btnPlayGame = new JButton("Play");
		GridBagConstraints gbc_btnPlayGame = new GridBagConstraints();
		gbc_btnPlayGame.insets = new Insets(0, 0, 5, 5);
		gbc_btnPlayGame.gridx = 2;
		gbc_btnPlayGame.gridy = 7;
		add(btnPlayGame, gbc_btnPlayGame);
		btnPlayGame.addActionListener(this);
		/////////////////////////LAYOUT ENDS HERE //////////////////////////////////	
		
		btnCancel = new JButton("Cancel");
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 0, 0, 5);
		gbc_btnCancel.gridx = 2;
		gbc_btnCancel.gridy = 9;
		add(btnCancel, gbc_btnCancel);
		btnCancel.addActionListener(this);
		
	}
		

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
	
		if(e.getActionCommand().equals("Play"))
		{
			System.out.println("Play clicked !");
			actionType = "Play";
			if(!(listMetadata==null)  && (selectedID!= null))
			{
				SessionMetadata sessionMetadata = sessionListData.get(selectedID);
				controller.actionPlay(sessionMetadata);
			}
			else if((listMetadata==null))
			{
				JOptionPane.showOptionDialog(null, "Save atleast one Game to play !!", "ERROR", JOptionPane.DEFAULT_OPTION,
				        JOptionPane.INFORMATION_MESSAGE, null, null, null);
			}		
		}
		else if(e.getActionCommand().equals("Cancel"))
		{
			controller.actionCancel();
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		
		while(ListUtility.isChanged)
		{
			ListUtility.isChanged = false;
			selectedID = ListUtility.responseMetadata.getSelected_id();
			SessionMetadata sessionMetadata = sessionListData.get(selectedID);
			//Vector<CampaignMapsMetadata> campaignMapsMetadata = sessionMetadata.getVector_sessionMap();
			//load_CampaignInfoTable(campaignMapsMetadata);
						
		}
		
		
	}

}
