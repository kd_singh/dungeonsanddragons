package View;

import Controller.IndexViewController;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 * The class defines the main window for the Game that has Menu Panel with options
 * for the Game.
 */
public class IndexView extends JPanel implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	static IndexView mainFrame;
	JPanel parentPanel;
	JPanel buttonPanel;
	JButton btn_NewGame, btn_LoadGame, btn_CreateMap,btn_CharacterList,btn_ItemList, btn_demo;
	
	IndexViewController indexViewController = new IndexViewController();
	
	public IndexView() {
		initialize();
	}
	
	/**
	 * This method is used to initialize the Index Window with its panels and buttons
	 * 
	 * @author karandsingh
	 * @since 30 January 2017
	 * 
	 */
	public void initialize()
	{
		
		parentPanel = this;
		parentPanel.setBounds(0, 0, 500, 500);
		parentPanel.setLayout(null);
		
        buttonPanel = new JPanel();
        buttonPanel.setBounds(0, 0, 350, 350);
        buttonPanel.setLayout(null);
        
		btn_NewGame = new JButton("New Game");
		btn_NewGame.setBounds(90, 145, 150, 25);
        btn_NewGame.addActionListener(this);
        buttonPanel.add(btn_NewGame);
        
        btn_LoadGame = new JButton("Load Game");
        btn_LoadGame.setBounds(90, 309, 150, 25);
        btn_LoadGame.addActionListener(this);
        //btn_LoadGame.setVisible(true);
        buttonPanel.add(btn_LoadGame);
        
        btn_CreateMap = new JButton("Create Map");
        btn_CreateMap.setBounds(90, 186, 150, 25);
        btn_CreateMap.addActionListener(this);
        buttonPanel.add(btn_CreateMap);
        
        btn_CharacterList = new JButton("Character Lists");
        btn_CharacterList.setBounds(90, 227, 150, 25);
        btn_CharacterList.addActionListener(this);
        buttonPanel.add(btn_CharacterList);
        
        btn_ItemList = new JButton("Create Items");
        btn_ItemList.setBounds(90, 268, 150, 25);
        btn_ItemList.addActionListener(this);
        buttonPanel.add(btn_ItemList);
        
        
        
        parentPanel.add(buttonPanel);
        
        JButton btnPlaymap = new JButton("PlayMap");
        btnPlaymap.addActionListener(this);
        btnPlaymap.setBounds(113, 84, 97, 25);
        buttonPanel.add(btnPlaymap);
        
        
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getActionCommand().equals("New Game"))
		{
			new IndexViewController().LoadCampaignMainMenu();
			
		}else if(e.getActionCommand().equals("Characters")){
			System.out.println("Character button Clicked !");
		}
		else if(e.getActionCommand().equals("Create Map")){
			new IndexViewController().LoadMapCreator(8);
		}
		else if(e.getActionCommand().equals("Character Lists")){
			indexViewController.LoadCharacterMainMenu();
		}
		else if(e.getActionCommand().equals("Create Items"))
		{
			indexViewController.LoadItemCreator();
		}
		
		else if(e.getActionCommand().equals("PlayMap"))
		{
			indexViewController.LoadPlayGame();
		}
		else if(e.getActionCommand().equals("Load Game"))
		{
			indexViewController.LoadLoadGame();
		}
	}
}
