package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import Controller.GamePlayViewController;
import Controller.IndexViewController;
import Controller.InventoryViewController;
import Controller.LoadGameViewController;
import Model.CharacterModel;
import Model.ItemModel;
import behavior.BehaviorFactory;
import main.MainFrame;
import metadatas.AbilityModifierMetadata;
import metadatas.AbilityScoreMetadata;
import metadatas.BackpackMetadata;
import metadatas.BonusMetadata;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterTypeMetadata;
import metadatas.CharacterWearableMetadata;
import metadatas.GridCellMetadata;
import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import metadatas.ListMetadata;
import metadatas.ScoreBonusMetadata;
import tests.gamePlay.GamePlayUtilityTest;
import utilities.CharacterType;
import utilities.CharacterUtility;
import utilities.Constants;
import utilities.GamePlayUtility;
import utilities.Grid;
import utilities.GridCell;
import utilities.MapUtility;

/**
 * This class is responsible for the Game Play Engine. It displays the map, provides player with gameplay rules and
 * allows user to have a dynamic experience.
 * @author VijayKaranSingh
 * 
 */


public class GamePlayView extends JPanel implements ActionListener,PropertyChangeListener,Observer{

	private JPanel panel_AbilityScores;
	private JPanel panel_Wearables;
	private JPanel panel_Backpack;


	private DefaultTableModel abilityTableModel;
	private DefaultTableModel wearablesTableModel;
	private DefaultTableModel backpackTableModel;

	private String[] tableAbilityScoresHeader = {"Charac","Abil Score","Abil Modif"};
	private String[] tableWearablesHeader = {"Item Id","Item Name","Item Name"};
	private String[] tableBackpackHeader = {"Slot","Item Type","Item Name"};

	private JTable tableAbilityScores;
	private JTable table_Wearables;
	private JTable table_Backpack;

	private JScrollPane tableAbilityScoresScroller;
	private JScrollPane tableWearablesScroller;
	private JScrollPane tableBackpackScroller;

	private ListMetadata listMetadata;
	public String selectedID = "2";

	private CharacterModel characterModel;
	private ItemModel itemModel;

	private CharacterMetadata characterMetadata;
	private CharacterMetadata selectedCharacterMetadata;
	private CampaignMetadata campaignMetadata;
	private GamePlayViewController gamePlayViewController;

	public static HashMap<String, CharacterMetadata> characterListData;
	public static HashMap<String, ItemMetadata> itemListData;
	HashMap<String, CharacterMetadata> mapCharacterList;
	Vector<CharacterTypeMetadata> vector_characterType;
	HashMap<String, HashMap<String, ItemListMetaInterface>> characterItemMap;
	Map<Integer, CharacterTypeMetadata> turnCharacterMap;
	CharacterTypeMetadata playingCharacterType;
	public static int currentTurn = 1;
	boolean mapCompleted = false;

	Grid grid;
	GridCell gridCell_matrix[][];
	int mapSize;
	int mapSeq;

	private JButton btnForward, btnBack, btnRight, btnLeft;
	private JButton changeTurn;
	int playerPosX;
	int playerPosY;

	int NewPosX;
	int NewPosY;

	String playerName;
	String playerValue;

	private JLabel levelBox;
	private JLabel hitPoints;
	private JLabel armorClass;

	/*
	 * Bonus metadata containing enchantment details.
	 */
	private BonusMetadata bonusMetadata;
	private ScoreBonusMetadata scoreBonusMetadata;

	/*
	 * Instance of character utility class.
	 */
	private CharacterUtility characterUtility;
	private GamePlayUtility gamePlayUtility;
	private JButton btnUp;
	private JButton btnDown;
	private JButton btnQuit;
	private JButton btnOpenInventory;

	MapUtility mapUtility;

	/**
	 * COnstructor to initialize the Blank GamePlayView Object.
	 */
	public GamePlayView(){

	}

	/**
	 * Main Contsructor that Creates the panel for Game Play View.
	 * 
	 * @param grid the grid object to render the map grid
	 * @param characterMetaData the metadata of the character to play with
	 * @param campaignMetadata the metadata of the campaign selected by the player
	 * @param gameController the COntroller Object
	 * @param mapSequence the sequence of the maps in selected campaign
	 */

	public GamePlayView(Grid grid, int size, CharacterMetadata characterMetaData, CampaignMetadata campaignMetadata, 
			GamePlayViewController gameController, String mapSequence, CharacterModel characterModel, 
			HashMap<String, CharacterMetadata> mapCharacterList, Vector<CharacterTypeMetadata> vector_characterType) 
	{
		mapUtility = new MapUtility();
		this.grid = grid;
		this.characterMetadata = characterMetaData;
		this.campaignMetadata  = campaignMetadata;
		this.characterModel = characterModel;
		this.mapCharacterList = mapCharacterList;
		this.vector_characterType = vector_characterType;
		this.characterItemMap = new HashMap<>();
		this.gamePlayUtility = new GamePlayUtility();

		grid.addPropertyChangeListener(this);

		playingCharacterType = new CharacterTypeMetadata();
		playingCharacterType.setType(CharacterType.Player.toString());
		playingCharacterType.setCharacterMetadata(characterMetaData);
		characterMetaData.setCharacterType(CharacterType.Player.toString());

		vector_characterType.add(playingCharacterType);
		
		this.turnCharacterMap =  gamePlayUtility.turnGenerator(vector_characterType);
		setCharacterStrategy();
		selectedID = characterMetaData.getCharacter_id();
		characterListData = characterModel.getCharacterMap();
		gamePlayViewController = gameController;
		mapSize = size;
		mapSeq = Integer.parseInt(mapSequence);

		characterUtility = new CharacterUtility();

		itemModel = new ItemModel();
		itemListData = itemModel.getItemMap();

		updateCharacterLevel();

		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.WEST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JLabel lblCharacterView = new JLabel("Character View");
		panel.add(lblCharacterView);

		JPanel panel_1 = new JPanel();
		panel_1.setDoubleBuffered(false);
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);

		for(int i=0; i< size; i++)
		{
			for(int j=0; j< size; j++)
			{
				if(grid.get_GridCellType(i, j).equals("ENTRY"))
				{
					grid.update_GridCellValue(i, j, characterMetaData.getCharacter_name());
					grid.update_GridCellType(i, j, "PLAYER");
					playerPosX = i;
					playerPosY = j;
					playerName = characterMetaData.getCharacter_name();
					playerValue = "PLAYER";
					break;
				} 
				else if(grid.get_GridCellType(i, j).equals("PLAYER"))
				{
					playerPosX = i;
					playerPosY = j;
					playerName = characterMetaData.getCharacter_name();
					playerValue = "PLAYER";
					break;
				}
				else if(grid.get_GridCellType(i, j).equals("PLAYER&KEY"))
				{
					playerPosX = i;
					playerPosY = j;
					playerName = characterMetaData.getCharacter_name();
					playerValue = "PLAYER&KEY";
					break;
				}
			}
		}


		////// Character View Ability Scores Table //////////////


		panel_AbilityScores = new JPanel();
		panel_AbilityScores.setBorder(new TitledBorder(null, "Ability Scores", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		abilityTableModel = new DefaultTableModel(0,0);
		abilityTableModel.setColumnIdentifiers(tableAbilityScoresHeader);

		tableAbilityScores = new JTable(abilityTableModel);
		tableAbilityScores.setPreferredScrollableViewportSize(new Dimension(300,100));

		tableAbilityScoresScroller = new JScrollPane(tableAbilityScores);

		characterMetadata = characterListData.get(selectedID);

		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();
		load_AbilityScoresTable(abilityScoreMetadata, abilityModifierMetadata);	

		Component verticalStrut_1 = Box.createVerticalStrut(20);
		panel.add(verticalStrut_1);

		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		panel.add(panel_AbilityScores, gbc_panel_2);

		GridBagLayout gbl_panel_2 = new GridBagLayout();
		panel_AbilityScores.setLayout(gbl_panel_2);

		GridBagConstraints gbc_table = new GridBagConstraints();
		panel_AbilityScores.add(tableAbilityScoresScroller, gbc_table);

		JLabel lblInventoryView = new JLabel("Inventory View");
		panel.add(lblInventoryView);

		levelBox = new JLabel("Level");
		levelBox.setBounds(0,0,50,50);
		levelBox.setText("Level: " + characterMetadata.getCharacter_level());
		panel.add(levelBox);

		hitPoints = new JLabel("Hit Points");
		hitPoints.setBounds(0,0,50,50);
		hitPoints.setText("Hit Points: " + characterMetadata.getHitPoints());
		panel.add(hitPoints);

		armorClass = new JLabel("Armor Class");
		armorClass.setBounds(0,0,50,50);
		armorClass.setText("Armor Class: " + characterMetadata.getArmorClass());
		panel.add(armorClass);


		btnOpenInventory = new JButton("Open Inventory");
		panel.add(btnOpenInventory);
		btnOpenInventory.addActionListener(this);

		Component verticalStrut = Box.createVerticalStrut(20);
		panel.add(verticalStrut);


		///// END /////

		/////////////////////////////WEARABLES PANEL/////////////////////////////////
		panel_Wearables = new JPanel();
		panel_Wearables.setBorder(new TitledBorder(null, "Wearable Items", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_weareables = new GridBagConstraints();
		panel.add(panel_Wearables, gbc_panel_weareables);
		GridBagLayout gbl_panel_wearables = new GridBagLayout();
		panel_Wearables.setLayout(gbl_panel_wearables);

		wearablesTableModel = new DefaultTableModel(0,0);
		wearablesTableModel.setColumnIdentifiers(tableWearablesHeader);

		table_Wearables = new JTable(wearablesTableModel);
		table_Wearables.setPreferredScrollableViewportSize(new Dimension(300,100));
		tableWearablesScroller = new JScrollPane(table_Wearables);
		CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
		load_WearablesTable(characterWearableMetadata);	


		GridBagConstraints gbc_table_wearables = new GridBagConstraints();
		panel_Wearables.add(tableWearablesScroller, gbc_table_wearables);
		///////////////////////////////////ENDS///////////////////////////////////////


		//////////////////////////////Panel Backpack/////////////////////////////////
		panel_Backpack = new JPanel();
		panel_Backpack.setBorder(new TitledBorder(null, "Backpack", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_backpack = new GridBagConstraints();
		panel.add(panel_Backpack, gbc_panel_backpack);
		GridBagLayout gbl_panel_backpack = new GridBagLayout();
		panel_Backpack.setLayout(gbl_panel_backpack);

		backpackTableModel = new DefaultTableModel(0, 0);
		backpackTableModel.setColumnIdentifiers(tableBackpackHeader);

		table_Backpack = new JTable(backpackTableModel);
		table_Backpack.setPreferredScrollableViewportSize(new Dimension(300,100));
		tableBackpackScroller = new JScrollPane(table_Backpack);

		Vector<BackpackMetadata> vector_BackpackMetadata = characterMetadata.getBackpackMetadata();
		load_BackpackTable(vector_BackpackMetadata);

		GridBagConstraints gbc_table_backpack = new GridBagConstraints();
		gbc_table_backpack.gridx = 0;
		gbc_table_backpack.gridy = 0;
		panel_Backpack.add(tableBackpackScroller, gbc_table_backpack);
		////////////////////////////////////ENDS//////////////////////////////////////



		grid.setBounds(5, 15, 605, 605);
		panel_1.add(grid);

		btnForward = new JButton("Forward");
		btnForward.setBounds(264, 679, 97, 25);
		panel_1.add(btnForward);
		btnForward.addActionListener(this);

		btnBack = new JButton("Back");
		btnBack.setBounds(264, 736, 97, 25);
		panel_1.add(btnBack);
		btnBack.addActionListener(this);

		btnRight = new JButton("Right");
		btnRight.setBounds(384, 711, 97, 25);
		panel_1.add(btnRight);
		btnRight.addActionListener(this);

		btnLeft = new JButton("Left");
		btnLeft.setBounds(140, 711, 97, 25);
		panel_1.add(btnLeft);
		btnLeft.addActionListener(this);

		changeTurn = new JButton("Change Turn");
		changeTurn.setBounds(250, 711, 120, 25);
		panel_1.add(changeTurn);
		changeTurn.addActionListener(this);
				
		btnQuit = new JButton("QUIT GAME");
		btnQuit.setBounds(600, 711, 97, 25);
		panel_1.add(btnQuit);
		btnQuit.addActionListener(this);
		
		
//		while(!mapCompleted)
//		{
//			CharacterTypeMetadata currentTurnCharacter = turnCharacterMap.get(currentTurn);
//			
//			currentTurn++;
//		}
		
	}

	public void setCharacterStrategy()
	{
		for(int i = 0; i < vector_characterType.size(); i++)
		{
			CharacterTypeMetadata iter_metadata = vector_characterType.get(i);

			iter_metadata.getCharacterMetadata().setBehaviour((BehaviorFactory.getBehaviour(iter_metadata.getType())));
		}
		//		
		//		Set<String> keys = mapCharacterList_Type.keySet();
		//		
		//		Iterator<String> iter = keys.iterator();
		//		
		//		while(iter.hasNext())
		//		{
		//			String char_id = iter.next();
		//			
		//			CharacterMetadata tempCharacterMetadata = characterModel.getCharacterMap().get(char_id);
		//			tempCharacterMetadata.setBehaviour(BehaviorFactory.getBehaviour(mapCharacterList_Type.get(char_id)));
		//		}
	}

	public void highlightCell(CharacterMetadata characterMetadata, int posX, int posY)
	{
		ItemListMetaInterface weapon = getWearableWeapon(characterMetadata);
		int range = Integer.parseInt(weapon.getRange());
		ArrayList<String> rangeList = mapUtility.getRange(posX, posY, range, grid.get_GridSize());
		for(int i=0; i<rangeList.size();i++)
		{	
			String[] cellRange = rangeList.get(i).split(",");
			grid.getGridCell(Integer.parseInt(cellRange[0]), Integer.parseInt(cellRange[1])).setBackground(Color.LIGHT_GRAY);
		}
	}

	public ItemListMetaInterface getWearableWeapon(CharacterMetadata characterMetadata)
	{
		CharacterWearableMetadata charWearableMetadata = characterMetadata.getCharacterWearableMetadata();
		if(!charWearableMetadata.getItm_weapon().equals(""))
		{
			String itemId = charWearableMetadata.getItm_weapon();
			ItemListMetaInterface weapon = itemModel.getItemListByID(Constants.ItemType.Weapon.getItemId(), itemId);

			return weapon;
		}

		return null;
	}


	/**
	 * Loads the Data into Wearable Table
	 * @param characterWearableMetadata
	 */
	public void load_WearablesTable(CharacterWearableMetadata characterWearableMetadata)
	{
		if (wearablesTableModel.getRowCount() > 0) 
		{
			for (int i = wearablesTableModel.getRowCount() - 1; i > -1; i--) {
				wearablesTableModel.removeRow(i);
			}
		}

		wearablesTableModel.addRow( new Object[]
				{	
						"HELMET",
						getItemTypeList("HELMET",characterWearableMetadata.getItm_helmet())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"ARMOR",
						getItemTypeList("ARMOR",characterWearableMetadata.getItm_armor())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"SHIELD",
						getItemTypeList("SHIELD",characterWearableMetadata.getItm_shield())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"RING",
						getItemTypeList("RING",characterWearableMetadata.getItm_ring())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"BELT",
						getItemTypeList("BELT",characterWearableMetadata.getItm_belt())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"BOOTS",
						getItemTypeList("BOOTS",characterWearableMetadata.getItm_boots())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"WEAPON",
						getItemTypeList("WEAPON",characterWearableMetadata.getItm_weapon())
				}
				);
	}


	/**
	 * Loads the Data into AblityScores Table
	 * @param abilityScoreMetadata It is the Ability scores metadata for the character.
	 * @param abilityModifierMetadata It is the Ability modifier score metadata for the character.
	 */
	public void load_AbilityScoresTable(AbilityScoreMetadata abilityScoreMetadata, 
			AbilityModifierMetadata abilityModifierMetadata)
	{
		if (abilityTableModel.getRowCount() > 0) 
		{
			for (int i = abilityTableModel.getRowCount() - 1; i > -1; i--) {
				abilityTableModel.removeRow(i);
			}
		}

		scoreBonusMetadata = new ScoreBonusMetadata();
		if(!(selectedCharacterMetadata==null))
		{
			bonusMetadata = selectedCharacterMetadata.getBonusMetadata();
		}
		else
		{
			bonusMetadata = characterMetadata.getBonusMetadata();

		}
		scoreBonusMetadata = characterUtility.parseScoreAndBonus(abilityScoreMetadata, bonusMetadata);

		abilityTableModel.addRow( new Object[]
				{	
						"STRENGTH",
						scoreBonusMetadata.getStrResult(),
						scoreBonusMetadata.getStrModifier()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"DEXTERITY",
						scoreBonusMetadata.getDexResult(),
						scoreBonusMetadata.getDexModifier()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"CONSTITUTION",
						scoreBonusMetadata.getConResult(),
						scoreBonusMetadata.getConModifier()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"INTELLIGENCE",
						scoreBonusMetadata.getIntResult(),
						scoreBonusMetadata.getIntModifier()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"WISDOM",
						scoreBonusMetadata.getWisResult(),
						scoreBonusMetadata.getWisModifier()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"CHARISMA",
						scoreBonusMetadata.getChaResult(),
						scoreBonusMetadata.getChaModifier()
				}
				);

	}

	/**
	 * Loads the Data into Wearable Table
	 * @param characterWearableMetadata
	 */
	public void load_WearableDetailsTable(CharacterWearableMetadata characterWearableMetadata)
	{
		if(wearablesTableModel.getRowCount()>0)
		{
			for (int i = wearablesTableModel.getRowCount() - 1; i > -1; i--) 
			{
				wearablesTableModel.removeRow(i);
			}
		}

		if(!characterWearableMetadata.getItm_helmet().equals(""))
		{
			String temp_itemType = "HELMET";
			String temp_itemID = characterWearableMetadata.getItm_helmet();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("HELMET").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;
				}
			}
		}

		if(!characterWearableMetadata.getItm_armor().equals(""))
		{
			String temp_itemType = "ARMOR";
			String temp_itemID = characterWearableMetadata.getItm_armor();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("ARMOR").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}

		if(!characterWearableMetadata.getItm_ring().equals(""))
		{
			String temp_itemType = "RING";
			String temp_itemID = characterWearableMetadata.getItm_ring();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("RING").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}

		if(!characterWearableMetadata.getItm_boots().equals(""))
		{
			String temp_itemType = "BOOTS";
			String temp_itemID = characterWearableMetadata.getItm_boots();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("BOOTS").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}

		if(!characterWearableMetadata.getItm_belt().equals(""))
		{
			String temp_itemType = "BELT";
			String temp_itemID = characterWearableMetadata.getItm_belt();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("BELT").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}

		if(!characterWearableMetadata.getItm_shield().equals(""))
		{
			String temp_itemType = "SHIELD";
			String temp_itemID = characterWearableMetadata.getItm_shield();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("SHIELD").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
				}
			}
		}

		if(!characterWearableMetadata.getItm_weapon().equals(""))
		{
			String temp_itemType = "WEAPON";
			String temp_itemID = characterWearableMetadata.getItm_weapon();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("WEAPON").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}
	}

	/**
	 * This method searches the item from the list of items.
	 * 
	 * @param ItemType It is the type of item to be searched.
	 * @param ItemId It is the Id of the item to be searched.
	 * @return an itemListMetadata that contains all the required details about an item.
	 */
	public ItemListMetaInterface getItemTypeList(String ItemType,String ItemId)
	{
		ItemMetadata itemMetadatas = itemListData.get(ItemType);
		if(!(itemMetadatas==null))
		{
			Vector<ItemListMetaInterface> vector_ItemListMetadatas = itemMetadatas.getItemListMetadata();
			for(int i=0;i<vector_ItemListMetadatas.size();i++)
			{
				ItemListMetaInterface itemListMetadata = vector_ItemListMetadatas.get(i);
				if(!(itemMetadatas==null)){
					if(ItemId.equals(itemListMetadata.getList_item_id()))
					{
						return itemListMetadata;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Loads the Data into BackPack Table
	 * 
	 * @param vector_BackpackMetadata return back pack value
	 */
	public void load_BackpackTable(Vector<BackpackMetadata> vector_BackpackMetadata)
	{

		if (backpackTableModel.getRowCount() > 0) 
		{
			for (int i = backpackTableModel.getRowCount() - 1; i > -1; i--) {
				backpackTableModel.removeRow(i);
			}
		}

		BackpackMetadata backpackMetadata;

		for(int i=0; i < vector_BackpackMetadata.size(); i++)
		{
			String temp_itemType = vector_BackpackMetadata.get(i).getItemType();
			String temp_itemID = vector_BackpackMetadata.get(i).getItemId();

			Vector<ItemListMetaInterface> vector_itemListMetadata = itemListData.get(temp_itemType).getItemListMetadata();

			for(int k = 0; k < vector_itemListMetadata.size(); k++)
			{
				if(vector_itemListMetadata.get(k).getList_item_id().equals(temp_itemID))
				{
					String temp_ItemName = vector_itemListMetadata.get(k).getList_item_name();

					backpackTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getActionCommand() == "Forward"){

			System.out.println("Forward Pressed");

			movePlayer(playerPosX, playerPosY, playerPosX-1, playerPosY);

		}

		if(e.getActionCommand() == "Back"){
			System.out.println("Back Pressed");

			movePlayer(playerPosX, playerPosY, playerPosX+1, playerPosY);

		}

		if(e.getActionCommand() == "Left"){
			System.out.println("Left Pressed");

			movePlayer(playerPosX, playerPosY, playerPosX, playerPosY-1);

		}

		if(e.getActionCommand() == "Right"){
			System.out.println("Right Pressed");

			movePlayer(playerPosX, playerPosY, playerPosX, playerPosY+1);

		}
		
		if(e.getActionCommand()=="Change Turn")
		{
			System.out.println("Change Turn!!");
			
			if(!mapCompleted)
			{
				CharacterTypeMetadata currentTurnCharacter = turnCharacterMap.get(currentTurn);
				
				CharacterMetadata currentCharacter = currentTurnCharacter.getCharacterMetadata();
				
				if(currentTurnCharacter.getType().equals(CharacterType.Player.toString()))
				{
					
				}
				else
				{	
					HashMap<String,Integer> currentPlayerLoc = getCurrentLocation(currentCharacter);
					HashMap<String,Integer> playingPlayerLoc = null;
					
					if(currentTurnCharacter.getType().equals(CharacterType.Monster.toString()))
					{
						playingPlayerLoc = getCurrentLocation(characterMetadata);

						currentCharacter.executeMoveBehavior(currentPlayerLoc.get("X"), currentPlayerLoc.get("Y"),
								playingPlayerLoc.get("X"), playingPlayerLoc.get("Y"));
					}
					else
					{
						currentCharacter.executeMoveBehavior(currentPlayerLoc.get("X"), currentPlayerLoc.get("Y"),
								0, 0);
					}
					
				}
				
				if(currentTurn == vector_characterType.size())
				{
					currentTurn = 1;
				}
				else
				{
					currentTurn++;
				}
			}
		}


		if(e.getActionCommand()=="Open Inventory")
		{
			InventoryViewController controller = new InventoryViewController();

			InventoryView inventoryView = new InventoryView(characterMetadata, controller);
			characterMetadata.addObserver(this);

			inventoryView.setVisible(true);

			gamePlayViewController.callObservers(characterMetadata);
		}

		if(e.getActionCommand() == "QUIT GAME")
		{
			System.out.println("Quit Game Pressed");

			int dialogButton = JOptionPane.YES_NO_OPTION;
			int dialogResult = JOptionPane.showConfirmDialog(this, "Do you want to save this game !!", "SAVE!!!", dialogButton);

			if(dialogResult == 0) 
			{

				System.out.println("Yes option");
				gamePlayViewController.actionSave(campaignMetadata, String.valueOf(mapSeq), vector_characterType, grid);

			} else {
				System.out.println("No Option");
				new IndexViewController().LoadCampaignMainMenu();
			} 

		}
	}
	
	public HashMap<String,Integer> getCurrentLocation(CharacterMetadata currentCharacter)
	{
		Iterator<String> iter = mapCharacterList.keySet().iterator();
		
		while(iter.hasNext())
		{
			String tempLoc = iter.next();
			
			if(mapCharacterList.get(tempLoc) == currentCharacter)
			{
				HashMap<String,Integer> locationMap = new HashMap<>();
				locationMap.put("X", Integer.parseInt(String.valueOf(tempLoc.charAt(0))));
				locationMap.put("Y", Integer.parseInt(String.valueOf(tempLoc.charAt(1))));
				return locationMap;
			}
		}
		
		return null;
	}

	public void movePlayer(int curX, int curY, int newX, int newY){

		if(newX<0 || newX >= mapSize || newY <0 || newY >= mapSize){
			System.out.println("You Cannot Move Outside the Map!!");
		}

		else{

			if(checkCell(newX, newY).equals("WALL")){
				System.out.println("Cannot move against the wall");				
			}

			else if(checkCell(newX, newY).equals("KEY")){

				playerValue = "PLAYER&KEY";
				grid.update_GridCellValue(newX, newY, playerName);
				grid.update_GridCellType(newX, newY, playerValue);

				grid.update_GridCellValue(curX, curY, "");
				grid.update_GridCellType(curX, curY, "B");

				System.out.println("Voila !! You Got the Key !! Proceed to Exit !!");
				playerPosX = newX;
				playerPosY = newY;
			}

			else if(checkCell(newX, newY).equals("B")){

				grid.update_GridCellValue(newX, newY, playerName);
				grid.update_GridCellType(newX, newY, playerValue);

				grid.update_GridCellValue(curX, curY, "");
				grid.update_GridCellType(curX, curY, "B");

				playerPosX = newX;
				playerPosY = newY;
			}

			else if(checkCell(newX, newY).equals("B")){

				grid.update_GridCellValue(newX, newY, playerName);
				grid.update_GridCellType(newX, newY, playerValue);

				grid.update_GridCellValue(curX, curY, "");
				grid.update_GridCellType(curX, curY, "B");

				playerPosX = newX;
				playerPosY = newY;
			}

			else if(checkCell(newX, newY).equals("MONSTER")){

				System.out.println("Monster Attack in Process!!");

				CharacterMetadata monsterMetaData;

				String monsterID = grid.get_GridCellValue(newX, newY);

				monsterMetaData = characterListData.get(monsterID);

				Vector<BackpackMetadata> monsterBackpackMetadata = monsterMetaData.getBackpackMetadata();
				int monsterBackItems = monsterBackpackMetadata.size();

				CharacterWearableMetadata monsterWearableMetaData = monsterMetaData.getCharacterWearableMetadata();
				int monsterWearing;

				Vector<BackpackMetadata> monsterWearMetadata = new Vector<>();

				int backFilled = characterMetadata.getBackpackMetadata().size();

				BackpackMetadata[] monsterWearItems = new BackpackMetadata[7];
				int ptr = 0;
				int slots = 0;

				if(!monsterWearableMetaData.getItm_helmet().equals(""))
				{
					BackpackMetadata bm = new BackpackMetadata();
					String itemID = monsterWearableMetaData.getItm_helmet();
					//ItemListMetadata itemListMetadata = itemModel.getItemListByID("HELMET", itemID);
					bm.setItemId(itemID);
					bm.setItemType("HELMET");
					//setBonus(bonusMetadata, itemListMetadata);
					monsterWearMetadata.add(bm);
				}

				if(!monsterWearableMetaData.getItm_armor().equals(""))
				{
					BackpackMetadata bm = new BackpackMetadata();
					String itemID = monsterWearableMetaData.getItm_armor();
					//ItemListMetadata itemListMetadata = itemModel.getItemListByID("ARMOR", itemID);
					//setBonus(bonusMetadata, itemListMetadata);
					bm.setItemId(itemID);
					bm.setItemType("ARMOR");
					//setBonus(bonusMetadata, itemListMetadata);
					monsterWearMetadata.add(bm);
				}

				if(!monsterWearableMetaData.getItm_ring().equals(""))
				{
					BackpackMetadata bm = new BackpackMetadata();
					String itemID = monsterWearableMetaData.getItm_ring();
					//ItemListMetadata itemListMetadata = itemModel.getItemListByID("RING", itemID);
					//setBonus(bonusMetadata, itemListMetadata);
					bm.setItemId(itemID);
					bm.setItemType("RING");
					//setBonus(bonusMetadata, itemListMetadata);
					monsterWearMetadata.add(bm);
				}

				if(!monsterWearableMetaData.getItm_boots().equals(""))
				{
					BackpackMetadata bm = new BackpackMetadata();
					String itemID = monsterWearableMetaData.getItm_boots();
					//ItemListMetadata itemListMetadata = itemModel.getItemListByID("BOOTS", itemID);
					//setBonus(bonusMetadata, itemListMetadata);
					bm.setItemId(itemID);
					bm.setItemType("BOOTS");
					//setBonus(bonusMetadata, itemListMetadata);
					monsterWearMetadata.add(bm);
				}

				if(!monsterWearableMetaData.getItm_belt().equals(""))
				{
					BackpackMetadata bm = new BackpackMetadata();
					String itemID = monsterWearableMetaData.getItm_belt();
					//ItemListMetadata itemListMetadata = itemModel.getItemListByID("BELT", itemID);
					bm.setItemId(itemID);
					bm.setItemType("BELT");
					//setBonus(bonusMetadata, itemListMetadata);
					monsterWearMetadata.add(bm);
				}

				if(!monsterWearableMetaData.getItm_shield().equals(""))
				{
					BackpackMetadata bm = new BackpackMetadata();
					String itemID = monsterWearableMetaData.getItm_shield();
					//ItemListMetadata itemListMetadata = itemModel.getItemListByID("SHIELD", itemID);
					bm.setItemId(itemID);
					bm.setItemType("SHIELD");
					//setBonus(bonusMetadata, itemListMetadata);
					monsterWearMetadata.add(bm);
				}

				if(!monsterWearableMetaData.getItm_weapon().equals(""))
				{
					BackpackMetadata bm = new BackpackMetadata();
					String itemID = monsterWearableMetaData.getItm_weapon();
					//ItemListMetadata itemListMetadata = itemModel.getItemListByID("WEAPON", itemID);
					bm.setItemId(itemID);
					bm.setItemType("WEAPON");
					//setBonus(bonusMetadata, itemListMetadata);
					monsterWearMetadata.add(bm);
				}

				monsterWearMetadata.addAll(monsterBackpackMetadata);

				System.out.println("Your Backpack Capacity :" + (10-backFilled));
				System.out.println("Monster Wearable + Backpack :" + monsterWearMetadata.size());

				if(monsterWearMetadata.size() > (10-backFilled)){
					slots = 10 - backFilled;
				}
				else
					slots = monsterWearMetadata.size();

				if(backFilled < 10){
					for(int i=0; i<slots;i++){
						characterMetadata.actionMoveToBackPack(monsterWearMetadata.get(i));
					}
				}
				else
					System.out.println("No Empty Slot in backpack");



				grid.update_GridCellValue(newX, newY, playerName);
				grid.update_GridCellType(newX, newY, playerValue);

				grid.update_GridCellValue(curX, curY, "");
				grid.update_GridCellType(curX, curY, "B");

				playerPosX = newX;
				playerPosY = newY;

				System.out.println("Monster Killed");
			}

			else if(checkCell(newX, newY).equals("FRIEND")){
				System.out.println("Friend Here");

				if(!characterMetadata.getCharacter_id().equals(mapCharacterList.get(newX+""+newY).getCharacter_id()))
				{
					makeInventorySwitch(mapCharacterList.get(newX+""+newY));
				}
				else
				{
					System.out.println("You have the same inventory and backpack!!!");
				}
			}

			else if(checkCell(newX, newY).equals("CHEST")){

				String items = grid.get_GridCellValue(newX, newY);
				String[] itemSet = items.split("#");
				int slots;
				int backFilled = characterMetadata.getBackpackMetadata().size();
				System.out.println("Backpack size before Loot :" + backFilled);
				int totalItems = itemSet.length-1;

				if(backFilled<10){
					if(totalItems > (10-backFilled)){
						slots = 10 - backFilled;
					}
					else
						slots = totalItems;

					for(int i=1;i<=slots;i++){

						BackpackMetadata backpackMetadata = new BackpackMetadata();

						backpackMetadata.setItemId(itemSet[i].split("_")[1]);
						backpackMetadata.setItemType(itemSet[i].split("_")[0]);

						characterMetadata.actionMoveToBackPack(backpackMetadata);
					}

					grid.update_GridCellValue(newX, newY, playerName);
					grid.update_GridCellType(newX, newY, playerValue);

					grid.update_GridCellValue(curX, curY, "");
					grid.update_GridCellType(curX, curY, "B");

					playerPosX = newX;
					playerPosY = newY;
					System.out.println("Chest Looted!! New Backpack Size is :" + characterMetadata.getBackpackMetadata().size());
				}
				else
					System.out.println("No Empty Slot in backpack");
			}

			else if(checkCell(newX, newY).equals("EXIT"))
			{
				if(playerValue == "PLAYER&KEY")
				{
					mapCompleted = true;
					
					System.out.println("Congratulations!! Level Completed!!");
					JOptionPane.showOptionDialog(null, "Congratulations!! Level Completed", "Progress", JOptionPane.DEFAULT_OPTION,
							JOptionPane.INFORMATION_MESSAGE, null, null, null);

					gamePlayViewController.incrementAndSave(characterMetadata.getCharacter_id());

					String campSize = campaignMetadata.getCampaign_size();

					if(mapSeq < Integer.parseInt(campSize)){
						mapSeq = mapSeq +1;
						gamePlayViewController.nextLevel(campaignMetadata,characterMetadata,mapSeq);

					}
					else 
					{
						JOptionPane.showOptionDialog(null, "Congratulations!! Campaign Completed", "Progress", JOptionPane.DEFAULT_OPTION,
								JOptionPane.INFORMATION_MESSAGE, null, null, null);
						gamePlayViewController.actionComplete();

					}

				}
				else
					System.out.println("You cannot Complete Level Without Key!!");
			}
		}
	}

	public void makeInventorySwitch(CharacterMetadata friendsMetadata)
	{
		InventoryViewController controller = new InventoryViewController();

		InventoryView inventoryView = new InventoryView(characterMetadata, friendsMetadata, controller);
		characterMetadata.addObserver(this);

		inventoryView.setVisible(true);

		gamePlayViewController.callObservers(characterMetadata);
	}

	public String checkCell(int x, int y){

		return grid.get_GridCellType(x, y);
	}

	@Override
	public void update(Observable o, Object arg) 
	{
		// TODO Auto-generated method stub
		if(!(selectedCharacterMetadata==null))
		{
			load_AbilityScoresTable(selectedCharacterMetadata.getAbilityScoreMetadata(),selectedCharacterMetadata.getAbilityModifierMetadata());
			load_WearableDetailsTable(selectedCharacterMetadata.getCharacterWearableMetadata());
			load_BackpackTable(selectedCharacterMetadata.getBackpackMetadata());
		}
		else
		{
			load_AbilityScoresTable(characterMetadata.getAbilityScoreMetadata(),characterMetadata.getAbilityModifierMetadata());
			load_WearableDetailsTable(characterMetadata.getCharacterWearableMetadata());
			load_BackpackTable(characterMetadata.getBackpackMetadata());
		}
	}

	public void updateCharacterLevel()
	{
		int player_level = Integer.parseInt(characterMetadata.getCharacter_level());

		for (Map.Entry<String, CharacterMetadata> e : mapCharacterList.entrySet())
		{
			CharacterMetadata tempCharacterMetadata = e.getValue();

			int tempLevel = Integer.parseInt(tempCharacterMetadata.getCharacter_level());

			if(player_level != tempLevel)
			{
				int hit_points = characterUtility.calculateHitPoint(player_level, Integer.parseInt(tempCharacterMetadata.getAbilityModifierMetadata().getAbm_constitution()));
				tempCharacterMetadata.setHitPoints(String.valueOf(hit_points));

				if(!tempCharacterMetadata.getBonusMetadata().getAttackBonus().equals(""))
				{
					int attack_bonus = characterUtility.calculateAttackBonus(player_level, Integer.parseInt(tempCharacterMetadata.getBonusMetadata().getAttackBonus()));
					tempCharacterMetadata.setAttackBonus(String.valueOf(attack_bonus));
				}
				else
				{
					int attack_bonus = characterUtility.calculateAttackBonus(player_level, 0);
					tempCharacterMetadata.setAttackBonus(String.valueOf(attack_bonus));
				}
				tempCharacterMetadata.setCharacter_level(String.valueOf(player_level));
			}

			CharacterWearableMetadata characterWearableMetadata = tempCharacterMetadata.getCharacterWearableMetadata();
			BonusMetadata bonusMetadata = tempCharacterMetadata.getBonusMetadata();
			HashMap<String, ItemListMetaInterface> itemMap = new HashMap<>();

			if(!characterWearableMetadata.getItm_helmet().equals(""))
			{
				String itemID = characterWearableMetadata.getItm_helmet();
				ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("HELMET", itemID);
				itemListMetadata.setList_item_bonus(String.valueOf(getItemBonusByLevel(player_level)));

				setBonus(bonusMetadata, itemListMetadata, tempCharacterMetadata);
				itemMap.put("HELMET", itemListMetadata);
			}

			if(!characterWearableMetadata.getItm_armor().equals(""))
			{
				String itemID = characterWearableMetadata.getItm_armor();
				ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("ARMOR", itemID);
				itemListMetadata.setList_item_bonus(String.valueOf(getItemBonusByLevel(player_level)));

				setBonus(bonusMetadata, itemListMetadata, tempCharacterMetadata);
				itemMap.put("ARMOR", itemListMetadata);
			}

			if(!characterWearableMetadata.getItm_ring().equals(""))
			{
				String itemID = characterWearableMetadata.getItm_ring();
				ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("RING", itemID);
				itemListMetadata.setList_item_bonus(String.valueOf(getItemBonusByLevel(player_level)));

				setBonus(bonusMetadata, itemListMetadata, tempCharacterMetadata);
				itemMap.put("RING", itemListMetadata);
			}

			if(!characterWearableMetadata.getItm_boots().equals(""))
			{
				String itemID = characterWearableMetadata.getItm_boots();
				ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("BOOTS", itemID);
				itemListMetadata.setList_item_bonus(String.valueOf(getItemBonusByLevel(player_level)));

				setBonus(bonusMetadata, itemListMetadata, tempCharacterMetadata);
				itemMap.put("BOOTS", itemListMetadata);
			}

			if(!characterWearableMetadata.getItm_belt().equals(""))
			{
				String itemID = characterWearableMetadata.getItm_belt();
				ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("BELT", itemID);
				itemListMetadata.setList_item_bonus(String.valueOf(getItemBonusByLevel(player_level)));

				setBonus(bonusMetadata, itemListMetadata, tempCharacterMetadata);
				itemMap.put("BELT", itemListMetadata);
			}

			if(!characterWearableMetadata.getItm_shield().equals(""))
			{
				String itemID = characterWearableMetadata.getItm_shield();
				ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("SHIELD", itemID);
				itemListMetadata.setList_item_bonus(String.valueOf(getItemBonusByLevel(player_level)));

				setBonus(bonusMetadata, itemListMetadata, tempCharacterMetadata);
				itemMap.put("SHIELD", itemListMetadata);
			}

			if(!characterWearableMetadata.getItm_weapon().equals(""))
			{
				String itemID = characterWearableMetadata.getItm_weapon();
				ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("WEAPON", itemID);
				itemListMetadata.setList_item_bonus(String.valueOf(getItemBonusByLevel(player_level)));

				setBonus(bonusMetadata, itemListMetadata, tempCharacterMetadata);
				itemMap.put("WEAPON", itemListMetadata);
			}

			characterItemMap.put(e.getKey(), itemMap);


		}

	}

	public int getItemBonusByLevel(int player_level)
	{
		if(player_level >= 1 && player_level <= 4)
			return 1;
		else if(player_level >= 5 && player_level <= 8)
			return 2;
		else if(player_level >= 9 && player_level <= 12)
			return 3;
		else if(player_level >= 13 && player_level <= 16)
			return 4;
		else if(player_level >= 17)
			return 5;

		return 0;
	}

	/**
	 * This method is used to setBonus based on item selected. Controlled is passed on through the view.
	 * 
	 * @param bonusMetadata It contains the details about the bonus that a player has.
	 * @param itemListMetadata It contains the details about the item list selected.
	 */
	public void setBonus(BonusMetadata bonusMetadata, ItemListMetaInterface itemListMetadata, CharacterMetadata characterMetadata)
	{
		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();

		int existingBonus=0;
		int newBonus;

		switch (itemListMetadata.getList_item_enchanment().toUpperCase()) 
		{
		case "ARMORCLASS":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			bonusMetadata.setArmorClassBonus(String.valueOf(newBonus));
			int armorClass = characterUtility.calulateArmorClass(Integer.parseInt(abilityModifierMetadata.getAbm_dexterity()), Integer.parseInt(bonusMetadata.getArmorClassBonus()));
			characterMetadata.setArmorClass(String.valueOf(armorClass));
			break;
		case "ATTACKBONUS":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			bonusMetadata.setAttackBonus(String.valueOf(newBonus));
			int attackBonus = characterUtility.calculateAttackBonus((Integer.parseInt(characterMetadata.getCharacter_level())), Integer.parseInt(bonusMetadata.getAttackBonus()));
			characterMetadata.setAttackBonus(String.valueOf(attackBonus));
			break;
		case "DAMAGEBONUS":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			bonusMetadata.setDamageBonus(String.valueOf(newBonus));

			int damageBonus = characterUtility.calculateDamageBonus(Integer.parseInt(abilityModifierMetadata.getAbm_strength()), Integer.parseInt(bonusMetadata.getDamageBonus()));
			characterMetadata.setDamageBonus(String.valueOf(damageBonus));

			break;
		case "STRENGTH":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			bonusMetadata.setStrBonus(String.valueOf(newBonus));
			break;
		case "DEXTERITY":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			bonusMetadata.setDexBonus(String.valueOf(newBonus));
			break;
		case "WISDOM":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			bonusMetadata.setWisBonus(String.valueOf(newBonus));
			break;
		case "CHARISMA":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			bonusMetadata.setChaBonus(String.valueOf(newBonus));
			break;
		case "CONSTITUTION":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			bonusMetadata.setConBonus(String.valueOf(newBonus));
			break;
		case "INTELLIGENCE":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			bonusMetadata.setIntBonus(String.valueOf(newBonus));
			break;
		}

		abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
		characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);
	}


	public void updateSelectedCharacerLevel()
	{
		//		System.out.println("Selected Character Hit Points in respect to the Playing Character Level : " + selectedCharacterMetadata.getHitPoints());
		//		System.out.println("Selected Character Attack Bonus in respect to the Playing Character Level : " + selectedCharacterMetadata.getAttackBonus());
		if(!(selectedCharacterMetadata==null))
		{
			levelBox.setText("Level: " + selectedCharacterMetadata.getCharacter_level());
			hitPoints.setText("Hit Points: " + selectedCharacterMetadata.getHitPoints());
			armorClass.setText("Armor Class:" + selectedCharacterMetadata.getArmorClass());
		}
		else
		{
			levelBox.setText("Level: " + characterMetadata.getCharacter_level());
			hitPoints.setText("Hit Points: " + characterMetadata.getHitPoints());
			armorClass.setText("Armor Class:" + characterMetadata.getArmorClass());
		}
	}


	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		while(GridCell.isChanged)
		{
			GridCell.isChanged = false;
			GridCellMetadata gridCell = GridCell.responseCellData;
			int xPos = gridCell.getxPosition();
			int yPos = gridCell.getyPosition();
			System.out.println("X and Y Position: " + xPos + " and " + yPos);

			if(grid.get_GridCellType(xPos, yPos).equals("MONSTER") || grid.get_GridCellType(xPos, yPos).equals("FRIEND") || grid.get_GridCellType(xPos, yPos).equals("PLAYER&KEY") || grid.get_GridCellType(xPos, yPos).equals("PLAYER") ){
				String name = grid.get_GridCellValue(xPos, yPos);
				System.out.println("X and Y Position: " + xPos + " and " + yPos);

				selectedCharacterMetadata = mapCharacterList.get(xPos+""+yPos);
				updateSelectedCharacerLevel();
				System.out.println("Name of the character is: " + name);
				characterMetadata.addObserver(this);
				gamePlayViewController.callObservers(characterMetadata);

			}

			if(grid.get_GridCellType(xPos, yPos).equals("CHEST")){
				System.out.println("This Chest Contains Item types :" + grid.get_GridCellValue(xPos, yPos));
			}
		}
	}

	/**
	 * This function returns the slots to fill and hence iterator for Looting Chest or Monster.
	 * @param backFilled current number of items in backpack
	 * @param totalItems items available to loot
	 * @return the slots that can be filled at this time
	 */
	public int getBackpackSlots(int backFilled, int totalItems){
		int slots = 0;
		if(backFilled<10){
			if(totalItems > (10-backFilled)){
				slots = 10 - backFilled;
			}
			else
				slots = totalItems;
		}
		return slots;
	}

}
