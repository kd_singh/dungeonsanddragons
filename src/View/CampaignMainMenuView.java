package View;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import Controller.CampaignMainMenuViewController;
import Model.CampaignModel;
import Model.CharacterModel;
import metadatas.CampaignMapsMetadata;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.ListMetadata;
import utilities.ListPanelUtility;
import utilities.ListUtility;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * This Campaign Main View is the first screen which appears when a user clicks new game. It is 
 * used to view,play,edit existing campaigns or create a new campaign.
 * 
 *
 * @author Tarandeep Singh
 * @version 1.0.0
 * @since 07 February 2017
 * @see CampaignModel
 * @see ListMetadata
 * @see CampaignMainMenuViewController
 * @see ListPanelUtility
 * @see DefaultTableModel
 */
public class CampaignMainMenuView extends JPanel implements PropertyChangeListener, ActionListener,ListSelectionListener
{
	
	/**
	 * All swing objects used for the Campaign Main Menu View
	 */
	private JPanel panel;
	private JPanel parentPanel;
	private JPanel panel_ListHeader;
	private JPanel panel_CharacterListHeader;
	private JPanel panel_CampaignInfo;
	private JTable tableCampaignInfo;
	private JScrollPane tableCampaignInfoScroller;
	private JLabel lblSelectACampaign;
	private JLabel lblSelectACharacter;
	private JButton btnPlayCampaign;
	private JButton btnEdit;
	private JButton btnCreate;
	private JList CharacterList;
	private JScrollPane scrollPane = new JScrollPane();

	/**
	 * DefaultTabelModel instantiated 
	 */

	DefaultTableModel tableModel;
	
	String[] tableHeader = {"Map Sequence","Map Name"};
	private CharacterModel characterModel;
	CampaignModel campaignModel;
	ListMetadata listMetadata;
	ListMetadata listMetadataChar;
	ListPanelUtility listPanel;
	ListPanelUtility listPanelChar;
	String selectedID;
	String selectedIDChar;
	String actionType;
	
	CharacterMetadata characterMetadata;
	
	/**
	 * A HashMap that contains all the Campaigns inside this.
	 */
	public HashMap<String, CampaignMetadata> campaignListData;	
	
	/**
	 * A HashMap that contains all the Characters inside this.
	 */
	public static HashMap<String, CharacterMetadata> characterListData;
	
	/**
	 * An object created for CampaignMainMenuViewController
	 */
	CampaignMainMenuViewController controller;
	private JButton btnCancel;
	private JList list;

	/**
	 * Constructor to point to current object of controller and campaign model
	 * with calling Initialize method <code> init_() </code>.
	 * @param controller Controller for campaign main menu
	 * @param campaignModel Model for campaign
	 */
	public CampaignMainMenuView(CampaignMainMenuViewController controller, CampaignModel campaignModel) {
		
		this.controller = controller;
		this.campaignModel = campaignModel;
		init_();
	}

	/**
	 * This method initializes the view for Campaign Main Menu 
	 */
	
	public void init_() 
	{

		/**
		 * Returns the state of this class <code>CampaignModel</code> wrapped
		 * in the form of HashMap into campaignListData
		 * 
		 */
		characterModel = new CharacterModel();
		campaignListData = campaignModel.getCampaignMap();
		characterListData = characterModel.getCharacterMap();
		
		///////////////////BASIC LAYOUT OF THE VIEW BEGINS HERE//////////////////////
		parentPanel = this;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		setLayout(gridBagLayout);
		parentPanel.setBounds(0, 0, 800, 800);
		//////////////////HEADING OR VIEW PURPOSE ENDS HERE//////////////////////////
		

		//////////////////HEADING OR VIEW PURPOSE BEGINS HERE////////////////////////
		lblSelectACampaign = new JLabel("CAMPAIGN SELECTION MENU");
		lblSelectACampaign.setVerticalAlignment(SwingConstants.TOP);
		lblSelectACampaign.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblSelectACampaign = new GridBagConstraints();
		gbc_lblSelectACampaign.insets = new Insets(0, 0, 5, 5);
		gbc_lblSelectACampaign.gridx = 4;
		gbc_lblSelectACampaign.gridy = 2;
		add(lblSelectACampaign, gbc_lblSelectACampaign);
		//////////////////HEADING OR VIEW PURPOSE ENDS HERE//////////////////////////
		
		
		///////////////////////CHARACTER LIST DISPLAYED HERE//////////////////////////		
		panel_CharacterListHeader = new JPanel();
		panel_CharacterListHeader.setBorder(new TitledBorder(null, "Character List", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_headerCharacter = new GridBagConstraints();
		gbc_panel_headerCharacter.insets = new Insets(0, 0, 5, 5);
		gbc_panel_headerCharacter.gridx = 2;
		gbc_panel_headerCharacter.gridy = 5;
		add(panel_CharacterListHeader, gbc_panel_headerCharacter);
		GridBagLayout gbl_panel_headerCharacter = new GridBagLayout();
		panel_CharacterListHeader.setLayout(gbl_panel_headerCharacter);
		CharacterList = new JList(characterModel.getListMetadata());
		GridBagConstraints gbc_listPanelCharacter = new GridBagConstraints();
		panel_CharacterListHeader.add(CharacterList, gbc_listPanelCharacter);
		CharacterList.setToolTipText("");
		CharacterList.addListSelectionListener((ListSelectionListener) this);
		CharacterList.setBounds(0,0,100,100);	
//		/////////////////////////// CHARACTER LIST ENDS HERE ///////////////////////////
//		
//		//Get selected object from the list panel, give the value to Hashmap characterList
//
		if(!(CharacterList.getSelectedValue()==null))
		{
		listMetadataChar = (ListMetadata) CharacterList.getSelectedValue();
		selectedIDChar = listMetadataChar.getItem_ID();
		}
		
		
		
		/**
		 * Calling the ListPanelUtility to list the Campaign names present in Campaign file
		 * 
		 */
		listPanel = new ListPanelUtility(campaignModel.getListMetadata());
		
		///////////////////////CAMPAIGN LIST DISPLAYED HERE//////////////////////////		
		panel_ListHeader = new JPanel();
		panel_ListHeader.setBorder(new TitledBorder(null, "Campaign List", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_header = new GridBagConstraints();
		gbc_panel_header.insets = new Insets(0, 0, 5, 0);
		gbc_panel_header.gridx = 5;
		gbc_panel_header.gridy = 5;
		add(panel_ListHeader, gbc_panel_header);
		GridBagLayout gbl_panel_header = new GridBagLayout();
		panel_ListHeader.setLayout(gbl_panel_header);
		GridBagConstraints gbc_listPanel = new GridBagConstraints();
		panel_ListHeader.add(listPanel, gbc_listPanel);
		listPanel.addPropertyChangeListener(this);
		listPanel.setBounds(0,0,100,100);	
		/////////////////////////// CAMPAIGN LIST ENDS HERE ///////////////////////////	
		
		/*
		 * Storing the  ListMetada of selected item in the List and then fetching Item_ID
		 *  
		 */
		if(!(listPanel.getSelectedObject()==null))	
		{
			listMetadata = listPanel.getSelectedObject();
			selectedID = listMetadata.getItem_ID();
		}
		/////////////////////////// SETTING THE LAYOUT ///////////////////////////
		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 3;
		gbc_panel.gridy = 6;
		add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		/////////////////////////// SETTING THE LAYOUT ENDS ///////////////////////////
		
		tableModel = new DefaultTableModel(0,0);
		tableModel.setColumnIdentifiers(tableHeader);
		
		if(!(listMetadata==null))
		{
		CampaignMetadata campaignMetadata = campaignListData.get(listMetadata.getItem_ID());
		Vector<CampaignMapsMetadata> campaignMapsMetadata = campaignMetadata.getVector_campaignMap();
		load_CampaignInfoTable(campaignMapsMetadata);
		}
		
		///////////////////////// CAMPAIGN INFORMATION TABLE ////////////////////////
		
		panel_CampaignInfo = new JPanel();
		GridBagConstraints gbc_panel_CampaignInfo = new GridBagConstraints();
		gbc_panel_CampaignInfo.insets = new Insets(0, 0, 5, 0);
		gbc_panel_CampaignInfo.gridx = 5;
		gbc_panel_CampaignInfo.gridy = 7;
		add(panel_CampaignInfo, gbc_panel_CampaignInfo);
		panel_CampaignInfo.setBorder(new TitledBorder(null, "Campaign Information", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		tableCampaignInfo = new JTable(tableModel);
		tableCampaignInfo.setPreferredScrollableViewportSize(new Dimension(300,100));
		tableCampaignInfoScroller = new JScrollPane(tableCampaignInfo);
		
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		panel_CampaignInfo.setLayout(gbl_panel_2);
		
		GridBagConstraints gbc_table = new GridBagConstraints();
		panel_CampaignInfo.add(tableCampaignInfoScroller, gbc_table);
		
		///////////////////////// TABLE ENDS HERE ///////////////////////////////////
		

		///////////////////////// PLAY BUTTON LAYOUT ////////////////////////////////
		btnPlayCampaign = new JButton("Play");
		GridBagConstraints gbc_btnPlayCampaign = new GridBagConstraints();
		gbc_btnPlayCampaign.insets = new Insets(0, 0, 5, 5);
		gbc_btnPlayCampaign.gridx = 4;
		gbc_btnPlayCampaign.gridy = 8;
		add(btnPlayCampaign, gbc_btnPlayCampaign);
		btnPlayCampaign.addActionListener(this);
		/////////////////////////LAYOUT ENDS HERE //////////////////////////////////
		
		///////////////////////// EDIT BUTTON LAYOUT ///////////////////////////////
		btnEdit = new JButton("Edit");
		GridBagConstraints gbc_btnEdit = new GridBagConstraints();
		gbc_btnEdit.insets = new Insets(0, 0, 5, 5);
		gbc_btnEdit.gridx = 4;
		gbc_btnEdit.gridy = 9;
		add(btnEdit, gbc_btnEdit);
		btnEdit.addActionListener(this);
		/////////////////////////LAYOUT ENDS HERE /////////////////////////////////
		
		///////////////////////// CREATE BUTTON LAYOUT ////////////////////////////
		btnCreate = new JButton("Create");
		GridBagConstraints gbc_btnCreate = new GridBagConstraints();
		gbc_btnCreate.insets = new Insets(0, 0, 5, 5);
		gbc_btnCreate.gridx = 4;
		gbc_btnCreate.gridy = 10;
		add(btnCreate, gbc_btnCreate);
		btnCreate.addActionListener(this);
		
		btnCancel = new JButton("Cancel");
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 0, 0, 5);
		gbc_btnCancel.gridx = 4;
		gbc_btnCancel.gridy = 11;
		add(btnCancel, gbc_btnCancel);
		btnCancel.addActionListener(this);
		///////////////////////// LAYOUT ENDS HERE ///////////////////////////////
		}
		
		

	/**
	 * This method returns the Meta Data
	 *	@param	campaignMapsMetadatas CampaignMapsMetadata is stored in this vector object
	 */
	public void load_CampaignInfoTable(Vector<CampaignMapsMetadata> campaignMapsMetadatas)
	{
		if (tableModel.getRowCount() > 0) 
		{
		    for (int i = tableModel.getRowCount() - 1; i > -1; i--) {
		    	tableModel.removeRow(i);
		    }
		}
		
		for(int i = 0; i<campaignMapsMetadatas.size(); i++)
		{
			CampaignMapsMetadata item_campaignMaps = campaignMapsMetadatas.get(i);
			
			tableModel.addRow( new Object[]
					{	
						item_campaignMaps.getMap_sequence(),
						item_campaignMaps.getMap_name()
					}
				);
		}
	}

	@Override
	/**
	 * {@inheritDoc}
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("Edit"))
			{
				System.out.println("Edit clicked !");
				actionType = "Edit";
				if(!(listMetadata==null))
				{
					CampaignMetadata campaignMetadata = campaignListData.get(selectedID);
					controller.actionEdit(campaignMetadata, actionType);
				}
				else
				{
					JOptionPane.showOptionDialog(null, "Create atleast 1 campaign", "ERROR", JOptionPane.DEFAULT_OPTION,
					        JOptionPane.INFORMATION_MESSAGE, null, null, null);
				}
				
				
			}
			else if(e.getActionCommand().equals("Create"))
			{
				System.out.println("Create clicked !");
				actionType = "Create";	
				controller.actionCreate(actionType);
	
			}
		
			if(e.getActionCommand().equals("Play"))
			{
				System.out.println("Play clicked !");
				actionType = "Play";
				if(!(listMetadata==null) && (CharacterList.getModel().getSize()!=0) && (selectedIDChar != null))
				{
					CampaignMetadata campaignMetadata = campaignListData.get(selectedID);
					characterMetadata = characterListData.get(selectedIDChar);
					controller.actionPlay(campaignMetadata,actionType,characterMetadata);
				}
				else
					if(selectedIDChar == null)
					{
						JOptionPane.showOptionDialog(null, "Select a Character to Play with !!", "ERROR", JOptionPane.DEFAULT_OPTION,
						        JOptionPane.INFORMATION_MESSAGE, null, null, null);
					}
				else
					if((listMetadata==null))
					{
					JOptionPane.showOptionDialog(null, "Create atleast 1 campaign to play !!", "ERROR", JOptionPane.DEFAULT_OPTION,
					        JOptionPane.INFORMATION_MESSAGE, null, null, null);
					}
					else
						if((CharacterList.getModel().getSize()==0))
					{
							JOptionPane.showOptionDialog(null, "Create atleast 1 Character to play with !!", "ERROR", JOptionPane.DEFAULT_OPTION,
							        JOptionPane.INFORMATION_MESSAGE, null, null, null);
					}
			}
			else if(e.getActionCommand().equals("Cancel"))
			{
				controller.actionCancel();
			}
	}

	@Override
	/**
	 * {@inheritDoc}
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
	
		while(ListUtility.isChanged)
		{
			ListUtility.isChanged = false;
			selectedID = ListUtility.responseMetadata.getSelected_id();
			CampaignMetadata campaignMetadata = campaignListData.get(selectedID);
			Vector<CampaignMapsMetadata> campaignMapsMetadata = campaignMetadata.getVector_campaignMap();
			load_CampaignInfoTable(campaignMapsMetadata);
						
		}
		
	}

	@Override
	/**
	 * {@inheritDoc}
	 */
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		
		if(!e.getValueIsAdjusting())
		{
			listMetadataChar = (ListMetadata) CharacterList.getSelectedValue();
			selectedIDChar = listMetadataChar.getItem_ID();
			characterMetadata = characterListData.get(selectedIDChar);
		}
		
		//System.out.println(characterMetadata);
	}
}

