package View;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import Controller.GamePlayViewController;
import Controller.InventoryViewController;
import Model.CharacterModel;
import Model.ItemModel;
import metadatas.BackpackMetadata;
import metadatas.BonusMetadata;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterWearableMetadata;
import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import metadatas.ListMetadata;
import metadatas.ScoreBonusMetadata;
import utilities.CharacterUtility;
import utilities.Grid;
import utilities.GridCell;

public class InventoryView extends JFrame implements ActionListener,Observer
{

	private JPanel panel_AbilityScores;
	private JPanel panel_Wearables;
	private JPanel panel_Backpack;

	private JButton btnSave;

	private DefaultTableModel abilityTableModel;
	private DefaultTableModel wearablesTableModel;
	private DefaultTableModel backpackTableModel;

	private String[] tableAbilityScoresHeader = {"Charac","Abil Score","Abil Modif"};
	private String[] tableWearablesHeader = {"Item Id","Item Name","Item Name"};
	private String[] tableBackpackHeader = {"Slot","Item Type","Item Name"};

	private JTable tableAbilityScores;
	private JTable table_Wearables;
	private JTable table_Backpack;

	private JScrollPane tableAbilityScoresScroller;
	private JScrollPane tableWearablesScroller;
	private JScrollPane tableBackpackScroller;

	private ListMetadata listMetadata;
	public String selectedID = "2";

	private CharacterModel characterModel;
	private ItemModel itemModel;

	private CharacterMetadata characterMetadata;
	private CharacterMetadata friendsMetadata;
	private CampaignMetadata campaignMetadata;
	private GamePlayViewController gamePlayViewController;

	public static HashMap<String, CharacterMetadata> characterListData;
	public static HashMap<String,ItemMetadata> itemListData;

	Grid grid;
	GridCell gridCell_matrix[][];
	int mapSize;
	int mapSeq;

	private JButton btnForward, btnBack, btnRight, btnLeft;
	int playerPosX;
	int playerPosY;

	int NewPosX;
	int NewPosY;

	String playerName;
	String playerValue;

	/*
	 * Bonus metadata containing enchantment details.
	 */
	private BonusMetadata bonusMetadata;
	private ScoreBonusMetadata scoreBonusMetadata;

	/*
	 * Instance of character utility class.
	 */
	private CharacterUtility characterUtility;
	private JButton btnUp;
	private JButton btnDown;
	private JButton btnSave_1;
	private InventoryViewController controller;
	private JButton btnSwitch;

	public InventoryView(CharacterMetadata characterMetadata, InventoryViewController controller)
	{
		this.characterMetadata = characterMetadata;
		this.controller = controller;

		itemModel = new ItemModel();
		itemListData = itemModel.getItemMap();

		init_();
	}
	
	/**
	 * @wbp.parser.constructor
	 */
	public InventoryView(CharacterMetadata characterMetadata, CharacterMetadata friendsMetadata, InventoryViewController controller)
	{
		this.characterMetadata = characterMetadata;
		this.controller = controller;
		this.friendsMetadata = friendsMetadata;

		itemModel = new ItemModel();
		itemListData = itemModel.getItemMap();
		
		init_();
	}

	private void init_() 
	{

		////// Panel for Equipped Items View ////////
		JPanel parentPanel = new JPanel();
		panel_Wearables = new JPanel();
		panel_Wearables.setBorder(new TitledBorder(null, "Wearable Items", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_weareables = new GridBagConstraints();
		gbc_panel_weareables.insets = new Insets(0, 0, 5, 0);
		gbc_panel_weareables.gridx = 1;
		gbc_panel_weareables.gridy = 1;
		//add(panel_Wearables, gbc_panel_weareables);
		parentPanel.add(panel_Wearables, gbc_panel_weareables);
		GridBagLayout gbl_panel_wearables = new GridBagLayout();
		panel_Wearables.setLayout(gbl_panel_wearables);

		wearablesTableModel = new DefaultTableModel(0,0);
		wearablesTableModel.setColumnIdentifiers(tableWearablesHeader);

		table_Wearables = new JTable(wearablesTableModel);
		table_Wearables.setPreferredScrollableViewportSize(new Dimension(300,100));
		tableWearablesScroller = new JScrollPane(table_Wearables);

		/*if(!(listMetadata==null))
			{
				CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
				load_WearablesTable(characterWearableMetadata);	
			}*/

		CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
		//		load_WearablesTable(characterWearableMetadata);	
		load_WearableDetailsTable(characterWearableMetadata);

		GridBagConstraints gbc_table_wearables = new GridBagConstraints();
		gbc_table_wearables.insets = new Insets(0, 0, 5, 0);
		gbc_table_wearables.gridx = 0;
		gbc_table_wearables.gridy = 0;
		panel_Wearables.add(tableWearablesScroller, gbc_table_wearables);
		parentPanel.add(panel_Wearables);

		btnUp = new JButton("UP");
		GridBagConstraints gbc_btnUp = new GridBagConstraints();
		gbc_btnUp.insets = new Insets(0, 0, 5, 0);
		gbc_btnUp.gridx = 0;
		gbc_btnUp.gridy = 1;
		panel_Wearables.add(btnUp, gbc_btnUp);
		btnUp.addActionListener(this);

		btnDown = new JButton("DOWN");
		GridBagConstraints gbc_btnDown = new GridBagConstraints();
		gbc_btnDown.gridx = 0;
		gbc_btnDown.gridy = 2;
		panel_Wearables.add(btnDown, gbc_btnDown);
		btnDown.addActionListener(this);

		/////// ENDS /////


		//// Backpack View /////

		panel_Backpack = new JPanel();
		panel_Backpack.setBorder(new TitledBorder(null, "Backpack", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_backpack = new GridBagConstraints();
		gbc_panel_backpack.insets = new Insets(0, 0, 5, 0);
		gbc_panel_backpack.gridx = 1;
		gbc_panel_backpack.gridy = 2;

		parentPanel.add(panel_Backpack, gbc_panel_backpack);
		GridBagLayout gbl_panel_backpack = new GridBagLayout();
		panel_Backpack.setLayout(gbl_panel_backpack);

		backpackTableModel = new DefaultTableModel(0, 0);
		backpackTableModel.setColumnIdentifiers(tableBackpackHeader);

		table_Backpack = new JTable(backpackTableModel);
		table_Backpack.setPreferredScrollableViewportSize(new Dimension(300,100));
		tableBackpackScroller = new JScrollPane(table_Backpack);

		/*		
			if(!(listMetadata==null))
			{
				Vector<BackpackMetadata> vector_BackpackMetadata = characterMetadata.getBackpackMetadata();
				load_BackpackTable(vector_BackpackMetadata);
			}*/

		Vector<BackpackMetadata> vector_BackpackMetadata = characterMetadata.getBackpackMetadata();
		load_BackpackTable(vector_BackpackMetadata);


		GridBagConstraints gbc_table_backpack = new GridBagConstraints();
		gbc_table_backpack.insets = new Insets(0, 0, 5, 0);
		gbc_table_backpack.gridx = 0;
		gbc_table_backpack.gridy = 0;
		panel_Backpack.add(tableBackpackScroller, gbc_table_backpack);
		
		btnSwitch = new JButton("SWITCH");
		GridBagConstraints gbc_btnSwitch = new GridBagConstraints();
		gbc_btnSwitch.insets = new Insets(0, 0, 5, 0);
		gbc_btnSwitch.gridx = 0;
		gbc_btnSwitch.gridy = 1;
		panel_Backpack.add(btnSwitch, gbc_btnSwitch);
		btnSwitch.addActionListener(this);

		btnSave_1 = new JButton("Save");
		GridBagConstraints gbc_btnSave_1 = new GridBagConstraints();
		gbc_btnSave_1.gridx = 0;
		gbc_btnSave_1.gridy = 2;
		panel_Backpack.add(btnSave_1, gbc_btnSave_1);
		btnSave_1.addActionListener(this);

		//////// ENDS //////////

		getContentPane().add(parentPanel);
		this.setBounds(100, 100, 500, 500);
	}

	/**
	 * Loads the Data into Wearable Table
	 * @param characterWearableMetadata
	 */
	public void load_WearableDetailsTable(CharacterWearableMetadata characterWearableMetadata)
	{
		if(wearablesTableModel.getRowCount()>0)
		{
			for (int i = wearablesTableModel.getRowCount() - 1; i > -1; i--) 
			{
				wearablesTableModel.removeRow(i);
			}
		}

		if(!characterWearableMetadata.getItm_helmet().equals(""))
		{
			String temp_itemType = "HELMET";
			String temp_itemID = characterWearableMetadata.getItm_helmet();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("HELMET").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;
				}
			}
		}

		if(!characterWearableMetadata.getItm_armor().equals(""))
		{
			String temp_itemType = "ARMOR";
			String temp_itemID = characterWearableMetadata.getItm_armor();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("ARMOR").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}

		if(!characterWearableMetadata.getItm_ring().equals(""))
		{
			String temp_itemType = "RING";
			String temp_itemID = characterWearableMetadata.getItm_ring();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("RING").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}

		if(!characterWearableMetadata.getItm_boots().equals(""))
		{
			String temp_itemType = "BOOTS";
			String temp_itemID = characterWearableMetadata.getItm_boots();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("BOOTS").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}

		if(!characterWearableMetadata.getItm_belt().equals(""))
		{
			String temp_itemType = "BELT";
			String temp_itemID = characterWearableMetadata.getItm_belt();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("BELT").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}

		if(!characterWearableMetadata.getItm_shield().equals(""))
		{
			String temp_itemType = "SHIELD";
			String temp_itemID = characterWearableMetadata.getItm_shield();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("SHIELD").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
				}
			}
		}

		if(!characterWearableMetadata.getItm_weapon().equals(""))
		{
			String temp_itemType = "WEAPON";
			String temp_itemID = characterWearableMetadata.getItm_weapon();
			Vector<ItemListMetaInterface> vector_itemList = itemListData.get("WEAPON").getItemListMetadata();

			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();

					wearablesTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});

					break;

				}
			}
		}
	}

	/**
	 * Loads the Data into BackPack Table
	 * 
	 * @param vector_BackpackMetadata return back pack value
	 */
	public void load_BackpackTable(Vector<BackpackMetadata> vector_BackpackMetadata)
	{

		if (backpackTableModel.getRowCount() > 0) 
		{
			for (int i = backpackTableModel.getRowCount() - 1; i > -1; i--) {
				backpackTableModel.removeRow(i);
			}
		}

		BackpackMetadata backpackMetadata;

		for(int i=0; i < vector_BackpackMetadata.size(); i++)
		{
			String temp_itemType = vector_BackpackMetadata.get(i).getItemType();
			String temp_itemID = vector_BackpackMetadata.get(i).getItemId();

			Vector<ItemListMetaInterface> vector_itemListMetadata = itemListData.get(temp_itemType).getItemListMetadata();

			for(int k = 0; k < vector_itemListMetadata.size(); k++)
			{
				if(vector_itemListMetadata.get(k).getList_item_id().equals(temp_itemID))
				{
					String temp_ItemName = vector_itemListMetadata.get(k).getList_item_name();

					backpackTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if(e.getActionCommand()=="DOWN")
		{
			int size = 0;

			if(!(characterMetadata.getBackpackMetadata() == null))
			{
				size = characterMetadata.getBackpackMetadata().size();
			}

			if(table_Wearables.getSelectedRow() != -1 && size < 10)
			{
				Vector tableWearableModel = (Vector)wearablesTableModel.getDataVector().elementAt(table_Wearables.getSelectedRow());
				BackpackMetadata backpackMetadata = new BackpackMetadata();

				backpackMetadata.setItemId((String) tableWearableModel.get(0));
				backpackMetadata.setItemType((String) tableWearableModel.get(1));
				characterMetadata.addObserver(this);
				controller.actionMoveToBackPack(characterMetadata,backpackMetadata);
			}
		}
		else if(e.getActionCommand()=="UP")
		{
			if(table_Backpack.getSelectedRow() != -1)
			{
				Vector tableBackpackModel = (Vector)backpackTableModel.getDataVector().elementAt(table_Backpack.getSelectedRow());

				BackpackMetadata backpackMetadata = new BackpackMetadata();

				backpackMetadata.setItemId((String) tableBackpackModel.get(0));
				backpackMetadata.setItemType((String) tableBackpackModel.get(1));
				characterMetadata.addObserver(this);
				controller.actionMoveToWearables(characterMetadata,backpackMetadata);
			}
		}
		else if(e.getActionCommand()=="SWITCH")
		{
			if(table_Wearables.getSelectedRow() != -1)
			{
				Vector tableWearableModel = (Vector)wearablesTableModel.getDataVector().elementAt(table_Wearables.getSelectedRow());
				BackpackMetadata playingCharBackpackMetadata = new BackpackMetadata();

				playingCharBackpackMetadata.setItemId((String) tableWearableModel.get(0));
				playingCharBackpackMetadata.setItemType((String) tableWearableModel.get(1));
				
				characterMetadata.addObserver(this);
				controller.actionSwitchWithWearables(characterMetadata, friendsMetadata, playingCharBackpackMetadata);
			}
		}
		else if(e.getActionCommand()=="Save")
		{
			String charName = characterMetadata.getCharacter_name();
			boolean response = controller.actionSave(characterMetadata, charName);
			if(response == true)
			{
				int res = JOptionPane.showOptionDialog(null, "Inventory swap is done successfully", "Confirmation", JOptionPane.DEFAULT_OPTION,
						JOptionPane.INFORMATION_MESSAGE, null, null, null);
				if(res == JOptionPane.OK_OPTION)
				{
					this.dispose();
				}
				System.out.println(res);
			}
			else
			{
				int res = JOptionPane.showOptionDialog(null, "Please try again!!!", "Confirmation", JOptionPane.DEFAULT_OPTION,
						JOptionPane.INFORMATION_MESSAGE, null, null, null);
			}
		}

	}

	@Override
	public void update(Observable o, Object arg) 
	{
		if(characterMetadata.getBackpackMetadata() != null)
		{
			Vector<BackpackMetadata> vector_backpackMetadata = characterMetadata.getBackpackMetadata();
			load_BackpackTable(vector_backpackMetadata);
		}

		if(characterMetadata.getCharacterWearableMetadata() != null)
		{
			CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			load_WearableDetailsTable(characterWearableMetadata);
		}

	}

	
}
