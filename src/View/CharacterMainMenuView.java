package View;

import javax.swing.JPanel;
import metadatas.ListMetadata;
import metadatas.ScoreBonusMetadata;
import utilities.CharacterUtility;
import utilities.ListPanelUtility;
import utilities.ListUtility;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Controller.CharacterMainMenuViewController;
import Model.CharacterModel;
import Model.ItemModel;
import metadatas.AbilityModifierMetadata;
import metadatas.AbilityScoreMetadata;
import metadatas.BackpackMetadata;
import metadatas.BonusMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterWearableMetadata;
import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;

import java.awt.GridBagLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

/**
 * This class renders the Character Main screen. This screen displays the list of characters, character details,
 * create character button and edit character button.
 * 
 * @author Gurvinder Singh
 * @author Karan Deep Singh 
 */

public class  CharacterMainMenuView  extends JPanel implements ActionListener, PropertyChangeListener {

	/*
	 * Instance of the controller. 
	 */
	private CharacterMainMenuViewController controller;

	/*
	 * JPanel for the CharacterMainMenuView.
	 */
	private JPanel panel;
	private JPanel panel_ListHeader;
	private JPanel panel_Wearables;
	private JPanel panel_AbilityScores;
	private JPanel panel_Backpack;

	/*
	 * JTable for the CharacterMainMenuView.
	 */
	private JTable table_Wearables;
	private JTable tableAbilityScores;
	private JTable table_Backpack;

	/*
	 * JScrollPane for the character details.
	 */
	private JScrollPane tableAbilityScoresScroller;
	private JScrollPane tableWearablesScroller;
	private JScrollPane tableBackpackScroller;

	/*
	 * Button for character creation.
	 */
	private JButton btnCharacter; 
	private JButton btnCreateCharacter; 
	private JButton btnEditCharacter;

	/*
	 * Character and Item Model
	 */
	private CharacterModel characterModel;
	private ItemModel itemModel;

	/*
	 * Instance of ListPanelUtility for creating custom list.
	 */
	private ListPanelUtility listPanel;

	/*
	 *List Metadata for the ListPanel utlity. 
	 */
	private ListMetadata listMetadata;

	/*
	 * String to get selected character.
	 */
	public String selectedID;

	/*
	 * It is an object of Character metadata 
	 */
	private CharacterMetadata characterMetadata;

	/*
	 * Hashmap for all the Items and characters in the system.
	 */
	public static HashMap<String, CharacterMetadata> characterListData;
	public static HashMap<String,ItemMetadata> itemListData;

	/*
	 * Default Table Model
	 */
	private DefaultTableModel abilityTableModel;
	private DefaultTableModel wearablesTableModel;
	private DefaultTableModel backpackTableModel;

	/*
	 * Bonus metadata containing enchantment details.
	 */
	private BonusMetadata bonusMetadata;
	private ScoreBonusMetadata scoreBonusMetadata;

	/*
	 * Instance of character utility class.
	 */
	private CharacterUtility characterUtility;

	/*
	 * Table headers for the JTable displayed on page.
	 */
	private String[] tableAbilityScoresHeader = {"Characteristics","Ability Scores","Ability Modifier"};
	private String[] tableWearablesHeader = {"Item Type","Item Name"};
	private String[] tableBackpackHeader = {"Slot Number","Item Type","Item Name"};
	private JButton btnCancel;

	/**
	 * Constructor for the CharacterMainView. It initializes the view with all the display.
	 * 
	 * @param controller It is an object of CharacterMainMenuViewController.
	 */
	public CharacterMainMenuView(CharacterMainMenuViewController controller) {
		this.controller = controller;
		init_();
	}

	/**
	 * Initializes the Panel View for the Character Menu
	 */
	private void init_() 
	{
		characterModel = new CharacterModel();
		itemModel = new ItemModel();
		characterListData = characterModel.getCharacterMap();
		itemListData = itemModel.getItemMap();

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{0.0, 1.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0};
		setLayout(gridBagLayout);

		///////////////////////// ABILITY SCORE TABLE /////////////////////////

		panel_ListHeader = new JPanel();
		panel_ListHeader.setBorder(new TitledBorder(null, "Character List", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_header = new GridBagConstraints();
		gbc_panel_header.insets = new Insets(0, 0, 5, 5);
		gbc_panel_header.gridx = 0;
		gbc_panel_header.gridy = 0;
		add(panel_ListHeader, gbc_panel_header);
		GridBagLayout gbl_panel_header = new GridBagLayout();
		panel_ListHeader.setLayout(gbl_panel_header);
		listPanel = new ListPanelUtility(characterModel.getListMetadata());
		GridBagConstraints gbc_listPanel = new GridBagConstraints();
		panel_ListHeader.add(listPanel, gbc_listPanel);
		listPanel.setToolTipText("");
		listPanel.addPropertyChangeListener(this);
		listPanel.setBounds(0,0,100,100);


		//Get selected object from the list panel, give the value to Hashmap characterList

		if(!(listPanel.getSelectedObject()==null))
		{
			listMetadata = listPanel.getSelectedObject();
			selectedID = listMetadata.getItem_ID();
		}
		///////////////////////// ENDS /////////////////////////

		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		panel.setLayout(gbl_panel);

		///////////////////////// ABILITY SCORE TABLE /////////////////////////

		panel_AbilityScores = new JPanel();
		panel_AbilityScores.setBorder(new TitledBorder(null, "Ability Scores", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		abilityTableModel = new DefaultTableModel(0,0);
		abilityTableModel.setColumnIdentifiers(tableAbilityScoresHeader);

		tableAbilityScores = new JTable(abilityTableModel);
		tableAbilityScores.setPreferredScrollableViewportSize(new Dimension(300,100));

		tableAbilityScoresScroller = new JScrollPane(tableAbilityScores);

		if(!(listMetadata==null))
		{
			characterMetadata = characterListData.get(listMetadata.getItem_ID());
			AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
			AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();
			load_AbilityScoresTable(abilityScoreMetadata, abilityModifierMetadata);		
		}
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		panel.add(panel_AbilityScores, gbc_panel_2);

		GridBagLayout gbl_panel_2 = new GridBagLayout();
		panel_AbilityScores.setLayout(gbl_panel_2);

		GridBagConstraints gbc_table = new GridBagConstraints();
		panel_AbilityScores.add(tableAbilityScoresScroller, gbc_table);
		///////////////////////// ENDS /////////////////////////

		btnCreateCharacter = new JButton("+ Character");
		btnCreateCharacter.addActionListener(this);
		GridBagConstraints gbc_btnCreateCharacter = new GridBagConstraints();
		gbc_btnCreateCharacter.insets = new Insets(0, 0, 5, 5);
		gbc_btnCreateCharacter.gridx = 0;
		gbc_btnCreateCharacter.gridy = 2;
		add(btnCreateCharacter, gbc_btnCreateCharacter);

		btnEditCharacter = new JButton("Edit Character");
		btnEditCharacter.addActionListener(this);
		GridBagConstraints gbc_btnEditCharacter = new GridBagConstraints();
		gbc_btnEditCharacter.insets = new Insets(0, 0, 0, 5);
		gbc_btnEditCharacter.gridx = 0;
		gbc_btnEditCharacter.gridy = 3;
		add(btnEditCharacter, gbc_btnEditCharacter);

		/////////////////////////////WEARABLES PANEL/////////////////////////////////
		panel_Wearables = new JPanel();
		panel_Wearables.setBorder(new TitledBorder(null, "Wearable Items", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_weareables = new GridBagConstraints();
		gbc_panel_weareables.insets = new Insets(0, 0, 5, 0);
		gbc_panel_weareables.gridx = 1;
		gbc_panel_weareables.gridy = 1;
		add(panel_Wearables, gbc_panel_weareables);
		GridBagLayout gbl_panel_wearables = new GridBagLayout();
		panel_Wearables.setLayout(gbl_panel_wearables);

		wearablesTableModel = new DefaultTableModel(0,0);
		wearablesTableModel.setColumnIdentifiers(tableWearablesHeader);

		table_Wearables = new JTable(wearablesTableModel);
		table_Wearables.setPreferredScrollableViewportSize(new Dimension(300,100));
		tableWearablesScroller = new JScrollPane(table_Wearables);

		if(!(listMetadata==null))
		{
			CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			load_WearablesTable(characterWearableMetadata);	
		}


		GridBagConstraints gbc_table_wearables = new GridBagConstraints();
		panel_Wearables.add(tableWearablesScroller, gbc_table_wearables);

		///////////////////////////////////ENDS///////////////////////////////////////

		//////////////////////////////Panel Backpack/////////////////////////////////
		panel_Backpack = new JPanel();
		panel_Backpack.setBorder(new TitledBorder(null, "Backpack", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_backpack = new GridBagConstraints();
		gbc_panel_backpack.insets = new Insets(0, 0, 5, 0);
		gbc_panel_backpack.gridx = 1;
		gbc_panel_backpack.gridy = 2;
		add(panel_Backpack, gbc_panel_backpack);
		GridBagLayout gbl_panel_backpack = new GridBagLayout();
		panel_Backpack.setLayout(gbl_panel_backpack);

		backpackTableModel = new DefaultTableModel(0, 0);
		backpackTableModel.setColumnIdentifiers(tableBackpackHeader);

		table_Backpack = new JTable(backpackTableModel);
		table_Backpack.setPreferredScrollableViewportSize(new Dimension(300,100));
		tableBackpackScroller = new JScrollPane(table_Backpack);

		if(!(listMetadata==null))
		{
			Vector<BackpackMetadata> vector_BackpackMetadata = characterMetadata.getBackpackMetadata();
			load_BackpackTable(vector_BackpackMetadata);
		}

		GridBagConstraints gbc_table_backpack = new GridBagConstraints();
		gbc_table_backpack.gridx = 0;
		gbc_table_backpack.gridy = 0;
		panel_Backpack.add(tableBackpackScroller, gbc_table_backpack);
		
		btnCancel = new JButton("Cancel");
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.gridx = 1;
		gbc_btnCancel.gridy = 3;
		add(btnCancel, gbc_btnCancel);
		
		btnCancel.addActionListener(this);
		////////////////////////////////////ENDS//////////////////////////////////////

	}

	/**
	 * Loads the Data into AblityScores Table
	 * @param abilityScoreMetadata It is the Ability scores metadata for the character.
	 * @param abilityModifierMetadata It is the Ability modifier score metadata for the character.
	 */
	public void load_AbilityScoresTable(AbilityScoreMetadata abilityScoreMetadata, 
			AbilityModifierMetadata abilityModifierMetadata)
	{
		if (abilityTableModel.getRowCount() > 0) 
		{
			for (int i = abilityTableModel.getRowCount() - 1; i > -1; i--) {
				abilityTableModel.removeRow(i);
			}
		}

		scoreBonusMetadata = new ScoreBonusMetadata();
		characterUtility = new CharacterUtility();
		bonusMetadata = characterMetadata.getBonusMetadata();
		scoreBonusMetadata = characterUtility.parseScoreAndBonus(abilityScoreMetadata, bonusMetadata);

		abilityTableModel.addRow( new Object[]
				{	
						"STRENGTH",
						scoreBonusMetadata.getStrResult(),
						abilityModifierMetadata.getAbm_strength()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"DEXTERITY",
						scoreBonusMetadata.getDexResult(),
						abilityModifierMetadata.getAbm_dexterity()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"CONSTITUTION",
						scoreBonusMetadata.getConResult(),
						abilityModifierMetadata.getAbm_constitution()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"INTELLIGENCE",
						scoreBonusMetadata.getIntResult(),
						abilityModifierMetadata.getAbm_intelligence()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"WISDOM",
						scoreBonusMetadata.getWisResult(),
						abilityModifierMetadata.getAbm_wisdom()
				}
				);
		abilityTableModel.addRow( new Object[]
				{	
						"CHARISMA",
						scoreBonusMetadata.getChaResult(),
						abilityModifierMetadata.getAbm_charisma()
				}
				);

	}

	/**
	 * Loads the Data into Wearable Table
	 * @param characterWearableMetadata
	 */
	public void load_WearablesTable(CharacterWearableMetadata characterWearableMetadata)
	{
		if (wearablesTableModel.getRowCount() > 0) 
		{
			for (int i = wearablesTableModel.getRowCount() - 1; i > -1; i--) {
				wearablesTableModel.removeRow(i);
			}
		}

		wearablesTableModel.addRow( new Object[]
				{	
						"HELMET",
						getItemTypeList("HELMET",characterWearableMetadata.getItm_helmet())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"ARMOR",
						getItemTypeList("ARMOR",characterWearableMetadata.getItm_armor())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"SHIELD",
						getItemTypeList("SHIELD",characterWearableMetadata.getItm_shield())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"RING",
						getItemTypeList("RING",characterWearableMetadata.getItm_ring())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"BELT",
						getItemTypeList("BELT",characterWearableMetadata.getItm_belt())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"BOOTS",
						getItemTypeList("BOOTS",characterWearableMetadata.getItm_boots())
				}
				);
		wearablesTableModel.addRow( new Object[]
				{	
						"WEAPON",
						getItemTypeList("WEAPON",characterWearableMetadata.getItm_weapon())
				}
				);
	}

	/**
	 * This method searches the item from the list of items.
	 * 
	 * @param ItemType It is the type of item to be searched.
	 * @param ItemId It is the Id of the item to be searched.
	 * @return an itemListMetadata that contains all the required details about an item.
	 */
	public ItemListMetaInterface getItemTypeList(String ItemType,String ItemId)
	{
		ItemMetadata itemMetadatas = itemListData.get(ItemType);
		if(!(itemMetadatas==null))
		{
			Vector<ItemListMetaInterface> vector_ItemListMetadatas = itemMetadatas.getItemListMetadata();
			for(int i=0;i<vector_ItemListMetadatas.size();i++)
			{
				ItemListMetaInterface itemListMetadata = vector_ItemListMetadatas.get(i);
				if(ItemId.equals(itemListMetadata.getList_item_id()))
				{
					return itemListMetadata;
				}
			}
		}

		return null;
	}


	/**
	 * Loads the Data into BackPack Table
	 * 
	 * @param vector_BackpackMetadata return back pack value
	 */
	public void load_BackpackTable(Vector<BackpackMetadata> vector_BackpackMetadata)
	{

		if (backpackTableModel.getRowCount() > 0) 
		{
			for (int i = backpackTableModel.getRowCount() - 1; i > -1; i--) {
				backpackTableModel.removeRow(i);
			}
		}

		BackpackMetadata backpackMetadata;
		for(int i=0; i<vector_BackpackMetadata.size();i++)
		{
			backpackMetadata = vector_BackpackMetadata.get(i);
			ItemMetadata itemMetadatas = itemListData.get(backpackMetadata.getItemType());
			Vector<ItemListMetaInterface> vector_ItemListMetadatas = itemMetadatas.getItemListMetadata();

			for(int j=0;j<vector_ItemListMetadatas.size();j++)
			{
				ItemListMetaInterface itemListMetadata = vector_ItemListMetadatas.get(j);
				if(backpackMetadata.getItemId().equals(itemListMetadata.getList_item_id()))
				{
					backpackTableModel.addRow(new String[]
							{
									"Slot "+i,
									backpackMetadata.getItemType(),
									itemListMetadata.getList_item_name()

							});
				}

			}

		}
	}

	/**
	 * It is the default listener for PropertyChangeListener event.
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt)
	{
		while(ListUtility.isChanged)
		{
			ListUtility.isChanged = false;
			selectedID = ListUtility.responseMetadata.getSelected_id();
			characterMetadata = characterListData.get(ListUtility.responseMetadata.getSelected_id());
			AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
			AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();

			//String[] backpackItem = characterModel.getCharacter_backpack().split("#");
			Vector<BackpackMetadata> backpackItems = characterMetadata.getBackpackMetadata();
			CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();

			load_AbilityScoresTable(abilityScoreMetadata, abilityModifierMetadata);
			load_WearablesTable(characterWearableMetadata);
			load_BackpackTable(backpackItems);
		}
	}

	/**
	 * It is the default listener for the button press event i.e. ActionListener
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "+ Character") 
		{
			controller.createCharacter();
		} else if (e.getActionCommand() == "Edit Character") 
		{
			if(!(listMetadata==null))
			{
				controller.editCharacter(characterMetadata);
			}
			else
			{
				JOptionPane.showOptionDialog(null, "Create atleast 1 character!!", "ERROR", JOptionPane.DEFAULT_OPTION,
				        JOptionPane.INFORMATION_MESSAGE, null, null, null);
			}
		}
		else if(e.getActionCommand()=="Cancel")
		{
			controller.actionCancel();
		}
	}
}

