package View;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import Controller.CampaignCreateViewController;
import Controller.IndexViewController;
import Model.CampaignModel;
import Model.MapModel;
import metadatas.CampaignMapsMetadata;
import metadatas.CampaignMetadata;
import metadatas.ListMetadata;
import metadatas.MapMetadata;
import utilities.ListPanelUtility;
import utilities.ListUtility;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JOptionPane;

/**
 * This Campaign Create View is the next screen which appears when a user clicks Create or Edit on 
 * the Campaign Main Menu screen. This is used for editing an existing campaign or creating a new one.
 * 
 *
 * @author Tarandeep Singh
 * @version 1.0.0
 * @since 07 February 2017
 * @see CampaignMainMenuView
 * @see CampaignModel
 * @see ListMetadata
 * @see CampaignCreateViewController
 * @see ListPanelUtility
 * @see DefaultTableModel
 * @see MapModel
 */

public class CampaignCreateView extends JPanel implements ActionListener, PropertyChangeListener, Observer
{

	/**
	 * All swing objects used for the Campaign Create View
	 */
	private JPanel panel;
	private JPanel parentPanel;
	private JPanel panel_ListHeader;
	private JPanel panel_CampaignInfo;
	private JTable tableCampaignInfo;
	private JScrollPane tableCampaignInfoScroller;
	private JLabel lblSelectACampaign;
	private JButton btnAdd;
	private JButton btnDelete;
	private JButton btnSave;
	private JButton btnCreateMap;
	private JButton btnEditMap;
	private JButton btnCancel;
	public JTextField textField;
	
	/**
	 * DefaultTabelModel instantiated 
	 */
	DefaultTableModel tableModel;
	
	String[] tableHeader = {"Map Sequence","Map Name"};
	CampaignModel campaignModel;
	MapModel mapModel;
	CampaignCreateViewController controller;
	CampaignMainMenuView campaignMainMenuView;
	CampaignMetadata campaignMetadata;
	ListMetadata listMetadata;
	ListPanelUtility listPanel;
	String selectedID;
	String actionType;

	/**
	 * A HashMap that contains all the Campaigns inside this.
	 */
	public static HashMap<String, CampaignMetadata> campaignListData;	
	
	/**
	 * A HashMap that contains all the Maps inside this.
	 */
	public static HashMap<String, MapMetadata> mapListData;
	
	/**
	 * Constructor to point to current object of controller, campaign model, campaignMetadata
	 * and mapModel with calling Initialize method <code> init_() </code>.
	 * @param controller The Campaign Create view controller
	 * @param campaignModel The campaign model
	 * @param campaignMetadata The meta data related to a campaign
	 * @param mapModel The map model
	 * @param actionType User input for action type
	 */
	public CampaignCreateView(CampaignCreateViewController controller, CampaignModel campaignModel,
			CampaignMetadata campaignMetadata, MapModel mapModel, String actionType) 
	{
		this.controller = controller;
		this.campaignModel = campaignModel;
		this.campaignMetadata = campaignMetadata;
		this.mapModel = mapModel;
		this.actionType = actionType;
		
		init_();
	}
	
	/**
	 * This method initializes the view for Campaign Create view 
	 */
	public void init_() 
	{
		
		/**
		 * This conditional works in the case when editing an existing campaign
		 */
		
		if(actionType == "Edit")
		{
			campaignListData = campaignModel.getCampaignMap();
			mapListData = mapModel.getParser();
	
			///////////////////BASIC LAYOUT OF THE VIEW BEGINS HERE//////////////////////
			parentPanel = this;
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0};
			gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
			setLayout(gridBagLayout);
			parentPanel.setBounds(0, 0, 800, 800);
			//////////////////BASIC LAYOUT OF THE VIEW ENDS HERE/////////////////////////
			
			//////////////////HEADING OR VIEW PURPOSE BEGINS HERE////////////////////////
			lblSelectACampaign = new JLabel("CAMPAIGN EDITING MENU");
			lblSelectACampaign.setVerticalAlignment(SwingConstants.TOP);
			lblSelectACampaign.setFont(new Font("Tahoma", Font.BOLD, 14));
			GridBagConstraints gbc_lblSelectACampaign = new GridBagConstraints();
			gbc_lblSelectACampaign.insets = new Insets(0, 0, 5, 5);
			gbc_lblSelectACampaign.gridx = 2;
			gbc_lblSelectACampaign.gridy = 0;
			add(lblSelectACampaign, gbc_lblSelectACampaign);
			//////////////////HEADING OR VIEW PURPOSE ENDS HERE//////////////////////////
			
			//////////////////TEXT FIELD FOR CAMPAIGN NAME///////////////////////////////
			textField = new JTextField();
			GridBagConstraints gbc_textField = new GridBagConstraints();
			gbc_textField.insets = new Insets(0, 0, 5, 5);
			gbc_textField.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField.gridx = 0;
			gbc_textField.gridy = 4;
			add(textField, gbc_textField);
			textField.setColumns(10);
			textField.setText(campaignMetadata.getCampaign_name());
			//////////////////TEXT FIELD ENDS////////////////////////////////////////////

			///////////////////BUTTON TO ADD MAP/////////////////////////////////////////
			btnAdd = new JButton("Add");
			GridBagConstraints gbc_btnPlayCampaign = new GridBagConstraints();
			gbc_btnPlayCampaign.insets = new Insets(0, 0, 5, 5);
			gbc_btnPlayCampaign.gridx = 2;
			gbc_btnPlayCampaign.gridy = 5;
			add(btnAdd, gbc_btnPlayCampaign);
			btnAdd.addActionListener(this);
			//////////////////BUTTON ENDS HERE///////////////////////////////////////////
			
			/////////////////LIST UTILITY USED FOR MAP LIST//////////////////////////////
			listPanel = new ListPanelUtility(mapModel.getListMetadata());
			listMetadata = listPanel.getSelectedObject();
			selectedID = listMetadata.getItem_ID();
			listPanel.addPropertyChangeListener(this);
			/////////////////LIST UTILITY ENDS HERE//////////////////////////////////////
				
			/////////////////LAYOUT FOR MAP LIST/////////////////////////////////////////
			panel_ListHeader = new JPanel();
			panel_ListHeader.setBorder(new TitledBorder(null, "Map List", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel_header = new GridBagConstraints();
			gbc_panel_header.insets = new Insets(0, 0, 5, 5);
			gbc_panel_header.gridx = 0;
			gbc_panel_header.gridy = 6;
			add(panel_ListHeader, gbc_panel_header);
			GridBagLayout gbl_panel_header = new GridBagLayout();
			panel_ListHeader.setLayout(gbl_panel_header);
			GridBagConstraints gbc_listPanel = new GridBagConstraints();
			panel_ListHeader.add(listPanel, gbc_listPanel);
			listPanel.setBounds(0,0,100,100);
			/////////////////////////// ENDS HERE////////////////////////////////////////
			
			/////////////////BUTTON TO CREATE A MAP//////////////////////////////////////
			btnCreateMap = new JButton("+ CREATE MAP");
			GridBagConstraints gbc_btnCreateMap = new GridBagConstraints();
			gbc_btnCreateMap.insets = new Insets(0, 0, 5, 5);
			gbc_btnCreateMap.gridx = 0;
			gbc_btnCreateMap.gridy = 8;
			add(btnCreateMap, gbc_btnCreateMap);
			btnCreateMap.addActionListener(this);
			/////////////////BUTTON ENDS HERE/////////////////////////////////////////////
			
			/////////////////BUTTON TO EDIT A MAP/////////////////////////////////////////
			btnEditMap = new JButton("EDIT MAP");
			GridBagConstraints gbc_btnEditMap = new GridBagConstraints();
			gbc_btnEditMap.insets = new Insets(0, 0, 5, 5);
			gbc_btnEditMap.gridx = 0;
			gbc_btnEditMap.gridy = 10;
			add(btnEditMap, gbc_btnEditMap);
			btnEditMap.addActionListener(this);
			/////////////////BUTTON ENDS HERE/////////////////////////////////////////////
			
			/////////////////BUTTON TO DELETE A MAP FROM CAMPAIGN/////////////////////////
			btnDelete = new JButton("Delete");
			GridBagConstraints gbc_btnEdit = new GridBagConstraints();
			gbc_btnEdit.insets = new Insets(0, 0, 5, 5);
			gbc_btnEdit.gridx = 2;
			gbc_btnEdit.gridy = 6;
			add(btnDelete, gbc_btnEdit);
			btnDelete.addActionListener(this);
			/////////////////BUTTON ENDS HERE/////////////////////////////////////////////
			
			/**
			 * DefaultTabelModel type tableModel created 
			 */
			tableModel = new DefaultTableModel(0,0);
			tableModel.setColumnIdentifiers(tableHeader);
			
			/**
			 * Vector created to store campaignMapsMetadata, data corresponding to maps 
			 * present in a campaign
			 */
			Vector<CampaignMapsMetadata> campaignMapsMetadata = campaignMetadata.getVector_campaignMap();
			
			/**
			 * Campaign Information table loaded with campaignMapsMetadata.
			 */
			load_CampaignInfoTable(campaignMapsMetadata);
			
			///////////////////////// CAMPAIGN INFORMATION TABLE /////////////////////////
			panel_CampaignInfo = new JPanel();
			GridBagConstraints gbc_panel_CampaignInfo = new GridBagConstraints();
			gbc_panel_CampaignInfo.insets = new Insets(0, 0, 5, 0);
			gbc_panel_CampaignInfo.gridx = 3;
			gbc_panel_CampaignInfo.gridy = 6;
			add(panel_CampaignInfo, gbc_panel_CampaignInfo);
			panel_CampaignInfo.setBorder(new TitledBorder(null, "Campaign Information", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			
			tableCampaignInfo = new JTable(tableModel);
			tableCampaignInfo.setPreferredScrollableViewportSize(new Dimension(300,100));
			
			tableCampaignInfoScroller = new JScrollPane(tableCampaignInfo);
			
			GridBagLayout gbl_panel_2 = new GridBagLayout();
			panel_CampaignInfo.setLayout(gbl_panel_2);
			
			GridBagConstraints gbc_table = new GridBagConstraints();
			panel_CampaignInfo.add(tableCampaignInfoScroller, gbc_table);
			///////////////////////// ENDS HERE ////////////////////////////////////////////
			
			///////////////////////// BASIC LAYOUT CONTINUE ////////////////////////////////
			panel = new JPanel();
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 3;
			gbc_panel.gridy = 7;
			add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0};
			gbl_panel.rowHeights = new int[]{0, 0};
			gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			///////////////////////// BASIC LAYOUT ENDS ///////////////////////////////////
		
			///////////////////////// SAVE BUTTON /////////////////////////////////////////
			btnSave = new JButton("Save");
			GridBagConstraints gbc_btnSave = new GridBagConstraints();
			gbc_btnSave.insets = new Insets(0, 0, 0, 5);
			gbc_btnSave.gridx = 2;
			gbc_btnSave.gridy = 9;
			add(btnSave, gbc_btnSave);
			btnSave.addActionListener(this);
			///////////////////////// ENDS HERE ///////////////////////////////////////////
		}
		
		/**
		 * This conditional works in the case when creating a new campaign
		 */
		else if(actionType == "Create")
		{
			
			campaignListData = campaignModel.getCampaignMap();
			mapListData = mapModel.getParser();
	
			///////////////////BASIC LAYOUT OF THE VIEW BEGINS HERE//////////////////////
			parentPanel = this;
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0};
			gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
			setLayout(gridBagLayout);
			parentPanel.setBounds(0, 0, 800, 800);
			///////////////////BASIC LAYOUT OF THE VIEW ENDS HERE//////////////////////
			
			/////////////////// VIEW HEADING OR TITLE BEGINS HERE//////////////////////
			lblSelectACampaign = new JLabel("CAMPAIGN CREATION MENU");
			lblSelectACampaign.setVerticalAlignment(SwingConstants.TOP);
			lblSelectACampaign.setFont(new Font("Tahoma", Font.BOLD, 14));
			GridBagConstraints gbc_lblSelectACampaign = new GridBagConstraints();
			gbc_lblSelectACampaign.insets = new Insets(0, 0, 5, 5);
			gbc_lblSelectACampaign.gridx = 2;
			gbc_lblSelectACampaign.gridy = 0;
			add(lblSelectACampaign, gbc_lblSelectACampaign);
			/////////////////// VIEW HEADING OR TITLE ENDS HERE/////////////////////////
			
			/////////////////// CAMPAIGN NAME IN TEXT FIELD/////////////////////////////
			textField = new JTextField();
			GridBagConstraints gbc_textField = new GridBagConstraints();
			gbc_textField.insets = new Insets(0, 0, 5, 5);
			gbc_textField.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField.gridx = 0;
			gbc_textField.gridy = 4;
			add(textField, gbc_textField);
			textField.setColumns(10);
			///////////////////////// ENDS /////////////////////////////////////////////
			
			///////////////////////// ADD BUTTON ///////////////////////////////////////
			btnAdd = new JButton("Add");
			GridBagConstraints gbc_btnPlayCampaign = new GridBagConstraints();
			gbc_btnPlayCampaign.insets = new Insets(0, 0, 5, 5);
			gbc_btnPlayCampaign.gridx = 2;
			gbc_btnPlayCampaign.gridy = 5;
			add(btnAdd, gbc_btnPlayCampaign);
			btnAdd.addActionListener(this);
			///////////////////////// ADD BUTTON ENDS ///////////////////////////////////
			
			/////////////////////// LIST PANEL FOR MAP LIST /////////////////////////////
			listPanel = new ListPanelUtility(mapModel.getListMetadata());
			listMetadata = listPanel.getSelectedObject();
			selectedID = listMetadata.getItem_ID();
			listPanel.addPropertyChangeListener(this);
			
			panel_ListHeader = new JPanel();
			panel_ListHeader.setBorder(new TitledBorder(null, "Map List", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			GridBagConstraints gbc_panel_header = new GridBagConstraints();
			gbc_panel_header.insets = new Insets(0, 0, 5, 5);
			gbc_panel_header.gridx = 0;
			gbc_panel_header.gridy = 6;
			add(panel_ListHeader, gbc_panel_header);
			GridBagLayout gbl_panel_header = new GridBagLayout();
			panel_ListHeader.setLayout(gbl_panel_header);
			GridBagConstraints gbc_listPanel = new GridBagConstraints();
			panel_ListHeader.add(listPanel, gbc_listPanel);
			listPanel.setBounds(0,0,100,100);
			/////////////////////// LIST PANEL ENDS HERE /////////////////////////////////

			/////////////////////// CREATE BUTTON TO CREATE MAP //////////////////////////
			btnCreateMap = new JButton("+ CREATE MAP");
			GridBagConstraints gbc_btnCreateMap = new GridBagConstraints();
			gbc_btnCreateMap.insets = new Insets(0, 0, 5, 5);
			gbc_btnCreateMap.gridx = 0;
			gbc_btnCreateMap.gridy = 8;
			add(btnCreateMap, gbc_btnCreateMap);
			btnCreateMap.addActionListener(this);
			/////////////////////// CREATE BUTTON ENDS HERE //////////////////////////////
			
			/////////////////BUTTON TO EDIT A MAP/////////////////////////////////////////
			btnEditMap = new JButton("EDIT MAP");
			GridBagConstraints gbc_btnEditMap = new GridBagConstraints();
			gbc_btnEditMap.insets = new Insets(0, 0, 5, 5);
			gbc_btnEditMap.gridx = 0;
			gbc_btnEditMap.gridy = 10;
			add(btnEditMap, gbc_btnEditMap);
			btnEditMap.addActionListener(this);
			/////////////////BUTTON ENDS HERE////////////////////////////////////////////
				
			/////////////////////// DELETE BUTTON TO DELETE MAP /////////////////////////
			btnDelete = new JButton("Delete");
			GridBagConstraints gbc_btnEdit = new GridBagConstraints();
			gbc_btnEdit.insets = new Insets(0, 0, 5, 5);
			gbc_btnEdit.gridx = 2;
			gbc_btnEdit.gridy = 6;
			add(btnDelete, gbc_btnEdit);
			btnDelete.addActionListener(this);
			/////////////////////// DELETE BUTTON ENDS HERE //////////////////////////////
			
			///////////////////////// CAMPAIGN INFORMATION TABLE /////////////////////////
			tableModel = new DefaultTableModel(0,0);
			tableModel.setColumnIdentifiers(tableHeader);
			
			panel_CampaignInfo = new JPanel();
			GridBagConstraints gbc_panel_CampaignInfo = new GridBagConstraints();
			gbc_panel_CampaignInfo.insets = new Insets(0, 0, 5, 0);
			gbc_panel_CampaignInfo.gridx = 3;
			gbc_panel_CampaignInfo.gridy = 6;
			add(panel_CampaignInfo, gbc_panel_CampaignInfo);
			panel_CampaignInfo.setBorder(new TitledBorder(null, "Campaign Information", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			
			tableCampaignInfo = new JTable(tableModel);
			tableCampaignInfo.setPreferredScrollableViewportSize(new Dimension(300,100));
			
			tableCampaignInfoScroller = new JScrollPane(tableCampaignInfo);
			
			GridBagLayout gbl_panel_2 = new GridBagLayout();
			panel_CampaignInfo.setLayout(gbl_panel_2);
			
			GridBagConstraints gbc_table = new GridBagConstraints();
			panel_CampaignInfo.add(tableCampaignInfoScroller, gbc_table);
			///////////////////////// TABLE ENDS HERE ////////////////////////////////////
			
			panel = new JPanel();
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 3;
			gbc_panel.gridy = 7;
			add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0};
			gbl_panel.rowHeights = new int[]{0, 0};
			gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			
			///////////////////////// SAVE BUTTON ////////////////////////////////////////
			btnSave = new JButton("Save");
			GridBagConstraints gbc_btnSave = new GridBagConstraints();
			gbc_btnSave.insets = new Insets(0, 0, 0, 5);
			gbc_btnSave.gridx = 2;
			gbc_btnSave.gridy = 9;
			add(btnSave, gbc_btnSave);
			btnSave.addActionListener(this);
			///////////////////////// BUTTON ENDS HERE ////////////////////////////////////
			
		}
		
		///////////////////////// Cancel BUTTON /////////////////////////////////////////
		btnCancel = new JButton("Cancel");
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 0, 0, 5);
		gbc_btnCancel.gridx = 2;
		gbc_btnCancel.gridy = 11;
		add(btnCancel, gbc_btnCancel);
		btnCancel.addActionListener(this);
		///////////////////////// ENDS HERE ///////////////////////////////////////////
	}
	

	/**
	 * Function to load Campaign Information table using campaign map meta data
	 * @param campaignMapsMetadatas The meta data for maps in campaigns 
	 */
	
	public void load_CampaignInfoTable(Vector<CampaignMapsMetadata> campaignMapsMetadatas)
	{
		if (tableModel.getRowCount() > 0) 
		{
		    for (int i = tableModel.getRowCount() - 1; i > -1; i--) {
		    	tableModel.removeRow(i);
		    }
		}
		
		for(int i = 0; i<campaignMapsMetadatas.size(); i++)
		{
			CampaignMapsMetadata item_campaignMaps = campaignMapsMetadatas.get(i);
			
			tableModel.addRow( new Object[]
					{	
						item_campaignMaps.getMap_sequence(),
						item_campaignMaps.getMap_name()
					}
				);
			
		}
	}
	
	/**
	 * Function to check if a map is present in campaign table
	 * @return boolean
	 */
	public boolean isMapPresent()
	{
		TableModel tableModel = tableCampaignInfo.getModel();
		if(tableModel.getRowCount()==0)
		{
			return false;
		}
		return true;
	}
	
	@Override
	/**
	 * {@inheritDoc}
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getActionCommand().equals("Add") && actionType.equals("Edit"))
		{
			MapMetadata mapMetadata = mapListData.get(selectedID);
			controller.actionAdd(mapMetadata);
		}
		else if(e.getActionCommand().equals("Add") && actionType.equals("Create"))
		{
			
			String newCampaignName = textField.getText();
			MapMetadata mapMetadata = mapListData.get(selectedID);
			controller.actionAddToNewCampaign(mapMetadata, newCampaignName);
		}
		else if(e.getActionCommand().equals("Delete") && actionType.equals("Edit"))
		{
			System.out.println("Delete clicked !");
			MapMetadata mapMetadata = mapListData.get(selectedID);
			controller.actionDelete(mapMetadata);
		}
		else if(e.getActionCommand().equals("Delete") && actionType.equals("Create"))
		{
			
			String newCampaignName = textField.getText();
			MapMetadata mapMetadata = mapListData.get(selectedID);
			controller.actionDeleteFromNewCampaign(mapMetadata, newCampaignName);
		}
		else if(e.getActionCommand().equals("Save"))
		{
			String newCampaignName = textField.getText();
			if(newCampaignName.isEmpty() )
			{
				
				if(!isMapPresent())
				{
					JOptionPane.showOptionDialog(null, "Campaign Name cannot be empty and should have atleast 1 map!!", "ERROR", JOptionPane.DEFAULT_OPTION,
					        JOptionPane.INFORMATION_MESSAGE, null, null, null);
				}
				else
				{
				JOptionPane.showOptionDialog(null, "Your campaign name cannot be empty!!", "ERROR", JOptionPane.DEFAULT_OPTION,
				        JOptionPane.INFORMATION_MESSAGE, null, null, null);
				}
			}
			else if(!isMapPresent())
			{
				JOptionPane.showOptionDialog(null, "Your Campaign should have atleast 1 Map!!", "ERROR", JOptionPane.DEFAULT_OPTION,
				        JOptionPane.INFORMATION_MESSAGE, null, null, null);
			}
			else
			{
				controller.actionSave(newCampaignName);
				int res = JOptionPane.showOptionDialog(null, "Your Campaign has been saved successfully", "Confirmation", JOptionPane.DEFAULT_OPTION,
				        JOptionPane.INFORMATION_MESSAGE, null, null, null);
				if(res == JOptionPane.OK_OPTION)
				{
					new IndexViewController().LoadCampaignMainMenu();
				}
				System.out.println(res);			
			}
		}
		else if(e.getActionCommand().equals("+ CREATE MAP"))
		{
			controller.actionCreateMap();
		}
		else if(e.getActionCommand().equals("EDIT MAP"))
		{
			MapMetadata mapMetadata = mapListData.get(selectedID);
			controller.actionEditMap(mapMetadata);
		}
		else if(e.getActionCommand()=="Cancel")
		{
			controller.actionCancel();
		}
	}
	
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		// TODO Auto-generated method stub
		while(ListUtility.isChanged)
		{
			ListUtility.isChanged = false;
			selectedID = ListUtility.responseMetadata.getSelected_id();
		}
	}
	@Override
	public void update(Observable o, Object arg) 
	{
		if(actionType.equals("Edit"))
		{
			load_CampaignInfoTable(campaignMetadata.getVector_campaignMap());
		}
		else
		if(actionType.equals("Create"))
		{
			load_CampaignInfoTable(campaignMetadata.getVector_campaignMap());
		}
	}
}
