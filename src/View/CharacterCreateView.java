	package View;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;
import java.awt.event.*;
import java.util.*;
import Controller.*;
import Model.*;
import metadatas.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import utilities.CharacterUtility;
import utilities.Constants;

/**
 * This Character Create View is used to generate new character, edit existing character
 *
 * @author Han
 * @author Gurvinder Singh
 * @author Karan Deep Singh
 */
public class CharacterCreateView extends JPanel implements ItemListener, ActionListener, FocusListener, Observer{
	
	/*
	 * Private JPanels for the character screen.
	 */
	private JPanel parentPanel;
	private JPanel pCharacteristic;
	private JPanel namePanel;

	
	/*
	 * Private Text fields for the character screen.
	 */
	public JTextField tfStrScore; 
	private JTextField tfStrMod;  
	private JTextField tfDexScore;
	private JTextField tfDexMod;
	private JTextField tfConScore;
	private JTextField tfConMod;
	private JTextField tfIntScore;
	private JTextField tfIntMod; 
	private JTextField tfWisScore;
	private JTextField tfWisMod;
	private JTextField tfChaScore;
	private JTextField tfChaMod;
	private JTextField tfCharacterName;
	private JTextField tfArmorClass;
	private JTextField tfHitPoint;
	private JTextField tfAttackBonus;
	private JTextField tfDamageBonus;
	private JTextField tfLevel;
	
	/*
	 * Private JLabel for the character screen.
	 */
	private JLabel lblArmorClass;
	private JLabel lblHitPoint;
	private JLabel lblAttackBonus;
	private JLabel lblDamageBonus;
	private JLabel lblLevel;
	private JLabel lblHelmet;
	private JLabel lblArmor;
	private JLabel lblShield;
	private JLabel lblRing;
	private JLabel lblBelt;
	private JLabel lblBoots;
	private JLabel lblWeapon;
	private JLabel lblCharacteristic;

	/*
	 * Private JButton for the character screen.
	 */
	private JButton btnCancel;
	private JButton btnGenerate; 
	private JButton btnSave;
	private JButton buttonUP;
	private JButton buttonDOWN;
	
	/*
	 * Private JComboBox for the character screen.
	 */
	private JComboBox cbCharacterType;
	private JComboBox cbHelmet;
	private JComboBox cbWeapon;
	private JComboBox cbRing;
	private JComboBox cbBelt;
	private JComboBox cbBoots;
	private JComboBox cbArmor;
	private JComboBox cbShield;
	
	/*
	 * Private JTable for the character screen.
	 */
	private JTable tableBackPackItems;
	private JTable tableInventoryItems;
	
	/*
	 * Private JScrollpane for the character screen.
	 */
	private JScrollPane itemDetailsScroller;
	private JScrollPane itemInventoryScroller;
	
	/*
	 * Specifies the current focussed Text field
	 */
	private String currentFocus;
	private String mode;
	
	/*
	 * Controller object for the CharacterCreateViewController
	 */
	private CharacterCreateViewController controller;

	/*
	 * static object for logging.
	 */
	public static final Logger log = LogManager.getLogger(CharacterCreateView.class);
	
	/*
	 * Private itemModel instance.
	 */
	private ItemModel itemModel;
	
	/*
	 * ItemMap containing all the items along with their id's.
	 */
	private HashMap<String, ItemMetadata> itemMap;
	
	/*
	 * Boolean variable specifying if the character screen is opened for creating a character.
	 */
	private Boolean initialRun = true;
	
	/*
	 * JLabel for displaying error messages.
	 */
	private JLabel lblErrortext;
	
	/*
	 * Object of character metadata instance.
	 */
	private CharacterMetadata characterMetadata;
	
	/*
	 * Private Table instance for displaying the items.
	 */
	private DefaultTableModel itemTableModel;
	private DefaultTableModel itemInventoryTableModel;
	
	/*
	 * Table header for display in items screen.
	 */
	String[] itemTableHeader = {"Item Id","Item Type","Item Name"};


	/**
	 * Constructor of the items create view.
	 * 
	 * @param characterMetadata Instance of the character metadata.
	 * @param controller Instance of the Controller.
	 * @param mode Specifies if mode is create character or edit a character.
	 * @param itemModel Instance of the item model class.
	 */
	public CharacterCreateView(CharacterMetadata characterMetadata, CharacterCreateViewController controller, String mode, ItemModel itemModel) 
	{
		this.characterMetadata = characterMetadata;
		this.controller = controller;
		this.mode = mode;
		this.itemModel = itemModel;
		
		itemMap = itemModel.getItemMap();
		initialize();
	}

	/**
	 * Initialize the contents of the panel.
	 */
	public void initialize() 
	{
		
		parentPanel = this;
		parentPanel.setBounds(0, 0, 1000, 800);
		parentPanel.setLayout(null);

		//Define top name panel
		namePanel = new JPanel();
		namePanel.setBounds(0, 0, 975, 73);
		namePanel.setLayout(null);

		JLabel lblCharacterName = new JLabel("Character Name:");
		lblCharacterName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCharacterName.setBounds(5, 5, 135, 22);

		tfCharacterName = new JTextField();
		tfCharacterName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfCharacterName.setColumns(10);
		tfCharacterName.setBounds(150, 5, 207, 25);

		JLabel lblCharacterType = new JLabel("Fighter Type:");
        lblCharacterType.setFont(new Font("Tahoma", Font.PLAIN, 18));
        lblCharacterType.setBounds(370, 5, 130, 22);

        cbCharacterType = new JComboBox();
        cbCharacterType.setBounds(510, 5, 120, 24);
//        cbCharacterType.addActionListener(this);
        cbCharacterType.addItem("Bully");
        cbCharacterType.addItem("Nimble");
        cbCharacterType.addItem("Tank");

		btnGenerate = new JButton("Generate");
		btnGenerate.addActionListener(this);
		btnGenerate.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnGenerate.setBounds(640, 5, 100, 25);

		btnSave = new JButton("Save");
		btnSave.addActionListener(this);
		btnSave.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSave.setBounds(750, 5, 100, 25);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(this);
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnCancel.setBounds(860, 5, 100, 25);

		//Add elements to name panel
		namePanel.add(lblCharacterName);
		namePanel.add(tfCharacterName);
        namePanel.add(lblCharacterType);
        namePanel.add(cbCharacterType);
        namePanel.add(btnGenerate);
		namePanel.add(btnSave);
		namePanel.add(btnCancel);

		//Define and Ability Panel and its elements------------------------------------------------------------------------------
		JPanel pAbility = new JPanel();
		pAbility.setBounds(0, 71, 400, 300);
		pAbility.setLayout(null);

		JLabel labelAbilityName = new JLabel("Ability Name");
		labelAbilityName.setBounds(5, 5, 130, 25);
		labelAbilityName.setFont(new Font("Tahoma", Font.BOLD, 20));

		JLabel labelScore = new JLabel("Score");
		labelScore.setBounds(170, 5, 80, 25);
		labelScore.setFont(new Font("Tahoma", Font.BOLD, 20));

		JLabel labelModifier = new JLabel("Modifier");
		labelModifier.setBounds(240, 5, 130, 25);
		labelModifier.setFont(new Font("Tahoma", Font.BOLD, 20));

		JLabel lblStr = new JLabel("Strength (STR)");
		lblStr.setBounds(5, 42, 119, 22);
		lblStr.setLabelFor(tfStrScore);
		lblStr.setFont(new Font("Tahoma", Font.PLAIN, 18));

		tfStrScore = new JTextField();
		tfStrScore.setName("str");
		tfStrScore.addFocusListener(this);
		tfStrScore.setBounds(170, 42, 40, 25);
		tfStrScore.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfStrScore.setColumns(10);

		tfStrMod = new JTextField();
		tfStrMod.setBounds(240, 42, 40, 25);
		tfStrMod.setEditable(false);
		tfStrMod.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfStrMod.setColumns(10);

		JLabel lblDex = new JLabel("Dexterity (DEX)");
		lblDex.setBounds(5, 82, 122, 22);
		lblDex.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDex.setLabelFor(tfDexScore);

		tfDexScore = new JTextField();
		tfDexScore.setName("dex");
		tfDexScore.addFocusListener(this);
		tfDexScore.setBounds(170, 82, 40, 25);
		tfDexScore.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfDexScore.setColumns(10);

		tfDexMod = new JTextField();
		tfDexMod.setBounds(240, 82, 40, 25);
		tfDexMod.setEditable(false);
		tfDexMod.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfDexMod.setColumns(10);

		JLabel lblCon = new JLabel("Constitution (CON)");
		lblCon.setBounds(5, 120, 151, 22);
		lblCon.setFont(new Font("Tahoma", Font.PLAIN, 18));

		tfConScore = new JTextField();
		tfConScore.setName("con");
		tfConScore.addFocusListener(this);
		tfConScore.setBounds(170, 120, 40, 25);
		lblCon.setLabelFor(tfConScore);
		tfConScore.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfConScore.setColumns(10);

		tfConMod = new JTextField();
		tfConMod.setBounds(240, 120, 40, 25);
		tfConMod.setEditable(false);
		tfConMod.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfConMod.setColumns(10);

		JLabel lblInt = new JLabel("Intelligence (INT)");
		lblInt.setBounds(5, 160, 140, 22);
		lblInt.setFont(new Font("Tahoma", Font.PLAIN, 18));

		tfIntScore = new JTextField();
		tfIntScore.setName("int");
		tfIntScore.addFocusListener(this);
		tfIntScore.setBounds(170, 160, 40, 25);
		lblInt.setLabelFor(tfIntScore);
		tfIntScore.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfIntScore.setColumns(10);

		tfIntMod = new JTextField();
		tfIntMod.setBounds(240, 160, 40, 25);
		tfIntMod.setEditable(false);
		tfIntMod.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfIntMod.setColumns(10);

		JLabel lblWis = new JLabel("Wisdom (WIS)");
		lblWis.setBounds(5, 200, 119, 22);
		lblWis.setFont(new Font("Tahoma", Font.PLAIN, 18));

		tfWisScore = new JTextField();
		tfWisScore.setName("wis");
		tfWisScore.addFocusListener(this);
		tfWisScore.setBounds(170, 200, 40, 25);
		lblWis.setLabelFor(tfWisScore);
		tfWisScore.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfWisScore.setColumns(10);

		tfWisMod = new JTextField();
		tfWisMod.setBounds(240, 200, 40, 25);
		tfWisMod.setEditable(false);
		tfWisMod.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfWisMod.setColumns(10);

		JLabel lblCha = new JLabel("Charistma (CHA)");
		lblCha.setBounds(5, 240, 132, 22);
		lblCha.setFont(new Font("Tahoma", Font.PLAIN, 18));

		tfChaScore = new JTextField();
		tfChaScore.setName("cha");
		tfChaScore.addFocusListener(this);
		tfChaScore.setBounds(170, 240, 40, 25);
		lblCha.setLabelFor(tfChaScore);
		tfChaScore.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfChaScore.setColumns(10);

		tfChaMod = new JTextField();
		tfChaMod.setBounds(240, 240, 40, 25);
		tfChaMod.setEditable(false);
		tfChaMod.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfChaMod.setColumns(10);

		//Add elements to the ability panel
		pAbility.add(labelAbilityName);
		pAbility.add(labelModifier);
		pAbility.add(labelScore);
		pAbility.add(lblStr);
		pAbility.add(tfStrScore);
		pAbility.add(tfStrMod);
		pAbility.add(lblDex);
		pAbility.add(tfDexScore);
		pAbility.add(tfDexMod);
		pAbility.add(lblCon);
		pAbility.add(tfConScore);
		pAbility.add(tfConMod);
		pAbility.add(lblInt);
		pAbility.add(tfIntScore);
		pAbility.add(tfIntMod);

		pAbility.add(lblWis);
		pAbility.add(tfWisScore);
		pAbility.add(tfWisMod);
		pAbility.add(lblCha);
		pAbility.add(tfChaScore);
		pAbility.add(tfChaMod);

		//Define and wearable panel------------------------------------------------------------------------------

		JPanel pWearable = new JPanel();
		pWearable.setBounds(400, 71, 575, 300);
		pWearable.setLayout(null);

		JLabel LabelInventory = new JLabel("Wearable");
		LabelInventory.setBounds(5, 5, 161, 25);
		LabelInventory.setFont(new Font("Tahoma", Font.BOLD, 20));

		lblHelmet = new JLabel("Helmet");
		lblHelmet.setBounds(5, 50, 77, 22);
		lblHelmet.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblArmor = new JLabel("Armor");
		lblArmor.setBounds(5, 83, 119, 22);
		lblArmor.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblShield = new JLabel("Shield");
		lblShield.setBounds(5, 117, 77, 22);
		lblShield.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblRing = new JLabel("Ring");
		lblRing.setBounds(5, 153, 77, 22);
		lblRing.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblBelt = new JLabel("Belt");
		lblBelt.setBounds(5, 189, 119, 22);
		lblBelt.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblBoots = new JLabel("Boots");
		lblBoots.setBounds(5, 225, 119, 22);
		lblBoots.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblWeapon = new JLabel("Weapon");
		lblWeapon.setBounds(5, 261, 119, 22);
		lblWeapon.setFont(new Font("Tahoma", Font.PLAIN, 18));


		cbHelmet = new JComboBox();
		cbHelmet.setBounds(245, 50, 119, 24);
		cbHelmet.addActionListener(this);

		cbArmor = new JComboBox();
		cbArmor.setBounds(245, 83, 119, 24);
		cbArmor.addActionListener(this);

		cbShield = new JComboBox();
		cbShield.setBounds(245, 117, 119, 24);
		cbShield.addActionListener(this);

		cbRing = new JComboBox();
		cbRing.setBounds(245, 157, 119, 24);
		cbRing.addActionListener(this);

		cbBelt = new JComboBox();
		cbBelt.setBounds(245, 189, 119, 24);
		cbBelt.addActionListener(this);

		cbBoots = new JComboBox();
		cbBoots.setBounds(245, 225, 119, 24);
		cbBoots.addActionListener(this);

		cbWeapon = new JComboBox();
		cbWeapon.setBounds(245, 261, 119, 24);
		cbWeapon.addActionListener(this);
		

		loadWearablePanel();


		//Add elements to wearable panel
		pWearable.add(LabelInventory);
		pWearable.add(lblHelmet);
		pWearable.add(cbHelmet);
		pWearable.add(lblArmor);
		pWearable.add(cbArmor);
		pWearable.add(cbShield);
		pWearable.add(lblShield);
		pWearable.add(cbRing);
		pWearable.add(lblRing);
		pWearable.add(cbBelt);
		pWearable.add(lblBelt);
		pWearable.add(cbBoots);
		pWearable.add(lblBoots);
		pWearable.add(cbWeapon);
		pWearable.add(lblWeapon);

		//Define characteristic Panel to the frame------------------------------------------------------------------------------
		pCharacteristic = new JPanel();
		pCharacteristic.setBounds(0, 371, 400, 400);
		pCharacteristic.setLayout(null);

		lblCharacteristic = new JLabel("Characteristic");
		lblCharacteristic.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblCharacteristic.setBounds(5, 0, 161, 25);
		pCharacteristic.add(lblCharacteristic);

		lblLevel = new JLabel("Level");
		lblLevel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLevel.setBounds(5, 32, 119, 22);
		pCharacteristic.add(lblLevel);

		tfLevel = new JTextField();
		tfLevel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfLevel.setBounds(181, 30, 60, 25);
		tfLevel.setName("level");
		tfLevel.addFocusListener(this);
		pCharacteristic.add(tfLevel);

		lblArmorClass = new JLabel("Armor class");
		lblArmorClass.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblArmorClass.setBounds(5, 70, 120, 22);
		pCharacteristic.add(lblArmorClass);

		tfArmorClass = new JTextField();
		tfArmorClass.setEditable(false);
		tfArmorClass.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfArmorClass.setBounds(181, 70, 60, 25);
		pCharacteristic.add(tfArmorClass);

		lblHitPoint = new JLabel("Hit point");
		lblHitPoint.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblHitPoint.setBounds(5, 110, 122, 22);
		pCharacteristic.add(lblHitPoint);

		tfHitPoint = new JTextField();
		tfHitPoint.setEditable(false);
		tfHitPoint.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfHitPoint.setBounds(181, 110, 60, 25);
		pCharacteristic.add(tfHitPoint);

		lblAttackBonus = new JLabel("Attack bonus");
		lblAttackBonus.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAttackBonus.setBounds(5, 150, 151, 22);
		pCharacteristic.add(lblAttackBonus);

		tfAttackBonus = new JTextField();
		tfAttackBonus.setEditable(false);
		tfAttackBonus.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfAttackBonus.setColumns(10);
		tfAttackBonus.setBounds(181, 150, 60, 25);
		pCharacteristic.add(tfAttackBonus);

		lblDamageBonus = new JLabel("Damage bonus");
		lblDamageBonus.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDamageBonus.setBounds(5, 190, 150, 25);
		pCharacteristic.add(lblDamageBonus);

		tfDamageBonus = new JTextField();
		tfDamageBonus.setEditable(false);
		tfDamageBonus.setFont(new Font("Tahoma", Font.PLAIN, 18));
		tfDamageBonus.setBounds(181, 190, 60, 25);
		pCharacteristic.add(tfDamageBonus);


		//Define backpack panel and its elements------------------------------------------------------------------------------
		JPanel pBackpack = new JPanel();
		pBackpack.setBounds(400, 371, 575, 400);
		pBackpack.setLayout(null);

		JLabel LabelSavedItem = new JLabel("Backpack");
		LabelSavedItem.setFont(new Font("Tahoma", Font.BOLD, 20));
		LabelSavedItem.setBounds(25, 151, 161, 25);
		pBackpack.add(LabelSavedItem);

		setLayout(null);

		//Add form element the panel
		parentPanel.add(namePanel);
		
		lblErrortext = new JLabel("errorText");
		lblErrortext.setBounds(79, 46, 655, 20);
		namePanel.add(lblErrortext);
		parentPanel.add(pAbility);
		parentPanel.add(pWearable);
		lblErrortext.setVisible(false);
		
		parentPanel.add(pCharacteristic);
		parentPanel.add(pBackpack);

		
		itemTableModel = new DefaultTableModel(0,0);
		itemTableModel.setColumnIdentifiers(itemTableHeader);
		tableBackPackItems = new JTable(itemTableModel);
		
		itemDetailsScroller = new JScrollPane(tableBackPackItems);
		itemDetailsScroller.setBounds(25, 188, 550, 114);
		itemDetailsScroller.setViewportView(tableBackPackItems);
		
		
		itemInventoryTableModel = new DefaultTableModel(0,0);
		itemInventoryTableModel.setColumnIdentifiers(itemTableHeader);
		tableInventoryItems = new JTable(itemInventoryTableModel);
		
		itemInventoryScroller = new JScrollPane(tableInventoryItems);
		itemInventoryScroller.setBounds(25, 6, 550, 114);
		itemInventoryScroller.setViewportView(tableInventoryItems);

		pBackpack.add(itemDetailsScroller);
		pBackpack.add(itemInventoryScroller);

		buttonDOWN = new JButton("DOWN");
		buttonDOWN.setBounds(324, 132, 95, 29);
		pBackpack.add(buttonDOWN);
		buttonDOWN.addActionListener(this);

		buttonUP = new JButton("UP");
		buttonUP.setBounds(218, 131, 95, 29);
		pBackpack.add(buttonUP);
		
		JButton btnBackpack = new JButton("Remove");
		btnBackpack.setBounds(258, 318, 115, 29);
		btnBackpack.addActionListener(this);
		
		pBackpack.add(btnBackpack);
		buttonUP.addActionListener(this);

		///////////////////////////////////Load Value into the panel////////////////////////////////////////////


		//        loadAbilityPanel(abilityScoreMetadata, abilityModifierMetadata);

		if(characterMetadata.getBackpackMetadata() != null)
		{
			Vector<BackpackMetadata> vector_backpackMetadata = characterMetadata.getBackpackMetadata();
			load_BackPackDetailsTable(vector_backpackMetadata);
		}
		
		if(characterMetadata.getCharacterWearableMetadata() != null)
		{
			CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			load_WearableDetailsTable(characterWearableMetadata);
		}
		
		if(characterMetadata.getCharacter_name() != null)
		{
			tfCharacterName.setText(characterMetadata.getCharacter_name());
		}
		
		loadAbilityPanel(characterMetadata.getAbilityScoreMetadata(),characterMetadata.getAbilityModifierMetadata(),characterMetadata.getBonusMetadata());
		loadCharacteristicPanel(characterMetadata.getCharacter_level(),characterMetadata.getHitPoints(),characterMetadata.getArmorClass(),characterMetadata.getAttackBonus(),characterMetadata.getDamageBonus());
	}
	
	/**
	 * This method is used to load items in the worn items table.
	 * 
	 * @param itemMetadata It is an object of the ItemMetadata containing Items List.
	 * @param itemId It is the Id to uniquely identify an item.
	 * @param textField It is an JTextField instance that will display the worn Items.
	 */
	public void loadWornItems(ItemMetadata itemMetadata, String itemId, JTextField textField)
	{
		Vector<ItemListMetaInterface> wornItemsSearchList = itemMetadata.getItemListMetadata();
		
		for(int i=0;i<wornItemsSearchList.size();i++)
		{
			if(itemId.equals(wornItemsSearchList.get(i).getList_item_id()))
			{
				textField.setText(wornItemsSearchList.get(i).getList_item_name());
			}
		}
	}

	/**
	 * This method specifies if the character creation is done or not.
	 * 
	 * @param buttonType It specifies the button that will be displayed during creating and editing a character.
	 */
	public void initialLoad(String buttonType) 
	{
		if (buttonType.equals("Create")) {
			//ComboBox
			cbHelmet.setSelectedItem(null);
			cbArmor.setSelectedItem(null);
			cbBelt.setSelectedItem(null);
			cbBoots.setSelectedItem(null);
			cbShield.setSelectedItem(null);
			cbRing.setSelectedItem(null);
			cbWeapon.setSelectedItem(null);
			//textfield
			tfLevel.setEditable(false);
			//button
			btnGenerate.setText("Generate");
		} else if (buttonType.equals("Edit")) {
			btnGenerate.setText("Random Value");
		}
		//Load is done, change initial run to false, so combobox action will happen
		initialRun = false;
	}
	
	/**
	 * This panel is used to load the character name panel.
	 * 
	 * @param characterName It is used to store character name.
	 */
	public void LoadNamePanel(String characterName) 
	{
		tfCharacterName.setText(characterName);

	}
	
	
	/**
	 * Load the Activitiy Panel in the Character Create View page
	 * @param abilityScoreMetadata ability score object to publish into the ability score text fields
	 * @param abilityModifierMetadata ability modifier object to publish into the ability modifier text fields
	 * @param bonusMetadata
	 */
	public void loadAbilityPanel(AbilityScoreMetadata abilityScoreMetadata, AbilityModifierMetadata abilityModifierMetadata, BonusMetadata bonusMetadata) 
	{
		//set ability score

		ScoreBonusMetadata scoreBonusMetadata = new ScoreBonusMetadata();
		CharacterUtility characterUtility = new CharacterUtility();
		scoreBonusMetadata = characterUtility.parseScoreAndBonus(abilityScoreMetadata, bonusMetadata);
		tfStrScore.setText(String.valueOf(scoreBonusMetadata.getStrResult()));
		tfDexScore.setText(String.valueOf(scoreBonusMetadata.getDexResult()));
		tfIntScore.setText(String.valueOf(scoreBonusMetadata.getIntResult()));
		tfConScore.setText(String.valueOf(scoreBonusMetadata.getConResult()));
		tfWisScore.setText(String.valueOf(scoreBonusMetadata.getWisResult()));
		tfChaScore.setText(String.valueOf(scoreBonusMetadata.getChaResult()));
		
		//set ability modifier
		tfStrMod.setText(abilityModifierMetadata.getAbm_strength());
		tfDexMod.setText(abilityModifierMetadata.getAbm_dexterity());
		tfIntMod.setText(abilityModifierMetadata.getAbm_intelligence());
		tfConMod.setText(abilityModifierMetadata.getAbm_constitution());
		tfWisMod.setText(abilityModifierMetadata.getAbm_wisdom());
		tfChaMod.setText(abilityModifierMetadata.getAbm_charisma());
	}

	/**
	 * This is used to load the character characteristics.
	 * 
	 * @param level It is the character level.
	 * @param hitPoint It is the character hitpoint.
	 * @param armorClass It is the character armor class.
	 * @param attackBonus It is the attack bonus.
	 * @param damageBonus It is the damage bonus.
	 */
	private void loadCharacteristicPanel(String level, String hitPoint, String armorClass, String attackBonus, String damageBonus) 
	{
		tfLevel.setText(level);
		tfHitPoint.setText(hitPoint);
		tfArmorClass.setText(armorClass);
		tfAttackBonus.setText(attackBonus);
		tfDamageBonus.setText(damageBonus);
	}
	
	/**
	 * This method loads the wearables panel.
	 */
	private void loadWearablePanel() 
	{
		//load item list from itemModel

			ItemMetadata itemMetadata;
			Vector<ItemListMetaInterface> listEquipement = new Vector<>();

			Constants.ItemType itemType[] = Constants.ItemType.values();
			for (Constants.ItemType name : itemType) 
			{
				itemMetadata = itemMap.get(name.getItemId());
				
				if (itemMetadata!=null) 
				{
					listEquipement = itemMetadata.getItemListMetadata();
					ItemListMetadata emptyItem = new ItemListMetadata();
					emptyItem.setList_item_id("");
					emptyItem.setList_item_bonus("0");
					emptyItem.setList_item_name("None");
					listEquipement.add(emptyItem);
					switch (name.getItemId().toUpperCase()) {
					case "HELMET":
						for (ItemListMetaInterface item: listEquipement) {
							cbHelmet.addItem(item);
							if (mode == "edit" && characterMetadata.getCharacterWearableMetadata().getItm_helmet().equals(item.getList_item_id())) {
								cbHelmet.setSelectedItem(item);
							} else if (mode == "create" && item.getList_item_name().equals("None")) {
								cbHelmet.setSelectedItem(item);
							}
						}
						break;
					case "ARMOR":
						for (ItemListMetaInterface item: listEquipement) {
							cbArmor.addItem(item);
							if (mode == "edit" && characterMetadata.getCharacterWearableMetadata().getItm_armor().equals(item.getList_item_id())) {
								cbArmor.setSelectedItem(item);
							}else if (mode == "create" && item.getList_item_name().equals("None")) {
								cbArmor.setSelectedItem(item);
							}
						}
						break;
					case "RING":
						for (ItemListMetaInterface item: listEquipement) {
							cbRing.addItem(item);
							if (mode == "edit" && characterMetadata.getCharacterWearableMetadata().getItm_ring().equals(item.getList_item_id())) {
								cbRing.setSelectedItem(item);
							}else if (mode == "create" && item.getList_item_name().equals("None")) {
								cbRing.setSelectedItem(item);
							}
						}
						break;
					case "BOOTS":
						for (ItemListMetaInterface item: listEquipement) {
							cbBoots.addItem(item);
							if (mode == "edit" && characterMetadata.getCharacterWearableMetadata().getItm_boots().equals(item.getList_item_id())) {
								cbBoots.setSelectedItem(item);
							}else if (mode == "create" && item.getList_item_name().equals("None")) {
								cbBoots.setSelectedItem(item);
							}
						}
						break;
					case "BELT":
						for (ItemListMetaInterface item: listEquipement) {
							cbBelt.addItem(item);
							if (mode == "edit" && characterMetadata.getCharacterWearableMetadata().getItm_belt().equals(item.getList_item_id())) {
								cbBelt.setSelectedItem(item);
							}else if (mode == "create" && item.getList_item_name().equals("None")) {
								cbBelt.setSelectedItem(item);
							}
						}
						break;
					case "SHIELD":
						for (ItemListMetaInterface item: listEquipement) {
							cbShield.addItem(item);
							if (mode == "edit" && characterMetadata.getCharacterWearableMetadata().getItm_shield().equals(item.getList_item_id())) {
								cbShield.setSelectedItem(item);
							}else if (mode == "create" && item.getList_item_name().equals("None")) {
								cbShield.setSelectedItem(item);
							}
						}
						break;
					case "WEAPON":
						for (ItemListMetaInterface item: listEquipement) {
							cbWeapon.addItem(item);
							if (mode == "edit" && characterMetadata.getCharacterWearableMetadata().getItm_weapon().equals(item.getList_item_id())) {
								cbWeapon.setSelectedItem(item);
							}else if (mode == "create" && item.getList_item_name().equals("None")) {
								cbWeapon.setSelectedItem(item);
							}
						}
						break;
					}
				}

			}
		}

	/**
	 * This method loads the backpack table.
	 * 
	 * @param vector_backpackMetadata It contains list of items in the backpack.
	 */
	public void load_BackPackDetailsTable(Vector<BackpackMetadata> vector_backpackMetadata)
	{
		if(itemTableModel.getRowCount()>0)
		{
			for (int i = itemTableModel.getRowCount() - 1; i > -1; i--) 
			{
				itemTableModel.removeRow(i);
			}
		}
		
		for(int i=0; i < vector_backpackMetadata.size(); i++)
		{
			String temp_itemType = vector_backpackMetadata.get(i).getItemType();
			String temp_itemID = vector_backpackMetadata.get(i).getItemId();
			
			Vector<ItemListMetaInterface> vector_itemListMetadata = itemMap.get(temp_itemType).getItemListMetadata();
			
			for(int k = 0; k < vector_itemListMetadata.size(); k++)
			{
				if(vector_itemListMetadata.get(k).getList_item_id().equals(temp_itemID))
				{
					String temp_ItemName = vector_itemListMetadata.get(k).getList_item_name();
					
					itemTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
				}
			}
		}
	}

	/**
	 * It loads the wearable table.
	 * 
	 * @param characterWearableMetadata It contains the list of items in the character wearable list.
	 */
	public void load_WearableDetailsTable(CharacterWearableMetadata characterWearableMetadata)
	{
		if(itemInventoryTableModel.getRowCount()>0)
		{
			for (int i = itemInventoryTableModel.getRowCount() - 1; i > -1; i--) 
			{
				itemInventoryTableModel.removeRow(i);
			}
		}
		
		if(!characterWearableMetadata.getItm_helmet().equals(""))
		{
			String temp_itemType = "HELMET";
			String temp_itemID = characterWearableMetadata.getItm_helmet();
			Vector<ItemListMetaInterface> vector_itemList = itemMap.get("HELMET").getItemListMetadata();
			
			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();
					
					itemInventoryTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
					
					break;
				}
			}
		}
		
		if(!characterWearableMetadata.getItm_armor().equals(""))
		{
			String temp_itemType = "ARMOR";
			String temp_itemID = characterWearableMetadata.getItm_armor();
			Vector<ItemListMetaInterface> vector_itemList = itemMap.get("ARMOR").getItemListMetadata();
			
			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();
					
					itemInventoryTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
					
					break;

				}
			}
		}
		
		if(!characterWearableMetadata.getItm_ring().equals(""))
		{
			String temp_itemType = "RING";
			String temp_itemID = characterWearableMetadata.getItm_ring();
			Vector<ItemListMetaInterface> vector_itemList = itemMap.get("RING").getItemListMetadata();
			
			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();
					
					itemInventoryTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
					
					break;

				}
			}
		}
		
		if(!characterWearableMetadata.getItm_boots().equals(""))
		{
			String temp_itemType = "BOOTS";
			String temp_itemID = characterWearableMetadata.getItm_boots();
			Vector<ItemListMetaInterface> vector_itemList = itemMap.get("BOOTS").getItemListMetadata();
			
			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();
					
					itemInventoryTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
					
					break;

				}
			}
		}
		
		if(!characterWearableMetadata.getItm_belt().equals(""))
		{
			String temp_itemType = "BELT";
			String temp_itemID = characterWearableMetadata.getItm_belt();
			Vector<ItemListMetaInterface> vector_itemList = itemMap.get("BELT").getItemListMetadata();
			
			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();
					
					itemInventoryTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
					
					break;

				}
			}
		}
		
		if(!characterWearableMetadata.getItm_shield().equals(""))
		{
			String temp_itemType = "SHIELD";
			String temp_itemID = characterWearableMetadata.getItm_shield();
			Vector<ItemListMetaInterface> vector_itemList = itemMap.get("SHIELD").getItemListMetadata();
			
			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();
					
					itemInventoryTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
				}
			}
		}
		
		if(!characterWearableMetadata.getItm_weapon().equals(""))
		{
			String temp_itemType = "WEAPON";
			String temp_itemID = characterWearableMetadata.getItm_weapon();
			Vector<ItemListMetaInterface> vector_itemList = itemMap.get("WEAPON").getItemListMetadata();
			
			for(int i = 0; i<vector_itemList.size(); i++)
			{
				if(temp_itemID.equals(vector_itemList.get(i).getList_item_id()))
				{
					String temp_ItemName = vector_itemList.get(i).getList_item_name();
					
					itemInventoryTableModel.addRow(new Object[]{
							temp_itemID,
							temp_itemType,
							temp_ItemName
					});
					
					break;

				}
			}
		}
	}
	
	/**
	 * Perform the action for specific button
	 * @param e event generated on button click or Combobox changed.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//flag initial run when combobox got populated
		//we don't want to do anything in the initial run
		if (initialRun != true) 
		{
			if (e.getActionCommand() == "comboBoxChanged") 
			{
				HashMap<String, ItemListMetadata> itemListMetadatas = new HashMap<>();
				ItemListMetadata item = null;
				String itemName = null;
				
				if (cbHelmet == e.getSource()) {
					Object o = cbHelmet.getSelectedItem();
					item = (ItemListMetadata) o;
					itemListMetadatas.put(Constants.ItemType.Helmet.getItemId(), item);
					itemName = Constants.ItemType.Helmet.getItemId();
				}
				if (cbWeapon == e.getSource()) {
					Object o = cbWeapon.getSelectedItem();
					item = (ItemListMetadata) o;
					itemListMetadatas.put(Constants.ItemType.Weapon.getItemId(), item);
					itemName = Constants.ItemType.Weapon.getItemId();
				}
				if (cbBoots == e.getSource()) {
					Object o = cbBoots.getSelectedItem();
					item = (ItemListMetadata) o;
					itemListMetadatas.put(Constants.ItemType.Boots.getItemId(), item);
					itemName = Constants.ItemType.Boots.getItemId();
				}
				if (cbBelt == e.getSource()) {
					Object o = cbBelt.getSelectedItem();
					item = (ItemListMetadata) o;
					itemListMetadatas.put(Constants.ItemType.Belt.getItemId(), item);
					itemName = Constants.ItemType.Belt.getItemId();
				}
				if (cbShield == e.getSource()) {
					Object o = cbShield.getSelectedItem();
					item = (ItemListMetadata) o;
					itemListMetadatas.put(Constants.ItemType.Shield.getItemId(), item);
					itemName = Constants.ItemType.Shield.getItemId();
				}
				if (cbRing == e.getSource()) {
					Object o = cbRing.getSelectedItem();
					item = (ItemListMetadata) o;
					itemListMetadatas.put(Constants.ItemType.Ring.getItemId(), item);
					itemName = Constants.ItemType.Ring.getItemId();
				}
				if (cbArmor == e.getSource()) {
					Object o = cbArmor.getSelectedItem();
					item = (ItemListMetadata) o;
					itemListMetadatas.put(Constants.ItemType.Armor.getItemId(), item);
					itemName = Constants.ItemType.Armor.getItemId();
				}
				
				if(!(item.getList_item_id().equals("")&&item.getList_item_name().equals("None")))
				{
					controller.comboBoxAction(itemListMetadatas, item, itemName);
				}
			}
			
			if(e.getActionCommand()=="DOWN")
			{
				int size = 0;
				
				if(!(characterMetadata.getBackpackMetadata() == null))
				{
					size = characterMetadata.getBackpackMetadata().size();
				}
				
				if(tableInventoryItems.getSelectedRow() != -1 && size < 10)
				{
					Vector tableWearableModel = (Vector)itemInventoryTableModel.getDataVector().elementAt(tableInventoryItems.getSelectedRow());
					BackpackMetadata backpackMetadata = new BackpackMetadata();
					
					backpackMetadata.setItemId((String) tableWearableModel.get(0));
					backpackMetadata.setItemType((String) tableWearableModel.get(1));
					
					controller.actionMoveToBackPack(backpackMetadata);
				}
			}
			else if(e.getActionCommand()=="UP")
			{
				if(tableBackPackItems.getSelectedRow() != -1)
				{
					Vector tableBackpackModel = (Vector)itemTableModel.getDataVector().elementAt(tableBackPackItems.getSelectedRow());
					
					BackpackMetadata backpackMetadata = new BackpackMetadata();
					
					backpackMetadata.setItemId((String) tableBackpackModel.get(0));
					backpackMetadata.setItemType((String) tableBackpackModel.get(1));
					
					controller.actionMoveToWearables(backpackMetadata);
				}
			}
			else if(e.getActionCommand() == "Save")
			{
				String charName = tfCharacterName.getText();
				boolean response = controller.actionSave(characterMetadata, charName);
				if(response == true)
				{
					int res = JOptionPane.showOptionDialog(null, "Your Character has been saved successfully", "Confirmation", JOptionPane.DEFAULT_OPTION,
					        JOptionPane.INFORMATION_MESSAGE, null, null, null);
					if(res == JOptionPane.OK_OPTION)
					{
						controller.actionBackToMain();
					}
					System.out.println(res);
				}
				else
				{
					int res = JOptionPane.showOptionDialog(null, "Please press Generate button atleast once or Verify if the character name is not empty", "Confirmation", JOptionPane.DEFAULT_OPTION,
					        JOptionPane.INFORMATION_MESSAGE, null, null, null);
				}
			}
			else if(e.getActionCommand() == "Cancel")
			{
				controller.actionBackToMain();
			}
			
			else if(e.getActionCommand()== "Remove")
			{
				if(tableBackPackItems.getSelectedRow() != -1)
				{
					boolean isRemoved;
					Vector tableBackpackModel = (Vector)itemTableModel.getDataVector().elementAt(tableBackPackItems.getSelectedRow());
					
					BackpackMetadata backpackMetadata = new BackpackMetadata();
					
					backpackMetadata.setItemId((String) tableBackpackModel.get(0));
					backpackMetadata.setItemType((String) tableBackpackModel.get(1));
					
					isRemoved = controller.actionRemoveFromBackpack(backpackMetadata);
					if(isRemoved == false)
					{
						lblErrortext.setText("Item Not Removed from backpack!!");
						lblErrortext.setVisible(true);
						try {
							wait(5);
							lblErrortext.setVisible(false);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
					}
				}
			}

		}
		
		//buttons
		if(e.getActionCommand() == "Generate")
		{
            String fighterType = cbCharacterType.getSelectedItem().toString();
            controller.generate(fighterType);
		}
		else if (e.getActionCommand() == "Random Value")
		{
			if(currentFocus==null)
			{
				lblErrortext.setVisible(true);
				lblErrortext.setText("Select a Ability before generating any value!!!");
			}
			else
			{
				lblErrortext.setVisible(false);
				if(!currentFocus.equals("level"))
				{
					controller.randomValue(currentFocus);	
				}
				else
				{
					controller.getUpdatedHitPoints(tfLevel.getText());
				}
				
			}
		}
	}

	/**
	 * Get the name of the currently selected ability text field, this value will be use as a parameter in the randomValue()
	 * @param e event generated on focus gained.
	 */
	@Override
	public void focusGained(FocusEvent e) 
	{
		String componentName = e.getComponent().getName();
		switch (componentName.toUpperCase()){
		case "STR":
			currentFocus = componentName;
			log.info(currentFocus);
			break;
		case "DEX":
			currentFocus = componentName;
			log.info(currentFocus);
			break;
		case "INT":
			currentFocus = componentName;
			log.info(currentFocus);
			break;
		case "CON":
			currentFocus = componentName;
			log.info(currentFocus);
			break;
		case "WIS":
			currentFocus = componentName;
			log.info(currentFocus);
			break;
		case "CHA":
			currentFocus = componentName;
			log.info(currentFocus);
			break;
		case "LEVEL":
			currentFocus = componentName;
			log.info(currentFocus);
			break;
		}

	}

	/**
	 * Update method for the observer interface.
	 */
	@Override
	public void update(Observable o, Object arg) 
	{
		String level = ((CharacterMetadata)o).getCharacter_level();
		String hitPoint = ((CharacterMetadata)o).getHitPoints();
		String armorClass= characterMetadata.getArmorClass();
		String attackBonus = ((CharacterMetadata)o).getAttackBonus();
		String damageBonus = ((CharacterMetadata)o).getDamageBonus();
		AbilityScoreMetadata abilityScoreMetadata = ((CharacterMetadata)o).getAbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = ((CharacterMetadata)o).getAbilityModifierMetadata();
		BonusMetadata bonusMetadata = ((CharacterMetadata)o).getBonusMetadata();
		loadAbilityPanel(abilityScoreMetadata, abilityModifierMetadata, bonusMetadata);
		loadCharacteristicPanel(level,hitPoint,armorClass,attackBonus,damageBonus);
		
		if(characterMetadata.getBackpackMetadata() != null)
		{
			Vector<BackpackMetadata> vector_backpackMetadata = characterMetadata.getBackpackMetadata();
			load_BackPackDetailsTable(vector_backpackMetadata);
		}
		
		if(characterMetadata.getCharacterWearableMetadata() != null)
		{
			CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			load_WearableDetailsTable(characterWearableMetadata);
		}
	}

	/**
	 * Default method for item state change.
	 */
	@Override
	public void itemStateChanged(ItemEvent e) 
	{

	}

	/**
	 * Default method when focus is lost.
	 */
	@Override
	public void focusLost(FocusEvent e) 
	{
        controller.getUpdatedHitPoints(tfLevel.getText());
		
	}
}
