package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import Controller.ItemCreateViewController;
import Model.ItemModel;
import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import utilities.Constants;
import utilities.Constants.ItemType;
import utilities.EnchantmentType;

public class ItemCreateView extends JPanel implements ListSelectionListener, ActionListener,Observer
{
	/**
	 * JList for the Item type.
	 */
	private JList listItemType;
	
	/**
	 * JList for enchantment type list.
	 */
	private JList listEnchantmentType;
	
	private JList listAvailDecorators;
	/**
	 * JList for the enchantment bonus.
	 */
	private JList listEnchantmentBonus;

	/**
	 * Scroll panes for the items.
	 */
	private JScrollPane paneItems;
	
	/**
	 * Scrollpane for enchantment scree.
	 */
	private JScrollPane paneEnchantment;
	
	/**
	 * Scrollpane for enchantment bonus.
	 */
	private JScrollPane paneEnchantmentBonus;
	
	/**
	 * Scrollpane for item details.
	 */
	private JScrollPane itemDetailsScroller;
	
	/**
	 * Button for clearing JList.
	 */
	private JButton btnClear;
	
	/**
	 * Button for edit.
	 */
	private JButton btnEdit;

	/**
	 * Parent panel for items.
	 */
	private JPanel parentPanel;
	/**
	 * Panel for items screen.
	 */
	private JPanel panel;
	/**
	 * Panel for enchantment type.
	 */
	private JPanel panel_EnchantmentType;
	/**
	 * Panel for enchantment bonus.
	 */
	private JPanel panel_EnchantmentBonus;
	/**
	 * panel for Item type.
	 */
	private JPanel panel_ItemType;
	/**
	 * panel for items details.
	 */
	private JPanel panel_ItemDetails;
	
	private JPanel panel_Decorator;

	/**
	 * It is the enchantment bonus.
	 */
	private String[] enchantmentBonus = {"1","2","3","4","5"};
	
	/**
	 * It is the ItemId selected for editing purpose.
	 */
	private String editItemId;
	
	/**
	 * It is the Item type for the item selected for editing.
	 */
	private String editItemType;
	
	private String decoratedString = "0";

	/**
	 * It is an instance of enchantment enum.
	 */
	private EnchantmentType enchantmentType;

	/**
	 * It is the instance of Item view controller.
	 */
	private ItemCreateViewController controller;

	/**
	 * It is the collection of item types.
	 */
	private Vector<ItemMetadata> itemMetadatas;
	
	/**
	 * It is the collection of items of specific type.
	 */
	private Vector<ItemListMetaInterface> itemListMetadatas = null;

	/**
	 * It is the default table model for items.
	 */
	private DefaultTableModel itemTableModel;
	
	/**
	 * it is the instance of item model.
	 */
	private ItemModel itemModel;
	
	private String range="0";
	

	private DefaultListModel weaponEnchantment;
	private DefaultListModel weaponEnchantmentSelected;

	/**
	 * It is the header for item table.
	 */
	private String[] itemTableHeader = {"Item Id","Item Type","Item Name", "Item Enchantment Type", "Item Bonus"};

	/**
	 * It is the Default list model for ListItemModel.
	 */
	private DefaultListModel<String> listItemModel = new DefaultListModel<String>();
	
	/**
	 * It is the collection of item id and enchantment map.
	 */
	static public HashMap<String, String[]> itemEnchantmentMap;
	
	/**
	 * It is the collection of Enchantment id and enchantment type.
	 */
	public HashMap<String,EnchantmentType[]> itemEnchantment;
	
	/**
	 * Hashmap for item id and item map.
	 */
	private HashMap<String,ItemMetadata> itemMap;

	/**
	 * Text for storing item name.
	 */
	public JTextField txtItemname;
	
	/**
	 * Label for displaying error text.
	 */
	public JLabel lblErrorText;
	
	/**
	 * Table to display item details.
	 */
	private JTable tableItemDetails;
	private JTextField rangeValue;
	private JList listSelectedDecorator;
	private JScrollPane scrollPaneSelectedDecorator;
	
	/**
	 * It is the constructor for the ItemCreateView.
	 * 	
	 * @param controller It is the instance of the Items controller.
	 */
	public ItemCreateView(ItemCreateViewController controller)
	{
		this.controller = controller;
		init_();
	}


	/**
	 * It initializes the Item create view.
	 */
	private void init_() 
	{

		itemEnchantment = new HashMap<String,EnchantmentType[]>();

		itemEnchantment.put("Helmet", EnchantmentType.getEnchantmentTypeHelmet());
		itemEnchantment.put("Armor",  EnchantmentType.getEnchantmentTypeArmorShield());
		itemEnchantment.put("Shield", EnchantmentType.getEnchantmentTypeArmorShield());
		itemEnchantment.put("Ring", EnchantmentType.getEnchantmentTypeRing());
		itemEnchantment.put("Belt", EnchantmentType.getEnchantmentTypeBelt());
		itemEnchantment.put("Boots", EnchantmentType.getEnchantmentTypeBoots());
		itemEnchantment.put("Weapon", EnchantmentType.getEnchantmentTypeWeapon());

		parentPanel = this;
		setLayout(null);
		int ordinal = ItemType.valueOf("Ring").ordinal();
		panel_ItemType = new JPanel();
		panel_ItemType.setBorder(new TitledBorder(null, "Item Type", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_ItemType.setBounds(29, 67, 162, 129);
		add(panel_ItemType);
		panel_ItemType.setLayout(null);

		listItemType = new JList(this.getListModel());
		listItemType.setToolTipText("Item Type List");
		listItemType.setBounds(35, 53, 122, 121);
		listItemType.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listItemType.setVisibleRowCount(3);
		listItemType.setSelectedIndex(0);
		listItemType.setToolTipText("Item Type List");

		paneItems = new JScrollPane(listItemType);
		paneItems.setBounds(6, 22, 150, 100);
		panel_ItemType.add(paneItems);
		paneItems.setVisible(true);

		panel_EnchantmentType = new JPanel();
		panel_EnchantmentType.setBorder(new TitledBorder(null, "Enchantment Type", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_EnchantmentType.setBounds(221, 67, 162, 129);
		add(panel_EnchantmentType);
		panel_EnchantmentType.setLayout(null);

		listEnchantmentType = new JList(this.getEnchanmentModel());
		listEnchantmentType.setToolTipText("Enchantment Type List");
		listEnchantmentType.setBounds(200, 52, 122, 121);
		listEnchantmentType.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listEnchantmentType.setVisibleRowCount(3);
		listEnchantmentType.setSelectedIndex(0);
		listEnchantmentType.setToolTipText("Enchantment Type List");
		paneEnchantment = new JScrollPane(listEnchantmentType);
		paneEnchantment.setBounds(6, 22, 150, 100);
		panel_EnchantmentType.add(paneEnchantment);
		paneEnchantment.setVisible(true);

		JButton btnSave = new JButton("Save");
		btnSave.setBounds(83, 374, 115, 29);
		add(btnSave);

		panel_EnchantmentBonus = new JPanel();
		panel_EnchantmentBonus.setBounds(398, 67, 162, 129);
		add(panel_EnchantmentBonus);
		panel_EnchantmentBonus.setBorder(new TitledBorder(null, "Enchantment Bonus", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_EnchantmentBonus.setLayout(null);

		listEnchantmentBonus = new JList(this.getBonusModel());
		listEnchantmentBonus.setToolTipText("Enchantment Bonus");
		listEnchantmentBonus.setBounds(365, 52, 122, 121);
		listEnchantmentBonus.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listEnchantmentBonus.setVisibleRowCount(5);
		listEnchantmentBonus.setSelectedIndex(0);
		paneEnchantmentBonus = new JScrollPane(listEnchantmentBonus);
		paneEnchantmentBonus.setBounds(6, 22, 150, 100);
		panel_EnchantmentBonus.add(paneEnchantmentBonus);
		paneEnchantmentBonus.setVisible(true);


		listItemType.addListSelectionListener(this);
		listEnchantmentType.addListSelectionListener(this);
		listEnchantmentBonus.addListSelectionListener(this);
		btnSave.addActionListener(this);

		txtItemname = new JTextField();
		txtItemname.setToolTipText("Enter Item Name");
		txtItemname.setBounds(224, 16, 146, 26);
		add(txtItemname);
		txtItemname.setColumns(10);

		JLabel lblItemName = new JLabel("Enter Item Name");
		lblItemName.setBounds(54, 19, 137, 20);
		add(lblItemName);

		lblErrorText = new JLabel("Error Text");
		lblErrorText.setBounds(121, 199, 415, 20);
		lblErrorText.setVisible(false);
		add(lblErrorText);

		panel_ItemDetails = new JPanel();
		panel_ItemDetails.setBorder(new TitledBorder(null, "Item Details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_ItemDetails.setBounds(15, 427, 571, 143);
		add(panel_ItemDetails);
		panel_ItemDetails.setLayout(null);

		itemTableModel = new DefaultTableModel(0,0);
		itemTableModel.setColumnIdentifiers(itemTableHeader);

		tableItemDetails = new JTable(itemTableModel);

		//		tableItemDetails.setPreferredScrollableViewportSize(new Dimension(300,100));
		itemDetailsScroller = new JScrollPane(tableItemDetails);
		itemDetailsScroller.setBounds(6, 22, 550, 114);
		itemDetailsScroller.setViewportView(tableItemDetails);

		panel_ItemDetails.add(itemDetailsScroller);

		btnEdit = new JButton("Edit");
		btnEdit.setBounds(221, 586, 115, 29);
		add(btnEdit);
		load_ItemDetailsTable(itemTableModel);
		btnEdit.addActionListener(this);
		
		btnClear = new JButton("Clear");
		btnClear.setBounds(362, 374, 115, 29);
		add(btnClear);
		btnClear.addActionListener(this);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(221, 634, 115, 29);
		add(btnCancel);
		btnCancel.addActionListener(this);
		
		JLabel lblRange = new JLabel("Range");
		lblRange.setBounds(29, 259, 69, 20);
		add(lblRange);
		
		rangeValue = new JTextField();
		rangeValue.setBounds(83, 251, 146, 26);
		add(rangeValue);
		rangeValue.setColumns(10);
		
		weaponEnchantment = new DefaultListModel<>();
		weaponEnchantment.addElement("No Selection");
		weaponEnchantment.addElement("Freezing");
		weaponEnchantment.addElement("Burning");
		weaponEnchantment.addElement("Slaying");
		weaponEnchantment.addElement("Frightening");
		weaponEnchantment.addElement("Pacifying");
		
		weaponEnchantmentSelected = new DefaultListModel<>();
		
		panel_Decorator = new JPanel();
		panel_Decorator.setBorder(new TitledBorder(null, "Item Decorator: Select -> Stacked", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_Decorator.setBounds(244, 235, 316, 129);
		add(panel_Decorator);
		panel_Decorator.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 22, 115, 100);
		panel_Decorator.add(scrollPane);
		
		listAvailDecorators = new JList(weaponEnchantment);
		scrollPane.setViewportView(listAvailDecorators);
		listAvailDecorators.addListSelectionListener(this);
		
		listSelectedDecorator = new JList(weaponEnchantmentSelected);
		listSelectedDecorator.setBounds(194, 50, 113, 98);
		
		scrollPaneSelectedDecorator = new JScrollPane();
		scrollPaneSelectedDecorator.setBounds(194, 22, 115, 100);
		scrollPaneSelectedDecorator.setViewportView(listSelectedDecorator);
		panel_Decorator.add(scrollPaneSelectedDecorator);
		panel_Decorator.setVisible(false);

	}
	
	/**
	 * This method generates the default list model to be used for JList for enchantment type.
	 * 
	 * @return It returns the enchantment type corresponding to item type.
	 */
	public DefaultListModel<EnchantmentType> getEnchanmentModel()
	{
		DefaultListModel<EnchantmentType> filledEnchanmentModel = new DefaultListModel<EnchantmentType>();
		EnchantmentType[] enchantementType = EnchantmentType.getEnchantmentTypeHelmet();
		for(int i=0;i<enchantementType.length;i++)
		{

			filledEnchanmentModel.addElement(enchantementType[i]);
		}

		return filledEnchanmentModel;
	}

	/**
	 * This method returns the default list mode for the Item bonus list.
	 * 
	 * @return it returns a default list model for bonus.
	 */
	public DefaultListModel<String> getBonusModel()
	{
		DefaultListModel<String> filledBonusModel = new DefaultListModel<String>();
		String[] bonusType = enchantmentBonus;
		for(int i=0;i<bonusType.length;i++)
		{

			filledBonusModel.addElement(bonusType[i]);
		}

		return filledBonusModel;
	}

	/**
	 * This method is for Item Type list.
	 * 
	 * @return it returns the default list model for the item types.
	 */
	public DefaultListModel<ItemType> getListModel()
	{
		DefaultListModel<ItemType> filledListModel = new DefaultListModel<ItemType>();
		ItemType[] itemType = Constants.ItemType.values();
		for(int i=0;i<itemType.length;i++)
		{

			filledListModel.addElement(itemType[i]);
		}

		return filledListModel;

	}

	/**
	 * This method is used to set the enchantment model based on item type selected.
	 * 
	 * @param listModel It is the list model for the enchantment list.
	 */
	public void setListEnchantModel(DefaultListModel<EnchantmentType> listModel)
	{
		listEnchantmentType.removeListSelectionListener(this);
		listEnchantmentType.setModel(listModel);
		listEnchantmentType.setSelectedIndex(0);
		listEnchantmentType.addListSelectionListener(this);
	}

	/**
	 * This method generates the List model for the list.
	 * 
	 * @param enchantmentType it is the array of enchantment types.
	 * @return a filled list model.
	 */
	public DefaultListModel<EnchantmentType> generateListModel(EnchantmentType[] enchantmentType)
	{
		DefaultListModel<EnchantmentType> filledListModel = new DefaultListModel<EnchantmentType>();
		EnchantmentType[] enchantmentTypeData = enchantmentType;
		for(int i=0;i<enchantmentTypeData.length;i++)
		{

			filledListModel.addElement(enchantmentTypeData[i]);
		}

		return filledListModel;
	}

	/**
	 * This method fills the Item details table.
	 * 
	 * @param itemTableModel It is the table model containing table model 
	 */
	public void load_ItemDetailsTable(DefaultTableModel itemTableModel)
	{

		if(itemTableModel.getRowCount()>0)
		{
			for (int i = itemTableModel.getRowCount() - 1; i > -1; i--) 
			{
				itemTableModel.removeRow(i);
			}
		}

		itemModel = controller.itemModel;
		itemMap = itemModel.getItemMap();

		itemMetadatas = new Vector<ItemMetadata>();

		ItemMetadata itemMetadata;
		ItemListMetaInterface itemListMetadata;
		for(String key: itemMap.keySet())
		{
			itemMetadata = itemMap.get(key);
			itemListMetadatas = (((ItemMetadata)itemMap.get(key)).getItemListMetadata());

			for(int i=0; i<itemListMetadatas.size();i++)
			{
				itemListMetadata = itemListMetadatas.get(i);
				ItemType itemTypeFound = ItemType.find(itemMetadata.getItem_type());
				EnchantmentType enchantmentTypeFound = EnchantmentType.find(itemListMetadata.getList_item_enchanment());

				itemTableModel.addRow(new Object[]{
						itemListMetadata.getList_item_id(),
						itemTypeFound.getItemName(),
						itemListMetadata.getList_item_name(),
						enchantmentTypeFound.getEnchantmentName(),
						itemListMetadata.getList_item_bonus()
				});
			}
		}
	}

	/**
	 * It is the Default listener method for JList
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(!e.getValueIsAdjusting())
		{
			if(e.getSource() == listEnchantmentType)
			{

				if(listEnchantmentType.getSelectedIndex()==-1)
				{
				}
				else
				{
					EnchantmentType selectedItem = (EnchantmentType) listEnchantmentType.getModel().getElementAt(listEnchantmentType.getSelectedIndex()); 
				}

			}
			else if(e.getSource() == listItemType)
			{
				ItemType selectedItem = (ItemType) listItemType.getModel().getElementAt(listItemType.getSelectedIndex()); 

				if(itemEnchantment.containsKey(selectedItem.getItemName()))
				{
					setListEnchantModel(generateListModel(itemEnchantment.get(selectedItem.getItemName())));
				}
				

				if(selectedItem.getItemName().equals("Weapon"))
				{
					panel_Decorator.setVisible(true);
				}
				else
				{
					panel_Decorator.setVisible(false);
				}

			}
			else if (e.getSource() == listEnchantmentBonus)
			{
			}
			else if(e.getSource() == listAvailDecorators)
			{
				JList weaponEnchantmentAvailableSel = (JList) e.getSource();
				ListModel listWeaponEnchantModel = weaponEnchantmentAvailableSel.getModel();
				if(weaponEnchantmentAvailableSel.getSelectedIndex()!= -1)
				{
				String weaponEnchant = (String) listWeaponEnchantModel.getElementAt(weaponEnchantmentAvailableSel.getSelectedIndex());
				
				if(weaponEnchant.equals("No Selection"))
				{
				}
				else if(weaponEnchant.equals("Freezing"))
				{
					decoratedString += ","+Constants.Freezing;
					weaponEnchantmentSelected.addElement("Freezing");
				}
				else if(weaponEnchant.equals("Burning"))
				{
					decoratedString += ","+Constants.Burning;
					weaponEnchantmentSelected.addElement("Burning");

				}
				else if(weaponEnchant.equals("Slaying"))
				{
					decoratedString += ","+Constants.Slaying;
					weaponEnchantmentSelected.addElement("Slaying");

				}
				else if(weaponEnchant.equals("Frightening"))
				{
					decoratedString += ","+Constants.Frightening;
					weaponEnchantmentSelected.addElement("Frightening");

				}
				else if(weaponEnchant.equals("Pacifying"))
				{
					decoratedString += ","+Constants.Pacifying;
					weaponEnchantmentSelected.addElement("Pacifying");

				}
				
				System.out.println(decoratedString);
				}
			}
		}		
	}
	
	/**
	 * It is the default listener for the Action listener.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {


		if(e.getActionCommand()=="Edit")
		{

			if(tableItemDetails.getSelectedRow()==-1)
			{
				lblErrorText.setText("Select a row before editing an Item!!!!");
				lblErrorText.setVisible(true);
			}
			else
			{
				lblErrorText.setVisible(false);
				Vector tableSelectedModel = (Vector)itemTableModel.getDataVector().elementAt(tableItemDetails.getSelectedRow());
				editItemId = tableSelectedModel.get(0).toString();
				editItemType = tableSelectedModel.get(1).toString();
				ItemListMetaInterface editItemListMetadata = itemModel.getItemListByID(editItemType.toUpperCase(), editItemId);
				String decoratedWeapon = editItemListMetadata.getDecoraterWeapon();
				String[] decoratedWeaponEnchantment = decoratedWeapon.split(",");
				
				for(int j=0;j<decoratedWeaponEnchantment.length;j++)
				{
					if(decoratedWeaponEnchantment[j].equals("1"))
					{
						weaponEnchantmentSelected.addElement("Freezing");
					}
					else if(decoratedWeaponEnchantment[j].equals("2"))
					{
						weaponEnchantmentSelected.addElement("Burning");
					}
					else if(decoratedWeaponEnchantment[j].equals("3"))
					{
						weaponEnchantmentSelected.addElement("Slaying");
					}
					else if(decoratedWeaponEnchantment[j].equals("4"))
					{
						weaponEnchantmentSelected.addElement("Frightening");
					}
					else if(decoratedWeaponEnchantment[j].equals("5"))
					{
						weaponEnchantmentSelected.addElement("Pacifying");
					}
					else
					{
						
					}
				}
				
				String itemName = tableSelectedModel.get(2).toString();
				String itemEnchantmentType = tableSelectedModel.get(3).toString();
				int itemBonus = Integer.valueOf(tableSelectedModel.get(4).toString());
				ItemType itemTypeFound = ItemType.find(editItemType);
				EnchantmentType enchantmentTypeFound = EnchantmentType.find(itemEnchantmentType);
				System.out.println(itemTypeFound.getItemId());
				int selectedItemrowNum = ItemType.valueOf(itemTypeFound.getItemName()).ordinal();
				System.out.println(selectedItemrowNum);
				txtItemname.setText(itemName);
				listItemType.setSelectedIndex(selectedItemrowNum);
				int listSelIndex = 0;
				ListModel enchantmentModel = listEnchantmentType.getModel();
				for(int i=0;i<enchantmentModel.getSize();i++)
				{
					if(enchantmentModel.getElementAt(i).toString().equals(enchantmentTypeFound.getEnchantmentName()))
					{
						listSelIndex = i;
						break;
					}
				}
				listEnchantmentType.setSelectedIndex(listSelIndex);
				listEnchantmentBonus.setSelectedIndex(itemBonus-1);
				
				
			}

		}
		else if(e.getActionCommand()=="Clear")
		{
			txtItemname.setText("");
			listItemType.setSelectedIndex(0);
			listEnchantmentType.setSelectedIndex(0);
			listEnchantmentBonus.setSelectedIndex(0);
			lblErrorText.setVisible(false);
			
			editItemId = null;
		}
		else if(e.getActionCommand()=="Cancel")
		{
			controller.actionCancel();
		}
		else
		{
			
			ItemType newItemType = ((ItemType) listItemType.getModel().getElementAt(listItemType.getSelectedIndex()));
			String itemType = newItemType.getItemId();
			String itemName = txtItemname.getText();
			String enchantmentType = ((EnchantmentType)listEnchantmentType.getModel().getElementAt(listEnchantmentType.getSelectedIndex())).getEnchantmentId();
			String enchantmentBonus = (String)listEnchantmentBonus.getModel().getElementAt(listEnchantmentBonus.getSelectedIndex());
			
			if(itemName.equals(""))
			{
				lblErrorText.setText("Please Enter the correct Item Name!!!");
				lblErrorText.setVisible(true);
			}
			else
			{
				lblErrorText.setVisible(false);
				if(editItemId==null)
				{
					if(itemType.equals("WEAPON"))
					{
						System.out.println("Creating Decorated Armor");
						System.out.println("Decorated String " + decoratedString);

						range = rangeValue.getText();
						controller.actionSaveItem(itemType, itemName, enchantmentType, enchantmentBonus,decoratedString,range);
					}
					else
					{
						decoratedString="0";
						range="0";
						System.out.println("Decorated String " + decoratedString);
						controller.actionSaveItem(itemType, itemName, enchantmentType, enchantmentBonus,decoratedString,range);
					}
				}
				else
				{	
					if(editItemType==newItemType.getItemName())
					{
					controller.actionEditItem(editItemId, itemType, itemName, enchantmentType, enchantmentBonus,decoratedString,range);
					editItemId = null;
					}
					else
					{
						lblErrorText.setText("Item Type Can not be changed during editing!!");
						lblErrorText.setVisible(true);
					}
				}
			}
		}

	}

	
	/**
	 * Default method for observer class.
	 */
	@Override
	public void update(Observable o, Object arg) 
	{
		load_ItemDetailsTable(itemTableModel);
		txtItemname.setText("");
		listItemType.setSelectedIndex(0);
		listEnchantmentType.setSelectedIndex(0);
		listEnchantmentBonus.setSelectedIndex(0);

	}
}
