package Model;

import java.util.HashMap;
import java.util.Observable;
import javax.swing.DefaultListModel;
import metadatas.CampaignMetadata;
import parsers.CampaignParser;
import utilities.Constants;
import utilities.FileHandlerUtility;
import metadatas.ListMetadata;

/**
 * CampaignModel represents the state of all the Campaigns stored in file. 
 * It preserves the state of these Campaigns inside a HashMap <code>campaignMap</code> 
 * and uses this map to display the Campaigns and edit them. All the reading 
 * and writing operation from the file are done from this class.
 * 
 * @author Tarandeep Singh
 * @version 1.0.0
 * @since 07 February 2017
 * @see CampaignParser
 * @see FileHandlerUtility
 * @see CampaignMetadata
 * @see Observable
 */

public class CampaignModel extends Observable
{
	/**
	 * A HashMap with <key, value> as <code><String, CampaignMetadata></code> that
	 * contains all the Campaigns inside this.
	 */
	HashMap<String, CampaignMetadata> campaignMap = null;
	
	/**
	 * Reference variable to <code>FileHandlerUtility</code> for file handling 
	 * operations like reading or writing.
	 */
	FileHandlerUtility fileUtility;
	
	/**
	 * Constant to store the name of the Campaign file.
	 */
	private String itemFile = Constants.CAMPAIGN_FILE_NAME;
	
	/**
	 * Constructor to fetch the list of Campaigns from the file using 
	 * class <code>CampaignModel</code>. It gets all the data and sets 
	 * it to HashMap <code>campaignMap</code>.
	 */
	public CampaignModel() 
	{
		CampaignParser parser = new CampaignParser();
		campaignMap = parser.parseCampaign();
	}
	
	/**
	 * Returns the state of this class <code>CampaignModel</code> wrapped
	 * in the form of HashMap.
	 * @return Returns the HashMap <code>campaignMap</code>
	 */
	public HashMap<String, CampaignMetadata> getCampaignMap()
	{
		return campaignMap;
	}
	
	/**
	 * Used to generate <code>DefaultListModel</code> for rendering the data inside
	 * <code>ListPanel</code> based upon the requested Campaign.
	 * @return <code>DefaultListModel</code> for the given campaign
	 */	
	public DefaultListModel<ListMetadata> getListMetadata()
	{
		DefaultListModel<ListMetadata> listModel = new DefaultListModel<ListMetadata>();
		
		try
		{
			for (String key : campaignMap.keySet()) 
			{
				ListMetadata listMetadata = new ListMetadata();
				CampaignMetadata item_model = campaignMap.get(key);
				
				listMetadata.setItem_ID(item_model.getCampaign_id());
				listMetadata.setItem_Name(item_model.getCampaign_name());
				
				listModel.addElement(listMetadata);
			}
		}
		catch(Exception e)
		{
			e.getMessage();
		}
		
		return listModel;
	}
	
	/**
	 * Used to convert the HashMap <code>campaignMap</code> back to String to 
	 * write it back into the Campaign File.
	 * @return Converts the <code>campaignMap</code> back to String
	 */
	public String convertToString()
	{
		String Return="[\n\t";
		
		for(String key : campaignMap.keySet())
		{
			CampaignMetadata item_model = campaignMap.get(key);
			Return = Return + item_model.convertToString(item_model);
			Return=  Return + ",\n";
		}
		
		Return = Return.substring(0, Return.length()-2);
		Return = Return + "\n]";
		
		return Return;
	}

	/**
	 * Writes the converted string back to the Campaign file.
	 * @see FileHandlerUtility
	 */
	public void writeItem()
	{
		boolean response = false;
		
		fileUtility = new FileHandlerUtility();
		String jsonData = convertToString();
		response = fileUtility.writeAllData(jsonData, itemFile);
		
		if(response)
		{
			setChanged();
			notifyObservers(this);
		}
	}
	
	/**
	 * Saves the edited or created Campaign to Campaign file.
	 * @param campaignMetadata Data related to campaign
	 * @see FileHandlerUtility
	 */
	public boolean actionSaveCampaign(CampaignMetadata campaignMetadata)
	{
		if(campaignMetadata.getCampaign_name()==null)
		{
			return false;
		}
		else
		{
			if(!(campaignMetadata.getVector_campaignMap() == null))
			{
				if(campaignMetadata.getCampaign_id() == null)
				{
					int size = campaignMap.size() + 1;
					campaignMetadata.setCampaign_id(String.valueOf(size));
					
					campaignMap.put(campaignMetadata.getCampaign_id(), campaignMetadata);
					writeItem();
				}
				else
				{
					campaignMap.put(campaignMetadata.getCampaign_id(), campaignMetadata);
					writeItem();
				}
				return true;
			}
			return false;
		}	
	}
}
