package Model;

import java.util.HashMap;
import java.util.Observable;
import java.util.Vector;

import javax.swing.DefaultListModel;

import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import metadatas.ListMetadata;
import parsers.ItemParser;
import utilities.Constants;
import utilities.FileHandlerUtility;

/**
 * ItemModel represents the state of all the Items stored in file. 
 * It preserves the state of these items inside a HashMap <code>itemMap</code> 
 * and uses this map to display the items and edit them. All the reading 
 * and writing operation from the file are done from this class.
 * 
 * @author Gurvinder Singh
 * @author Karan Deep Singh
 * 
 * @see ItemParser
 * @see FileHandlerUtility
 * @see ItemMetadata
 * @see Observable
 */
public class ItemModel extends Observable
{
	/**
	 * A HashMap with <key, value> as <code><String, ItemMetadata></code> that
	 * contains all the Maps inside this.
	 */
	private HashMap<String, ItemMetadata> itemMap = null;
	
	/**
	 * Reference variable to <code>FileHandlerUtility</code> for file handling 
	 * operations like reading or writing.
	 */
	private FileHandlerUtility fileUtility = null;
	
	/**
	 * Constant to store the name of the Item file.
	 */
	private String itemFile = Constants.ITEM_FILE_NAME;

	/**
	 * Constructor to fetch the list of items from the file using 
	 * class <code>ItemModel</code>. It gets all the data and sets 
	 * it to HashMap <code>itemMap</code>.
	 */
	public ItemModel()
	{
		ItemParser parser = new ItemParser();
		itemMap = parser.parseItem();
	}
	
	/**
	 * Returns the state of this class <code>ItemModel</code> wrapped
	 * in the form of HashMap.
	 * 
	 * @return Returns the HashMap <code>itemMap</code>
	 */
	public HashMap<String, ItemMetadata> getItemMap()
	{
		return itemMap;
	}
	
	/**
	 * Used to generate <code>DefaultListModel</code> for rendering the data inside
	 * <code>ListPanel</code> based upon the requested Item Type.
	 * @param itemType Type of the Item to be rendered
	 * @return <code>DefaultListModel</code> for the given item
	 */
	public DefaultListModel<ListMetadata> getListMetadata(String itemType)
	{
		DefaultListModel<ListMetadata> listModel = new DefaultListModel<ListMetadata>();
		
		try
		{
			for (String key : itemMap.keySet()) 
			{
				if(key.equals(itemType))
				{
					Vector<ItemListMetaInterface> vector_itemListMetadata = itemMap.get(itemType).getItemListMetadata();
					
					for(int i = 0; i<vector_itemListMetadata.size(); i++)
					{
						ListMetadata listMetadata = new ListMetadata();
						ItemListMetaInterface temp_item = vector_itemListMetadata.get(i);
						listMetadata.setItem_ID(temp_item.getList_item_id());
						listMetadata.setItem_Name(temp_item.getList_item_name());
						
						listModel.addElement(listMetadata);
					}

				}
			}
		}
		catch(Exception e)
		{
			e.getMessage();
		}
		return listModel;
	}
	
	/**
	 * Used to convert the HashMap <code>itemMap</code> back to String to 
	 * write it back into the Item File.
	 * 
	 * @return Converts the <code>itemMap</code> back to String
	 */
	public String convertToString()
	{	
		String Return="[\n\t";
		
		for(String key : itemMap.keySet())
		{
			ItemMetadata item_metadata = itemMap.get(key);
			Return = Return + item_metadata.convertToString(item_metadata);
			Return=  Return + ",\n";
		}
		
		Return = Return.substring(0, Return.length()-2);
		Return = Return + "\n]";
		
		return Return;		
	}
	
	/**
	 * Writes the converted string back to the Item file.
	 * 
	 * @see FileHandlerUtility
	 */
	public void writeItem()
	{
		boolean response = false;
		
		fileUtility = new FileHandlerUtility();
		String jsonData = convertToString();
		response = fileUtility.writeAllData(jsonData, itemFile);
		
		if(response)
		{
			setChanged();
			notifyObservers(this);
		}
	}

	public void actionAddItem(String itemType, ItemListMetaInterface itemListMetadata)
	{
		ItemMetadata tempItemMetadata;
		
		if(itemMap.containsKey(itemType))
		{
			tempItemMetadata = itemMap.get(itemType);
			Vector<ItemListMetaInterface> tempItemListMetadata = tempItemMetadata.getItemListMetadata();
			int size = tempItemListMetadata.size();

			String newItemID = String.valueOf(size + 1);
			
			itemListMetadata.setList_item_id(newItemID);
			tempItemListMetadata.add(itemListMetadata);
		}
		else
		{
			tempItemMetadata = new ItemMetadata();
			Vector<ItemListMetaInterface> tempItemListMetadata = new Vector<>();
			int size = 0;

			String newItemID = String.valueOf(size + 1);
			
			itemListMetadata.setList_item_id(newItemID);
			tempItemListMetadata.add(itemListMetadata);
			
			tempItemMetadata.setItem_type(itemType);
			tempItemMetadata.setItemListMetadata(tempItemListMetadata);
			
			itemMap.put(itemType, tempItemMetadata);
		}
		
		writeItem();
	}
	
	public void actionEditItem(String itemType, ItemListMetadata edit_ItemListMetadata)
	{
		ItemMetadata tempItemMetadata;
		ItemListMetaInterface old_ItemListMetadata;
		if(itemMap.containsKey(itemType))
		{
			tempItemMetadata = itemMap.get(itemType);
			Vector<ItemListMetaInterface> tempItemListMetadata = tempItemMetadata.getItemListMetadata();
			
			old_ItemListMetadata = selectEditItemMetadata(tempItemListMetadata,edit_ItemListMetadata.getList_item_id());
			int size = tempItemListMetadata.size();

			old_ItemListMetadata.setList_item_name(edit_ItemListMetadata.getList_item_name());
			old_ItemListMetadata.setList_item_enchanment(edit_ItemListMetadata.getList_item_enchanment());
			old_ItemListMetadata.setList_item_bonus(edit_ItemListMetadata.getList_item_bonus());
		}
		
		writeItem();
	}
	
	public ItemListMetaInterface selectEditItemMetadata(Vector<ItemListMetaInterface> tempItemListMetadata, String editItemId)
	{
		ItemListMetaInterface editItemMetadata = null;
		int size = tempItemListMetadata.size();
		
		for(int i=0;i<size;i++)
		{	
			if(tempItemListMetadata.get(i).getList_item_id().equals(editItemId))
			{
				editItemMetadata = tempItemListMetadata.get(i);
			}
			
		}
		return editItemMetadata;
	}
	
	public ItemListMetaInterface getItemListByID(String itemType, String itemID)
	{
		ItemMetadata itemMetadata = itemMap.get(itemType);
		Vector<ItemListMetaInterface> vector_itemListMetadata = itemMetadata.getItemListMetadata();

		for(int i = 0; i < vector_itemListMetadata.size(); i++)
		{
			if(vector_itemListMetadata.get(i).getList_item_id().equals(itemID))
			{
				return vector_itemListMetadata.get(i);
			}
		}
		
		return null;
	}
	
}
