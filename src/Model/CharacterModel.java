package Model;

import java.util.HashMap;
import javax.swing.DefaultListModel;
import metadatas.AbilityScoreMetadata;
import metadatas.CharacterMetadata;
import parsers.CharacterParser;
import utilities.Constants;
import utilities.FileHandlerUtility;
import metadatas.ListMetadata;

/**
 * This class is used to get and set the character properties. It is a model class for characters.
 * It preserves the state of these Characters inside a HashMap <code>characterMap</code> 
 * and uses this map to display the Characters and edit them. All the reading 
 * and writing operation from the file are done from this class.
 * 
 * @author Gurvinder Singh
 * @author Karan Deep Singh
 * @see FileHandlerUtility
 * @see CharacterMetadata
 * 
 */
public class CharacterModel 
{
	/**
	 * A HashMap with <key, value> as <code><String, CharacterMetadata></code> that
	 * contains all the Characters inside this.
	 */
	HashMap<String, CharacterMetadata> characterMap = null;

	/**
	 * Reference variable to <code>FileHandlerUtility</code> for file handling 
	 * operations like reading or writing.
	 */
	private FileHandlerUtility fileUtility = null;

	/**
	 * Constant to store the name of the Campaign file.
	 */
	private String characterFile = Constants.CHARACTER_FILE_NAME;

	/**
	 * Constructor to fetch the list of Characters from the file using 
	 * class <code>CharacterModel</code>. It gets all the data and sets 
	 * it to HashMap <code>characterMap</code>.
	 */
	public CharacterModel()
	{
		CharacterParser characterParser = new CharacterParser();
		characterMap = characterParser.parseCharacter();
	}
	
	/**
	 * Returns the state of this class <code>CharacterModel</code> wrapped
	 * in the form of HashMap.
	 * @return Returns the HashMap <code>characterMap</code>
	 */
	public HashMap<String,CharacterMetadata> getCharacterMap()
	{
		return characterMap;
	}

	/**
	 * Used to generate <code>DefaultListModel</code> for rendering the data inside
	 * <code>ListPanel</code> based upon the requested Character.
	 * @return <code>DefaultListModel</code> for the given character
	 */	
	public DefaultListModel<ListMetadata> getListMetadata()
	{
		DefaultListModel<ListMetadata> listModel = new DefaultListModel<ListMetadata>();

		try
		{
			for (String key : characterMap.keySet()) 
			{
				ListMetadata listMetadata = new ListMetadata();
				CharacterMetadata character_metadata = characterMap.get(key);

				listMetadata.setItem_ID(character_metadata.getCharacter_id());
				listMetadata.setItem_Name(character_metadata.getCharacter_name());

				listModel.addElement(listMetadata);
			}
		}
		catch(Exception e)
		{
			e.getMessage();
		}
		return listModel;
	}

	/**
	 * Converts the entire Hash Map to String for write operation
	 * @return String format of HashMap
	 */
	public String convertToString()
	{

		CharacterMetadata characterMetadata = new CharacterMetadata();
		String Return="[\n\t";

		for(String key : characterMap.keySet())
		{
			CharacterMetadata character_model = characterMap.get(key);
			Return = Return + characterMetadata.convertToString(character_model);
			Return=  Return + ",\n";
		}

		Return = Return.substring(0, Return.length()-2);
		Return = Return + "\n]";

		return Return;
	}

	/**
	 * Writes the converted string back to the Character file.
	 * @see FileHandlerUtility
	 */
	public void writeCharacter()
	{
		boolean response = false;

		fileUtility = new FileHandlerUtility();
		String jsonData = convertToString();
		response = fileUtility.writeAllData(jsonData, characterFile);

	}
	
	public CharacterMetadata getCharacterMetadataByName(String characterName)
	{
		CharacterMetadata characterMetadata;
		
		for(String key: characterMap.keySet())
		{
			characterMetadata = characterMap.get(key);
			if(characterMetadata.getCharacter_name().equals(characterName))
			{
				return characterMetadata;
			}
		}
		
		return null;
	}

	/**
	 * Saves the edited or created Character to Character file.
	 * @param characterMetadata Data related to character
	 * @see FileHandlerUtility
	 */
	public boolean actionSaveCharacter(CharacterMetadata characterMetadata)
	{
		if(!characterMetadata.getCharacter_name().equals(""))
		{
			if(checkIfAbilityNotZero(characterMetadata))
			{
				if(characterMetadata.getCharacter_id() == null)
				{
					int size = characterMap.size() + 1;
					characterMetadata.setCharacter_id(String.valueOf(size));

					characterMap.put(String.valueOf(size), characterMetadata);
				}
				else
				{
					characterMap.put(characterMetadata.getCharacter_id(), characterMetadata);
				}

				writeCharacter();
				return true;
			}
				
		}
		return false;
	}
	
	/**
	 * For Validation purpose to check if the Ability Score for the character is not zero.
	 * @param characterMetadata Data related to character
	 * @return boolean value indicating the value of the ability score
	 * 
	 */
	public boolean checkIfAbilityNotZero(CharacterMetadata characterMetadata)
	{
		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
		if(abilityScoreMetadata.getAb_charisma().equals("0")&& abilityScoreMetadata.getAb_constitution().equals("0")&&
				abilityScoreMetadata.getAb_dexterity().equals("0") && abilityScoreMetadata.getAb_intelligence().equals("0") &&
				abilityScoreMetadata.getAb_strength().equals("0") && abilityScoreMetadata.getAb_wisdom().equals("0"))
		{
			return false;
		}
		
	return true;	
	}
	
	public void incrementLevel(String characterID)
	{
		CharacterMetadata characterMetadata = characterMap.get(characterID);
		
		int curr_level = Integer.parseInt(characterMetadata.getCharacter_level());
		curr_level++;
		
		characterMetadata.setCharacter_level(String.valueOf(curr_level));
		characterMetadata.setCharacteristic();
		
		writeCharacter();
		
	}
}
