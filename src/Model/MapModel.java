package Model;

import java.util.HashMap;
import java.util.Vector;
import java.util.logging.FileHandler;

import javax.swing.DefaultListModel;

import parsers.CampaignParser;
//import metadatas.CampaignMapsMetadata;
import parsers.MapParser;
import utilities.Constants;
import utilities.FileHandlerUtility;
import metadatas.CampaignMetadata;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import metadatas.ListMetadata;
import metadatas.MapMetadata;

/**
 * Class to Handle the Model for Maps.
 * @author Vijay Karan Singh
 * @see MapParser
 */
public class MapModel {
	
	HashMap<String, MapMetadata> mapList = null;
	FileHandlerUtility fileHandlerUtility;
	String mapfile = Constants.MAP_FILE_NAME;
	
	/**
	 * Constructor to initialize the Map Model Object
	 */
	public MapModel() 
	{
		MapParser parser = new MapParser();
		mapList = parser.parseMap();
	}
	
	/**
	 * Get the List of Maps
	 * @return HashMap consisting of List of Maps
	 */
	public HashMap<String, MapMetadata> getParser()
	{
		return mapList;
	}
	
	/**
	 * Get the Metadata for the List of Maps
	 * @return The Object Containing the Map List Metadata
	 */
	public DefaultListModel<ListMetadata> getListMetadata()
	{
		DefaultListModel<ListMetadata> listModel = new DefaultListModel<ListMetadata>();
		
		try
		{
			for (String key : mapList.keySet()) 
			{
				ListMetadata listMetadata = new ListMetadata();
				MapMetadata item_model = mapList.get(key);
				
				listMetadata.setItem_ID(item_model.getMap_id());
				listMetadata.setItem_Name(item_model.getMap_name());
				
				listModel.addElement(listMetadata);
			}
		}
		catch(Exception e)
		{
			e.getMessage();
		}
		
		return listModel;
	}

	/**
	 * Get the Specic Map from List of Maps
	 * @param mapID id of the map to get
	 * @return Metadata of Selected Map
	 */
	public MapMetadata getMapByID(String mapID)
	{
		return mapList.get(mapID);
	}
	
	/**
	 * Writes the converted string back to the Maps file.
	 * 
	 * @see FileHandlerUtility
	 */
	public void writeMap()
	{
		boolean response = false;
		
		fileHandlerUtility = new FileHandlerUtility();
		String jsonData = convertToString();
		response = fileHandlerUtility.writeAllData(jsonData, mapfile);
	}
	
	/**
	 * Used to convert the HashMap <code>mapList</code> back to String to 
	 * write it back into the Item File.
	 * 
	 * @return Converts the <code>mapList</code> back to String
	 */
	public String convertToString()
	{	
		String Return="[\n\t";
		
		for(String key : mapList.keySet())
		{
			MapMetadata map_metadata = mapList.get(key);
			Return = Return + map_metadata.convertToString(map_metadata);
			Return=  Return + ",\n";
		}
		
		Return = Return.substring(0, Return.length()-2);
		Return = Return + "\n]";
		
		return Return;		
	}
	
	
	/**
	 * Prepares the Map Metadata along with map id to call the writeMap method.
	 * @param mapMetadata Map metadata to store.
	 */
	public void actionAddMap(MapMetadata mapMetadata)
	{
		if(mapMetadata.getMap_id() == null)
		{
			String map_id = String.valueOf(mapList.size() + 1);
			mapMetadata.setMap_id(map_id);
			
			mapList.put(map_id, mapMetadata);
		}
		else if(mapList.containsKey(mapMetadata.getMap_id()))
		{
			MapMetadata currentMapMetadata = mapList.get(mapMetadata.getMap_id());
			
			currentMapMetadata.setMap_layout(mapMetadata.getMap_layout());
			currentMapMetadata.setMap_name(mapMetadata.getMap_name());
			currentMapMetadata.setMap_size(mapMetadata.getMap_size());
		}
		
		writeMap();
	}
}
