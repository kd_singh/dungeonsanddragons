package Model;

import java.util.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import Controller.CharacterCreateViewController;

/**	 
 * @author Han Heng
 */
public class DiceModel 
{
	public static final Logger log = LogManager.getLogger(CharacterCreateViewController.class);
	private Random ran;
	 
	public DiceModel(){
	}

    /**
     * get random value based number of roll and dice face, will never return 0 as a random value
     * @param time number of time that we roll the dice
     * @param face number of face of the dice
     * @return the sum of each random dice roll
     */
	 public int roll1d10(int time, int face, int con)
	 {
		 int min = 1;
		 int max =face+1;
		 ran =  new Random();
		 int totalRandomValue = 0;
		 List<Integer> ranList = new ArrayList<Integer>();
		 for(int i=1; i<=time; i++)
		 {
			 int hitpoint = ran.nextInt(max - min) + min;
			 //random hitpoint plus constitution at each level
			 int hitpointPlusCon = hitpoint + con;
             log.info("Level: " + i + " Hitpoint= " + hitpoint + ", con modifier: " + con + " = " + hitpointPlusCon);
             totalRandomValue += hitpointPlusCon;
             log.info("Level: " + i + ", hitpoint = " + totalRandomValue);
		 }
		 return totalRandomValue;
	}

	public int rolld20(){
	 	int min = 1;
	 	int max = 20 +1;
		ran = new Random();
		int result = ran.nextInt(max - min) + min;
	 	return result;
	}

    /**
     * Get random value for character create. return the max 3 random value
     * no matter how many time the "time" variable is passed into this function
     * @param time number of times the dice is rolled
     * @param face number of face of the dice
     * @return the sum of each random dice roll
     */
	public int roll4d6(int time, int face)
	{
		 int min = 1;
		 int max =face+1;
		 ran =  new Random();
		 int totalRandomValue = 0;
	
		 List<Integer> ranList = new ArrayList<Integer>();
	
		 for(int i=0; i<time; i++){
			 int value = ran.nextInt(max - min) + min;
			 ranList.add(value);
		 }
	  
		 Collections.sort(ranList, Collections.reverseOrder());
	  
		 for(int i=0; i<3; i++){
			 totalRandomValue+=ranList.get(i);
		 }
		 
		 return totalRandomValue;
	}
}