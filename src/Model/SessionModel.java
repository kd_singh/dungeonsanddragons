package Model;

import java.util.HashMap;
import java.util.Observable;
import javax.swing.DefaultListModel;
import metadatas.CampaignMetadata;
import parsers.CampaignParser;
import parsers.SessionParser;
import utilities.Constants;
import utilities.FileHandlerUtility;
import metadatas.ListMetadata;
import metadatas.SessionMetadata;

/**
 * SessionModel represents the state of all the Saved Sessions stored in file. 
 * It preserves the state of these Sessions inside a HashMap <code>sessionMap</code> 
 * and uses this map to display the Sessions. All the reading 
 * and writing operation from the file are done from this class.
 * 
 * @author Tarandeep Singh
 * @version 1.0.0
 * @since 11 April 2017
 * @see SessionParser
 * @see FileHandlerUtility
 * @see CampaignMetadata
 * @see Observable
 */

public class SessionModel extends Observable {

	/**
	 * A HashMap with <key, value> as <code><String, SessionMetadata></code> that
	 * contains all the Sessions inside this.
	 */
	HashMap<String, SessionMetadata> sessionMap = null;
	
	/**
	 * Reference variable to <code>FileHandlerUtility</code> for file handling 
	 * operations like reading or writing.
	 */
	FileHandlerUtility fileUtility;
	
	/**
	 * Constant to store the name of the Campaign file.
	 */
	private String itemFile = Constants.SESSION_FILE_NAME;
	
	/**
	 * Constructor to fetch the list of SESSIONS from the file using 
	 * class <code>SessionModel</code>. It gets all the data and sets 
	 * it to HashMap <code>sessionMap</code>.
	 */
	public SessionModel() 
	{
		SessionParser parser = new SessionParser();
		sessionMap = parser.parseSession();
	}
	
	/**
	 * Returns the state of this class <code>SessionModel</code> wrapped
	 * in the form of HashMap.
	 * @return Returns the HashMap <code>sessionMap</code>
	 */
	public HashMap<String, SessionMetadata> getSessionMap()
	{
		return sessionMap;
	}
	
	/**
	 * Used to generate <code>DefaultListModel</code> for rendering the data inside
	 * <code>ListPanel</code> based upon the requested Session.
	 * @return <code>DefaultListModel</code> for the given session
	 */	
	public DefaultListModel<ListMetadata> getListMetadata()
	{
		DefaultListModel<ListMetadata> listModel = new DefaultListModel<ListMetadata>();
		
		try
		{
			for (String key : sessionMap.keySet()) 
			{
				ListMetadata listMetadata = new ListMetadata();
				SessionMetadata item_model = sessionMap.get(key);
				
				listMetadata.setItem_ID(item_model.getSession_id());
				listMetadata.setItem_Name(item_model.getSession_name());
				
				listModel.addElement(listMetadata);
			}
		}
		catch(Exception e)
		{
			e.getMessage();
		}
		
		return listModel;
	}
	
	/**
	 * Used to convert the HashMap <code>sessionMap</code> back to String to 
	 * write it back into the Session File.
	 * @return Converts the <code>sessionMap</code> back to String
	 */
	public String convertToString()
	{
		String Return="[\n\t";
		
		for(String key : sessionMap.keySet())
		{
			SessionMetadata session_metadata = sessionMap.get(key);
			Return = Return + session_metadata.convertToString(session_metadata);
			Return=  Return + ",\n";
		}
		
		Return = Return.substring(0, Return.length()-2);
		Return = Return + "\n]";
		
		return Return;
	}
	
	/**
	 * Writes the converted string back to the Session file.
	 * @see FileHandlerUtility
	 */
	public void writeItem()
	{
		boolean response = false;
		
		fileUtility = new FileHandlerUtility();
		String jsonData = convertToString();
		response = fileUtility.writeAllData(jsonData, itemFile);
		
		if(response)
		{
			setChanged();
			notifyObservers(this);
		}
		}
	
	/**
	 * Saves the  Session to Session file.
	 * @param sessionMetadata Data related to session
	 * @see FileHandlerUtility
	 */
	public boolean actionSaveSession(SessionMetadata sessionMetadata)
	{
		if(sessionMetadata.getSession_id() == null)
		{
			int size = sessionMap.size() + 1;
			sessionMetadata.setSession_id(String.valueOf(size));
			
			sessionMap.put(sessionMetadata.getSession_id(), sessionMetadata);
			writeItem();
			return true;
		}
		else
		{
			sessionMap.put(sessionMetadata.getSession_id(), sessionMetadata);
			writeItem();
			return true;
		}
	}
}
