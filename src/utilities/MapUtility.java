package utilities;

import java.util.ArrayList;

public class MapUtility 
{
	public ArrayList<String> getRange(int x, int y,int range, int gridSize)
	{
		ArrayList<String> attackRange = new ArrayList<>();
		moveDown(x,y,range,attackRange,gridSize);
		moveUp(x,y,range,attackRange,gridSize);
		moveLeft(x,y,range,attackRange,gridSize);
		moveRight(x,y,range,attackRange,gridSize);
		moveLeftDiagonalUp(x,y,range,attackRange,gridSize);
		moveRightDiagonalUp(x,y,range,attackRange,gridSize);
		moveLeftDiagonalDown(x,y,range,attackRange,gridSize);
		moveRightDiagonalDown(x,y,range,attackRange,gridSize);
		System.out.println(":::::::::::::" + attackRange.size());
		return attackRange;
	}
	
	public void moveRightDiagonalDown(int x,int y,int range, ArrayList<String> attackRange,int gridSize)
	{
		int attackRangeDelimitXPos = x + range;
		x++;
		y++;
		while(x<=attackRangeDelimitXPos)
		{
			if(x<gridSize)
			{
				attackRange.add(x+","+y);
			}
			x++;
			y++;
		}
	}
	
	public void moveLeftDiagonalDown(int x,int y,int range, ArrayList<String> attackRange,int gridSize)
	{
		int attackRangeDelimitXPos = x + range;
		x++;
		y--;
		while(x<=attackRangeDelimitXPos)
		{
			if(x<gridSize)
			{
				attackRange.add(x+","+y);
			}
			x++;
			y--;
		}
	}
	
	public void moveRightDiagonalUp(int x,int y,int range, ArrayList<String> attackRange,int gridSize)
	{
		int attackRangeDelimitXPos = x - range;
		x--;
		y++;
		while(x>=attackRangeDelimitXPos)
		{
			if(x>=0)
			{
				attackRange.add(x+","+y);
			}
			x--;
			y++;
		}
	}
	
	public void moveLeftDiagonalUp(int x,int y,int range, ArrayList<String> attackRange,int gridSize)
	{
		int attackRangeDelimitXPos = x - range;
		x--;
		y--;
		while(x>=attackRangeDelimitXPos)
		{
			if(y>=0)
			{
				attackRange.add(x+","+y);
			}
			x--;
			y--;
		}
	}
	
	public void moveRight(int x,int y,int range, ArrayList<String> attackRange,int gridSize)
	{
		int attackRangeDelimit = y + range;
		y++;
		while(y<=attackRangeDelimit)
		{
			if(y<gridSize)
			{
				attackRange.add(x+","+y);
			}
			y++;
		}
	}
	
	public void moveLeft(int x,int y,int range, ArrayList<String> attackRange,int gridSize)
	{
		int attackRangeDelimit = y - range;
		y--;
		while(y>=attackRangeDelimit)
		{
			if(y>=0)
			{
				attackRange.add(x+","+y);
			}
			y--;
		}
	}
	
	public void moveUp(int x,int y,int range, ArrayList<String> attackRange,int gridSize)
	{
		int attackRangeDelimit = x - range;
		x--;
		while(x>=attackRangeDelimit)
		{
			if(x>=0)
			{
				attackRange.add(x+","+y);
			}
			x--;
		}
	}
	
	public void moveDown(int x,int y,int range, ArrayList<String> attackRange,int gridSize)
	{
		int attackRangeDelimit = x + range;
		x++;
		while(x<=attackRangeDelimit)
		{
			if(x<gridSize)
			{
				attackRange.add(x+","+y);
			}
			x++;
		}
	}

	public static void main(String args[])
	{
		MapUtility mapUtility = new MapUtility();
		mapUtility.getRange(5, 3, 4, 8);
	}
}
