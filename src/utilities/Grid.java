package utilities;

import java.awt.Color;
import java.awt.GridLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JPanel;

import Model.CharacterModel;
import View.GamePlayView;
import metadatas.CharacterMetadata;
import metadatas.CharacterTypeMetadata;
import metadatas.GridCellMetadata;
import metadatas.MapMetadata;

/**
 * Grid Class extends JPanel to form the Panel consisting of cells of a map Grid.
 * Also this class handles the GridCell matrix data and model.
 * 
 * @author Karan Deep Singh
 * @see JPanel
 */
public class Grid extends JPanel implements PropertyChangeListener{
	
	GridCell gridCell_matrix[][];
	MapMetadata mapMetadata;
	CharacterModel characterModel;
	public HashMap<String, CharacterMetadata> mapCharacterList;
	
	Vector<CharacterTypeMetadata> vector_characterType;
	int gridSize;

	/**
	 * Constructor that initializes the Grid for Map View
	 * 
	 * @param gridSize size of the Grid
	 */
	public Grid(int gridSize)
	{
		this.gridSize = gridSize;
		mapMetadata = new MapMetadata();
		init_(this);
	}
	
	/**
	 * Constructor that renders the Grid for Map View to open in Edit Mode
	 * 
	 * @param mapMetadata size of the Grid
	 */
	public Grid(MapMetadata mapMetadata, CharacterModel characterModel, HashMap<String, CharacterMetadata> mapCharacterList,
			Vector<CharacterTypeMetadata> vector_characterType, String mode)
	{
		this.characterModel = characterModel;
		this.mapMetadata = mapMetadata;
		this.mapCharacterList = mapCharacterList;
		this.vector_characterType = vector_characterType;
		
		gridSize = Integer.parseInt(mapMetadata.getMap_size());
		
		fillGrid(this, mode);
	}
	
	/**
	 * This method initializes Grid object with GridCell Buttons
	 * 
	 * @param gridPanel Grid Object
	 */
	public void init_(Grid gridPanel)
	{
		gridCell_matrix = new GridCell[gridSize][gridSize];
		gridPanel.setLayout(new GridLayout(gridSize, gridSize));
		
		for(int i =0; i < gridSize; i++)
		{
			for(int j = 0; j < gridSize; j++)
			{
				gridCell_matrix[i][j]=new GridCell(i, j, "", "B");
				gridCell_matrix[i][j].addPropertyChangeListener(this);
				gridPanel.add(gridCell_matrix[i][j]);
			}
		}
	}
	
	/**
	 * Updates the size of the grid on change of the size
	 * @param gridSize Size of the Grid
	 */
	public void setNewSize(int gridSize)
	{
		this.gridSize = gridSize;
		this.removeAll();

		gridCell_matrix = null;
		init_(this);
	}
	
	
	public GridCell getGridCell(int x, int y)
	{
		return gridCell_matrix[x][y];
	}
	
	
	/**
	 * Gets the data from the Map and fills the <code>gridCell_matrix</code> when loaded
	 * 
	 * @param gridPanel
	 */
	public void fillGrid(Grid gridPanel, String mode)
	{
		int gridSize = Integer.parseInt(mapMetadata.getMap_size());
		
		Vector<GridCellMetadata> vector_GridMetadata = mapMetadata.getMap_layout();
		
		gridCell_matrix = new GridCell[gridSize][gridSize];
		gridPanel.setLayout(new GridLayout(gridSize, gridSize));
		
		
		for(int i =0; i < gridSize; i++)
		{
			for(int j = 0; j < gridSize; j++)
			{
				int pos = ((i*gridSize)+j);
				GridCellMetadata gridCellMetadata = vector_GridMetadata.get(pos);
				
				gridCell_matrix[gridCellMetadata.xPosition][gridCellMetadata.yPosition]=new GridCell(gridCellMetadata.getxPosition(), 
						gridCellMetadata.getyPosition(), gridCellMetadata.getValue(), gridCellMetadata.getType());
				gridCell_matrix[gridCellMetadata.xPosition][gridCellMetadata.yPosition].addPropertyChangeListener(this);
				gridPanel.add(gridCell_matrix[gridCellMetadata.xPosition][gridCellMetadata.yPosition]);
				
				if(mode.equals("PLAY") && (gridCellMetadata.type.equals("MONSTER") || gridCellMetadata.type.equals("FRIEND")))
				{
					String char_id = gridCellMetadata.value;
					String key = gridCellMetadata.xPosition + "" + gridCellMetadata.yPosition;
					
					mapCharacterList.put(key, characterModel.getCharacterMap().get(char_id));

					CharacterTypeMetadata characterTypeMetadata = new CharacterTypeMetadata();
					
					characterTypeMetadata.setType(gridCellMetadata.type);
					characterTypeMetadata.setCharacterMetadata(characterModel.getCharacterMap().get(char_id));
					
					vector_characterType.add(characterTypeMetadata);
				}
				
				if(mode.equals("LOADSESSION") && (gridCellMetadata.type.equals("MONSTER") || gridCellMetadata.type.equals("FRIEND")))
				{
					String char_id = gridCellMetadata.value;
					String key = gridCellMetadata.xPosition + "" + gridCellMetadata.yPosition;
					
					mapCharacterList.put(key, characterModel.getCharacterMap().get(char_id));

					CharacterTypeMetadata characterTypeMetadata = new CharacterTypeMetadata();
					
					characterTypeMetadata.setType(gridCellMetadata.type);
					characterTypeMetadata.setCharacterMetadata(characterModel.getCharacterMap().get(char_id));
					
					vector_characterType.add(characterTypeMetadata);
				}
				
				
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public MapMetadata getMapMetadata()
	{
		Vector<GridCellMetadata> gridCellMetadatas = new Vector<GridCellMetadata>();
		
		for(int i =0; i < gridSize; i++)
		{
			for(int j = 0; j < gridSize; j++)
			{
				GridCellMetadata tempMetadata = new GridCellMetadata();
				
				System.out.println(gridCell_matrix[i][j].getType());
				
				tempMetadata.setType(gridCell_matrix[i][j].getType());
				tempMetadata.setValue(gridCell_matrix[i][j].getValue());
				tempMetadata.setxPosition(gridCell_matrix[i][j].getxPosition());
				tempMetadata.setyPosition(gridCell_matrix[i][j].getyPosition());
						
				int pos = (i*gridSize) + j;

				gridCellMetadatas.add(pos, tempMetadata);
			}
		}
		
		mapMetadata.setMap_layout(gridCellMetadatas);
		return mapMetadata;
	}
	
	/**
	 * Updates the currently selected cell value
	 * 
	 * @param xPosition X-Index of the Selected Cell
	 * @param yPosition Y-Index of the Selected Cell
	 * @param value Value of the Selected Cell
	 */
	public void update_GridCellValue(int xPosition, int yPosition, String value)
	{
		gridCell_matrix[xPosition][yPosition].setValue(value);
		gridCell_matrix[xPosition][yPosition].setText(value);

	}
	
	/**
	 * Updates the currently selected cell type
	 * 
	 * @param xPosition X-Index of the Selected Cell
	 * @param yPosition Y-Index of the Selected Cell
	 * @param type Value of the Selected Cell
	 */
	public void update_GridCellType(int xPosition, int yPosition, String type)
	{
		gridCell_matrix[xPosition][yPosition].setType(type);
		gridCell_matrix[xPosition][yPosition].setBackground(Color.decode(Constants.colors.get(type)));
	}
	
	/**
	 * Get the Grid Cell Type 
	 * 
	 * @param xPosition X-Index of the Selected Cell
	 * @param yPosition Y-Index of the Selected Cell
	 * @return Type of the cell selected
	 */
	public String get_GridCellType(int xPosition, int yPosition)
	{
		return gridCell_matrix[xPosition][yPosition].getType();
	}
	
	
	/**
	 * Get the Grid Cell Value 
	 * 
	 * @param xPosition X-Index of the Selected Cell
	 * @param yPosition Y-Index of the Selected Cell
	 * @return Value of the selected cell
	 */
	public String get_GridCellValue(int xPosition, int yPosition)
	{
		return gridCell_matrix[xPosition][yPosition].getValue();
	}
	
	/**
	 * Get the Grid Size saved for the Map
	 * 
	 * @return Size of the map grid
	 */
	public int get_GridSize(){
		return this.gridSize;
	}
	
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		
		firePropertyChange("GridCellClick", evt.getOldValue(), evt.getNewValue());
		
	}
	
	public void displayCell(int x, int y){
		System.out.println("Position " + x + "," + y + " - " + gridCell_matrix[x][y].getType() + " " + 
							gridCell_matrix[x][y].getValue());
	}	
}
