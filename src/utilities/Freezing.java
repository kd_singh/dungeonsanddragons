package utilities;

import java.util.HashMap;

import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadataDecorator;

public class Freezing extends ItemListMetadataDecorator 
{
	
	String list_item_id;
	String list_item_name;
	String list_item_enchanment;
	String list_item_bonus;
	HashMap<String, String> weaponEnchantmentMap;


	public Freezing(ItemListMetaInterface decoratedItemListMetadata) 
	{
		super(decoratedItemListMetadata);
	}
	
	public void setWeaponEnchantmentType(String value)
	{
		String key = String.valueOf(super.getWeaponEnchantmentType().size()+1);
		super.getWeaponEnchantmentType().put(key, value);
	}
	
	public HashMap<String, String> getWeaponEnchantmentType()
	{
//		String key = String.valueOf(super.getWeaponEnchantmentType().size()+1);
//		super.getWeaponEnchantmentType().put(key, Constants.Freezing);
		return super.getWeaponEnchantmentType();
	}
	
	public String getList_item_id() 
	{
		return super.getList_item_id();
	}


	public void setList_item_id(String list_item_id) 
	{
		super.setList_item_id(list_item_id);
	}


	public String getList_item_name()
	{
		return super.getList_item_name();
	}


	public void setList_item_name(String list_item_name) 
	{
		super.setList_item_name(list_item_name);
	}


	public String getList_item_enchanment() 
	{
		return super.getList_item_enchanment();
	}


	public void setList_item_enchanment(String list_item_enchanment) 
	{
		super.setList_item_enchanment(list_item_enchanment);
	}


	public String getList_item_bonus() 
	{
		return super.getList_item_bonus();
	}


	public void setList_item_bonus(String list_item_bonus) 
	{
		super.setList_item_bonus(list_item_bonus);
	}
	public void setRange(String range)
	{
		super.setRange(range);
	}

	public void setDecoraterWeapon(String decoraterWeapon)
	{
		super.setDecoraterWeapon(decoraterWeapon);
	}

	public String getRange() 
	{
		return super.getRange();
	}

	public String getDecoraterWeapon() 
	{
		return super.getDecoraterWeapon();
	}

}
