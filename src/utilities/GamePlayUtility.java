package utilities;

import Model.DiceModel;
import metadatas.CharacterMetadata;
import metadatas.CharacterTypeMetadata;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by han on 2017-04-06.
 */
public class GamePlayUtility {

    private static Logger log = LogManager.getLogger(CharacterUtility.class);
    DiceModel dice;

    public GamePlayUtility() {
        dice = new DiceModel();
    }


    public HashMap<Integer, CharacterTypeMetadata> turnGenerator(Vector<CharacterTypeMetadata> characterTypeMetadataVector)
    {
    	int []arr = new int[characterTypeMetadataVector.size()];
    	HashMap<Integer, CharacterTypeMetadata> characterTurnMap = new HashMap<>();
        Map<Integer, CharacterTypeMetadata> characterTurn = new TreeMap<>(new MyComparator());
        
        int ctr = 0;
        for (CharacterTypeMetadata item: characterTypeMetadataVector)
        {
        	CharacterMetadata eachCharacter = item.getCharacterMetadata();
            int initiative = getInitiative(eachCharacter);

//            while (isExist(initiative, characterTurn))
            while (characterTurn.containsKey(initiative))
            {
                initiative = getInitiative(eachCharacter);
            }
            characterTurn.put(initiative, item);
            
            arr[ctr] = initiative;
            ctr++;

        }

        Arrays.sort(arr);
        
        for(int i=arr.length;i>0;i--)
        {
        	characterTurnMap.put((arr.length-i)+1, characterTurn.get(arr[i-1]));
        }
//        Map<Integer, CharacterMetadata> sortedCharacterTurn = new TreeMap<>(characterTurn);

//        int orderValue = 0;
//        for (Map.Entry<Integer, CharacterMetadata> entry: characterTurn.entrySet()) {
//            entry.s
//        }

        //think about removing the character when the character is dead


        return characterTurnMap;
    }


    class MyComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer first, Integer second) {

            return second.compareTo(first);
        }
    }

    public boolean isExist(int key, Map<Integer, CharacterMetadata> characterTurn){
        boolean existent = false;

        if (characterTurn.get(key) != null)
            existent = true;
        else
            existent = false;
        return existent;
    }

    public int getInitiative(CharacterMetadata eachCharacter) {
        int diceRoll = dice.rolld20();
        int dexMod = Integer.parseInt(eachCharacter.getAbilityModifierMetadata().getAbm_dexterity());
        int initiative = dexMod + diceRoll;
        return initiative;
    }


}
