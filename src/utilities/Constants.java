package utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * This class defines the constants that will be used across the whole project 
 */

public class Constants {

	public static final String PATH = "res//characters";
	public static final String CHARACTER_FILE_NAME = "CHR_LIST.txt";
	public static final String CAMPAIGN_FILE_NAME = "CAMP_LIST.txt";
	public static final String ITEM_FILE_NAME = "ITEM_LIST.txt";
	public static final String MAP_FILE_NAME = "MAP_LIST.txt";
	public static final String SESSION_FILE_NAME = "SESSION_LIST.txt";

	public static final String Freezing = "1";
	public static final String Burning = "2";
	public static final String Slaying = "3";
	public static final String Frightening = "4";
	public static final String Pacifying = "5";



	///////////////////////////// COLORS //////////////////////////////////
	public static final String GRASS_COLOR = "#7DCEA0";
	public static final String WALL_COLOR = "#854442";
	public static final String ENTRY_COLOR = "#77aaff";
	public static final String EXIT_COLOR = "#6bb120";
	public static final String MONSTER_COLOR = "#000000";
	public static final String FRIEND_COLOR = "#ffffff";
	public static final String CHEST_COLOR = "#a7102f";
	public static final String KEY_COLOR = "#ffed50";	
	public static final String WEAPON_COLOR = "#c8902f";
	public static final String ARMOUR_COLOR = "#f3ebb8";	
	public static final String HELMET_COLOR = "#930038";
	public static final String SHIELD_COLOR = "#540079";	
	public static final String RING_COLOR = "#003893";
	public static final String BELT_COLOR = "#ff5400";
	public static final String BOOTS_COLOR = "#8a8787";
	public static final String PLAYER_COLOR = "#3498DB";
	public static final String PLAYER_HAS_KEY_COLOR = "#FF6666";

	public static final Map<String, String> colors; 
	static
	{ 
		colors = new HashMap<String, String>() {{ 
			put("B", GRASS_COLOR);
			put("ENTRY", ENTRY_COLOR);
			put("EXIT", EXIT_COLOR);
			put("WALL", WALL_COLOR);
			put("MONSTER", MONSTER_COLOR);
			put("FRIEND", FRIEND_COLOR);
			put("CHEST", CHEST_COLOR);
			put("KEY", KEY_COLOR);
			put("WEAPON", WEAPON_COLOR);
			put("ARMOUR", ARMOUR_COLOR);
			put("HELMET", HELMET_COLOR);
			put("SHIELD", SHIELD_COLOR);
			put("RING", RING_COLOR);
			put("BELT", BELT_COLOR);
			put("BOOTS", BOOTS_COLOR);
			put("", GRASS_COLOR);
			put("PLAYER", PLAYER_COLOR);
			put("PLAYER&KEY", PLAYER_HAS_KEY_COLOR);
			put(null, GRASS_COLOR);
		}}; 
	}



	///////////////////////////ITEM ENUM//////////////////////////////////
	private static String[] equipementTypes = {"HELMET", "ARMOR", "BELT", "SHIELD", "RING", "BOOTS"};
	private static String[] abilityType = {"STRENGTH", "DEXTERITY", "INTELLIGENCE", "CONSTITUTION", "CHARISMA", "WISDOM"};

	public static enum ItemType 
	{

//		Helmet("HELMET","Helmet"),Armor("ARMOR","Armor"),Ring("RING","Ring"),Boots("BOOTS","Boots"),Belt("BELT","Belt"),Sword("WEAPON","Weapon"),
		Helmet("HELMET","Helmet"),Armor("ARMOR","Armor"),Ring("RING","Ring"),Boots("BOOTS","Boots"),Belt("BELT","Belt"),Weapon("WEAPON","Weapon"),
		Shield("SHIELD","Shield");

		private final String itemId;
		private final String itemName;
		private final List<String> values = new ArrayList<String>();;

		private ItemType(String itemId,String itemName){
			this.itemId = itemId;
			this.itemName = itemName;
			this.values.add(itemId);
			this.values.add(itemName);
		}

		public String getItemId()
		{
			return itemId;
		}
		
		public String getItemName()
		{
			return itemName;
		}

		public List<String> getValues() 
		{
			return values;
		}

		public static ItemType find(String name) 
		{
			for (ItemType item : ItemType.values()) 
			{
				if (item.getValues().contains(name)) 
				{
					return item;
				}
			}
			return null;
		}

	}
}
