package utilities;

/**
 * Created by han on 2017-04-06.
 */
public enum CharacterType {
    Player("PLAYER", "PLAYER"),
    Monster("MONSTER", "MONSTER"),
    Friendly("FRIENDLY", "FRIENDLY");


    private final String characterId;
    private final String characterName;

    CharacterType(String characterId, String characterName ) {
        this.characterId = characterId;
        this.characterName = characterName;
    }
}
