package utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

/*
 * This class is used as a single source for handling all file related operations
 */

public class FileHandlerUtility
{
	
	private File folder;
	
	/**
	 * This method is used to get the all files in the specified folder
	 * 
	 * @param pathName
	 * @return File List returns the list of files in any folder
	 */
	public File[] getListOfFiles(String pathName){
		folder = new File(pathName);
		return folder.listFiles();
	}
	/**
	 * This method reads all the data from any file passed as parameter.
	 * 
	 * @param fileName
	 * @return response it contains all the file data as a single string
	 * @throws IOException
	 */
	public String readAllData(String fileName) throws IOException 
	{
		StringBuilder response = null;
		
		try
		{
			String currentLine;
			response = new StringBuilder();
			FileReader fileReader = new FileReader(new File(Constants.PATH+"//"+fileName));
			BufferedReader file = new BufferedReader(fileReader);
			
			while((currentLine = file.readLine()) != null)
			{
				response = response.append(currentLine);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return response.toString();
		
	}

	public boolean writeAllData(String jsonData,String fileName)
	{
		try 
		{
			FileWriter fileWriter = new FileWriter(new File(Constants.PATH+"//"+fileName).getAbsoluteFile());
			BufferedWriter dataWriter = new BufferedWriter(fileWriter);
			dataWriter.write(jsonData);
			dataWriter.close();
			
		} 
		catch (IOException e) 
		{
			return false;
		}
		
		return true;
	}





}
