package utilities;

import metadatas.AbilityModifierMetadata;
import metadatas.AbilityScoreMetadata;
import metadatas.BonusMetadata;

import java.util.List;

/**
 * this is the builder who know how to build a tank fighter's ability score
 * ability score priority based on constitution > dexterity > strength > intelligence > charisma > wisdom
 * Created by han on 2017-03-12.
 */
public class TankBuilder extends CharacterBuilder {
    AbilityScoreMetadata abilityScoreMetadata ;
    AbilityModifierMetadata abilityModifierMetadata ;
    BonusMetadata bonusMetadata;


    public TankBuilder() {
        characterUtility = new CharacterUtility();
    }

    /**
     * Generate ability score for tank fighter
     */
    @Override
    public void generateAbilityScore() {
        abilityScoreMetadata = new AbilityScoreMetadata();
        List<Integer> scoreList =characterUtility.randomBuilderAbilityScore(4, 6);
        abilityScoreMetadata.setAb_constitution(String.valueOf(scoreList.get(0)));
        abilityScoreMetadata.setAb_dexterity(String.valueOf(scoreList.get(1)));
        abilityScoreMetadata.setAb_strength(String.valueOf(scoreList.get(2)));
        abilityScoreMetadata.setAb_intelligence(String.valueOf(scoreList.get(3)));
        abilityScoreMetadata.setAb_charisma(String.valueOf(scoreList.get(4)));
        abilityScoreMetadata.setAb_wisdom(String.valueOf(scoreList.get(5)));
        characterMetadata.setAbilityScoreMetadata(abilityScoreMetadata);
    }


    /**
     * convert ability score to ability modifier
     */
    @Override
    public void generateAbilityModifier() {
        bonusMetadata = characterMetadata.getBonusMetadata();
        abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
        characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);
    }
}
