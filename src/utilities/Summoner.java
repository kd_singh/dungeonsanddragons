package utilities;

import metadatas.CharacterMetadata;

/**
 * Summoner (director in builder pattern) responsible for taking building request. the request and the type of builder
 * will be sent to the CharacterBuilder who will do the building
 * Created by han on 2017-03-12.
 */
public class Summoner {

    private CharacterBuilder characterBuilder;

    public void createCharacterMetadata(){
        CharacterMetadata characterMetadata = new CharacterMetadata();
    }

    /**
     * set the correct builder who know which fighter to be built
     * @param characterBuilder
     */
    public void setCharacterBuilder(CharacterBuilder characterBuilder) {
        this.characterBuilder = characterBuilder;
    }

    /**
     * set the character metadata (blue print) needed to build the fighter
     * @param characterMetadata
     */
    public void setCharacterMetadata(CharacterMetadata characterMetadata) {
        this.characterBuilder.characterMetadata = characterMetadata;
    }

    /**
     * Do the actual building
     */
    public void summonFighter(){
        characterBuilder.generateAbilityScore();
        characterBuilder.generateAbilityModifier();
    }

    /**
     * return the fighter back to the requester
     * @return the requested type of fighter
     */
    public CharacterMetadata getCharacterMetadata() {
        return characterBuilder.getCharacterMetadata();
    }

}
