package utilities;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import metadatas.ListMetadata;

/**
 * This class will be used JPanel for adding List with scroll pane wherever required.
 */

public class ListPanelUtility extends JPanel implements PropertyChangeListener
{
	private static final long serialVersionUID = 6887877948707716479L;
	static ListUtility listUtility;
	
	public ListPanelUtility(DefaultListModel<ListMetadata> listMetadata)
	{
		super();
		init_(this,listMetadata);
	}

	/**
	 * This method creates a Scroll pane for List whose data is passed as parameters.
	 * 
	 * @return Nothing
	 * @param ListPanelUtility, DefaultListModel<ListMetadata>
	 */
	private void init_(ListPanelUtility listPanelUtility, DefaultListModel<ListMetadata> listMetadata) 
	{
		try
		{	
			listUtility = new ListUtility(listMetadata);
			JScrollPane jPane = new JScrollPane(listUtility);
			jPane.setPreferredSize(new Dimension(150, 80));

			listPanelUtility.setEnabled(true);
			listPanelUtility.add(jPane);
			listPanelUtility.setVisible(true);
			listUtility = listPanelUtility.getListUtility();
			listUtility.addPropertyChangeListener(this);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	/**
	 * Returns the object of ListUtility from the ListPanelUtility
	 * 
	 * @return ListUtility
	 */
	public ListUtility getListUtility()
	{
		return listUtility;
	}
	
	/**
	 * Returns the current selected index in the ListUtility class
	 * 
	 * @return int
	 */
	public static int getSelectedIndex()
	{
		return listUtility.getSelectedIndex();
	}
	
	/**
	 * Returns the ListMetada of selected item in the List
	 * 
	 * @return ListMetadata
	 */
	public static ListMetadata getSelectedObject()
	{
		if(getSelectedIndex()==-1)
		{
			return null;
		}
		return (ListMetadata)listUtility.getModel().getElementAt(getSelectedIndex());
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		// TODO Auto-generated method stub
		firePropertyChange("SelectedListMetadata", evt.getOldValue(), evt.getNewValue());

	}
}
