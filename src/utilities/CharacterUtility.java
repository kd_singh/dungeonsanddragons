package utilities;

import Model.DiceModel;
import metadatas.*;
import org.apache.log4j.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CharacterUtility {

    private static Logger log = LogManager.getLogger(CharacterUtility.class);
    DiceModel dice;
    ScoreBonusMetadata scoreBonusMetadata;
    int dexBonus=0, intBonus=0, conBonus=0, chaBonus=0, wisBonus=0, strBonus=0;

    public CharacterUtility() {
        this.dice = new DiceModel();
        this.scoreBonusMetadata = new ScoreBonusMetadata();
    }


    /**
     * calculate the ability score and ability modifier based on the dice type and current selected text field
     * @param abilityScoreMetadata
     * @param abilityModifierMetadata
     * @param bonusMetadata
     * @param currentFocus
     * @return
     */
    public AbilityMetadata randomAbilityMetadata(AbilityScoreMetadata abilityScoreMetadata, AbilityModifierMetadata abilityModifierMetadata, BonusMetadata bonusMetadata, String currentFocus)
    {

        int score, modifier;

        switch (currentFocus.toUpperCase()){
            case "STR":
                if (!bonusMetadata.getStrBonus().equals("")) {
                    strBonus = Integer.parseInt(bonusMetadata.getStrBonus());
                }
                score = randomScoreMetadata(4, 6);
                modifier = scoreToModifier(score, strBonus);

                abilityScoreMetadata.setAb_strength(String.valueOf(score));
                abilityModifierMetadata.setAbm_strength(String.valueOf(modifier));
                break;
            case "INT":
                if (!bonusMetadata.getIntBonus().equals("")) {
                    intBonus = Integer.parseInt(bonusMetadata.getIntBonus());
                }
                score = randomScoreMetadata(4, 6);
                modifier = scoreToModifier(score, intBonus);

                abilityScoreMetadata.setAb_intelligence(String.valueOf(score));
                abilityModifierMetadata.setAbm_intelligence(String.valueOf(modifier));
                break;
            case "DEX":
                if (!bonusMetadata.getStrBonus().equals("")) {
                    dexBonus = Integer.parseInt(bonusMetadata.getDexBonus());
                }
                score = randomScoreMetadata(4, 6);
                modifier = scoreToModifier(score, dexBonus);

                abilityScoreMetadata.setAb_dexterity(String.valueOf(score));
                abilityModifierMetadata.setAbm_dexterity(String.valueOf(modifier));
                break;
            case "CON":
                if (!bonusMetadata.getConBonus().equals("")) {
                    conBonus = Integer.parseInt(bonusMetadata.getConBonus());
                }
                score = randomScoreMetadata(4, 6);
                modifier = scoreToModifier(score, conBonus);

                abilityScoreMetadata.setAb_constitution(String.valueOf(score));
                abilityModifierMetadata.setAbm_constitution(String.valueOf(modifier));
                break;
            case "CHA":
                if (!bonusMetadata.getChaBonus().equals("")) {
                    chaBonus = Integer.parseInt(bonusMetadata.getChaBonus());
                }
                score = randomScoreMetadata(4, 6);
                modifier = scoreToModifier(score, chaBonus);

                abilityScoreMetadata.setAb_charisma(String.valueOf(score));
                abilityModifierMetadata.setAbm_charisma(String.valueOf(modifier));
                break;
            case "WIS":
                if (!bonusMetadata.getWisBonus().equals("")) {
                    wisBonus = Integer.parseInt(bonusMetadata.getWisBonus());
                }
                score = randomScoreMetadata(4, 6);
                modifier = scoreToModifier(score, strBonus);

                abilityScoreMetadata.setAb_wisdom(String.valueOf(score));
                abilityModifierMetadata.setAbm_wisdom(String.valueOf(modifier));
                break;
        }
        return new AbilityMetadata(abilityScoreMetadata, abilityModifierMetadata);

    }


    /**
     * get random value for all the ability score inside AbilityScoreMetadata
     * @param face
     * @param time
     * @return
     */
    public AbilityScoreMetadata randomAbilityScoreMetadata(int face, int time) {
        AbilityScoreMetadata abilityScoreMetadata = new AbilityScoreMetadata();

        abilityScoreMetadata.setAb_strength(String.valueOf(randomScoreMetadata(face, time)));
        abilityScoreMetadata.setAb_dexterity(String.valueOf(randomScoreMetadata(face, time)));
        abilityScoreMetadata.setAb_intelligence(String.valueOf(randomScoreMetadata(face, time)));
        abilityScoreMetadata.setAb_constitution(String.valueOf(randomScoreMetadata(face, time)));
        abilityScoreMetadata.setAb_charisma(String.valueOf(randomScoreMetadata(face, time)));
        abilityScoreMetadata.setAb_wisdom(String.valueOf(randomScoreMetadata(face, time)));
        return abilityScoreMetadata;
    }

    /**
     * generate random ability score 6 times and sort it based on largest to smallest for the builder of each character fighter type
     * @param face
     * @param time
     * @return The list of sorted ability score
     */
    public List<Integer> randomBuilderAbilityScore(int face, int time){
        AbilityScoreMetadata abilityScoreMetadata = new AbilityScoreMetadata();
        List<Integer> scoreList = new ArrayList<Integer>();
        for(int i=0; i<6; i++) {
            scoreList.add(randomScoreMetadata(face, time));
        }
        Collections.sort(scoreList, Collections.reverseOrder());
        return scoreList;
    }

    /**
     * convert ability score object to ability modifier object
     * @param abilityScoreMetadata
     * @param bonusMetadata
     * @return
     */
    public AbilityModifierMetadata abilityScoreToAbilityModifier(AbilityScoreMetadata abilityScoreMetadata, BonusMetadata bonusMetadata) {
        AbilityModifierMetadata abilityModifierMetadata = new AbilityModifierMetadata();

        scoreBonusMetadata = parseScoreAndBonus(abilityScoreMetadata, bonusMetadata);

        abilityModifierMetadata.setAbm_strength(String.valueOf(scoreToModifier(scoreBonusMetadata.getStrScore() , scoreBonusMetadata.getStrBonus())));
        abilityModifierMetadata.setAbm_dexterity(String.valueOf(scoreToModifier(scoreBonusMetadata.getDexScore() , scoreBonusMetadata.getDexBonus())));
        abilityModifierMetadata.setAbm_intelligence(String.valueOf(scoreToModifier(scoreBonusMetadata.getIntScore() , scoreBonusMetadata.getIntBonus())));
        abilityModifierMetadata.setAbm_constitution(String.valueOf(scoreToModifier(scoreBonusMetadata.getConScore() , scoreBonusMetadata.getConBonus())));
        abilityModifierMetadata.setAbm_charisma(String.valueOf(scoreToModifier(scoreBonusMetadata.getChaScore() , scoreBonusMetadata.getChaBonus())));
        abilityModifierMetadata.setAbm_wisdom(String.valueOf(scoreToModifier(scoreBonusMetadata.getWisScore() , scoreBonusMetadata.getWisBonus())));
        return abilityModifierMetadata;
    }

    /**
     * Generate ability score based on the random dice 4d6
     * @param face face of dice
     * @param time number of roll
     * @return the sum of 3 highest random value
     */
    public int randomScoreMetadata(int face, int time) {
        int dice4d6 = dice.roll4d6(face, time);
        int scoreMetadata = dice4d6;
        return scoreMetadata;
    }


    /**
     * calculate HitPoint based on the level and the Constitution ability modifier and random 1d10 dice role
     * @param level level of the character
     * @param con constitution modifier of the character
     * @return int Total hitpoint
     */
    public int calculateHitPoint(int level, int con) {
        int hitPoint;
        if (level > 20)
            log.warn("Level shouldn't be more then 20!!!!!!!!!!!!!!!");
        hitPoint = dice.roll1d10(level,10, con);
        if (hitPoint < 1) {
            hitPoint = 1;
            log.info("hitpoint is less then 0, reset it to 1");
        }
        return hitPoint;
    }

    /**
     * Calculate the armor class based on the character fighter class base armor class (10) plus dexterity modifier of the character
     * and armor class bonus obtained from the item
     * @param dexMod
     * @param armorClassBonus
     * @return
     */
    public int calulateArmorClass(int dexMod, int armorClassBonus){
        int baseArmor = 10;
        int armorClass = baseArmor + armorClassBonus + dexMod;
        log.info("Armor Class = BaseArmor 10 + Armor Class Bonus " + armorClassBonus + " + dex modifier " +dexMod + " = " + armorClass );
        return armorClass;
    }

    /**
     * Calculate the attack bonus based on the level of the character and attack bonus obtained from the item
     * @param level
     * @param attackBonus
     * @return
     */
    public int calculateAttackBonus(int level, int attackBonus){
        int attack = level + attackBonus;
        log.info("Attack Bonus = level " + level + " + item attack bonus " + attackBonus + " = " + attack);
        return attack;
    }

    /**
     * calculate the damage bonus based on the strength modifier and damage bonus obtained from the item
     * @param strMod
     * @param damageBonus
     * @return
     */
    public int calculateDamageBonus(int strMod, int damageBonus){
        int damage = strMod + damageBonus;
        log.info("Damage Bonus = str modifier " + strMod + " + item damage bonus " + damageBonus + " = " + damage);
        return damage;
    }


    /**
     * Method used to generate ability modifier from ability score
     * ability score must also include ability bonus from the item
     * the calculation is used based on ((abilityScore + abilityBonus) - 10) / 2 , take the int value
     */
    public int scoreToModifier(int score, int bonus){
        int modifier = (score + bonus - 10)/2;
        log.info("Ability Score: " + score + " + " + "Ability Bonus: " + bonus + " = Total Ability Score "+ (score + bonus));
        log.info("Modifier: " + modifier);
        if (modifier < 0) {
            modifier = 0;
            log.warn("Modifier is less then 0, reset to 0");
        }
        return modifier;
    }

    /**
     * The ability score need to be updated everytime there's modification to the ability score bonus, serve as a transport
     * that we can use to update the ability text field that display ability score plus ability bonus
     * @param abilityScoreMetadata the current ability score
     * @param bonusMetadata the current ability bonus
     * @return return ScoreBonusMetadata that combine both ability score then bonus
     */

    public ScoreBonusMetadata parseScoreAndBonus(AbilityScoreMetadata abilityScoreMetadata, BonusMetadata bonusMetadata) {

        //set score
        if (abilityScoreMetadata.getAb_strength()!=null) {
            scoreBonusMetadata.setStrScore(Integer.parseInt(abilityScoreMetadata.getAb_strength()));
        }
        if (abilityScoreMetadata.getAb_dexterity()!=null) {
            scoreBonusMetadata.setDexScore(Integer.parseInt(abilityScoreMetadata.getAb_dexterity()));
        }
        if (abilityScoreMetadata.getAb_intelligence()!=null ) {
            scoreBonusMetadata.setIntScore(Integer.parseInt(abilityScoreMetadata.getAb_intelligence()));
        }
        if (abilityScoreMetadata.getAb_constitution()!=null) {
            scoreBonusMetadata.setConScore(Integer.parseInt(abilityScoreMetadata.getAb_constitution()));
        }
        if (abilityScoreMetadata.getAb_charisma()!=null) {
            scoreBonusMetadata.setChaScore(Integer.parseInt(abilityScoreMetadata.getAb_charisma()));
        }
        if (abilityScoreMetadata.getAb_wisdom()!=null) {
            scoreBonusMetadata.setWisScore(Integer.parseInt(abilityScoreMetadata.getAb_wisdom()));
        }

        //set bonus, if the value of the benusmetadata is empty, the bonuses in scoreBonusMetadata have to be 0
        //or the calculation of the ability modifier based on ability score and item bonus will be in consistent
        if (!bonusMetadata.getStrBonus().equals("")) {
            scoreBonusMetadata.setStrBonus(Integer.parseInt(bonusMetadata.getStrBonus()));
        }else {
            scoreBonusMetadata.setStrBonus(0);
        }

        if (!bonusMetadata.getDexBonus().equals("")) {
            scoreBonusMetadata.setDexBonus(Integer.parseInt(bonusMetadata.getDexBonus()));
        }else {
            scoreBonusMetadata.setDexBonus(0);
        }

        if (!bonusMetadata.getIntBonus().equals("")) {
            scoreBonusMetadata.setIntBonus(Integer.parseInt(bonusMetadata.getIntBonus()));
        }else {
            scoreBonusMetadata.setIntBonus(0);
        }

        if (!bonusMetadata.getConBonus().equals("")) {
            scoreBonusMetadata.setConBonus(Integer.parseInt(bonusMetadata.getConBonus()));
        }else {
            scoreBonusMetadata.setConBonus(0);
        }

        if (!bonusMetadata.getChaBonus().equals("")) {
            scoreBonusMetadata.setChaBonus(Integer.parseInt(bonusMetadata.getChaBonus()));
        }else {
            scoreBonusMetadata.setChaBonus(0);
        }

        if (!bonusMetadata.getWisBonus().equals("")) {
            scoreBonusMetadata.setWisBonus(Integer.parseInt(bonusMetadata.getWisBonus()));
        }else {
            scoreBonusMetadata.setWisBonus(0);
        }

        return scoreBonusMetadata;
    }


    /**
     * return the character proficiency bonus according to the level of the character
     * @param level
     * @return
     */
    public int getProficiencyBonus(int level) {
        int proficiencyBonus = 0;

        switch (level) {
            case 1:case 2:case 3:case 4:
                proficiencyBonus = 2;
                break;

            case 5:case 6:case 7:case 8:case 9:case 10:
                proficiencyBonus = 3;
                break;

            case 11:case 12:
                proficiencyBonus = 4;
                break;

            case 13:case 14:case 15:case 16:
                proficiencyBonus = 5;
                break;

            case 17:case 18:case 19:case 20:
                proficiencyBonus = 6;
                break;
        }

        return proficiencyBonus;

    }

}