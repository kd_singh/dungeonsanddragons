package utilities;

import metadatas.CharacterMetadata;

/**
 * Abstract character builder who will take command from summoner to build type of fighter
 * Created by han on 2017-03-12.
 */
public abstract class CharacterBuilder {

    public CharacterMetadata characterMetadata;
    CharacterUtility characterUtility;


    /**
     * set the character metadata (blue print) need to build the fighter
     * @param characterMetadata
     */
    public void setCharacterMetadata(CharacterMetadata characterMetadata) {
        this.characterMetadata = characterMetadata;
    }

    /**
     * return the built fighter character back to summoner
     * @return character metadata of the fighter
     */
    public CharacterMetadata getCharacterMetadata() {
        return characterMetadata;
    }

    /**
     * abstract method which generate ability score
     */
    public abstract void generateAbilityScore();

    /**
     * abstract method which generate ability modifier
     */
    public abstract void generateAbilityModifier();
}
