package utilities;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import metadatas.ListMetadata;
import metadatas.ListSel_ResponseMetadata;

/**
 * This class defines the ListUtility which acts as JList and populates the JList with data.
 */

public class ListUtility extends JList implements ListSelectionListener
{
	private static final long serialVersionUID = 5329013008745391148L;

	ListMetadata listData;
	ListUtility listUtility;

	public static ListSel_ResponseMetadata responseMetadata = new ListSel_ResponseMetadata();
	public static Boolean isChanged = false;

	public ListUtility(DefaultListModel<ListMetadata> listMetadata)
	{
		super();
		init_(this,listMetadata);
	}

	/**
	 * Populates list with data passed in List Model
	 * 
	 * @return Nothing
	 * @param ListUtility and DefaultListModel<ListMetadata>
	 */
	private void init_(ListUtility listUtility, DefaultListModel<ListMetadata> listMetadata) 
	{
		this.listUtility = listUtility;

		listUtility.setModel(listMetadata);
		listUtility.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listUtility.setVisibleRowCount(5);
		listUtility.setSelectedIndex(0);
		listUtility.addListSelectionListener(this);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) 
	{
		if(!e.getValueIsAdjusting())
		{

			isChanged = true;

			JList list = (ListUtility)e.getSource();
			ListModel model = list.getModel();

			int selected = list.getSelectedIndex();
			int previous = selected == e.getFirstIndex() ? e.getLastIndex() : e.getFirstIndex();

			Object newSelection = model.getElementAt(list.getSelectedIndex());
			Object previousSelection = model.getElementAt(previous);

			ListMetadata selectedMetadata = (ListMetadata) newSelection;
			ListMetadata previousMetadata = (ListMetadata) previousSelection;

			responseMetadata.setSelected_id(selectedMetadata.getItem_ID());
			responseMetadata.setSelected_name(selectedMetadata.getItem_Name());
			
			firePropertyChange("SelectedListMetadata", previousSelection, newSelection);

		}
	}
	
}
