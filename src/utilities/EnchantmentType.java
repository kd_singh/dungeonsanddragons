package utilities;

import java.util.ArrayList;
import java.util.List;

public enum EnchantmentType
{
	Intelligence("INTELLIGENCE","Intelligence"),Wisdom("WISDOM","Wisdom"),Strength("STRENGTH","Strength"),Consitution("CONSTITUTION","Constitution"),
	Charisma("CHARISMA","Charisma"),Dexterity("DEXTERITY","Dexterity"),
	ArmorClass("ARMORCLASS","Armor Class"),
	AttackBonus("ATTACKBONUS","Attack Bonus"),DamageBonus("DAMAGEBONUS","Damage Bonus");

	 
	
	private final String enchantmentId;
	private final String enchantmentName;
	private final List<String> values = new ArrayList<String>();;


	private EnchantmentType(String enchantmentId, String enchantmentName)
	{
		this.enchantmentId = enchantmentId;
		this.enchantmentName = enchantmentName;
		this.values.add(enchantmentId);
		this.values.add(enchantmentName);
	}
	
	public String getEnchantmentId()
	{
		return enchantmentId;
	}
	public String getEnchantmentName()
	{
		return enchantmentName;
	}
	
	public List<String> getValues() 
	{
		return values;
	}
	
	public static EnchantmentType find(String name) 
	{
		for (EnchantmentType enchantmentType : EnchantmentType.values())
		{
			if (enchantmentType.getValues().contains(name))
			{
				return enchantmentType;
			}
		}
		return null;
	}
	
	public static EnchantmentType[] getEnchantmentTypeHelmet()
	{
		return new EnchantmentType[]{Intelligence,Wisdom,ArmorClass};
	}
	
	public static EnchantmentType[] getEnchantmentTypeArmorShield()
	{
		return new EnchantmentType[]{ArmorClass};
	}
	
	public static EnchantmentType[] getEnchantmentTypeRing()
	{
		return new EnchantmentType[]{ArmorClass,Strength,Consitution,Wisdom,Charisma};
	}
	
	public static EnchantmentType[] getEnchantmentTypeBelt()
	{
		return new EnchantmentType[]{Consitution,Strength};
	}
	
	public static EnchantmentType[] getEnchantmentTypeBoots()
	{
		return new EnchantmentType[]{ArmorClass,Dexterity};
	}
	
	public static EnchantmentType[] getEnchantmentTypeWeapon()
	{
		return new EnchantmentType[]{AttackBonus,DamageBonus};
	}
	
	@Override
	public String toString()
	{
		return this.enchantmentName;
	}
	
	
}
