package utilities;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

import metadatas.GridCellMetadata;
 

/**
 * GridCell Class serves as the container for the Grid of Buttons to be used in Map.
 * It extends JButton class.
 * 
 * @author Karan Deep Singh
 *
 * @see JButton
 */
public class GridCell extends JButton implements ActionListener{

	private static final long serialVersionUID = 1L;
	String value, type;
	int xPosition, yPosition;
	GridCellMetadata gridcellMetadata = new GridCellMetadata();
	public static GridCellMetadata responseCellData = new GridCellMetadata();
	public static boolean isChanged = false;
	
	/** Constructor to initialize the Grid Cell
	 * @param xPosition x position of cell
	 * @param yPosition y position of cell
	 * @param value value of data stored in cell
	 * @param type type of data stored in cell
	 */
	public GridCell(int xPosition, int yPosition, String value, String type) 
	{
		super();
		super.setMargin(new Insets(0, 0, 0, 0));
		super.setMargin(new Insets(0, 0, 0, 0));
		super.setBorderPainted(true);
		super.setBorder(new LineBorder(Color.white, 1));
		super.setBackground(Color.decode(Constants.colors.get(type)));
		super.setPreferredSize(new Dimension(60, 60));
		super.setOpaque(true);
		this.setText(value);
		
		gridcellMetadata.setxPosition(xPosition);
		gridcellMetadata.setyPosition(yPosition);
		gridcellMetadata.setValue(value);
		gridcellMetadata.setType(type);
		
		init_(this, gridcellMetadata);
		
		this.addActionListener(this);
	}
	
	/**
	 * Called from Constructor to work on initialization.
	 * 
	 * @param cellButton Gridcell object
	 * @param gridcellMetadata Grid cell metadata object
	 */
	private void init_(GridCell cellButton, GridCellMetadata gridcellMetadata) 
	{
		this.type = gridcellMetadata.getType();
		this.value = gridcellMetadata.getValue();
		this.xPosition = gridcellMetadata.getxPosition();
		this.yPosition = gridcellMetadata.getyPosition();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		isChanged = true;
		
		responseCellData.setxPosition(xPosition);
		responseCellData.setyPosition(yPosition);
		responseCellData.setValue(value);
		responseCellData.setType(type);
		
		firePropertyChange("GridCellClick", gridcellMetadata, responseCellData);
	}

	/**
	 * Getter for the Cell Type
	 * @return type data stored in cell
	 */
	public String getType() 
	{
		return type;
	}

	/**
	 * Setter for the Cell Type
	 * @param type data of type to store in cell
	 */
	public void setType(String type) 
	{
		this.type = type;
	}

	/**
	 * Getter for the value of Cell
	 * @return value of cell data
	 */
	public String getValue() 
	{
		return value;
	}

	/** Setter for the Value of cell
	 * @param value data to store as Cell value
	 */
	public void setValue(String value) 
	{
		this.value = value;
	}

	/**
	 * Getter for the x position of the Cell
	 * @return x position of the cell
	 */
	public int getxPosition() 
	{
		return xPosition;
	}

	/**
	 * Setter for the x position of the cell
	 * @param xPosition integer denoting the x position of the cell.
	 */
	public void setxPosition(int xPosition) 
	{
		this.xPosition = xPosition;
	}

	/**
	 * Getter for the y position of the cell
	 * @return y position of the cell
	 */ 
	public int getyPosition() 
	{
		return yPosition;
	}

	/**
	 * Setter for the Y position of the Cell
	 * @param yPosition sets y position of the cell
	 */
	public void setyPosition(int yPosition) 
	{
		this.yPosition = yPosition;
	}
	
	/**
	 * Getter for the GridCell Metadata
	 * @return the GridCellMetadata object
	 */
	public GridCellMetadata getGridCellMetadata()
	{
		return gridcellMetadata;
	}
}
