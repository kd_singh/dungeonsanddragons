package tests.campaign;

import static org.junit.Assert.*;
import java.util.HashMap;
import org.junit.Test;
import Model.CampaignModel;
import Model.MapModel;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.MapMetadata;
import org.junit.Before;


public class CampaignValidTest {
	
	CampaignModel campaignModel;
	CampaignMetadata campaignMetadata;
	CharacterMetadata characterMetadata;
	MapMetadata	mapMetadata;
	MapModel mapModel;
	
	public HashMap<String, CampaignMetadata> campaignListData;
	public HashMap<String, MapMetadata> mapListData;
	
	@Before
	public void mapSetup()
	{
		mapModel = new MapModel();
		campaignModel = new CampaignModel();
		campaignMetadata = new CampaignMetadata();
		campaignListData = campaignModel.getCampaignMap();
		mapListData = mapModel.getParser();
		mapMetadata = mapListData.get("1");
		
	}
	
	
	@Test
	public void testValidationEmptyName() 
	{
		
		int size = campaignListData.size();
		System.out.println(size);
		campaignMetadata.actionAddMaps(mapMetadata);
		boolean result = campaignModel.actionSaveCampaign(campaignMetadata);
		assertFalse(result);
		
	}
	
	@Test
	public void testValidationNoMaps() 
	{
		
		int size = campaignListData.size();
		System.out.println(size);
		campaignMetadata.setCampaign_name("New Campaign");
		boolean result = campaignModel.actionSaveCampaign(campaignMetadata);
		assertFalse(result);
		
	}
	
	@Test
	public void testCampaignSizeChange() 
	{
		
		int size = campaignListData.size();
		System.out.println(size);
		campaignMetadata.setCampaign_name("New Campaign");
		campaignMetadata.actionAddMaps(mapMetadata);
		campaignModel.actionSaveCampaign(campaignMetadata);
		int newSize = campaignListData.size();
		assertNotEquals(size, newSize);
		
	}
	
	
}
