package tests.Map;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import Controller.MapCreateViewController;
import View.GamePlayView;
import View.MapCreateView;
import metadatas.GridCellMetadata;
import utilities.Grid;

/**
 * This is the Test Class for Map Create Classes
 * @author Vijay Karan Singh
 *
 */
public class testCreateMap 
{
	int xPos, yPos, x1Pos, y1Pos, gridSize;
	MapCreateView map;
	MapCreateViewController controller;
	Grid grid;
	GridCellMetadata gridcell;
	GamePlayView gamePlay;
	
	@Before public void beforeTest() 
	{
		xPos = 0;
		yPos = 7;
		
		x1Pos = 3;
		y1Pos = 4;
		gridSize = 8;
		
		controller = new MapCreateViewController(gridSize);
		grid = new Grid(gridSize);
		map = new MapCreateView(controller, grid, "");
		gamePlay = new GamePlayView();
		
	}

	/**
	 * Method to Test the Boundary Condition for Entry and Exit using <code>isBoundary()</code> in <code>MapCreateView</code> Class.
	 */
	@Test public void testBoundaryCondition()
	{
		boolean actualTrue = map.isBoundary(xPos, yPos);
		boolean actualFalse = map.isBoundary(x1Pos, y1Pos);
		assertEquals(true, actualTrue);
		assertEquals(false, actualFalse);
	}
	
	/**
	 * Method to test update of Cell Type.
	 */
	@Test public void testCellTypeUpdate()
	{
		String type = "WALL";
		grid.update_GridCellType(xPos, yPos, type);
		assertEquals(type, grid.get_GridCellType(xPos, yPos));
	}
	
	/**
	 * Method to test update of Cell Value.
	 */
	@Test public void testCellValueUpdate()
	{
		String value = "Entry";
		grid.update_GridCellValue(xPos, yPos, value);
		assertEquals(value, grid.get_GridCellValue(xPos, yPos));
	}
	
	/**
	 * Function to test the slots available in backpack for Chest or Monster Loot.
	 */
	@Test public void testLootCapacity(){
		int backfilled = 6;
		int itemsAvailable = 10;
		int expectedSlots = 4;
		assertEquals(expectedSlots, gamePlay.getBackpackSlots(backfilled, itemsAvailable));
	}
}
