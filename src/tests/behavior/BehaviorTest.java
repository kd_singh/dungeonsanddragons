package behavior;

import Model.CharacterModel;
import Model.ItemModel;
import metadatas.CharacterMetadata;
import metadatas.ItemListMetaInterface;
import metadatas.ItemMetadata;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import static org.junit.Assert.*;

/**
 * Created by Han on 2017-04-13.
 */
public class BehaviorTest {
    CharacterModel characterModel;
    ItemModel itemModel;
    Map<String, CharacterMetadata> characterMap;
    Map<String, ItemMetadata> itemMap;
    CharacterMetadata attacker, opponent;
    Vector<CharacterMetadata> characterMetadataVector;
    Vector<ItemMetadata> itemMetadataVector;
    ItemListMetaInterface weaponItem;
    Behavior behavior;
    @Before
    public void setup(){
        characterModel = new CharacterModel();
        itemModel = new ItemModel();
        characterMap = new HashMap<>();
        itemMap = new HashMap<>();
        characterMap = characterModel.getCharacterMap();
        itemMap = itemModel.getItemMap();
        behavior = new FriendlyBehavior();
        attacker = characterMap.get("1");
        opponent = characterMap.get("2");






    }


    @Test
    public void isHit() throws Exception {

    }

    @Test
    public void getDamage() throws Exception {
    }

    @Test
    public void getWeaponRange() throws Exception {
        int range = behavior.getWeaponRange(attacker);

        assertEquals(1,range);
    }

}