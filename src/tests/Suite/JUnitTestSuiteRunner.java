package tests.Suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import tests.Map.testCreateMap;
import tests.campaign.CampaignValidTest;
import tests.character.CharacterEnchantmentTest;
import tests.character.CharacterUtilityTest;
import tests.character.CharactersTest;
import tests.item.itemCreateTest;
import utilities.Summoner;

@RunWith(Suite.class)

@SuiteClasses({CampaignValidTest.class, 
               testCreateMap.class, 
               CharacterEnchantmentTest.class,
               CharactersTest.class, 
               CharacterUtilityTest.class,
               itemCreateTest.class,
               Summoner.class})

public class JUnitTestSuiteRunner {   
} 

