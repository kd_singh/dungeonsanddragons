package tests.character;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import Model.CharacterModel;
import Model.ItemModel;
import metadatas.CharacterMetadata;
import metadatas.CharacterWearableMetadata;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import utilities.Constants;

public class CharactersTest {

	CharacterModel characterModel;
	ItemModel itemModel;
	HashMap<String,CharacterMetadata> characterMap;
	HashMap<String,ItemMetadata> itemMap;
	CharacterWearableMetadata characterWearableMetadata;
	CharacterMetadata characterMetadata;
	ItemMetadata itemMetadata;
	ItemListMetadata itemListMetadata;
	Vector<ItemListMetadata> vector_ItemListMetadata;
	int countHelmet;
	int countArmor;
	int countShield;
	int countBoots;
	int countBelts;
	int countWeapon;
	
	@Before
	public void setupCharacters()
	{
		characterModel = new CharacterModel();
		itemModel = new ItemModel();
		characterMap = characterModel.getCharacterMap();
		itemMap = itemModel.getItemMap();
		
		countHelmet = 0;
		countArmor = 0;
		countShield =0;
		countBoots =0;
		countBelts =0;
		countWeapon =0;
	}
	
	@Test
	public void testOneHelmetPerCharacter() 
	{
		characterMetadata = characterMap.get("1");
		
		String itemHelmet = null;
		
		
		itemMetadata = itemMap.get(Constants.ItemType.Helmet.getItemId());
		vector_ItemListMetadata = itemMetadata.getItemListMetadata();
		
		if(characterWearableMetadata == null)
		{
			characterWearableMetadata = new CharacterWearableMetadata();
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(vector_ItemListMetadata.size()-1).getList_item_id());
			itemHelmet = characterWearableMetadata.getItm_helmet();
			characterMetadata.setCharacterWearableMetadata(characterWearableMetadata);
		}
		else
		{
			characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			itemHelmet = characterWearableMetadata.getItm_helmet();
		}
		
		for(int i =0; i<vector_ItemListMetadata.size(); i++)
		{
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(i).getList_item_id());
		}
		
		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{	
			itemListMetadata = vector_ItemListMetadata.get(i);
			if(itemHelmet.equals(itemListMetadata.getList_item_id()))
			{
				countHelmet++;
			}
		}
		
		assertEquals(1, countHelmet);
	}

	@Test
	public void testOneArmorPerCharacter() 
	{
		characterMetadata = characterMap.get("1");
		
		String itemArmor = null;
		
		
		itemMetadata = itemMap.get(Constants.ItemType.Armor.getItemId());
		vector_ItemListMetadata = itemMetadata.getItemListMetadata();
		
		if(characterWearableMetadata == null)
		{
			characterWearableMetadata = new CharacterWearableMetadata();
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(vector_ItemListMetadata.size()-1).getList_item_id());
			itemArmor = characterWearableMetadata.getItm_helmet();
			characterMetadata.setCharacterWearableMetadata(characterWearableMetadata);
		}
		else
		{
			characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			itemArmor = characterWearableMetadata.getItm_helmet();
		}
		
		for(int i =0; i<vector_ItemListMetadata.size(); i++)
		{
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(i).getList_item_id());
		}
		
		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{	
			itemListMetadata = vector_ItemListMetadata.get(i);
			if(itemArmor.equals(itemListMetadata.getList_item_id()))
			{
				countArmor++;
			}
		}
		
		assertEquals(1, countArmor);
	}

	@Test
	public void testOneShieldPerCharacter() 
	{
		characterMetadata = characterMap.get("1");
		
		String itemShield = null;
		
		
		itemMetadata = itemMap.get(Constants.ItemType.Shield.getItemId());
		vector_ItemListMetadata = itemMetadata.getItemListMetadata();
		
		if(characterWearableMetadata == null)
		{
			characterWearableMetadata = new CharacterWearableMetadata();
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(vector_ItemListMetadata.size()-1).getList_item_id());
			itemShield = characterWearableMetadata.getItm_helmet();
			characterMetadata.setCharacterWearableMetadata(characterWearableMetadata);
		}
		else
		{
			characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			itemShield = characterWearableMetadata.getItm_helmet();
		}
		
		for(int i =0; i<vector_ItemListMetadata.size(); i++)
		{
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(i).getList_item_id());
		}
		
		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{	
			itemListMetadata = vector_ItemListMetadata.get(i);
			if(itemShield.equals(itemListMetadata.getList_item_id()))
			{
				countShield++;
			}
		}
		
		assertEquals(1, countShield);
	}

	@Test
	public void testOneBeltPerCharacter() 
	{
		characterMetadata = characterMap.get("1");
		
		String itemBelt = null;
		
		
		itemMetadata = itemMap.get(Constants.ItemType.Belt.getItemId());
		vector_ItemListMetadata = itemMetadata.getItemListMetadata();
		
		if(characterWearableMetadata == null)
		{
			characterWearableMetadata = new CharacterWearableMetadata();
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(vector_ItemListMetadata.size()-1).getList_item_id());
			itemBelt = characterWearableMetadata.getItm_helmet();
			characterMetadata.setCharacterWearableMetadata(characterWearableMetadata);
		}
		else
		{
			characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			itemBelt = characterWearableMetadata.getItm_helmet();
		}
		
		for(int i =0; i<vector_ItemListMetadata.size(); i++)
		{
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(i).getList_item_id());
		}
		
		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{	
			itemListMetadata = vector_ItemListMetadata.get(i);
			if(itemBelt.equals(itemListMetadata.getList_item_id()))
			{
				countBelts++;
			}
		}
		
		assertEquals(1, countBelts);
	}
	
	@Test
	public void testOneBootsPerCharacter() 
	{
		characterMetadata = characterMap.get("1");
		
		String itemBoots = null;
		
		
		itemMetadata = itemMap.get(Constants.ItemType.Boots.getItemId());
		vector_ItemListMetadata = itemMetadata.getItemListMetadata();
		
		if(characterWearableMetadata == null)
		{
			characterWearableMetadata = new CharacterWearableMetadata();
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(vector_ItemListMetadata.size()-1).getList_item_id());
			itemBoots = characterWearableMetadata.getItm_helmet();
			characterMetadata.setCharacterWearableMetadata(characterWearableMetadata);
		}
		else
		{
			characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			itemBoots = characterWearableMetadata.getItm_helmet();
		}
		
		for(int i =0; i<vector_ItemListMetadata.size(); i++)
		{
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(i).getList_item_id());
		}
		
		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{	
			itemListMetadata = vector_ItemListMetadata.get(i);
			if(itemBoots.equals(itemListMetadata.getList_item_id()))
			{
				countBoots++;
			}
		}
		
		assertEquals(1, countBoots);
	}
	
	@Test
	public void testOneWeaponPerCharacter() 
	{
		characterMetadata = characterMap.get("1");
		
		String itemWeapon = null;
		
		
		itemMetadata = itemMap.get(Constants.ItemType.Sword.getItemId());
		vector_ItemListMetadata = itemMetadata.getItemListMetadata();
		
		if(characterWearableMetadata == null)
		{
			characterWearableMetadata = new CharacterWearableMetadata();
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(vector_ItemListMetadata.size()-1).getList_item_id());
			itemWeapon = characterWearableMetadata.getItm_helmet();
			characterMetadata.setCharacterWearableMetadata(characterWearableMetadata);
		}
		else
		{
			characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
			itemWeapon = characterWearableMetadata.getItm_helmet();
		}
		
		for(int i =0; i<vector_ItemListMetadata.size(); i++)
		{
			characterWearableMetadata.setItm_helmet(vector_ItemListMetadata.get(i).getList_item_id());
		}
		
		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{	
			itemListMetadata = vector_ItemListMetadata.get(i);
			if(itemWeapon.equals(itemListMetadata.getList_item_id()))
			{
				countWeapon++;
			}
		}
		
		assertEquals(1, countWeapon);
	}


}
