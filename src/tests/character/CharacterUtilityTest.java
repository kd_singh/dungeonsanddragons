package tests.character;

import Model.DiceModel;
import metadatas.AbilityModifierMetadata;
import metadatas.AbilityScoreMetadata;
import metadatas.BonusMetadata;
import utilities.CharacterUtility;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by han on 2017-02-14.
 */
public class CharacterUtilityTest {


    private CharacterUtility characterUtility;
    private AbilityScoreMetadata abilityScoreMetadata;
    private AbilityModifierMetadata abilityModifierMetadata;
    private BonusMetadata bonusMetadata;
    private int level;
    private DiceModel dice;
    @Before
    public void setUp() throws Exception {
        characterUtility = new CharacterUtility();
        bonusMetadata = new BonusMetadata();
        bonusMetadata.setArmorClassBonus("5");
        bonusMetadata.setAttackBonus("5");
        bonusMetadata.setDamageBonus("5");
        bonusMetadata.setStrBonus("3");
        bonusMetadata.setIntBonus("3");
        bonusMetadata.setDexBonus("3");
        bonusMetadata.setChaBonus("3");
        bonusMetadata.setConBonus("3");
        bonusMetadata.setWisBonus("3");

        abilityScoreMetadata = new AbilityScoreMetadata();
        abilityScoreMetadata.setAb_dexterity("15");
        abilityScoreMetadata.setAb_constitution("15");
        abilityScoreMetadata.setAb_intelligence("15");
        abilityScoreMetadata.setAb_charisma("15");
        abilityScoreMetadata.setAb_wisdom("15");
        abilityScoreMetadata.setAb_strength("15");

        abilityModifierMetadata = new AbilityModifierMetadata();
        abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
        dice = new DiceModel();


    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void abilityScoreToAbilityModifier() throws Exception {
        abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
        assertEquals(abilityModifierMetadata.getAbm_charisma(), "4");
    }



    @Test
    public void scoreToModifier() throws Exception {
        int result = characterUtility.scoreToModifier(15, 3);
        int testValue = Integer.valueOf(abilityModifierMetadata.getAbm_charisma());
        assertEquals(testValue, result);
    }

    @Test
    public void calulateArmorClass() throws Exception {
        int dexMode = Integer.valueOf(abilityModifierMetadata.getAbm_dexterity());
        int armorBonus = Integer.valueOf(bonusMetadata.getArmorClassBonus());
        int armorClass = characterUtility.calulateArmorClass(dexMode, armorBonus);
        assertEquals(armorClass, 19);
    }

    @Test
    public void calculateAttackBonus() throws Exception {
        int level = 10;
        int attackBonus = Integer.valueOf(bonusMetadata.getAttackBonus());
        int result = level + attackBonus;
        int testValue = characterUtility.calculateAttackBonus(level, attackBonus);
        assertEquals(testValue, result);
    }

    @Test
    public void calculateDamageBonus() throws Exception {
        int strMod = Integer.valueOf(abilityModifierMetadata.getAbm_strength());
        int damageBonus = Integer.valueOf(bonusMetadata.getDamageBonus());
        int result = strMod + damageBonus;
        int testValue = characterUtility.calculateAttackBonus(strMod, damageBonus);
        assertEquals(testValue, result);
    }

}