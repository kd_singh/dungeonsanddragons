package tests.character;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import com.sun.org.apache.bcel.internal.classfile.Constant;

import Controller.CharacterCreateViewController;
import Model.CharacterModel;
import Model.ItemModel;
import metadatas.BackpackMetadata;
import metadatas.BonusMetadata;
import metadatas.CharacterMetadata;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import metadatas.ScoreBonusMetadata;
import utilities.CharacterUtility;
import utilities.Constants;
import utilities.EnchantmentType;

public class CharacterEnchantmentTest {

	CharacterModel characterModel;
	ItemModel itemModel;

	ItemMetadata itemMetadata;
	CharacterMetadata characterMetadata;
	ItemListMetadata itemListMetadata;

	Vector<ItemMetadata> vector_itemMetadata;
	Vector<ItemListMetadata> vector_ItemListMetadata;

	HashMap<String,CharacterMetadata> characterMap;
	HashMap<String,ItemMetadata> itemMap;

	CharacterCreateViewController controller;

	BonusMetadata bonusMetadata;
	BackpackMetadata backpackMetadata;
	ScoreBonusMetadata scoreBonusMetadata;
	CharacterUtility charUtility;

	@Before
	public void setupTest()
	{
		characterModel = new CharacterModel();
		itemModel = new ItemModel();

		characterMap = characterModel.getCharacterMap();
		itemMap = itemModel.getItemMap();

		characterMap = characterModel.getCharacterMap();
		itemMap = itemModel.getItemMap();

		characterMetadata = characterMap.get("1");
		controller = new CharacterCreateViewController(characterMetadata, itemModel, characterModel);

		backpackMetadata = new BackpackMetadata();
		scoreBonusMetadata = new ScoreBonusMetadata();

		charUtility = new CharacterUtility();

	}


	@Test
	public void testCorrectEnchantmentRing() 
	{
		vector_ItemListMetadata = itemMap.get(Constants.ItemType.Ring.getItemId()).getItemListMetadata();

		scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());


		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{
			ItemListMetadata itemListMetadata = vector_ItemListMetadata.get(i);
			scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
			String itemType;

			if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Charisma.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Charisma.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getChaScore());
				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Ring.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getChaResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));


			}
			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Consitution.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Consitution.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getConResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Ring.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getConResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));


			}
			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Dexterity.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Dexterity.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getDexResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Ring.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getDexResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));


			}
			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Intelligence.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Intelligence.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getIntResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Ring.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getIntResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));

			}
			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Strength.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Strength.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getStrResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Ring.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getStrResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));


			}
			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Wisdom.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Wisdom.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getWisResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Ring.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getWisResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));

			}
			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.ArmorClass.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				int previousAC;
				bonusMetadata = characterMetadata.getBonusMetadata();
				if(!(bonusMetadata==null))
				{
					if(!bonusMetadata.getArmorClassBonus().equals(""))
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));
						System.out.println("Previous AC: " + previousAC);
					}
					else
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), 0);
					}

					backpackMetadata.setItemId(itemListMetadata.getList_item_id());
					backpackMetadata.setItemType(Constants.ItemType.Ring.getItemId());
					controller.actionMoveToWearables(characterMetadata, backpackMetadata);
					int newAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));

					assertNotEquals(previousAC, newAC);
				}
			}


		}
	}

	@Test
	public void testCorrectEnchantmentHelmet() 
	{
		vector_ItemListMetadata = itemMap.get(Constants.ItemType.Helmet.getItemId()).getItemListMetadata();

		scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());


		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{
			ItemListMetadata itemListMetadata = vector_ItemListMetadata.get(i);
			scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
			String itemType;


			if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Intelligence.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Intelligence.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getIntResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Helmet.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getIntResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));

			}
			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Wisdom.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Wisdom.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getWisResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Helmet.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getWisResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));

			}
			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.ArmorClass.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				int previousAC;
				bonusMetadata = characterMetadata.getBonusMetadata();
				if(!(bonusMetadata==null))
				{
					if(!bonusMetadata.getArmorClassBonus().equals(""))
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));
						System.out.println("Previous AC: " + previousAC);
					}
					else
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), 0);
					}

					backpackMetadata.setItemId(itemListMetadata.getList_item_id());
					backpackMetadata.setItemType(Constants.ItemType.Helmet.getItemId());
					controller.actionMoveToWearables(characterMetadata, backpackMetadata);
					int newAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));

					assertNotEquals(previousAC, newAC);
				}
			}


		}
	}

	@Test
	public void testCorrectEnchantmentArmor() 
	{
		vector_ItemListMetadata = itemMap.get(Constants.ItemType.Armor.getItemId()).getItemListMetadata();

		scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());


		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{
			ItemListMetadata itemListMetadata = vector_ItemListMetadata.get(i);
			scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
			String itemType;


			if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.ArmorClass.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				int previousAC;
				bonusMetadata = characterMetadata.getBonusMetadata();
				if(!(bonusMetadata==null))
				{
					if(!bonusMetadata.getArmorClassBonus().equals(""))
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));
						System.out.println("Previous AC: " + previousAC);
					}
					else
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), 0);
					}

					backpackMetadata.setItemId(itemListMetadata.getList_item_id());
					backpackMetadata.setItemType(Constants.ItemType.Armor.getItemId());
					controller.actionMoveToWearables(characterMetadata, backpackMetadata);
					int newAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));

					assertNotEquals(previousAC, newAC);
				}
			}


		}
	}

	@Test
	public void testCorrectEnchantmentBoots() 
	{
		vector_ItemListMetadata = itemMap.get(Constants.ItemType.Boots.getItemId()).getItemListMetadata();

		scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());


		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{
			ItemListMetadata itemListMetadata = vector_ItemListMetadata.get(i);
			scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
			String itemType;

			if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Dexterity.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Dexterity.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getDexResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Boots.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getDexResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));


			}

			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.ArmorClass.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				int previousAC;
				bonusMetadata = characterMetadata.getBonusMetadata();
				if(!(bonusMetadata==null))
				{
					if(!bonusMetadata.getArmorClassBonus().equals(""))
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));
						System.out.println("Previous AC: " + previousAC);
					}
					else
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), 0);
					}

					backpackMetadata.setItemId(itemListMetadata.getList_item_id());
					backpackMetadata.setItemType(Constants.ItemType.Boots.getItemId());
					controller.actionMoveToWearables(characterMetadata, backpackMetadata);
					int newAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));

					assertNotEquals(previousAC, newAC);
				}
			}


		}
	}

	@Test
	public void testCorrectEnchantmentBelt() 
	{
		vector_ItemListMetadata = itemMap.get(Constants.ItemType.Belt.getItemId()).getItemListMetadata();

		scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());


		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{
			ItemListMetadata itemListMetadata = vector_ItemListMetadata.get(i);
			scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
			String itemType;

			if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Consitution.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Consitution.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getConResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Belt.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getConResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));


			}

			else if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.Strength.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				itemType = EnchantmentType.Strength.getEnchantmentId();
				String previousAbScore = String.valueOf(scoreBonusMetadata.getStrResult());

				backpackMetadata.setItemId(itemListMetadata.getList_item_id());
				backpackMetadata.setItemType(Constants.ItemType.Belt.getItemId());
				controller.actionMoveToWearables(characterMetadata, backpackMetadata);
				scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
				String newAbScore = String.valueOf(scoreBonusMetadata.getStrResult());

				assertFalse(previousAbScore.equals(newAbScore));
				assertEquals(Integer.parseInt(previousAbScore) + Integer.parseInt(Bonus), Integer.parseInt(newAbScore));


			}
		}
	}


	@Test
	public void testCorrectEnchantmentShield() 
	{
		vector_ItemListMetadata = itemMap.get(Constants.ItemType.Shield.getItemId()).getItemListMetadata();

		scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());


		for(int i=0; i<vector_ItemListMetadata.size();i++)
		{
			ItemListMetadata itemListMetadata = vector_ItemListMetadata.get(i);
			scoreBonusMetadata = charUtility.parseScoreAndBonus(characterMetadata.getAbilityScoreMetadata(), characterMetadata.getBonusMetadata());
			String itemType;


			if(itemListMetadata.getList_item_enchanment().equals(EnchantmentType.ArmorClass.getEnchantmentId()))
			{
				String Bonus = itemListMetadata.getList_item_bonus();
				int previousAC;
				bonusMetadata = characterMetadata.getBonusMetadata();
				if(!(bonusMetadata==null))
				{
					if(!bonusMetadata.getArmorClassBonus().equals(""))
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));
						System.out.println("Previous AC: " + previousAC);
					}
					else
					{
						previousAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), 0);
					}

					backpackMetadata.setItemId(itemListMetadata.getList_item_id());
					backpackMetadata.setItemType(Constants.ItemType.Shield.getItemId());
					controller.actionMoveToWearables(characterMetadata, backpackMetadata);
					int newAC = charUtility.calulateArmorClass(scoreBonusMetadata.getDexBonus(), Integer.parseInt(bonusMetadata.getArmorClassBonus()));

					assertNotEquals(previousAC, newAC);
				}
			}

		}
	}
}
