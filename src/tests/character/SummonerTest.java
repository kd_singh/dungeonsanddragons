package tests.character;

import metadatas.CharacterMetadata;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import utilities.*;

import static org.junit.Assert.*;

/**
 * Created by dfz1537 on 3/22/2017.
 */
public class SummonerTest {

    Summoner summoner;
    CharacterMetadata characterMetadata;
    CharacterMetadata Builder;
    int str, dex, con, inte, wis, cha;
    Boolean condition;

    @Before
    public void setUp() throws Exception {
        summoner = new Summoner();
        characterMetadata = new CharacterMetadata();
    }

    public CharacterMetadata buildCharacter(CharacterBuilder builder){
        summoner.setCharacterBuilder(builder);
        summoner.setCharacterMetadata(characterMetadata);
        summoner.summonFighter();
        CharacterMetadata character = summoner.getCharacterMetadata();
        return character;
    }


    @After
    public void tearDown() throws Exception {

    }


    @Test
    public void testNimbleFighter() throws Exception{
        CharacterMetadata tankFighter = buildCharacter(new NimbleBuilder());
        str = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_strength());
        dex = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_dexterity());
        inte = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_intelligence());
        cha = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_charisma());
        wis = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_wisdom());
        con = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_constitution());
        if (dex>=con && con>=str && str>=inte && inte>=cha && cha>=wis){
            condition = true;
        }else
            condition = false;
        assertTrue(condition);
    }

    @Test
    public void testBullyFighter() throws Exception{
        CharacterMetadata tankFighter = buildCharacter(new BullyBuilder());
        str = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_strength());
        dex = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_dexterity());
        inte = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_intelligence());
        cha = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_charisma());
        wis = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_wisdom());
        con = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_constitution());
        if (str>=con && con>=dex && dex>=inte && inte>=cha && cha>=wis){
            condition = true;
        }else
            condition = false;
        assertTrue(condition);
    }

    @Test
    public void testTankFighter() throws Exception{
        CharacterMetadata tankFighter = buildCharacter(new TankBuilder());
        str = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_strength());
        dex = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_dexterity());
        inte = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_intelligence());
        cha = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_charisma());
        wis = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_wisdom());
        con = Integer.parseInt(tankFighter.getAbilityScoreMetadata().getAb_constitution());
        if (con>=dex && dex>=str && str>=inte && inte>=cha && cha>=wis){
            condition = true;
        }else
            condition = false;
        assertTrue(condition);
    }

}