package tests.item;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import Model.ItemModel;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import utilities.EnchantmentType;

public class itemCreateTest {

	ItemModel initialItemModel;
	ItemMetadata initialMetadata;
	ItemMetadata newItemMetadata;
	ItemListMetadata initialListMetadata;
	ItemListMetadata newItemListMetadata;
	
	Vector<ItemListMetadata> initialTotalItemListMetadatas;
	Vector<ItemListMetadata> newTotalItemListMetadatas;
	
	@Before
	public void itemSetup()
	{
		initialItemModel = new ItemModel();
		initialListMetadata = new ItemListMetadata();
		newItemListMetadata = new ItemListMetadata();
		initialMetadata = new ItemMetadata();
		newItemMetadata = new ItemMetadata();
	}
	
	@Test
	public void testActionAddItem()
	{
		//Getting the initial Items for item Type Helmet
		initialMetadata = initialItemModel.getItemMap().get("HELMET");
		initialTotalItemListMetadatas = initialMetadata.getItemListMetadata();
		
		//Creating a Item using ItemListMetadata
		initialListMetadata.setList_item_id(String.valueOf(initialTotalItemListMetadatas.size()+1));
		initialListMetadata.setList_item_enchanment(EnchantmentType.ArmorClass.getEnchantmentId());
		initialListMetadata.setList_item_bonus("1");
		initialListMetadata.setList_item_name("Test Item");
		
		int initialSize = initialTotalItemListMetadatas.size();
		
		//Adding a new Item in Items List
		initialItemModel.actionAddItem("HELMET", initialListMetadata);
		
		//Creating a new ItemModel to check the difference in count
		ItemModel newItemModel = new ItemModel();
		newItemMetadata = newItemModel.getItemMap().get("HELMET");
		newTotalItemListMetadatas = newItemMetadata.getItemListMetadata();

		int newSize = newTotalItemListMetadatas.size();
		
		assertEquals(initialSize, newSize-1);
		assertNotEquals(initialSize, newSize);
	}
	
	@Test
	public void testActionEditItem()
	{
		//Getting the initial Items for item Type Helmet
		initialMetadata = initialItemModel.getItemMap().get("HELMET");
		initialTotalItemListMetadatas = initialMetadata.getItemListMetadata();
		
		//Getting the first Helmet in the list
		initialListMetadata = initialTotalItemListMetadatas.get(0);
		
		//Setting new values for an existing item
		initialListMetadata.setList_item_name("TestingEditItems");
		initialListMetadata.setList_item_bonus("5");
		initialItemModel.actionEditItem("HELMET", initialListMetadata);
		
		//Creating a new ItemModel to check the difference in count
		ItemModel newItemModel = new ItemModel();
		newItemMetadata = newItemModel.getItemMap().get("HELMET");
		newTotalItemListMetadatas = newItemMetadata.getItemListMetadata();
		
		//Getting the first helmet in the list
		newItemListMetadata = newTotalItemListMetadatas.get(0);
		
		assertEquals(initialListMetadata.getList_item_bonus(), newItemListMetadata.getList_item_bonus());
		assertEquals(initialListMetadata.getList_item_name(), newItemListMetadata.getList_item_name());
		
		
	}

}
