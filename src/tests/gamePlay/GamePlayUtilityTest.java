package tests.gamePlay;

import metadatas.*;
import org.junit.Before;
import org.junit.Test;
import utilities.CharacterUtility;
import utilities.GamePlayUtility;

import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import static org.junit.Assert.*;

/**
 * Created by han on 2017-04-09.
 */
public class GamePlayUtilityTest {
    GamePlayUtility gamePlayUtility;
    CharacterMetadata playerMetadata, monsterMetadata, friendlyMetadata, zombieMetadata;
    CharacterTypeMetadata player, monster, friendly, zombie;
    Vector<CharacterTypeMetadata> characterMetadataVector;
    Map<Integer, CharacterTypeMetadata> characterTurn, sortedCharacterTurn;
    AbilityScoreMetadata score;
    AbilityModifierMetadata modifier;
    CharacterUtility characterUtility;
    BonusMetadata bonusMetadata;

    @Before
    public void setup(){
        characterUtility = new CharacterUtility();
        bonusMetadata = new BonusMetadata();
        score = characterUtility.randomAbilityScoreMetadata(6, 4);

        modifier = characterUtility.abilityScoreToAbilityModifier(score, bonusMetadata);
        gamePlayUtility = new GamePlayUtility();

        player = new CharacterTypeMetadata();
        playerMetadata = new CharacterMetadata();
        playerMetadata.setAbilityModifierMetadata(modifier);
        player.setCharacterMetadata(playerMetadata);

        monster = new CharacterTypeMetadata();
        monsterMetadata = new CharacterMetadata();
        monsterMetadata.setAbilityModifierMetadata(modifier);
        monster.setCharacterMetadata(monsterMetadata);

        friendly = new CharacterTypeMetadata();
        friendlyMetadata = new CharacterMetadata();
        friendlyMetadata.setAbilityModifierMetadata(modifier);
        friendly.setCharacterMetadata(friendlyMetadata);

        zombie = new CharacterTypeMetadata();
        zombieMetadata = new CharacterMetadata();
        zombieMetadata.setAbilityModifierMetadata(modifier);
        zombie.setCharacterMetadata(zombieMetadata);

        characterMetadataVector = new Vector<>();
        characterMetadataVector.add(player);
        characterMetadataVector.add(monster);
        characterMetadataVector.add(zombie);
        characterMetadataVector.add(friendly);

    }

    @Test
    public void turnGenerator() throws Exception {
        sortedCharacterTurn = gamePlayUtility.turnGenerator(characterMetadataVector);
        boolean result = false;
        Iterator it = sortedCharacterTurn.entrySet().iterator();
        Map.Entry entry1 = (Map.Entry)it.next();
        int key1 = (Integer)entry1.getKey();
        while (it.hasNext()) {
            Map.Entry entry2 = (Map.Entry)it.next();
            int key2 = (Integer)entry2.getKey();

            if (key1 > key2)
                result = true;
            else
                result = false;

            key1 = key2;
        }

        assertTrue(result);

    }

}