package main;

import java.awt.EventQueue;
import javax.swing.JFrame;

import View.CharacterCreateView;
import View.IndexView;


/**
 * This Class is used to initialize the main application
 * 
 * @author Han
 * @since 05 February 2017
 */
public class MainClass 
{
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					MainFrame.initialize();
					JFrame singleFrame = MainFrame.getInstance();
					singleFrame.add(new IndexView());
					singleFrame.setVisible(true);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}
}
