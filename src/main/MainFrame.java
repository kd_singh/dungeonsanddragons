package main;

import javax.swing.JFrame;
import javax.swing.WindowConstants;


/**
 * This JFrame Object is declared a singleton, this singleton JFrame can be called thoughout the whole application to repaint new panel as needed
 * 
 * @author Han
 * @since 05 February 2017
 * 
 */
public class MainFrame extends JFrame {
	
	public static MainFrame singleMainFrame;
	private static final String GAME_NAME = "DUNGEONS AND DRAGONS";
	
	private MainFrame(){
	}
	
	public static void initialize(){
		if (singleMainFrame == null){
			singleMainFrame = new MainFrame();
			singleMainFrame.setBounds(100, 100,1000,1000);
			singleMainFrame.setTitle(GAME_NAME);
			singleMainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		}
	}
	
	/**
	 * This method will return the one and only JFrame
	 * 
	 * @author Han
	 * @since 05 February 2017
	 * 
	 */
	public static MainFrame getInstance(){
		return singleMainFrame;
	}
}
