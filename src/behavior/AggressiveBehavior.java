package behavior;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import metadatas.CharacterMetadata;

/**
 * Created by dfz1537 on 3/21/2017.
 */
public class AggressiveBehavior extends Behavior{

    private static Logger log = LogManager.getLogger(AggressiveBehavior.class);

    /**
     * method decide what the aggressive NPC should do during it's turn
     */
    @Override
    public void executeAttack(CharacterMetadata attacker, CharacterMetadata opponent)
    {
    	if (isHit(attacker, opponent)) 
        {
            int damage = getDamage(attacker);
            int opponentHitPoint = Integer.parseInt(opponent.getHitPoints());

            opponentHitPoint -= damage;

            if (opponentHitPoint < 0) {
                //change the state of of the opponent ot dead
                log.info("Opponent is dead");
            }
            opponent.setHitPoints(String.valueOf(opponentHitPoint));
            log.info("Opponent's remaining hitpoint: " + opponentHitPoint);
        } 
    }

	@Override
	public void executeMove(int monsterX, int monsterY, int playerX, int playerY) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void executeLoot() {
		// TODO Auto-generated method stub
		
	}

}
