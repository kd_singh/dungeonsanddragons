package behavior;

public class BehaviorFactory 
{
	private BehaviorFactory(){};
	
	public static Behavior getBehaviour(String char_type)
	{
		if(char_type.equals("MONSTER"))
			return new AggressiveBehavior();
		else if(char_type.equals("FRIEND"))
			return new FriendlyBehavior();
		else if(char_type.equals("PLAYER"))
			return new HumanPlayerBehavior();
		
		return null;
	}
}
