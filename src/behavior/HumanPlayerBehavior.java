package behavior;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import metadatas.CharacterMetadata;

/**
 * Created by han on 2017-04-10.
 */
public class HumanPlayerBehavior extends Behavior {


    Logger log = LogManager.getLogger(FriendlyBehavior.class);

    /**
     * method decide what the Human Player should do during it's turn
     */

	@Override
	public void executeAttack(CharacterMetadata attacker, CharacterMetadata opponent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void executeMove() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void executeLoot() {
		// TODO Auto-generated method stub
		
	}
}
