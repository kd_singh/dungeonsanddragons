package behavior;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import metadatas.CharacterMetadata;

/**
 * Created by dfz1537 on 3/21/2017.
 */
public class ComputerPlayerBehavior extends Behavior {


    Logger log = LogManager.getLogger(ComputerPlayerBehavior.class);
    /**
     * method decide what the Computer player should do during it's turn
     */

	@Override
	public void executeAttack(CharacterMetadata attacker, CharacterMetadata opponent) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void executeMove() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void executeLoot() {
		// TODO Auto-generated method stub
		
	}
}
