package behavior;

import Model.DiceModel;
import Model.ItemModel;
import metadatas.CharacterMetadata;
import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import utilities.*;

import java.util.Map;
import java.util.Vector;

/**
 * Created by dfz1537 on 3/21/2017.
 */
public abstract class Behavior 
{

    private static Logger log = LogManager.getLogger(Behavior.class);

    DiceModel dice;
    CharacterUtility characterUtility;
    ItemModel itemModel;
    Map<String, ItemMetadata> itemMap;


    public Behavior() 
    {
        itemModel = new ItemModel();
        dice = new DiceModel();
        characterUtility = new CharacterUtility();
    }

    public abstract void executeAttack(CharacterMetadata attacker, CharacterMetadata opponent);
    
    public abstract void executeMove(int monX, int monY, int playX, int playY);

    public abstract void executeLoot();

    public boolean isHit(CharacterMetadata attacker, CharacterMetadata opponent){
        boolean hit;
        //Opponent Info
        int opponentAC = Integer.parseInt(opponent.getArmorClass());

        //Attacker Info
        int attackerLevel = Integer.parseInt(attacker.getCharacter_level());
        int attackerProficiencyBonus = characterUtility.getProficiencyBonus(attackerLevel);
        //attacker base bonus or always on bonus, obtain from the wearable that the character is equiping
        int attackerBaseAttackBonus = Integer.parseInt(attacker.getAttackBonus());
        int attackerStrBonus = Integer.parseInt(attacker.getAbilityModifierMetadata().getAbm_strength());
        int attackerDexBonus = Integer.parseInt(attacker.getAbilityModifierMetadata().getAbm_dexterity());



        //calculate attack roll
        int diceRoll = dice.rolld20();
        int attackRoll = attackerProficiencyBonus + attackerBaseAttackBonus + diceRoll;

        int weaponRange = getWeaponRange(attacker);

        //determine the type of the weapon
        if(weaponRange == 1){
            attackRoll += attackerStrBonus;
        }else{
            attackRoll += attackerDexBonus;
        }



        if(diceRoll == 1 || attackRoll <= opponentAC) {
            log.info("Attack bonus is less then Armor Class or the dice roll equal 0, it's a missed");
            return hit = false;
        }
        else {
            log.info("Attack bonus is larger then Armor Class or the dice roll equal 20, it's a hit");
            return hit = true;
        }

    }

    public int getDamage(CharacterMetadata attacker) 
    {
        int diceRoll = dice.rolld20();
        int damage;

        //Attacker Info
        int attackerLevel = Integer.parseInt(attacker.getCharacter_level());
        int attackerProficiencyBonus = characterUtility.getProficiencyBonus(attackerLevel);
        //attacker base bonus or always on bonus, obtain from the wearable that the character is equiping
        int attackerBaseAttackBonus = Integer.parseInt(attacker.getAttackBonus());
        int attackerStrBonus = Integer.parseInt(attacker.getAbilityModifierMetadata().getAbm_strength());
        int attackerDexBonus = Integer.parseInt(attacker.getAbilityModifierMetadata().getAbm_dexterity());

        int weaponRange = getWeaponRange(attacker);

        damage = diceRoll + attackerBaseAttackBonus;
        //if melee/range weapon then
        if (weaponRange == 1) {
            damage += attackerStrBonus;
        }else
            damage += attackerDexBonus;

        log.info("Opponent lose " + damage + " Hitpoint");
        return damage;
    }

    public int getWeaponRange(CharacterMetadata attacker){
        //get attacker carried weapon
        String weaponId = attacker.getCharacterWearableMetadata().getItm_weapon();
        ItemListMetaInterface weapon = itemModel.getItemListByID("WEAPON", weaponId);

        //get weapon range
        int weaponRange = Integer.parseInt(weapon.getRange());
        return weaponRange;
    }

    public void getEnchantment(CharacterMetadata attacker){
        String weaponId = attacker.getCharacterWearableMetadata().getItm_weapon();
        ItemListMetaInterface weapon = itemModel.getItemListByID("WEAPON", weaponId);

        String decoratedWeapon = weapon.getDecoraterWeapon();
        attacker.setWeapon(generateDecoratedMetadata(decoratedWeapon, new ItemListMetadata()));
    }

    public ItemListMetaInterface generateDecoratedMetadata(String decoratedWeapon, ItemListMetaInterface newItemMetadata)
    {
        String[] decorateHelperArray = decoratedWeapon.split(",");
        for(int i=decorateHelperArray.length-1;i>=0;i--)
        {
            switch(decorateHelperArray[i])
            {
                case "1":
                    newItemMetadata = new Freezing(newItemMetadata);
                    break;
                case "2":
                    newItemMetadata = new Burning(newItemMetadata);
                    break;
                case "3":
                    newItemMetadata = new Slaying(newItemMetadata);
                    break;
                case "4":
                    newItemMetadata = new Frightening(newItemMetadata);
                    break;
                case "5":
                    newItemMetadata = new Pacifying(newItemMetadata);
                    break;
                default:
                    break;
            }
        }

        return newItemMetadata;
    }

}
