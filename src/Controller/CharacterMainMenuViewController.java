package Controller;

import Model.CharacterModel;
import Model.ItemModel;
import View.CharacterCreateView;
import View.CharacterMainMenuView;
import View.IndexView;
import View.ItemCreateView;
import main.MainFrame;
import metadatas.AbilityModifierMetadata;
import metadatas.AbilityScoreMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterWearableMetadata;
import utilities.ListUtility;

import javax.swing.*;

/**
 * This is the controller class for Character Main menu screen. 
 * Listens to the property change events from the CharacterMainMenuView class.
 * 
 * @author Gurvinder Singh
 */

public class CharacterMainMenuViewController {
	
	/**
	 * Character Model containing all the details about a character.
	 */
	private CharacterModel characterModel;
	
	/**
	 * It is the single instance of the main display window.
	 */
	private JFrame singleMainFrame = MainFrame.getInstance();
	
	/**
	 * It is the instance of character create view.
	 */
	private CharacterCreateView characterCreateView;
	
	/**
	 * It is the character metadata for a single character.
	 */
	private CharacterMetadata characterMetadata;
	
	/**
	 * It specifies whether character creation is required or editing is required.
	 */
	private String mode;

	/**
	 * This is the main frame.
	 */
	private JFrame singleFrame; 
	
	/**
	 * It is a empty construction of the controller class.
	 */
	public CharacterMainMenuViewController()
	{
	}

	/**
	 * This constructor is used to set CharacterModel.
	 * 
	 * @param characterModel It is an instance of characterModel and contains details about all the characters in the system.
	 */
	public CharacterMainMenuViewController(CharacterModel characterModel)
	{
		this.characterModel = characterModel;
	}
	
	/**
	 * This method is called when an user click on create character.
	 */
	public void createCharacter() {
		//construct a new character
		mode="create";
		singleMainFrame.setBounds(0, 0, 1000, 800);
		characterMetadata = new CharacterMetadata();
		characterMetadata.setCharacter_level("1");
		ItemModel itemModel = new ItemModel();
		CharacterCreateViewController characterCreateViewController = new CharacterCreateViewController(characterMetadata, itemModel, characterModel);
		characterCreateView = new CharacterCreateView(characterMetadata, characterCreateViewController, mode, itemModel);
		characterMetadata.addObserver(characterCreateView);
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();

		singleMainFrame.add(characterCreateView);
		singleMainFrame.revalidate();

		characterCreateView.initialLoad("Create");
	}

	/**
	 * This is used to edit a character.
	 * 
	 * @param characterMetadata It is the metadata of the character selected for editing.
	 */
	public void editCharacter(CharacterMetadata characterMetadata)
	{
		
		mode = "edit";
		ItemModel itemModel = new ItemModel();
		CharacterCreateViewController characterCreateViewController = new CharacterCreateViewController(characterMetadata, itemModel, characterModel);
		characterCreateView = new CharacterCreateView(characterMetadata, characterCreateViewController, mode, itemModel);
		characterMetadata.addObserver(characterCreateView);
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		singleMainFrame.add(characterCreateView);
		singleMainFrame.setBounds(0, 0, 1000, 800);
		singleMainFrame.revalidate();
		characterCreateView.initialLoad("Edit");
	}
	
	/**
	 * This method is called on the characterMainView to go back to the Index View screen.
	 */
	public void actionCancel()
	{
		
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		// add Item Create View Panel
		singleMainFrame.add(new IndexView());
		singleMainFrame.revalidate();
	}
}
