package Controller;


import java.util.HashMap;
import java.util.Vector;

import javax.swing.JFrame;
import Model.CampaignModel;
import Model.CharacterModel;
import Model.ItemModel;
import Model.MapModel;
import View.CampaignMainMenuView;
import View.IndexView;
import View.MapCreateView;
import main.MainFrame;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterTypeMetadata;
import metadatas.MapMetadata;
import utilities.Grid;

/**
 * This Campaign Create View Controller contains all the methods corresponding
 * to all the operations that are performed in creating and editing Campaigns
 * 
 *
 * @author Tarandeep Singh
 * @version 1.0.0 
 * @since 07 February 2017
 * @see CampaignModel
 * @see CampaignMetadata
 */

public class CampaignCreateViewController 
{

	CampaignModel campaignModel;
	CampaignMetadata campaignMetadata;
	MapModel mapModel;
	
	/**
	 * Constructor with initializing campaignModel and campaignMetadata
	 * @param campaignModel Model for a campaign
	 * @param campaignMetadata This contains the data corresponding to campaign
	 * @param mapModel Model for map
	 * 
	 */
	public CampaignCreateViewController(CampaignModel campaignModel, CampaignMetadata campaignMetadata, MapModel mapModel) 
	{
		this.campaignModel = campaignModel;
		this.campaignMetadata = campaignMetadata;
		this.mapModel = mapModel;
	}
	
	/**
	 * Method to add a map in an existing Campaign
	 * @param mapMetadata This contains the data corresponding to maps
	 */
	public void actionAdd(MapMetadata mapMetadata)
	{
		campaignMetadata.actionAddMaps(mapMetadata);
	}
	
	/**
	 * Method to add a map in a new Campaign
	 * @param mapMetadata This contains the data corresponding to maps
	 * @param newCampaignName  and a new Campaign name
	 */
	public void actionAddToNewCampaign(MapMetadata mapMetadata, String newCampaignName)
	{
		campaignMetadata.setCampaign_name(newCampaignName);
		campaignMetadata.actionAddMaps(mapMetadata);
	}
	
	/**
	 * Method to delete map from a new campaign
	 * @param mapMetadata This contains the data corresponding to maps
	 * @param newCampaignName A new Campaign name 
	 */
	public void actionDeleteFromNewCampaign(MapMetadata mapMetadata, String newCampaignName)
	{
		campaignMetadata.setCampaign_name(newCampaignName);
		campaignMetadata.actionDeleteMaps(mapMetadata);
	}
	
	/**
	 * Method to delete map from an existing Campaign
	 * @param mapMetadata This contains the data corresponding to maps
	 */
	public void actionDelete(MapMetadata mapMetadata)
	{
		campaignMetadata.actionDeleteMaps(mapMetadata);
	}

	/**
	 * Method to Save a new or edited campaign
	 * @param newCampaignName A String to store new campaign name
	 */
	public void actionSave(String newCampaignName)
	{
		campaignMetadata.setCampaign_name(newCampaignName);
		campaignModel.actionSaveCampaign(campaignMetadata);
	}
	
	/**
	 * Method to create new map. It renders a new Map and calls <code>MapCreateViewController</code>
	 */
	public void actionCreateMap()
	{
		JFrame singleMainFrame = MainFrame.getInstance();
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		
		ItemModel itemModel = new ItemModel();
		CharacterModel characterModel = new CharacterModel();
		
		MapMetadata mapMetadata = new MapMetadata();
		Grid grid = new Grid(8);
		
		MapCreateViewController mapCreateController = new MapCreateViewController(itemModel,characterModel,mapModel, 
				mapMetadata, grid);
		MapCreateView mapCreateView = new MapCreateView(mapCreateController, grid, "");
		
		singleMainFrame.add(mapCreateView);
		singleMainFrame.revalidate();
	}
	
	/**
	 * Method to EDIT existing map. It renders existing selected Map and calls <code>MapCreateViewController</code>
	 * @param mapMetadata Map data of the existing selected Map
	 */
	public void actionEditMap(MapMetadata mapMetadata)
	{
		JFrame singleMainFrame = MainFrame.getInstance();
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		
		ItemModel itemModel = new ItemModel();
		CharacterModel characterModel = new CharacterModel();
		
		HashMap<String, CharacterMetadata> temp = null;
		Vector<CharacterTypeMetadata> vector_characterType = null
				;
		Grid grid = new Grid(mapMetadata, characterModel, temp, vector_characterType, "EDITMAP");
		
		MapCreateViewController mapCreateController = new MapCreateViewController(itemModel,characterModel,mapModel, 
				mapMetadata, grid);
		MapCreateView mapCreateView = new MapCreateView(mapCreateController, grid, mapMetadata.getMap_name());
		
		singleMainFrame.add(mapCreateView);
		singleMainFrame.revalidate();
	}
	
	/**
	 * This method is used go back to the campaign main menu screen
	 */
	public void actionCancel()
	{
		new IndexViewController().LoadCampaignMainMenu();
	}

}
