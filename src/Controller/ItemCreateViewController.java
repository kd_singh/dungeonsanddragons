package Controller;

import java.util.HashMap;

import javax.swing.JFrame;

import Model.ItemModel;
import View.IndexView;
import main.MainFrame;
import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadata;
import utilities.Burning;
import utilities.Freezing;
import utilities.Frightening;
import utilities.Pacifying;
import utilities.Slaying;

/**
 * This class is the Controller for creating and editing items.
 * 
 * @author Gurvinder Singh
 * @since 10 February 2017
 * @see ItemModel
 */

public class ItemCreateViewController {
	
	
	public ItemModel itemModel;
	
	/**
	 * It is the default constructor for the Item view.
	 */
	public ItemCreateViewController()
	{}

	/**
	 * It is a constructor of the class that initializes the Item model.
	 * 
	 * @param itemModel
	 */
	public ItemCreateViewController(ItemModel itemModel) 
	{
		this.itemModel = itemModel;
	}
	
	
	/**
	 * This method is used for saving an item in the items file and calls the appropriate function
	 * in the ItemModel class
	 * 
	 * @param itemType Describes the type of item i.e. Helmet, Armor etc.
	 * @param itemName It is the name of item to be created.
	 * @param itemEnchantment It the enchantment category that the item will effect.
	 * @param itemBonus It the amount of enchantment bonus that this item will have.
	 */
	public void actionSaveItem(String itemType, String itemName, String itemEnchantment, String itemBonus, String decoratedWeapon, String range)
	{
		System.out.println("In Save....");
		
		ItemListMetaInterface newItemMetadata = new ItemListMetadata();
		newItemMetadata.setList_item_name(itemName);
		newItemMetadata.setList_item_enchanment(itemEnchantment);
		newItemMetadata.setList_item_bonus(itemBonus);
		newItemMetadata.setRange(range);
		newItemMetadata.setDecoraterWeapon(decoratedWeapon);
		if(decoratedWeapon.equals("0"))
		{
			itemModel.actionAddItem(itemType, newItemMetadata);
		}
		else
		{
			ItemListMetaInterface decoratedNewItemMetadata = generateDecoratedMetadata(decoratedWeapon,newItemMetadata);
			HashMap<String, String> decoratedValues = decoratedNewItemMetadata.getWeaponEnchantmentType();
			System.out.println(decoratedValues.size());
			itemModel.actionAddItem(itemType, decoratedNewItemMetadata);
		}
	}
	
	public ItemListMetaInterface generateDecoratedMetadata(String decoratedWeapon, ItemListMetaInterface newItemMetadata)
	{
		String[] decorateHelperArray = decoratedWeapon.split(",");
		for(int i=decorateHelperArray.length-1;i>=0;i--)
		{
			switch(decorateHelperArray[i])
			{
			case "1":
				newItemMetadata = new Freezing(newItemMetadata);
				break;
			case "2":
				newItemMetadata = new Burning(newItemMetadata);
				break;
			case "3":
				newItemMetadata = new Slaying(newItemMetadata);
				break;
			case "4":
				newItemMetadata = new Frightening(newItemMetadata);
				break;
			case "5":
				newItemMetadata = new Pacifying(newItemMetadata);
				break;
			default:
				break;
			}
		}
		
		return newItemMetadata;
	}
	
	/**
	 * This method is used for editing an item in the items file and calls the appropriate function
	 * in the ItemModel class
	 * 
	 * @param editItemId It is the item id of the item to be edited.
	 * @param itemType Describes the type of item i.e. Helmet, Armor etc.
	 * @param itemName It is the name of item to be created.
	 * @param itemEnchantment It the enchantment category that the item will effect.
	 * @param itemBonus It the amount of enchantment bonus that this item will have.
	 */
	public void actionEditItem(String editItemId, String itemType, String itemName, String itemEnchantment, String itemBonus,String range, String decorated)
	{
		ItemListMetadata editItemListMetadata = new ItemListMetadata();
		editItemListMetadata.setList_item_id(editItemId);
		editItemListMetadata.setList_item_name(itemName);
		editItemListMetadata.setList_item_enchanment(itemEnchantment);
		editItemListMetadata.setList_item_bonus(itemBonus);
		editItemListMetadata.setRange(range);
		editItemListMetadata.setDecoraterWeapon(decorated);
		
		itemModel.actionEditItem(itemType, editItemListMetadata);
		
	}
	
	public void actionCancel()
	{
		JFrame singleMainFrame = MainFrame.getInstance();
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		// add Item Create View Panel
		singleMainFrame.add(new IndexView());
		singleMainFrame.revalidate();
	}
	

}
