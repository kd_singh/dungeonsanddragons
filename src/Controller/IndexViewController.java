package Controller;

import java.util.HashMap;
import javax.swing.JFrame;
import Model.CharacterModel;
import Model.CampaignModel;
import Model.ItemModel;
import Model.MapModel;
import Model.SessionModel;
import View.CampaignCreateView;
import View.CampaignMainMenuView;
import View.CharacterCreateView;
import View.CharacterMainMenuView;
import View.GamePlayView;
import View.ItemCreateView;
import View.LoadGameView;
import View.MapCreateView;
import main.MainFrame;
import metadatas.CharacterMetadata;
import metadatas.MapMetadata;
import utilities.Grid;

/**
 * This Controller is used to perform all the actions that can be done on the IndexView Window
 * 
 * @author Han
 * @since 05 February 2017
 * 
 */
public class IndexViewController {
	public static HashMap<String, MapMetadata> mapListData;
	public static HashMap<String, CharacterMetadata> characterListData;
	
	/**
	 * It is a singleton instance of the main frame.
	 */
	JFrame singleMainFrame = MainFrame.getInstance();

	/**
	 * This method loads the item create screen.
	 */
	public void LoadItemCreator(){
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();

		ItemModel itemModel = new ItemModel();
		ItemCreateViewController itemController = new ItemCreateViewController(itemModel);
		ItemCreateView itemView = new ItemCreateView(itemController);
		itemModel.addObserver(itemView);

		// add Item Create View Panel
		singleMainFrame.add(itemView);
		singleMainFrame.revalidate();
	}
	
	
	/** Called to Load the Map Create View.
	 * Initializes the Singleton Main Frame with all the required 
	 * Repaints the Main Frame to display the MapCreateView.
	 * 
	 * @author Vijay Karan Singh
	 * @param mapSize Size of the Map to Load
	 */
	public void LoadMapCreator(int mapSize){
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		
		ItemModel itemModel = new ItemModel();
		CharacterModel characterModel = new CharacterModel();
		MapModel mapModel = new MapModel();
		
		MapMetadata mapMetadata = new MapMetadata();
		Grid grid = new Grid(mapSize);
		
		MapCreateViewController mapCreateController = new MapCreateViewController(itemModel,characterModel,mapModel, 
				mapMetadata, grid);
		MapCreateView mapCreateView = new MapCreateView(mapCreateController, grid, "");
		// add Map Create View Panel
		singleMainFrame.add(mapCreateView);
		singleMainFrame.revalidate();
	}

	/**
	 * This method is used to load the campaign main menu view.
	 */
	public void LoadCampaignMainMenu(){

		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		
		MapModel mapModel = new MapModel();
		CharacterModel characterModel = new CharacterModel();
		
		CampaignModel campaignModel = new CampaignModel();
		CampaignMainMenuViewController campaignMainMenuViewController = new CampaignMainMenuViewController(campaignModel, mapModel, characterModel);
		CampaignMainMenuView campaignMainMenuView = new CampaignMainMenuView(campaignMainMenuViewController, campaignModel);
		
		singleMainFrame.add(campaignMainMenuView);
		singleMainFrame.revalidate();

	}

	/**
	 * This method is used to load character main menu.
	 */
	public void LoadCharacterMainMenu()
	{

		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		CharacterModel characterModel = new CharacterModel();
		CharacterMainMenuViewController characterMainMenuViewController = new CharacterMainMenuViewController(characterModel);
		CharacterMainMenuView characterMainMenuView = new CharacterMainMenuView(characterMainMenuViewController);
		singleMainFrame.add(characterMainMenuView);
		singleMainFrame.revalidate();
	}
	
	/**
	 * This method is test method for Play Game.
	 */
	public void LoadPlayGame(){
//		singleMainFrame.getContentPane().removeAll();
//		singleMainFrame.repaint();
//
//		
//		MapModel mapModel = new MapModel();
//		CharacterModel characterModel = new CharacterModel();
//		CharacterMetadata characterMetaData = new CharacterMetadata();
//		characterListData = characterModel.getCharacterMap();
//		characterMetaData = characterListData.get("3");
//		mapListData = mapModel.getParser();
//		//ItemCreateViewController itemController = new ItemCreateViewController(itemModel);
//		MapMetadata mapMetadata = mapListData.get("7");
//		String mapSize = mapMetadata.getMap_size();
//		int gameSize = Integer.parseInt(mapSize);
//		Grid grid = new Grid(mapMetadata, characterModel);
//		//GamePlayView gameView = new GamePlayView(grid, gameSize, characterMetaData);
//		//itemModel.addObserver(itemView);
//
//		// add Item Create View Panel
//		//singleMainFrame.add(gameView);
//		singleMainFrame.revalidate();
	}

	public void LoadLoadGame()
	{
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		
		SessionModel sessionModel = new SessionModel();
		CharacterModel characterModel = new CharacterModel();
		CampaignModel campaignModel = new  CampaignModel();
		LoadGameViewController loadGameViewViewController = new LoadGameViewController(sessionModel, campaignModel, characterModel);
		LoadGameView loadGameView = new LoadGameView(loadGameViewViewController, sessionModel);
		
		singleMainFrame.add(loadGameView);
		singleMainFrame.revalidate();

	}
}
