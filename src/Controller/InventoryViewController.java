package Controller;

import javax.swing.JFrame;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import Model.CharacterModel;
import Model.ItemModel;
import metadatas.AbilityModifierMetadata;
import metadatas.AbilityScoreMetadata;
import metadatas.BackpackMetadata;
import metadatas.BonusMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterWearableMetadata;
import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadata;
import utilities.CharacterUtility;

public class InventoryViewController 
{
	ItemModel itemModel;
	CharacterMetadata characterMetadata;
	JFrame singleMainFrame;
	private CharacterUtility characterUtility;
	public static final Logger log = LogManager.getLogger(CharacterCreateViewController.class);
	CharacterModel characterModel;
	public InventoryViewController()
	{
		itemModel = new ItemModel();
		characterUtility = new CharacterUtility();
		characterModel = new CharacterModel();
	}
	
	public void actionMoveToWearables(CharacterMetadata characterMetadata,BackpackMetadata backpackMetadata)
	{
		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();
		ItemListMetaInterface itemListMetadata = itemModel.getItemListByID(backpackMetadata.getItemType(), backpackMetadata.getItemId());
		
		setBonus(characterMetadata,bonusMetadata, itemListMetadata);
		characterMetadata.actionMoveToWearables(backpackMetadata);

	}
	
	public void actionMoveToBackPack(CharacterMetadata characterMetadata, BackpackMetadata backpackMetadata)
	{
		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();

		ItemListMetaInterface itemListMetadata = itemModel.getItemListByID(backpackMetadata.getItemType(), backpackMetadata.getItemId());

		reduceBonus(characterMetadata,bonusMetadata, itemListMetadata);
		characterMetadata.actionMoveToBackPack(backpackMetadata);
		
	}
	
	public void actionSwitchWithWearables(CharacterMetadata characterMetadata, CharacterMetadata friendsMetadata, BackpackMetadata playingCharBackpackMetadata)
	{
		BonusMetadata playingCharBonusMetadata = characterMetadata.getBonusMetadata();
		BonusMetadata friendsBonusMetadata = friendsMetadata.getBonusMetadata();

		ItemListMetaInterface playingCharItemListMetadata = itemModel.getItemListByID(playingCharBackpackMetadata.getItemType(), playingCharBackpackMetadata.getItemId());

		BackpackMetadata friendsBackPackMetadata = getFriendsRandomItem(friendsMetadata);
		
		if(friendsBackPackMetadata.getItemId() != null)
		{
			ItemListMetaInterface friendsItemListMetadata = itemModel.getItemListByID(friendsBackPackMetadata.getItemType(), friendsBackPackMetadata.getItemId());
			
			reduceBonus(characterMetadata, playingCharBonusMetadata, playingCharItemListMetadata);
			Boolean playinCharSetBonusFlag = characterMetadata.actionSwitchToWearables(friendsBackPackMetadata);
			characterMetadata.removeItemFromWearables(playingCharBackpackMetadata);
			
			if(playinCharSetBonusFlag)
				setBonus(characterMetadata, playingCharBonusMetadata, friendsItemListMetadata);
			
			reduceBonus(friendsMetadata, friendsBonusMetadata, friendsItemListMetadata);
			Boolean friendSetBonusFlag = friendsMetadata.actionSwitchToWearables(playingCharBackpackMetadata);
			friendsMetadata.removeItemFromWearables(friendsBackPackMetadata);
			
			if(friendSetBonusFlag)
				setBonus(friendsMetadata, friendsBonusMetadata, playingCharItemListMetadata);
			
			characterModel.actionSaveCharacter(characterMetadata);
			characterModel.actionSaveCharacter(friendsMetadata);
		}
		else
		{
			System.out.println("InventoryViewController : No Item Found in Friends Wearables to Switch !!");
		}
		
	}
	
	public BackpackMetadata getFriendsRandomItem(CharacterMetadata friendsMetadata)
	{
			CharacterWearableMetadata friendsWearable = friendsMetadata.getCharacterWearableMetadata();
			BackpackMetadata friendsBackPackMetadata = new BackpackMetadata();
			
			
			
			int ctr = 0;
			boolean flag = true;
			
			while(ctr < 10 && flag)
			{
				int range = (7 - 1) + 1;     
				int random = (int)(Math.random() * range) + 1;
				
				switch (random) 
				{
					case 1:
						
						if(!friendsWearable.getItm_helmet().equals(""))
						{
							String itemID = friendsWearable.getItm_helmet();
							
							friendsBackPackMetadata.setItemId(itemID);
							friendsBackPackMetadata.setItemType("HELMET");
							
							flag = false;
						}
						
						
						break;
						
					case 2:
						
						if(!friendsWearable.getItm_armor().equals(""))
						{
							String itemID = friendsWearable.getItm_armor();
							
							friendsBackPackMetadata.setItemId(itemID);
							friendsBackPackMetadata.setItemType("ARMOR");
							
							flag = false;
						}
						
						break;
						
					case 3:
						
						if(!friendsWearable.getItm_shield().equals(""))
						{
							String itemID = friendsWearable.getItm_shield();
							
							friendsBackPackMetadata.setItemId(itemID);
							friendsBackPackMetadata.setItemType("SHIELD");
							
							flag = false;

						}
				
						
						break;
						
					case 4:
						
						if(!friendsWearable.getItm_ring().equals(""))
						{
							String itemID = friendsWearable.getItm_ring();
							
							friendsBackPackMetadata.setItemId(itemID);
							friendsBackPackMetadata.setItemType("RING");
							
							flag = false;

						}
						
						break;
						
					case 5:
						
						if(!friendsWearable.getItm_belt().equals(""))
						{
							String itemID = friendsWearable.getItm_belt();
							
							friendsBackPackMetadata.setItemId(itemID);
							friendsBackPackMetadata.setItemType("BELT");
							
							flag = false;

						}
						
						break;
						
					case 6:
						
						if(!friendsWearable.getItm_boots().equals(""))
						{
							String itemID = friendsWearable.getItm_boots();
							
							friendsBackPackMetadata.setItemId(itemID);
							friendsBackPackMetadata.setItemType("BOOTS");
							
							flag = false;

						}
						
						
						break;
						
					case 7:
						
						if(!friendsWearable.getItm_weapon().equals(""))
						{
							String itemID = friendsWearable.getItm_weapon();
							
							friendsBackPackMetadata.setItemId(itemID);
							friendsBackPackMetadata.setItemType("WEAPON");
							
							flag = false;

						}
						
						break;
				
					default:
						break;
				}
				
				ctr++;
			}
			return friendsBackPackMetadata;
	}
	
	public void reduceBonus(CharacterMetadata characterMetadata,BonusMetadata bonusMetadata, ItemListMetaInterface itemListMetadata)
	{
		int selectedItemBonus = 0;
		int existingBonus = 0;
		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();

		switch (itemListMetadata.getList_item_enchanment().toUpperCase()) 
		{
		case "ARMORCLASS":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getArmorClassBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getArmorClassBonus()) - selectedItemBonus;
			}
			bonusMetadata.setArmorClassBonus(String.valueOf(existingBonus));
			int armorClass = Integer.parseInt(characterMetadata.getArmorClass());
			int newArmorClass = armorClass - Integer.parseInt(bonusMetadata.getArmorClassBonus());
			characterMetadata.setArmorClass(String.valueOf(newArmorClass));
			log.info("Total Armor Class: " + existingBonus);
			break;


		case "ATTACKBONUS":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getAttackBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getAttackBonus()) - selectedItemBonus;
			}
			bonusMetadata.setAttackBonus(String.valueOf(existingBonus));
			int oldAttackBonus = Integer.parseInt(characterMetadata.getAttackBonus());
			int newAttackBonus = oldAttackBonus - Integer.parseInt(bonusMetadata.getAttackBonus());
			characterMetadata.setAttackBonus(String.valueOf(newAttackBonus));
			log.info("Total Attack Bonus: " + existingBonus);
			break;

		case "DAMAGEBONUS":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getDamageBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getDamageBonus()) - selectedItemBonus;
			}
			bonusMetadata.setDamageBonus(String.valueOf(existingBonus));
			int oldDamageBonus = Integer.parseInt(characterMetadata.getDamageBonus());
			int newDamageBonus = oldDamageBonus - Integer.parseInt(bonusMetadata.getDamageBonus());
			characterMetadata.setDamageBonus(String.valueOf(newDamageBonus));
			log.info("Total Damage Bonus: " + existingBonus);
			break;

		case "STRENGTH":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getStrBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getStrBonus()) - selectedItemBonus;
			}
			bonusMetadata.setStrBonus(String.valueOf(existingBonus));
			log.info("Total Strength Bonus: " + existingBonus);
			break;

		case "DEXTERITY":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getDexBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getDexBonus()) - selectedItemBonus;
			}
			bonusMetadata.setDexBonus(String.valueOf(existingBonus));
			log.info("Total Dexterity Bonus: " + existingBonus);
			break;

		case "WISDOM":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getWisBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getWisBonus()) - selectedItemBonus;
			}
			bonusMetadata.setWisBonus(String.valueOf(existingBonus));
			log.info("Total Wisdom Bonus: " + existingBonus);
			break;

		case "CHARISMA":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getChaBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getChaBonus()) - selectedItemBonus;
			}
			bonusMetadata.setChaBonus(String.valueOf(existingBonus));
			log.info("Total Charisma Bonus: " + existingBonus);
			break;

		case "CONSTITUTION":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getConBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getConBonus()) - selectedItemBonus;
			}
			bonusMetadata.setConBonus(String.valueOf(existingBonus));
			log.info("Total Constitution Bonus: " + existingBonus);
			break;

		case "INTELLIGENCE":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getIntBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getIntBonus()) - selectedItemBonus;
			}
			bonusMetadata.setIntBonus(String.valueOf(existingBonus));
			log.info("Total Intelligence Bonus: " + existingBonus);
			break;
		}

		abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
		characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);
		
		characterMetadata.setCharacteristic();

	}
	
	public void setBonus(CharacterMetadata characterMetadata,BonusMetadata bonusMetadata, ItemListMetaInterface itemListMetadata)
	{
		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();

		int existingBonus=0;
		int newBonus;

		switch (itemListMetadata.getList_item_enchanment().toUpperCase()) 
		{
		case "ARMORCLASS":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getArmorClassBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getArmorClassBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setArmorClassBonus(String.valueOf(newBonus));
			int armorClass = characterUtility.calulateArmorClass(Integer.parseInt(abilityModifierMetadata.getAbm_dexterity()), Integer.parseInt(bonusMetadata.getArmorClassBonus()));
			characterMetadata.setArmorClass(String.valueOf(armorClass));
			log.info("Total Armor Class: " + newBonus);
			break;
		case "ATTACKBONUS":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getAttackBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getAttackBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setAttackBonus(String.valueOf(newBonus));
			int attackBonus = characterUtility.calculateAttackBonus((Integer.parseInt(characterMetadata.getCharacter_level())), Integer.parseInt(bonusMetadata.getAttackBonus()));
			characterMetadata.setAttackBonus(String.valueOf(attackBonus));
			log.info("Total Attack Bonus: " + newBonus);
			break;
		case "DAMAGEBONUS":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getDamageBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getDamageBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setDamageBonus(String.valueOf(newBonus));
			
			int damageBonus = characterUtility.calculateDamageBonus(Integer.parseInt(abilityModifierMetadata.getAbm_strength()), Integer.parseInt(bonusMetadata.getDamageBonus()));
			characterMetadata.setDamageBonus(String.valueOf(damageBonus));
			
			log.info("Total Damage Bonus: " + newBonus);
			break;
		case "STRENGTH":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getStrBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getStrBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setStrBonus(String.valueOf(newBonus));
			log.info("Total Strength Bonus: " + newBonus);
			break;
		case "DEXTERITY":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getDexBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getDexBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setDexBonus(String.valueOf(newBonus));
			log.info("Total Dexterity Bonus: " + newBonus);
			break;
		case "WISDOM":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getWisBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getWisBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setWisBonus(String.valueOf(newBonus));
			log.info("Total Wisdom Bonus: " + newBonus);
			break;
		case "CHARISMA":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getChaBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getChaBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setChaBonus(String.valueOf(newBonus));
			log.info("Total Charisma Bonus: " + newBonus);
			break;
		case "CONSTITUTION":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getConBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getConBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setConBonus(String.valueOf(newBonus));
			log.info("Total Constitution Bonus: " + newBonus);
			break;
		case "INTELLIGENCE":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getIntBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getIntBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setIntBonus(String.valueOf(newBonus));
			log.info("Total Intelligence Bonus: " + newBonus);
			break;
		}
		
		abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
		characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);
	}
	
	public boolean actionSave(CharacterMetadata characterMetadata, String charName)
	{
		characterMetadata.setCharacter_name(charName);
		boolean response = characterModel.actionSaveCharacter(characterMetadata);
		return response;
	}
}
