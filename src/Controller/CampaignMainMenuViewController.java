package Controller;

import java.util.HashMap;
import java.util.Vector;

import javax.swing.JFrame;
import Model.CampaignModel;
import Model.CharacterModel;
import Model.MapModel;
import View.CampaignCreateView;
import View.GamePlayView;
import View.IndexView;
import main.MainFrame;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterTypeMetadata;
import metadatas.MapMetadata;
import utilities.Grid;

/**
 * This Campaign Main Menu View Controller contains all the methods corresponding
 * to all the operations that are performed on Campaign Main Screen
 * 
 *
 * @author Tarandeep Singh
 * @version 1.0.0
 * @since 07 February 2017
 * @see CampaignModel
 */
public class CampaignMainMenuViewController
{

	CampaignModel campaignModel;
	MapModel mapModel;
	CharacterModel characterModel;
	public static HashMap<String, MapMetadata> mapListData;
	public HashMap<String, CharacterMetadata> mapCharacterList;
	Vector<CharacterTypeMetadata> vector_characterType;
	JFrame singleMainFrame;
	
	
	/**
	 * Constructor with initializing campaignModel
	 * @param campaignModel Model for Campaign
	 * @param mapModel Model for map
	 */
	public CampaignMainMenuViewController(CampaignModel campaignModel, MapModel mapModel, CharacterModel characterModel) 
	{
		this.campaignModel = campaignModel;
		this.mapModel = mapModel;
		this.characterModel = characterModel;
	}
	
	/**
	 * Method to Edit an existing Campaign, basically navigates to the Edit Campaign window
	 * @param campaignMetadata Corresponds to the meta data for a campaign
	 * @param actionType Takes the action from user 
	 */
	public void actionEdit(CampaignMetadata campaignMetadata, String actionType)
	{
		singleMainFrame = MainFrame.getInstance();

		CampaignCreateViewController campaignCreateViewController = new CampaignCreateViewController(campaignModel,
				campaignMetadata, mapModel);
		CampaignCreateView campaignCreateView = new CampaignCreateView(campaignCreateViewController, campaignModel,
				campaignMetadata, mapModel, actionType);
		
		campaignMetadata.addObserver(campaignCreateView);

		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		singleMainFrame.add(campaignCreateView);
		singleMainFrame.revalidate();
	}
	
	/**
	 * Method to Create a new Campaign, basically navigates to the Create a new Campaign window
	 * @param actionType Takes the action from user
	 */
	public void actionCreate(String actionType)
	{
		singleMainFrame = MainFrame.getInstance();

		CampaignMetadata campaignMetadata = new CampaignMetadata();
		CampaignCreateViewController campaignCreateViewController = new CampaignCreateViewController(campaignModel,
				campaignMetadata, mapModel);
		CampaignCreateView campaignCreateView = new CampaignCreateView(campaignCreateViewController, campaignModel, 
				campaignMetadata, mapModel, actionType);
		
		campaignMetadata.addObserver(campaignCreateView);
		
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		singleMainFrame.add(campaignCreateView);
		singleMainFrame.revalidate();
	}
	
	/**
	 * Method to Play the selected Campaign with selected character
	 */
	public void actionPlay(CampaignMetadata campaignMetadata, String actionType, CharacterMetadata characterMetadata)
	{
		singleMainFrame = MainFrame.getInstance();
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		MapModel mapModel = new MapModel();
		mapListData = mapModel.getParser();
		
		MapMetadata mapMetadata = mapListData.get(campaignMetadata.getVector_campaignMap().get(0).getMap_id());
		String mapSequence = campaignMetadata.getVector_campaignMap().get(0).getMap_sequence();
		String mapSize = mapMetadata.getMap_size();
		int gameSize = Integer.parseInt(mapSize);
		
		mapCharacterList = new HashMap<>();
		vector_characterType = new Vector();
		
		Grid grid = new Grid(mapMetadata, characterModel, mapCharacterList, vector_characterType, "PLAY");
		GamePlayViewController gameController = new GamePlayViewController(characterMetadata, characterModel);
		GamePlayView gameView = new GamePlayView(grid, gameSize, characterMetadata, campaignMetadata, gameController, mapSequence, 
				characterModel, mapCharacterList, vector_characterType);
		characterMetadata.addObserver(gameView);
		singleMainFrame.add(gameView);
		singleMainFrame.revalidate();
	}
	
	/**
	 * Method to cancel and move back to main screen
	 */
	public void actionCancel()
	{
		singleMainFrame = MainFrame.getInstance();
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		singleMainFrame.add(new IndexView());
		singleMainFrame.revalidate();
	}
}
