package Controller;

import java.util.HashMap;
import java.util.Vector;

import javax.swing.JFrame;
import Model.CampaignModel;
import Model.CharacterModel;
import Model.MapModel;
import Model.SessionModel;
import View.CampaignCreateView;
import View.GamePlayView;
import View.IndexView;
import main.MainFrame;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterTypeMetadata;
import metadatas.MapMetadata;
import metadatas.SessionMetadata;
import utilities.CharacterType;
import utilities.Grid;

/**
 * This Load Game View Controller contains all the methods corresponding
 * to all the operations that are performed on Load Game Screen
 * 
 *
 * @author Tarandeep Singh
 * @version 1.0.0
 * @since 09 April 2017
 */
public class LoadGameViewController {
	
	
	CampaignModel campaignModel;
	MapModel mapModel;
	CharacterModel characterModel;
	SessionModel sessionModel;
	
	public static HashMap<String, MapMetadata> mapListData;
	public HashMap<String, CharacterMetadata> mapCharacterList;
	Vector<CharacterTypeMetadata> vector_characterType;
	
	JFrame singleMainFrame;
	
	public LoadGameViewController(SessionModel sessionModel, CampaignModel campaignModel, CharacterModel characterModel)
	{
		this.sessionModel = sessionModel;
		this.campaignModel = campaignModel;
		this.characterModel = characterModel;
	}
	
	/**
	 * Method to Play the selected Session
	 */
	public void actionPlay(SessionMetadata sessionMetadata)
	{
		singleMainFrame = MainFrame.getInstance();
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		
		CampaignMetadata campaignMetadata = campaignModel.getCampaignMap().get(sessionMetadata.getCampaig_Id());
		
		MapMetadata mapMetadata = sessionMetadata.getMapMetadata();
		
		String mapSequence = sessionMetadata.getProgress_seq();
		String mapSize = mapMetadata.getMap_size();
		
		int gameSize = Integer.parseInt(mapSize);
		
		mapCharacterList = new HashMap<>();
		vector_characterType = new Vector();
		
		CharacterMetadata characterMetadata = null;
		
		Vector<CharacterTypeMetadata> vector_characterType = sessionMetadata.getVector_characterMetadata();
		
		for(int i = 0; i<vector_characterType.size(); i++)
		{
			if(vector_characterType.get(i).getType().equals(CharacterType.Player.toString()))
			{
				characterMetadata = vector_characterType.get(i).getCharacterMetadata();
			}
		}
		
		Grid grid = new Grid(mapMetadata, characterModel, mapCharacterList, vector_characterType, "LOADSESSION");
		GamePlayViewController gameController = new GamePlayViewController(characterMetadata, characterModel);
		GamePlayView gameView = new GamePlayView(grid, gameSize, characterMetadata, campaignMetadata, gameController, mapSequence, 
				characterModel, mapCharacterList, vector_characterType);
		characterMetadata.addObserver(gameView);
		singleMainFrame.add(gameView);
		singleMainFrame.revalidate();
	}
	
	/**
	 * Method to cancel and move back to main screen
	 */
	public void actionCancel()
	{
		singleMainFrame = MainFrame.getInstance();
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		singleMainFrame.add(new IndexView());
		singleMainFrame.revalidate();
	}

}
