package Controller;

import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Model.CharacterModel;
import Model.ItemModel;
import Model.MapModel;
import View.IndexView;
import View.MapCreateView;
import main.MainFrame;
import metadatas.GridCellMetadata;
import metadatas.MapMetadata;
import utilities.Grid;
import utilities.GridCell;

/**
 * This is the Controller Class for MapCreateView.
 * It helps to save the Map, Validate the Map, and Update the Model when required.
 * 
 * @author Vijay Karan Singh
 * @see MapModel
 * 
 */

public class MapCreateViewController
{
	/**
	 * Object of MapCreateView 
	 */
	MapCreateView view;
	
	/**
	 * Grid and gridCell Class Objects
	 */
	Grid grid;
	GridCell gridCell;
	
	/**
	 * Model Objects Used by the Controller
	 */
	public ItemModel itemModel;
	public CharacterModel characterModel;
	public MapModel mapModel;
	MapMetadata mapMetadata;
	
	JFrame singleMainFrame;
	
	/** Constructor for MapCreateViewController to initialize the Grid with user specific size.
	 * 
	 * @param size Map size to load
	 */
	public MapCreateViewController(int size)
	{
		this.grid = new Grid(size);
	}

	
	/**
	 * Constructor to initialize the Create Map View with respective Models.
	 * @param itemModel Item Model object to associate with Controller
	 * @param characterModel Character Model object to associate with Controller
	 * @param mapModel Map Model object to associate with Controller
	 * @param mapMetadata Mapmetadata object to associate with Controller
	 * @param grid Grid object to associate with Controller
	 */
	public MapCreateViewController(ItemModel itemModel,CharacterModel characterModel, MapModel mapModel,
			MapMetadata mapMetadata, Grid grid)
	{
		this.itemModel = itemModel;
		this.characterModel = characterModel;
		this.mapModel = mapModel;
		this.mapMetadata = mapMetadata;
		this.grid = grid;
		
	}
	
	/**
	 * Associates the View with the Controller.
	 * @param view View to associate with the controller
	 */
	public MapCreateViewController(MapCreateView view)
	{
		this.view = view;
	}
	
	/**
	 * Cancel the Map creation functionality. 
	 */
	public void cancel()
	{
		System.out.println("Operation cancelled");
	}
	
	/**
	 * Delete Item from Map Cell
	 */
	public void deleteItem()
	{
		System.out.println("Item Removed from Map!");
	}
	
	/**
	 * Function to Handle the Maps List Button Pressed.
	 * Redirect user to Map List Window. 
	 */
	public void showMapsList()
	{
		System.out.println("Maps List Page!!");
	}
	
	/**
	 * Function to be Removed after Testing
	 * @param e
	 */
	public void actionPerformed(ActionEvent e)
	{
		System.out.println("Hey");
		if(e.getActionCommand() == "Cancel"){
			System.out.println("Caught");
		}
	}

	/**
	 * Called to Update the Grid Cell with User Selected Item.
	 * 
	 * @param type Type of item to save in cell
	 * @param value Value of item to save in cell
	 * @param map_size Size of map to save in cell
	 */
	public void updateGridCell(String type, String value, String map_size)
	{
		GridCellMetadata gridCell = GridCell.responseCellData;
		gridCell.setType(type);
		gridCell.setValue(value);
		System.out.println("Selected Cell and Value - " + gridCell.getxPosition() + "," + gridCell.getyPosition() + " " + gridCell.getType()
		 + " " + gridCell.getValue());	
		grid.update_GridCellValue(gridCell.getxPosition(), gridCell.getyPosition(), value);
		grid.update_GridCellType(gridCell.getxPosition(), gridCell.getyPosition(), type);
	}
		
	/**
	 * Called to Handle the Save Map Operation for User.
	 * 
	 * @param map_name Name of Map
	 * @param size Size of map
	 */
	public boolean saveMap(String map_name, String size)
	{
		int allowedSave = 0;
		int allowedMonster = 0;
		for(int i=0; i< Integer.parseInt(size); i++){
			for(int j=0; j < Integer.parseInt(size); j++){
				if(grid.get_GridCellType(i, j) == "ENTRY" || grid.get_GridCellType(i, j) == "EXIT" ||
						grid.get_GridCellType(i, j) == "KEY"){
							allowedSave++;
				}
				if(grid.get_GridCellType(i, j) == "MONSTER"){
					allowedMonster++;
				}
			}
		}
		if(allowedSave < 3){
			System.out.println("There Must be an Entry, an Exit and a Key on the Map");
			return false;
		}
		else if(allowedMonster == 0){
			System.out.println("There Must be a Monster on the Map");
			
			return false;
		}
		else{
			mapMetadata = grid.getMapMetadata();
			mapMetadata.setMap_name(map_name);
			mapMetadata.setMap_size(size);
			
			mapModel.actionAddMap(mapMetadata);
			
			int res = JOptionPane.showOptionDialog(null, "Your Map has been saved successfully", "Confirmation", JOptionPane.DEFAULT_OPTION,
			        JOptionPane.INFORMATION_MESSAGE, null, null, null);
			if(res == JOptionPane.OK_OPTION)
			{
				new IndexViewController().LoadMapCreator(8);
			}	
			System.out.println("Voila!! Map Saved");
			System.out.println("Start Creating Another Map");
					return true;
		}	
	}
	
	
	/**
	 * Method to handle the Cancel Button Operation
	 */
	public void cancelOperation(){
		singleMainFrame = MainFrame.getInstance();
		singleMainFrame.getContentPane().removeAll();
		singleMainFrame.repaint();
		// add Item Create View Panel
		singleMainFrame.add(new IndexView());
		singleMainFrame.revalidate();
	}
}