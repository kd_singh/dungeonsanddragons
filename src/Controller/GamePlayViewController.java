package Controller;

import java.util.HashMap;
import java.util.Vector;

import javax.swing.JFrame;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import Model.CampaignModel;
import Model.CharacterModel;
import Model.ItemModel;
import Model.MapModel;
import Model.SessionModel;
import View.GamePlayView;
import View.IndexView;
import main.MainFrame;
import metadatas.AbilityModifierMetadata;
import metadatas.AbilityScoreMetadata;
import metadatas.BackpackMetadata;
import metadatas.BonusMetadata;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterTypeMetadata;
import metadatas.ItemListMetadata;
import metadatas.MapMetadata;
import metadatas.SessionMetadata;
import utilities.CharacterUtility;
import utilities.Grid;

public class GamePlayViewController 
{
	public static HashMap<String, MapMetadata> mapListData;
	public HashMap<String, CharacterMetadata> mapCharacterList;
	Vector<CharacterTypeMetadata> vector_characterType;
	
	CampaignModel campaignModel;
	MapModel mapModel;
	ItemModel itemModel;
	CharacterMetadata characterMetadata;
	CharacterModel characterModel;
	SessionModel sessionModel;
	
	JFrame singleMainFrame;
	
	private CharacterUtility characterUtility;
	public static final Logger log = LogManager.getLogger(CharacterCreateViewController.class);

	public void moveForward(){
		System.out.println("HiFrwrd");
	}
	
	public GamePlayViewController(CharacterMetadata characterMetadata, CharacterModel characterModel)
	{
		this.characterMetadata = characterMetadata;
		this.characterModel = characterModel;
		
		characterUtility = new CharacterUtility();
		itemModel = new ItemModel();
	}
	public GamePlayViewController(CampaignModel campaignModel, MapModel mapModel) 
	{
		characterUtility = new CharacterUtility();
		this.campaignModel = campaignModel;
		this.mapModel = mapModel;
		itemModel = new ItemModel();

	}
	
	public void nextLevel(CampaignMetadata campaignMetadata, CharacterMetadata characterMetadata, int mapSeq) {
		// TODO Auto-generated method stub
			
			singleMainFrame = MainFrame.getInstance();
			singleMainFrame.getContentPane().removeAll();
			singleMainFrame.repaint();
			MapModel mapModel = new MapModel();
			mapListData = mapModel.getParser();
			MapMetadata mapMetadata = mapListData.get(campaignMetadata.getVector_campaignMap().get(mapSeq-1).getMap_id());
			String mapSize = mapMetadata.getMap_size();
			int gameSize = Integer.parseInt(mapSize);
			
			mapCharacterList = new HashMap<>();
		    vector_characterType = new Vector<>();

			Grid grid = new Grid(mapMetadata, characterModel, mapCharacterList, vector_characterType, "PLAY");
			GamePlayView gameView = new GamePlayView(grid, gameSize, characterMetadata, campaignMetadata, this, 
					String.valueOf(mapSeq), characterModel, mapCharacterList, vector_characterType);
			singleMainFrame.add(gameView);
			singleMainFrame.revalidate();
		}

	public void actionComplete() {
		// TODO Auto-generated method stub
		
			singleMainFrame = MainFrame.getInstance();
			singleMainFrame.getContentPane().removeAll();
			singleMainFrame.repaint();
			singleMainFrame.add(new IndexView());
			singleMainFrame.revalidate();
	}
	
	public void actionSave(CampaignMetadata campaignMetadata, String progress_seq, 
			Vector<CharacterTypeMetadata> vector_characterType, Grid grid)
	{
		SessionMetadata sessionMetadata = new SessionMetadata();
		
		sessionMetadata.setCampaign_id(campaignMetadata.getCampaign_id());
		sessionMetadata.setSession_name(campaignMetadata.getCampaign_name());
		sessionMetadata.setProgress_seq(progress_seq);
		sessionMetadata.setVector_characterMetadata(vector_characterType);
		sessionMetadata.setMapMetadata(grid.getMapMetadata());
		
		sessionModel = new SessionModel();
		
		
		if(sessionModel.actionSaveSession(sessionMetadata))
		{
			singleMainFrame = MainFrame.getInstance();
			singleMainFrame.getContentPane().removeAll();
			singleMainFrame.repaint();
			// add Item Create View Panel
			singleMainFrame.add(new IndexView());
			singleMainFrame.revalidate();
		}
		
	}
	
//	public void actionMoveToWearables(CharacterMetadata characterMetadata,BackpackMetadata backpackMetadata)
//	{
//		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();
//		ItemListMetadata itemListMetadata = itemModel.getItemListByID(backpackMetadata.getItemType(), backpackMetadata.getItemId());
//		
//		setBonus(characterMetadata,bonusMetadata, itemListMetadata);
//		characterMetadata.actionMoveToWearables(backpackMetadata);
//
//	}
//	
//	public void actionMoveToBackPack(CharacterMetadata characterMetadata, BackpackMetadata backpackMetadata)
//	{
//		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();
//
//		ItemListMetadata itemListMetadata = itemModel.getItemListByID(backpackMetadata.getItemType(), backpackMetadata.getItemId());
//
//		reduceBonus(characterMetadata,bonusMetadata, itemListMetadata);
//		characterMetadata.actionMoveToBackPack(backpackMetadata);
//		
//	}
//	
//	public void reduceBonus(CharacterMetadata characterMetadata,BonusMetadata bonusMetadata, ItemListMetadata itemListMetadata)
//	{
//		int selectedItemBonus = 0;
//		int existingBonus = 0;
//		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
//		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();
//
//		switch (itemListMetadata.getList_item_enchanment().toUpperCase()) 
//		{
//		case "ARMORCLASS":
//
//			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getArmorClassBonus().equals(""))
//			{
//				existingBonus = Integer.parseInt(bonusMetadata.getArmorClassBonus()) - selectedItemBonus;
//			}
//			bonusMetadata.setArmorClassBonus(String.valueOf(existingBonus));
//			int armorClass = Integer.parseInt(characterMetadata.getArmorClass());
//			int newArmorClass = armorClass - Integer.parseInt(bonusMetadata.getArmorClassBonus());
//			characterMetadata.setArmorClass(String.valueOf(newArmorClass));
//			log.info("Total Armor Class: " + existingBonus);
//			break;
//
//
//		case "ATTACKBONUS":
//
//			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getAttackBonus().equals(""))
//			{
//				existingBonus = Integer.parseInt(bonusMetadata.getAttackBonus()) - selectedItemBonus;
//			}
//			bonusMetadata.setAttackBonus(String.valueOf(existingBonus));
//			int oldAttackBonus = Integer.parseInt(characterMetadata.getAttackBonus());
//			int newAttackBonus = oldAttackBonus - Integer.parseInt(bonusMetadata.getAttackBonus());
//			characterMetadata.setAttackBonus(String.valueOf(newAttackBonus));
//			log.info("Total Attack Bonus: " + existingBonus);
//			break;
//
//		case "DAMAGEBONUS":
//
//			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getDamageBonus().equals(""))
//			{
//				existingBonus = Integer.parseInt(bonusMetadata.getDamageBonus()) - selectedItemBonus;
//			}
//			bonusMetadata.setDamageBonus(String.valueOf(existingBonus));
//			int oldDamageBonus = Integer.parseInt(characterMetadata.getDamageBonus());
//			int newDamageBonus = oldDamageBonus - Integer.parseInt(bonusMetadata.getDamageBonus());
//			characterMetadata.setDamageBonus(String.valueOf(newDamageBonus));
//			log.info("Total Damage Bonus: " + existingBonus);
//			break;
//
//		case "STRENGTH":
//
//			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getStrBonus().equals(""))
//			{
//				existingBonus = Integer.parseInt(bonusMetadata.getStrBonus()) - selectedItemBonus;
//			}
//			bonusMetadata.setStrBonus(String.valueOf(existingBonus));
//			log.info("Total Strength Bonus: " + existingBonus);
//			break;
//
//		case "DEXTERITY":
//
//			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getDexBonus().equals(""))
//			{
//				existingBonus = Integer.parseInt(bonusMetadata.getDexBonus()) - selectedItemBonus;
//			}
//			bonusMetadata.setDexBonus(String.valueOf(existingBonus));
//			log.info("Total Dexterity Bonus: " + existingBonus);
//			break;
//
//		case "WISDOM":
//
//			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getWisBonus().equals(""))
//			{
//				existingBonus = Integer.parseInt(bonusMetadata.getWisBonus()) - selectedItemBonus;
//			}
//			bonusMetadata.setWisBonus(String.valueOf(existingBonus));
//			log.info("Total Wisdom Bonus: " + existingBonus);
//			break;
//
//		case "CHARISMA":
//
//			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getChaBonus().equals(""))
//			{
//				existingBonus = Integer.parseInt(bonusMetadata.getChaBonus()) - selectedItemBonus;
//			}
//			bonusMetadata.setChaBonus(String.valueOf(existingBonus));
//			log.info("Total Charisma Bonus: " + existingBonus);
//			break;
//
//		case "CONSTITUTION":
//
//			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getConBonus().equals(""))
//			{
//				existingBonus = Integer.parseInt(bonusMetadata.getConBonus()) - selectedItemBonus;
//			}
//			bonusMetadata.setConBonus(String.valueOf(existingBonus));
//			log.info("Total Constitution Bonus: " + existingBonus);
//			break;
//
//		case "INTELLIGENCE":
//
//			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getIntBonus().equals(""))
//			{
//				existingBonus = Integer.parseInt(bonusMetadata.getIntBonus()) - selectedItemBonus;
//			}
//			bonusMetadata.setIntBonus(String.valueOf(existingBonus));
//			log.info("Total Intelligence Bonus: " + existingBonus);
//			break;
//		}
//
//		abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
//		characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);
//		
//		characterMetadata.setCharacteristic();
//
//	}
//
//	
//	public void setBonus(CharacterMetadata characterMetadata,BonusMetadata bonusMetadata, ItemListMetadata itemListMetadata)
//	{
//		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
//		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();
//
//		int existingBonus=0;
//		int newBonus;
//
//		switch (itemListMetadata.getList_item_enchanment().toUpperCase()) 
//		{
//		case "ARMORCLASS":
//			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getArmorClassBonus().equals("")){
//				existingBonus = Integer.parseInt(bonusMetadata.getArmorClassBonus());
//			}
//			newBonus += existingBonus;
//			bonusMetadata.setArmorClassBonus(String.valueOf(newBonus));
//			int armorClass = characterUtility.calulateArmorClass(Integer.parseInt(abilityModifierMetadata.getAbm_dexterity()), Integer.parseInt(bonusMetadata.getArmorClassBonus()));
//			characterMetadata.setArmorClass(String.valueOf(armorClass));
//			log.info("Total Armor Class: " + newBonus);
//			break;
//		case "ATTACKBONUS":
//			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getAttackBonus().equals("")){
//				existingBonus = Integer.parseInt(bonusMetadata.getAttackBonus());
//			}
//			newBonus += existingBonus;
//			bonusMetadata.setAttackBonus(String.valueOf(newBonus));
//			int attackBonus = characterUtility.calculateAttackBonus((Integer.parseInt(characterMetadata.getCharacter_level())), Integer.parseInt(bonusMetadata.getAttackBonus()));
//			characterMetadata.setAttackBonus(String.valueOf(attackBonus));
//			log.info("Total Attack Bonus: " + newBonus);
//			break;
//		case "DAMAGEBONUS":
//			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getDamageBonus().equals("")){
//				existingBonus = Integer.parseInt(bonusMetadata.getDamageBonus());
//			}
//			newBonus += existingBonus;
//			bonusMetadata.setDamageBonus(String.valueOf(newBonus));
//			
//			int damageBonus = characterUtility.calculateDamageBonus(Integer.parseInt(abilityModifierMetadata.getAbm_strength()), Integer.parseInt(bonusMetadata.getDamageBonus()));
//			characterMetadata.setDamageBonus(String.valueOf(damageBonus));
//			
//			log.info("Total Damage Bonus: " + newBonus);
//			break;
//		case "STRENGTH":
//			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getStrBonus().equals("")){
//				existingBonus = Integer.parseInt(bonusMetadata.getStrBonus());
//			}
//			newBonus += existingBonus;
//			bonusMetadata.setStrBonus(String.valueOf(newBonus));
//			log.info("Total Strength Bonus: " + newBonus);
//			break;
//		case "DEXTERITY":
//			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getDexBonus().equals("")){
//				existingBonus = Integer.parseInt(bonusMetadata.getDexBonus());
//			}
//			newBonus += existingBonus;
//			bonusMetadata.setDexBonus(String.valueOf(newBonus));
//			log.info("Total Dexterity Bonus: " + newBonus);
//			break;
//		case "WISDOM":
//			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getWisBonus().equals("")){
//				existingBonus = Integer.parseInt(bonusMetadata.getWisBonus());
//			}
//			newBonus += existingBonus;
//			bonusMetadata.setWisBonus(String.valueOf(newBonus));
//			log.info("Total Wisdom Bonus: " + newBonus);
//			break;
//		case "CHARISMA":
//			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getChaBonus().equals("")){
//				existingBonus = Integer.parseInt(bonusMetadata.getChaBonus());
//			}
//			newBonus += existingBonus;
//			bonusMetadata.setChaBonus(String.valueOf(newBonus));
//			log.info("Total Charisma Bonus: " + newBonus);
//			break;
//		case "CONSTITUTION":
//			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getConBonus().equals("")){
//				existingBonus = Integer.parseInt(bonusMetadata.getConBonus());
//			}
//			newBonus += existingBonus;
//			bonusMetadata.setConBonus(String.valueOf(newBonus));
//			log.info("Total Constitution Bonus: " + newBonus);
//			break;
//		case "INTELLIGENCE":
//			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
//			if (!bonusMetadata.getIntBonus().equals("")){
//				existingBonus = Integer.parseInt(bonusMetadata.getIntBonus());
//			}
//			newBonus += existingBonus;
//			bonusMetadata.setIntBonus(String.valueOf(newBonus));
//			log.info("Total Intelligence Bonus: " + newBonus);
//			break;
//		}
//		
//		abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
//		characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);
//	}
//	
	
	public void callObservers(CharacterMetadata characterMetadata)
	{
		characterMetadata.sendNotification();
	}

	public void incrementAndSave(String characterID)
	{
		characterModel.incrementLevel(characterID);
	}
}

	
