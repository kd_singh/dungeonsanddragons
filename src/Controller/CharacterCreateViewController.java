package Controller;

import java.util.HashMap;
import java.util.Vector;
import metadatas.*;
import org.apache.log4j.*;
import Model.CharacterModel;
import Model.ItemModel;
import View.CharacterCreateView;
import utilities.*;

/**
 * This class is the controller for the CharacterCreateView 
 * 
 * @author Han
 * @author Gurvinder Singh
 * @author Karan Deep Singh
 * @see	CharacterCreateView
 */

public class CharacterCreateViewController
{
	/**
	 * It is an instance of the CharacterUtility class.
	 */
	private CharacterUtility characterUtility;

	/**
	 * It is an object for the CharacterMetadata class.
	 */
	public CharacterMetadata characterMetadata;

	/**
	 * It is an object for the items functionality.
	 */
	private ItemModel itemModel;

	/**
	 * It is an object for the character functionality.
	 */
	private CharacterModel characterModel;

	/**
	 * Instance of the Apache Log4J.
	 */
	public static final Logger log = LogManager.getLogger(CharacterCreateViewController.class);

	/**
	 * It is the constructor for the CharacterCreateViewController class
	 * 
	 * @param characterMetadata It contains all the details about an character.
	 * @param itemModel It contains details about all the items in the system.
	 * @param characterModel It contains details about all the characters in the system. 
	 */
	public CharacterCreateViewController(CharacterMetadata characterMetadata, ItemModel itemModel, CharacterModel characterModel)
	{
		characterUtility = new CharacterUtility();
		this.characterMetadata = characterMetadata;
		this.itemModel = itemModel;
		this.characterModel = characterModel;
	}

	/**
	 * This is a controller function that is invoked when cancel button is clicked on the view screen.
	 */
	public void actionBackToMain()
	{
		new IndexViewController().LoadCharacterMainMenu();
	}

	/**
	 * This is a controller function which is called to check if the item is present in the wearables of the
	 * character.
	 * 
	 * @param item It is an object of ItemListMetadata containing all the details about 
	 * @param itemName It is the name of the item that will be checked in the character Wearable.
	 * @return returns a true if item is present in character wearables list
	 */
	public boolean isItemWearable(ItemListMetadata item, String itemName)
	{
		boolean response = false;

		if(!isItemBackPack(item, itemName))
		{
			CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();

			switch (itemName) 
			{
			case "HELMET":
				if(characterWearableMetadata.getItm_helmet().equals(""))
				{
					characterWearableMetadata.setItm_helmet(item.getList_item_id());
					response = true;
				}
				else
				{	
					if(characterWearableMetadata.getItm_helmet().equals(item.getList_item_id()))
					{
						response = true;
					}
				}
				break;

			case "ARMOR":
				if(characterWearableMetadata.getItm_armor().equals(""))
				{
					characterWearableMetadata.setItm_armor(item.getList_item_id());
					response = true;
				}
				else
				{	
					if(characterWearableMetadata.getItm_armor().equals(item.getList_item_id()))
					{
						response = true;
					}
				}
				break;

			case "RING":
				if(characterWearableMetadata.getItm_ring().equals(""))
				{
					characterWearableMetadata.setItm_ring(item.getList_item_id());
					response = true;
				}
				else
				{	
					if(characterWearableMetadata.getItm_ring().equals(item.getList_item_id()))
					{
						response = true;
					}
				}
				break;

			case "BOOTS":
				if(characterWearableMetadata.getItm_boots().equals(""))
				{
					characterWearableMetadata.setItm_boots(item.getList_item_id());
					response = true;
				}
				else
				{	
					if(characterWearableMetadata.getItm_boots().equals(item.getList_item_id()))
					{
						response = true;
					}
				}
				break;

			case "BELT":
				if(characterWearableMetadata.getItm_belt().equals(""))
				{
					characterWearableMetadata.setItm_belt(item.getList_item_id());
					response = true;
				}
				else
				{	
					if(characterWearableMetadata.getItm_belt().equals(item.getList_item_id()))
					{
						response = true;
					}
				}
				break;

			case "SHIELD":
				if(characterWearableMetadata.getItm_shield().equals(""))
				{
					characterWearableMetadata.setItm_shield(item.getList_item_id());
					response = true;
				}
				else
				{	
					if(characterWearableMetadata.getItm_shield().equals(item.getList_item_id()))
					{
						response = true;
					}
				}
				break;

			case "WEAPON":
				if(characterWearableMetadata.getItm_weapon().equals(""))
				{
					characterWearableMetadata.setItm_weapon(item.getList_item_id());
					response = true;
				}
				else
				{	
					if(characterWearableMetadata.getItm_weapon().equals(item.getList_item_id()))
					{
						response = true;
					}
				}
				break;

			default:
				break;
			}
		}

		return response;
	}

	/**
	 * This is a controller function which is called to check if the item is present in the backpack of the
	 * character.
	 * 
	 * @param item It is an object of ItemListMetadata containing all the details about 
	 * @param itemName It is the name of the item that will be checked in the character backpack.
	 * @return returns a true if item is present in character backpack list
	 */
	public boolean isItemBackPack(ItemListMetadata item, String itemName)
	{
		Vector<BackpackMetadata> vector_backMetadata = characterMetadata.getBackpackMetadata();

		if(vector_backMetadata == null)
			return false;

		if(vector_backMetadata != null)
		{
			for(int i = 0; i < vector_backMetadata.size() ; i++)
			{
				if((vector_backMetadata.get(i).getItemId().equals(item.getList_item_id())) &&
						(vector_backMetadata.get(i).getItemType().equals(itemName)))
				{
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * This method is transferred control when an item is selected from the items dropdown combo-box in the 
	 * character create view screen.
	 * 
	 * @param itemListMetadatas It is the Hashmap of ItemId and ItemListMetdatas that are selected from the items combo-box at character create view screen.
	 * @param item It is an object of ItemListMetadata containing all the details about 
	 * @param itemName It is the name of the item that is selected.
	 */

	public void comboBoxAction(HashMap<String, ItemListMetadata> itemListMetadatas, ItemListMetadata item, String itemName)
	{
		CharacterWearableMetadata characterWearableMetadata = characterMetadata.getCharacterWearableMetadata();
		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();
		BonusMetadata bonusMetadata = new BonusMetadata();
		Vector<BackpackMetadata> vector_backMetadata = characterMetadata.getBackpackMetadata();

		if(!isItemWearable(item, itemName))
		{
			BackpackMetadata backpackMetadata = new BackpackMetadata();
			boolean flag = false;
			backpackMetadata.setItemId(item.getList_item_id());
			backpackMetadata.setItemType(itemName);

			if(vector_backMetadata == null)
			{
				vector_backMetadata = new Vector<>();
			}
			if(vector_backMetadata.size() < 10)
			{
				if(vector_backMetadata != null )
				{
					for(int i = 0; i < vector_backMetadata.size() ; i++)
					{
						if((vector_backMetadata.get(i).getItemId().equals(item.getList_item_id())) &&
								(vector_backMetadata.get(i).getItemType().equals(itemName)))
						{
							flag = true;
							break;
						}
					}

					if(!flag)
					{
						vector_backMetadata.add(backpackMetadata);
						characterMetadata.setBackpackMetadata(vector_backMetadata);
					}
				}
//				else
//				{
//					vector_backMetadata.add(backpackMetadata);
//					characterMetadata.setBackpackMetadata(vector_backMetadata);
//				}
			}
		}


		if(!characterWearableMetadata.getItm_helmet().equals(""))
		{
			String itemID = characterWearableMetadata.getItm_helmet();
			ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("HELMET", itemID);
			setBonus(bonusMetadata, itemListMetadata);
		}

		if(!characterWearableMetadata.getItm_armor().equals(""))
		{
			String itemID = characterWearableMetadata.getItm_armor();
			ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("ARMOR", itemID);
			setBonus(bonusMetadata, itemListMetadata);
		}

		if(!characterWearableMetadata.getItm_ring().equals(""))
		{
			String itemID = characterWearableMetadata.getItm_ring();
			ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("RING", itemID);
			setBonus(bonusMetadata, itemListMetadata);
		}

		if(!characterWearableMetadata.getItm_boots().equals(""))
		{
			String itemID = characterWearableMetadata.getItm_boots();
			ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("BOOTS", itemID);
			setBonus(bonusMetadata, itemListMetadata);
		}

		if(!characterWearableMetadata.getItm_belt().equals(""))
		{
			String itemID = characterWearableMetadata.getItm_belt();
			ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("BELT", itemID);
			setBonus(bonusMetadata, itemListMetadata);
		}

		if(!characterWearableMetadata.getItm_shield().equals(""))
		{
			String itemID = characterWearableMetadata.getItm_shield();
			ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("SHIELD", itemID);
			setBonus(bonusMetadata, itemListMetadata);
		}

		if(!characterWearableMetadata.getItm_weapon().equals(""))
		{
			String itemID = characterWearableMetadata.getItm_weapon();
			ItemListMetaInterface itemListMetadata = itemModel.getItemListByID("WEAPON", itemID);
			setBonus(bonusMetadata, itemListMetadata);
		}


		characterMetadata.setAbilityScoreMetadata(abilityScoreMetadata);
		characterMetadata.setBonusMetadata(bonusMetadata);

		characterMetadata.setCharacteristic();
		characterMetadata.sendNotification();

	}

	/**
	 * Use Builder pattern to generate a charactermetadata object based on the selected fighter type
     * @param fighterType
     */
	public void generate(String fighterType)
	{
        Summoner summoner = new Summoner();
        CharacterBuilder builder = null;
        switch (fighterType.toUpperCase()) {
            case "BULLY":
                builder = new BullyBuilder();
                break;
            case "NIMBLE":
                builder = new NimbleBuilder();
                break;
            case "TANK":
                builder = new TankBuilder();
                break;
        }
        //tell summoner the correct builder
        summoner.setCharacterBuilder(builder);
        //give characterMetadata to summoner so it can tell the builder to build upon that charactermetadata
        summoner.setCharacterMetadata(characterMetadata);
        summoner.summonFighter();
		log.info("Generate button pressed");
		//update the model
		int hitPoints;
		characterMetadata.setCharacteristic();
		hitPoints = characterUtility.calculateHitPoint(Integer.parseInt(characterMetadata.getCharacter_level()), Integer.parseInt(characterMetadata.getAbilityModifierMetadata().getAbm_constitution()));
		characterMetadata.setHitPoints(String.valueOf(hitPoints));
		characterMetadata.sendNotification();

	}

	/**
	 * Gets the updated hit points from the characterUtility class when Random button is clicked.
	 * @param level It is the level of the character.
	 */
	public void getUpdatedHitPoints(String level)
	{	
		characterMetadata.setCharacter_level(level);
		int hitPoints = characterUtility.calculateHitPoint(Integer.parseInt(level), Integer.parseInt(characterMetadata.getAbilityScoreMetadata().getAb_constitution()));
		characterMetadata.setHitPoints(String.valueOf(hitPoints));
		characterMetadata.setCharacteristic();
		characterMetadata.sendNotification();
	}

	/**
	 * perform random value generation for selected ability text fields
	 * 
	 * @param currentFocus It contains the name of current selected text field on the character create view 
	 */
	public void randomValue(String currentFocus)
	{
		AbilityMetadata abilityMetadata;
		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();
		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();
		abilityMetadata =  characterUtility.randomAbilityMetadata(abilityScoreMetadata, abilityModifierMetadata,bonusMetadata, currentFocus);
		characterMetadata.setAbilityScoreMetadata(abilityMetadata.getAbilityScoreMetadata());
		characterMetadata.setAbilityModifierMetadata(abilityMetadata.getAbilityModifierMetadata());
		if(currentFocus=="con")
		{
			getUpdatedHitPoints(characterMetadata.getCharacter_level());
		}
		characterMetadata.setCharacteristic();
		characterMetadata.sendNotification();

	}

	/**
	 * This method removes an item from the character's backpack.
	 * 
	 * @param backpackMetadata It is the character BackpackMetadata that contains list of items in character's backpack.
	 * @return It returns true when an item is removed from backpack.
	 */
	public boolean actionRemoveFromBackpack(BackpackMetadata backpackMetadata)
	{
		boolean isRemoved;
		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();

		ItemListMetaInterface itemListMetadata = itemModel.getItemListByID(backpackMetadata.getItemType(), backpackMetadata.getItemId());


		isRemoved = characterMetadata.actionRemovefromBackpack(backpackMetadata);

		characterMetadata.sendNotification();

		return isRemoved;
	}

	/**
	 * This method moves an item to backpack on selection.
	 * 
	 * @param backpackMetadata It contains the item details for the item that is to be removed from backpack.
	 */
	public void actionMoveToBackPack(BackpackMetadata backpackMetadata)
	{
		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();

		ItemListMetaInterface itemListMetadata = itemModel.getItemListByID(backpackMetadata.getItemType(), backpackMetadata.getItemId());

		reduceBonus(bonusMetadata, itemListMetadata);
		characterMetadata.actionMoveToBackPack(backpackMetadata);
		
	}

	/**
	 * This method is called when a user wants to move his item to wearables from backpack.
	 * 
	 * @param backpackMetadata It contains the item details for the item that is to be added to wearables.
	 */
	public void actionMoveToWearables(BackpackMetadata backpackMetadata)
	{
		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();
		ItemListMetaInterface itemListMetadata = itemModel.getItemListByID(backpackMetadata.getItemType(), backpackMetadata.getItemId());
		
		setBonus(bonusMetadata, itemListMetadata);
		characterMetadata.actionMoveToWearables(backpackMetadata);

	}

	/**
	 * This method is called when a user wants to move his item to wearables from backpack.
	 * 
	 * @param characterMetadata
	 * @param backpackMetadata
	 */
	public void actionMoveToWearables(CharacterMetadata characterMetadata, BackpackMetadata backpackMetadata)
	{
		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();
		ItemListMetaInterface itemListMetadata = itemModel.getItemListByID(backpackMetadata.getItemType(), backpackMetadata.getItemId());

		setBonus(bonusMetadata, itemListMetadata);
		characterMetadata.actionMoveToWearables(backpackMetadata);

	}

	/**
	 * This method is used to setBonus based on item selected. Controlled is passed on through the view.
	 * 
	 * @param bonusMetadata It contains the details about the bonus that a player has.
	 * @param itemListMetadata It contains the details about the item list selected.
	 */
	public void setBonus(BonusMetadata bonusMetadata, ItemListMetaInterface itemListMetadata)
	{
		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();

		int existingBonus=0;
		int newBonus;

		switch (itemListMetadata.getList_item_enchanment().toUpperCase()) 
		{
		case "ARMORCLASS":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getArmorClassBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getArmorClassBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setArmorClassBonus(String.valueOf(newBonus));
			int armorClass = characterUtility.calulateArmorClass(Integer.parseInt(abilityModifierMetadata.getAbm_dexterity()), Integer.parseInt(bonusMetadata.getArmorClassBonus()));
			characterMetadata.setArmorClass(String.valueOf(armorClass));
			log.info("Total Armor Class: " + newBonus);
			break;
		case "ATTACKBONUS":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getAttackBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getAttackBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setAttackBonus(String.valueOf(newBonus));
			int attackBonus = characterUtility.calculateAttackBonus((Integer.parseInt(characterMetadata.getCharacter_level())), Integer.parseInt(bonusMetadata.getAttackBonus()));
			characterMetadata.setAttackBonus(String.valueOf(attackBonus));
			log.info("Total Attack Bonus: " + newBonus);
			break;
		case "DAMAGEBONUS":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getDamageBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getDamageBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setDamageBonus(String.valueOf(newBonus));
			
			int damageBonus = characterUtility.calculateDamageBonus(Integer.parseInt(abilityModifierMetadata.getAbm_strength()), Integer.parseInt(bonusMetadata.getDamageBonus()));
			characterMetadata.setDamageBonus(String.valueOf(damageBonus));
			
			log.info("Total Damage Bonus: " + newBonus);
			break;
		case "STRENGTH":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getStrBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getStrBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setStrBonus(String.valueOf(newBonus));
			log.info("Total Strength Bonus: " + newBonus);
			break;
		case "DEXTERITY":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getDexBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getDexBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setDexBonus(String.valueOf(newBonus));
			log.info("Total Dexterity Bonus: " + newBonus);
			break;
		case "WISDOM":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getWisBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getWisBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setWisBonus(String.valueOf(newBonus));
			log.info("Total Wisdom Bonus: " + newBonus);
			break;
		case "CHARISMA":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getChaBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getChaBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setChaBonus(String.valueOf(newBonus));
			log.info("Total Charisma Bonus: " + newBonus);
			break;
		case "CONSTITUTION":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getConBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getConBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setConBonus(String.valueOf(newBonus));
			log.info("Total Constitution Bonus: " + newBonus);
			break;
		case "INTELLIGENCE":
			newBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getIntBonus().equals("")){
				existingBonus = Integer.parseInt(bonusMetadata.getIntBonus());
			}
			newBonus += existingBonus;
			bonusMetadata.setIntBonus(String.valueOf(newBonus));
			log.info("Total Intelligence Bonus: " + newBonus);
			break;
		}
		
		abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
		characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);
	}

	/**
	 * This method is used to reduceBonus based on item selected. Controlled is passed on through the view.
	 * 
	 * @param bonusMetadata It contains the details about the bonus that a player has.
	 * @param itemListMetadata It contains the details about the item list selected.
	 */

	public void reduceBonus(BonusMetadata bonusMetadata, ItemListMetaInterface itemListMetadata)
	{
		int selectedItemBonus = 0;
		int existingBonus = 0;
		AbilityScoreMetadata abilityScoreMetadata = characterMetadata.getAbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = characterMetadata.getAbilityModifierMetadata();

		switch (itemListMetadata.getList_item_enchanment().toUpperCase()) 
		{
		case "ARMORCLASS":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getArmorClassBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getArmorClassBonus()) - selectedItemBonus;
			}
			bonusMetadata.setArmorClassBonus(String.valueOf(existingBonus));
			int armorClass = Integer.parseInt(characterMetadata.getArmorClass());
			int newArmorClass = armorClass - Integer.parseInt(bonusMetadata.getArmorClassBonus());
			characterMetadata.setArmorClass(String.valueOf(newArmorClass));
			log.info("Total Armor Class: " + existingBonus);
			break;


		case "ATTACKBONUS":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getAttackBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getAttackBonus()) - selectedItemBonus;
			}
			bonusMetadata.setAttackBonus(String.valueOf(existingBonus));
			int oldAttackBonus = Integer.parseInt(characterMetadata.getAttackBonus());
			int newAttackBonus = oldAttackBonus - Integer.parseInt(bonusMetadata.getAttackBonus());
			characterMetadata.setAttackBonus(String.valueOf(newAttackBonus));
			log.info("Total Attack Bonus: " + existingBonus);
			break;

		case "DAMAGEBONUS":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getDamageBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getDamageBonus()) - selectedItemBonus;
			}
			bonusMetadata.setDamageBonus(String.valueOf(existingBonus));
			int oldDamageBonus = Integer.parseInt(characterMetadata.getDamageBonus());
			int newDamageBonus = oldDamageBonus - Integer.parseInt(bonusMetadata.getDamageBonus());
			characterMetadata.setDamageBonus(String.valueOf(newDamageBonus));
			log.info("Total Damage Bonus: " + existingBonus);
			break;

		case "STRENGTH":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getStrBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getStrBonus()) - selectedItemBonus;
			}
			bonusMetadata.setStrBonus(String.valueOf(existingBonus));
			log.info("Total Strength Bonus: " + existingBonus);
			break;

		case "DEXTERITY":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getDexBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getDexBonus()) - selectedItemBonus;
			}
			bonusMetadata.setDexBonus(String.valueOf(existingBonus));
			log.info("Total Dexterity Bonus: " + existingBonus);
			break;

		case "WISDOM":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getWisBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getWisBonus()) - selectedItemBonus;
			}
			bonusMetadata.setWisBonus(String.valueOf(existingBonus));
			log.info("Total Wisdom Bonus: " + existingBonus);
			break;

		case "CHARISMA":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getChaBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getChaBonus()) - selectedItemBonus;
			}
			bonusMetadata.setChaBonus(String.valueOf(existingBonus));
			log.info("Total Charisma Bonus: " + existingBonus);
			break;

		case "CONSTITUTION":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getConBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getConBonus()) - selectedItemBonus;
			}
			bonusMetadata.setConBonus(String.valueOf(existingBonus));
			log.info("Total Constitution Bonus: " + existingBonus);
			break;

		case "INTELLIGENCE":

			selectedItemBonus = Integer.parseInt(itemListMetadata.getList_item_bonus());
			if (!bonusMetadata.getIntBonus().equals(""))
			{
				existingBonus = Integer.parseInt(bonusMetadata.getIntBonus()) - selectedItemBonus;
			}
			bonusMetadata.setIntBonus(String.valueOf(existingBonus));
			log.info("Total Intelligence Bonus: " + existingBonus);
			break;
		}

		abilityModifierMetadata = characterUtility.abilityScoreToAbilityModifier(abilityScoreMetadata, bonusMetadata);
		characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);
		
		characterMetadata.setCharacteristic();

	}

	/**
	 * This method is used to save the charcter.
	 * 
	 * @param characterMetadata It contains the character details for saving or editing a character.
	 * @param charName It is the name of the character.
	 * @return It returns a true message when an item is saved in the file.
	 */
	public boolean actionSave(CharacterMetadata characterMetadata, String charName)
	{
		characterMetadata.setCharacter_name(charName);
		boolean response = characterModel.actionSaveCharacter(characterMetadata);
		return response;
	}
}
