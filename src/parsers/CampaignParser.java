package parsers;

import java.util.HashMap;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;
import Model.CampaignModel;
import metadatas.CampaignMapsMetadata;
import metadatas.CampaignMetadata;
import utilities.Constants;
import utilities.FileHandlerUtility;

/**
 * Parser file that fetches all the content from the Campaign File
 * and gets it into a JSON Array form which it is converted into
 * a HashMap for Campaigns.
 * 
 * @author Tarandeep Singh
 * @author Karan Deep Singh
 */
public class CampaignParser 
{
	/**
	 * Constant to store the name of the Campaign file.
	 */
	private String filename =  Constants.CAMPAIGN_FILE_NAME;
	
	/**
	 * Reference variable to <code>FileHandlerUtility</code> for file handling 
	 * operations like reading or writing.
	 */
	private FileHandlerUtility fileHandler = new FileHandlerUtility();
	
	/**
	 * This reads the content in <code>String</code> format from 
	 * the specified file.
	 * 
	 * @param fileName Name of the input Campaign file
	 * @return result String content read from the file
	 */
	public String getJSONString(String fileName)
	{
		String result = "";
		try
		{
			result = fileHandler.readAllData(fileName);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Parses the input String from the file into JSONArray and then convert it to 
	 * HashMap to send it to <code>CampaignModel</code>
	 * @return HashMap of the type <code>campaign_id</code> <code>CampaignMetadata</code>
	 */
	public HashMap<String, CampaignMetadata> parseCampaign()
	{
		String response = getJSONString(filename);
		Vector<CampaignMetadata> vector_campaignMetadata = new Vector<CampaignMetadata>();
		CampaignMetadata campaignMetadata = null;
		HashMap<String, CampaignMetadata> campaignMap = null;
		
		try 
		{
			JSONArray outerJSON = new JSONArray(response.trim());

			if (outerJSON != null && outerJSON.length() > 0)
			{

				for (int j = 0; j < outerJSON.length(); j++)
				{

					campaignMetadata = new CampaignMetadata();
					JSONObject jsonObject = outerJSON.getJSONObject(j);

					try
					{
						String campaign_id = jsonObject.getString("campaign_id");
						campaignMetadata.setCampaign_id(campaign_id);

						String campaign_name = jsonObject.getString("campaign_name");
						campaignMetadata.setCampaign_name(campaign_name);

						String campaign_size = jsonObject.getString("campaign_size");
						campaignMetadata.setCampaign_size(campaign_size);
						
						JSONArray innerJSON = jsonObject.getJSONArray("campaign_maps"); 
						Vector<CampaignMapsMetadata> vectorCampaignMaps = new Vector<CampaignMapsMetadata>();
						
						if (innerJSON != null && innerJSON.length() > 0)
						{
							for (int k = 0; k < innerJSON.length(); k++)
							{
								CampaignMapsMetadata campaignMaps = new CampaignMapsMetadata();
								JSONObject innerJsonObject = innerJSON.getJSONObject(k);

								try
								{
									String map_id = innerJsonObject.getString("map_id");
									campaignMaps.setMap_id(map_id);

									String map_name = innerJsonObject.getString("map_name");
									campaignMaps.setMap_name(map_name);

									String map_sequence = innerJsonObject.getString("map_sequence");
									campaignMaps.setMap_sequence(map_sequence);
									
									vectorCampaignMaps.add(campaignMaps);
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
							}
						}
						
						campaignMetadata.setVector_campaignMap(vectorCampaignMaps); 
						vector_campaignMetadata.add(campaignMetadata);
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					
				}
			}
			
			campaignMap = convertToMap(vector_campaignMetadata);
		}	
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return campaignMap;
	}
	
	/**
	 * Converts the input <code>vector_campaignMetadata</code> to the HashMap to send 
	 * it to <code>CampaignModel</code>
	 * @param vector_campaignMetadata Vector that has list of <code>CampaignMetadata</code>
	 * @return convertToMap HashMap of the type <code>campaign_id</code> <code>CampaignMetadata</code>
	 */
	public HashMap<String, CampaignMetadata> convertToMap(Vector<CampaignMetadata> vector_campaignMetadata)
	{
		HashMap<String, CampaignMetadata> campaignMap = new HashMap<String, CampaignMetadata>();
		
		for(int i = 0; i<vector_campaignMetadata.size(); i++)
		{

			CampaignMetadata campaign_Model = vector_campaignMetadata.get(i);
			campaignMap.put(campaign_Model.getCampaign_id(), campaign_Model);
		}
		
		return campaignMap;
	}
	
}