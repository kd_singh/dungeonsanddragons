package parsers;


import java.util.HashMap;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import metadatas.AbilityModifierMetadata;
import metadatas.AbilityScoreMetadata;
import metadatas.BackpackMetadata;
import metadatas.BonusMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterWearableMetadata;
import utilities.Constants;
import utilities.FileHandlerUtility;

/**
 * Parser file that fetches all the content from the Character File
 * and gets it into a JSON Array form which it is converted into
 * a HashMap for Character.
 * 
 * @author Gurvinder Singh
 * @author Karan Deep Singh
 */

public class CharacterParser 
{
	/**
	 * Constant to store the name of the Campaign file.
	 */
	private String fileName = Constants.CHARACTER_FILE_NAME;
	
	/**
	 * Reference variable to <code>FileHandlerUtility</code> for file handling 
	 * operations like reading or writing.
	 */
	private FileHandlerUtility fileHandler = new FileHandlerUtility();
	
	/**
	 * This method extracts the JSON data from the character file and stores it in a String variable
	 * @param fileName the name of the character file
	 * @return result the Json string extracted from the text file
	 */
	public String getJSONString(String fileName)
	{
		String result = "";
		try
		{
			result = fileHandler.readAllData(fileName);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	
	/**
	 * This method takes in the JSON string extracted using getJSONString method above and 
	 * stores the result in CharacterModel object.
	 * @return characterMap - the HashMap that maps characterID with the characterObject
	 */
	public HashMap<String, CharacterMetadata> parseCharacter()
	{
		String response = getJSONString(fileName);
		Vector<CharacterMetadata> characterLists = new Vector<CharacterMetadata>();
		CharacterMetadata characterMetadata = null;
		HashMap<String, CharacterMetadata> characterMap = null;
		
		try 
		{
			JSONArray outerJSON = new JSONArray(response.trim());

			if (outerJSON != null && outerJSON.length() > 0)
			{

				for (int j = 0; j < outerJSON.length(); j++)
				{


					characterMetadata = new CharacterMetadata();
					JSONObject jsonObject = outerJSON.getJSONObject(j);

					try
					{
						String character_id = jsonObject.getString("character_id");
						characterMetadata.setCharacter_id(character_id);

						String character_name = jsonObject.getString("character_name");
						characterMetadata.setCharacter_name(character_name);

						String character_level = jsonObject.getString("character_level");
						characterMetadata.setCharacter_level(character_level);

						AbilityScoreMetadata abilityScoreMetadata = new AbilityScoreMetadata();
						JSONObject ab_scoreObj = jsonObject.getJSONObject("ability_scores");
						
						String ab_strength = ab_scoreObj.getString("ab_strength");
						abilityScoreMetadata.setAb_strength(ab_strength);

						String ab_dexterity = ab_scoreObj.getString("ab_dexterity");
						abilityScoreMetadata.setAb_dexterity(ab_dexterity);

						String ab_constitution = ab_scoreObj.getString("ab_constitution");
						abilityScoreMetadata.setAb_constitution(ab_constitution);

						String ab_intelligence = ab_scoreObj.getString("ab_intelligence");
						abilityScoreMetadata.setAb_intelligence(ab_intelligence);

						String ab_wisdom = ab_scoreObj.getString("ab_wisdom");
						abilityScoreMetadata.setAb_wisdom(ab_wisdom);

						String ab_charisma = ab_scoreObj.getString("ab_charisma");
						abilityScoreMetadata.setAb_charisma(ab_charisma);

						characterMetadata.setAbilityScoreMetadata(abilityScoreMetadata);
						
						BonusMetadata bonusMetadata = new BonusMetadata();
						JSONObject bonus_ScoreMetadata = jsonObject.getJSONObject("item_bonus");
						
						String dex_bonus = bonus_ScoreMetadata.getString("dex_bonus") ;
						bonusMetadata.setDexBonus(dex_bonus);
						
						String int_bonus = bonus_ScoreMetadata.getString("int_bonus") ;
						bonusMetadata.setIntBonus(int_bonus);
						
						String str_bonus = bonus_ScoreMetadata.getString("str_bonus") ;
						bonusMetadata.setStrBonus(str_bonus);
						
						String wis_bonus = bonus_ScoreMetadata.getString("wis_bonus") ;
						bonusMetadata.setWisBonus(wis_bonus);
						
						String con_bonus = bonus_ScoreMetadata.getString("con_bonus") ;
						bonusMetadata.setConBonus(con_bonus);
						
						String cha_bonus = bonus_ScoreMetadata.getString("cha_bonus") ;
						bonusMetadata.setChaBonus(cha_bonus);
						
						String itm_armor_bonus = bonus_ScoreMetadata.getString("itm_armor_bonus") ;
						bonusMetadata.setArmorClassBonus(itm_armor_bonus);
						
						String itm_attack_bonus = bonus_ScoreMetadata.getString("itm_attack_bonus") ;
						bonusMetadata.setAttackBonus(itm_attack_bonus);
						
						String itm_damage_bonus = bonus_ScoreMetadata.getString("itm_damage_bonus") ;
						bonusMetadata.setDamageBonus(itm_damage_bonus);
						
						characterMetadata.setBonusMetadata(bonusMetadata);

						JSONObject abm_scoreObj = jsonObject.getJSONObject("ability_modifier");
						AbilityModifierMetadata abilityModifierMetadata = new AbilityModifierMetadata();

						String abm_strength;
						abm_strength = abm_scoreObj.getString("abm_strength");
						abilityModifierMetadata.setAbm_strength(abm_strength);

						String abm_dexterity;
						abm_dexterity = abm_scoreObj.getString("abm_dexterity");
						abilityModifierMetadata.setAbm_dexterity(abm_dexterity);

						String abm_constitution;
						abm_constitution = abm_scoreObj.getString("abm_constitution");
						abilityModifierMetadata.setAbm_constitution(abm_constitution);

						String abm_intelligence;
						abm_intelligence = abm_scoreObj.getString("abm_intelligence");
						abilityModifierMetadata.setAbm_intelligence(abm_intelligence);

						String abm_wisdom;
						abm_wisdom = abm_scoreObj.getString("abm_wisdom");
						abilityModifierMetadata.setAbm_wisdom(abm_wisdom);

						String abm_charisma;
						abm_charisma = abm_scoreObj.getString("abm_charisma");
						abilityModifierMetadata.setAbm_charisma(abm_charisma);

						characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);

						JSONObject character_wearableObj = jsonObject.getJSONObject("character_wearable");
						CharacterWearableMetadata characterWearableMetadata = new CharacterWearableMetadata();

						String itm_helmet;
						itm_helmet = character_wearableObj.getString("itm_helmet");
						characterWearableMetadata.setItm_helmet(itm_helmet);

						String itm_armor;
						itm_armor = character_wearableObj.getString("itm_armor");
						characterWearableMetadata.setItm_armor(itm_armor);

						String itm_shield;
						itm_shield = character_wearableObj.getString("itm_shield");
						characterWearableMetadata.setItm_shield(itm_shield);

						String itm_ring;
						itm_ring = character_wearableObj.getString("itm_ring");
						characterWearableMetadata.setItm_ring(itm_ring);

						String itm_belt; 
						itm_belt = character_wearableObj.getString("itm_belt");
						characterWearableMetadata.setItm_belt(itm_belt);

						String itm_boots;
						itm_boots = character_wearableObj.getString("itm_boots");
						characterWearableMetadata.setItm_boots(itm_boots);

						String itm_weapon;
						itm_weapon = character_wearableObj.getString("itm_weapon");
						characterWearableMetadata.setItm_weapon(itm_weapon);

						characterMetadata.setCharacterWearableMetadata(characterWearableMetadata);
						
						JSONArray innerJSON = jsonObject.getJSONArray("backpack_items"); 
						Vector<BackpackMetadata> vectorBackpackItems = new Vector<BackpackMetadata>();
						
						if (innerJSON != null && innerJSON.length() > 0)
						{
							for (int k = 0; k < innerJSON.length(); k++)
							{
								BackpackMetadata backpackItemMetadata = new BackpackMetadata();
								JSONObject innerJsonObject = innerJSON.getJSONObject(k);

								try
								{
									String item_type = innerJsonObject.getString("item_type");
									backpackItemMetadata.setItemType(item_type);

									String item_id = innerJsonObject.getString("item_id");
									backpackItemMetadata.setItemId(item_id);
									
									vectorBackpackItems.add(backpackItemMetadata);
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
							}
						}
						
						characterMetadata.setBackpackMetadata(vectorBackpackItems);
						
						String armor_class = jsonObject.getString("armor_class");
						characterMetadata.setArmorClass(armor_class);
						
						String attack_bonus = jsonObject.getString("attack_bonus");
						characterMetadata.setAttackBonus(attack_bonus);
						
						String damage_bonus = jsonObject.getString("damage_bonus");
						characterMetadata.setDamageBonus(damage_bonus);
						
						String hit_points = jsonObject.getString("hit_points");
						characterMetadata.setHitPoints(hit_points);
						
						characterLists.add(characterMetadata);
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}

				}
			}
			
			characterMap = convertToMap(characterLists);
			

		} catch (JSONException e) 
		{
			e.printStackTrace();
		}
		return characterMap;
	}

	/**
	 * Converts the input <code>vector_characterMetadata</code> to the HashMap to send 
	 * it to <code>CharacterModel</code>
	 * @param vector_characterMetadata Vector that has list of <code>CharacterMetadata</code>
	 * @return convertToMap HashMap of the type <code>character_id</code> <code>CharacterMetadata</code>
	 */
	public HashMap<String, CharacterMetadata> convertToMap(Vector<CharacterMetadata> vector_characterMetadata)
	{
		HashMap<String, CharacterMetadata> characterMap = new HashMap<String, CharacterMetadata>();
		
		for(int i = 0; i<vector_characterMetadata.size(); i++)
		{
			CharacterMetadata character_Metadata = vector_characterMetadata.get(i);
			characterMap.put(character_Metadata.getCharacter_id(), character_Metadata);
		}
		
		return characterMap;
		
	}
	
}
