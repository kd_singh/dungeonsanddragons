package parsers;


import java.util.HashMap;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;
import Model.CampaignModel;
import metadatas.AbilityModifierMetadata;
import metadatas.AbilityScoreMetadata;
import metadatas.BackpackMetadata;
import metadatas.BonusMetadata;
import metadatas.CampaignMapsMetadata;
import metadatas.CampaignMetadata;
import metadatas.CharacterMetadata;
import metadatas.CharacterTypeMetadata;
import metadatas.CharacterWearableMetadata;
import metadatas.GridCellMetadata;
import metadatas.MapMetadata;
import metadatas.SessionMetadata;
import utilities.Constants;
import utilities.FileHandlerUtility;

/**
 * Parser file that fetches all the content from the Session File
 * and gets it into a JSON Array form which it is converted into
 * a HashMap for Sessions.
 * 
 * @author Tarandeep Singh
 * @author Karan Deep Singh
 */
public class SessionParser {
	
	/**
	 * Constant to store the name of the Session file.
	 */
	private String filename =  Constants.SESSION_FILE_NAME;
	
	/**
	 * Reference variable to <code>FileHandlerUtility</code> for file handling 
	 * operations like reading or writing.
	 */
	private FileHandlerUtility fileHandler = new FileHandlerUtility();
	
	/**
	 * This reads the content in <code>String</code> format from 
	 * the specified file.
	 * 
	 * @param fileName Name of the input Session file
	 * @return result String content read from the file
	 */
	public String getJSONString(String fileName)
	{
		String result = "";
		try
		{
			result = fileHandler.readAllData(fileName);

		} catch (Exception e) 
		{
			e.printStackTrace();
		}

		return result;
	}


	/**
	 * Parses the input String from the file into JSONArray and then convert it to 
	 * HashMap to send it to <code>SessionModel</code>
	 * @return HashMap of the type <code>session_id</code> <code>SessionMetadata</code>
	 */
	public HashMap<String, SessionMetadata> parseSession()
	{
		Vector<SessionMetadata> vector_SessionMetadata = new Vector<>();
		SessionMetadata sessionMetadata;
		HashMap<String, SessionMetadata> sessionMap = null;
		String response = getJSONString(filename);
		
		try 
		{
			JSONArray outerJSON = new JSONArray(response.trim());

			if (outerJSON != null && outerJSON.length() > 0)
			{
				for (int j = 0; j < outerJSON.length(); j++)
				{
					sessionMetadata = new SessionMetadata();
					JSONObject jsonObject = outerJSON.getJSONObject(j);

					try
					{
						String session_id = jsonObject.getString("session_id");
						sessionMetadata.setSession_id(session_id);

						String session_name = jsonObject.getString("session_name");
						sessionMetadata.setSession_name(session_name);
						
						String campaign_id = jsonObject.getString("campaign_id");
						sessionMetadata.setCampaign_id(campaign_id);
						
						String progress_seq = jsonObject.getString("progress_seq");
						sessionMetadata.setProgress_seq(progress_seq);
						
						JSONArray intermediateJSON = jsonObject.getJSONArray("character_info");
						Vector<CharacterTypeMetadata> vectorcharacter_Type = new Vector<>();
						
						
						if (intermediateJSON != null && intermediateJSON.length() > 0)
						{
							CharacterTypeMetadata characterTypeMetadata;
							
							for(int m = 0; m<intermediateJSON.length(); m++)
							{
								characterTypeMetadata = new CharacterTypeMetadata();
								
								JSONObject intermediateJsonObject = intermediateJSON.getJSONObject(m);
								
								String char_type = intermediateJsonObject.getString("char_type");
								characterTypeMetadata.setType(char_type);
								
								CharacterMetadata characterMetadata = new CharacterMetadata();
								JSONObject characterObj = intermediateJsonObject.getJSONObject("char_data");
								
								String character_id = characterObj.getString("character_id");
								characterMetadata.setCharacter_id(character_id);

								String character_name = characterObj.getString("character_name");
								characterMetadata.setCharacter_name(character_name);

								String character_level = characterObj.getString("character_level");
								characterMetadata.setCharacter_level(character_level);

								AbilityScoreMetadata abilityScoreMetadata = new AbilityScoreMetadata();
								JSONObject ab_scoreObj = characterObj.getJSONObject("ability_scores");
								
								String ab_strength = ab_scoreObj.getString("ab_strength");
								abilityScoreMetadata.setAb_strength(ab_strength);

								String ab_dexterity = ab_scoreObj.getString("ab_dexterity");
								abilityScoreMetadata.setAb_dexterity(ab_dexterity);

								String ab_constitution = ab_scoreObj.getString("ab_constitution");
								abilityScoreMetadata.setAb_constitution(ab_constitution);

								String ab_intelligence = ab_scoreObj.getString("ab_intelligence");
								abilityScoreMetadata.setAb_intelligence(ab_intelligence);

								String ab_wisdom = ab_scoreObj.getString("ab_wisdom");
								abilityScoreMetadata.setAb_wisdom(ab_wisdom);

								String ab_charisma = ab_scoreObj.getString("ab_charisma");
								abilityScoreMetadata.setAb_charisma(ab_charisma);

								characterMetadata.setAbilityScoreMetadata(abilityScoreMetadata);
								
								BonusMetadata bonusMetadata = new BonusMetadata();
								JSONObject bonus_ScoreMetadata = characterObj.getJSONObject("item_bonus");
								
								String dex_bonus = bonus_ScoreMetadata.getString("dex_bonus") ;
								bonusMetadata.setDexBonus(dex_bonus);
								
								String int_bonus = bonus_ScoreMetadata.getString("int_bonus") ;
								bonusMetadata.setIntBonus(int_bonus);
								
								String str_bonus = bonus_ScoreMetadata.getString("str_bonus") ;
								bonusMetadata.setStrBonus(str_bonus);
								
								String wis_bonus = bonus_ScoreMetadata.getString("wis_bonus") ;
								bonusMetadata.setWisBonus(wis_bonus);
								
								String con_bonus = bonus_ScoreMetadata.getString("con_bonus") ;
								bonusMetadata.setConBonus(con_bonus);
								
								String cha_bonus = bonus_ScoreMetadata.getString("cha_bonus") ;
								bonusMetadata.setChaBonus(cha_bonus);
								
								String itm_armor_bonus = bonus_ScoreMetadata.getString("itm_armor_bonus") ;
								bonusMetadata.setArmorClassBonus(itm_armor_bonus);
								
								String itm_attack_bonus = bonus_ScoreMetadata.getString("itm_attack_bonus") ;
								bonusMetadata.setAttackBonus(itm_attack_bonus);
								
								String itm_damage_bonus = bonus_ScoreMetadata.getString("itm_damage_bonus") ;
								bonusMetadata.setDamageBonus(itm_damage_bonus);
								
								characterMetadata.setBonusMetadata(bonusMetadata);

								JSONObject abm_scoreObj = characterObj.getJSONObject("ability_modifier");
								AbilityModifierMetadata abilityModifierMetadata = new AbilityModifierMetadata();

								String abm_strength;
								abm_strength = abm_scoreObj.getString("abm_strength");
								abilityModifierMetadata.setAbm_strength(abm_strength);

								String abm_dexterity;
								abm_dexterity = abm_scoreObj.getString("abm_dexterity");
								abilityModifierMetadata.setAbm_dexterity(abm_dexterity);

								String abm_constitution;
								abm_constitution = abm_scoreObj.getString("abm_constitution");
								abilityModifierMetadata.setAbm_constitution(abm_constitution);

								String abm_intelligence;
								abm_intelligence = abm_scoreObj.getString("abm_intelligence");
								abilityModifierMetadata.setAbm_intelligence(abm_intelligence);

								String abm_wisdom;
								abm_wisdom = abm_scoreObj.getString("abm_wisdom");
								abilityModifierMetadata.setAbm_wisdom(abm_wisdom);

								String abm_charisma;
								abm_charisma = abm_scoreObj.getString("abm_charisma");
								abilityModifierMetadata.setAbm_charisma(abm_charisma);

								characterMetadata.setAbilityModifierMetadata(abilityModifierMetadata);

								JSONObject character_wearableObj = characterObj.getJSONObject("character_wearable");
								CharacterWearableMetadata characterWearableMetadata = new CharacterWearableMetadata();

								String itm_helmet;
								itm_helmet = character_wearableObj.getString("itm_helmet");
								characterWearableMetadata.setItm_helmet(itm_helmet);

								String itm_armor;
								itm_armor = character_wearableObj.getString("itm_armor");
								characterWearableMetadata.setItm_armor(itm_armor);

								String itm_shield;
								itm_shield = character_wearableObj.getString("itm_shield");
								characterWearableMetadata.setItm_shield(itm_shield);

								String itm_ring;
								itm_ring = character_wearableObj.getString("itm_ring");
								characterWearableMetadata.setItm_ring(itm_ring);

								String itm_belt; 
								itm_belt = character_wearableObj.getString("itm_belt");
								characterWearableMetadata.setItm_belt(itm_belt);

								String itm_boots;
								itm_boots = character_wearableObj.getString("itm_boots");
								characterWearableMetadata.setItm_boots(itm_boots);

								String itm_weapon;
								itm_weapon = character_wearableObj.getString("itm_weapon");
								characterWearableMetadata.setItm_weapon(itm_weapon);

								characterMetadata.setCharacterWearableMetadata(characterWearableMetadata);
								
								JSONArray innerJSON = characterObj.getJSONArray("backpack_items"); 
								Vector<BackpackMetadata> vectorBackpackItems = new Vector<BackpackMetadata>();
								
								if (innerJSON != null && innerJSON.length() > 0)
								{
									for (int k = 0; k < innerJSON.length(); k++)
									{
										BackpackMetadata backpackItemMetadata = new BackpackMetadata();
										JSONObject innerJsonObject = innerJSON.getJSONObject(k);

										try
										{
											String item_type = innerJsonObject.getString("item_type");
											backpackItemMetadata.setItemType(item_type);

											String item_id = innerJsonObject.getString("item_id");
											backpackItemMetadata.setItemId(item_id);
											
											vectorBackpackItems.add(backpackItemMetadata);
										}
										catch(Exception e)
										{
											e.printStackTrace();
										}
									}
								}
								
								characterMetadata.setBackpackMetadata(vectorBackpackItems);
								
								String armor_class = characterObj.getString("armor_class");
								characterMetadata.setArmorClass(armor_class);
								
								String attack_bonus = characterObj.getString("attack_bonus");
								characterMetadata.setAttackBonus(attack_bonus);
								
								String damage_bonus = characterObj.getString("damage_bonus");
								characterMetadata.setDamageBonus(damage_bonus);
								
								String hit_points = characterObj.getString("hit_points");
								characterMetadata.setHitPoints(hit_points);
								
								characterTypeMetadata.setCharacterMetadata(characterMetadata);
								
								vectorcharacter_Type.add(characterTypeMetadata);
							}
							
						}
						
						sessionMetadata.setVector_characterMetadata(vectorcharacter_Type);
						
						JSONObject intermediateMapJSON = jsonObject.getJSONObject("map_info");
						MapMetadata mapMetadata = new MapMetadata();
						
						String map_id = intermediateMapJSON.getString("map_id");
						mapMetadata.setMap_id(map_id);

						String map_name = intermediateMapJSON.getString("map_name");
						mapMetadata.setMap_name(map_name);
						
						String map_size = intermediateMapJSON.getString("map_size");
						mapMetadata.setMap_size(map_size);
						
						JSONArray innerMapJSON = intermediateMapJSON.getJSONArray("map_layout");
						Vector<GridCellMetadata> vector_GridCellData = new Vector<GridCellMetadata>();
						
						if (innerMapJSON != null && innerMapJSON.length() > 0)
						{
							for (int i = 0; i < innerMapJSON.length(); i++)
							{
								GridCellMetadata gridCellMetadata = new GridCellMetadata();
								JSONObject innerObject = innerMapJSON.getJSONObject(i);

								String xPosition = innerObject.getString("xposition");
								gridCellMetadata.setxPosition(Integer.parseInt(xPosition));
										
								String yPosition = innerObject.getString("yposition");
								gridCellMetadata.setyPosition(Integer.parseInt(yPosition));
									
								String value = innerObject.getString("value");
								gridCellMetadata.setValue(value);
										
								String type = innerObject.getString("type");
								gridCellMetadata.setType(type);
										
								vector_GridCellData.add(gridCellMetadata);			
							}
						}
								
						mapMetadata.setMap_layout(vector_GridCellData);
								
						sessionMetadata.setMapMetadata(mapMetadata);
					}
						
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
					
					vector_SessionMetadata.add(sessionMetadata);
				}
			
			}
			sessionMap = convertToMap(vector_SessionMetadata);
		}
		
		
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return sessionMap;
		}	

	
	/**
	 * Converts the input <code>vector_SessionMetadata</code> to the HashMap to send 
	 * it to <code>SessionModel</code>
	 * @param vector_SessionMetadata Vector that has list of <code>SessionMetadata</code>
	 * @return convertToMap HashMap of the type <code>session_id</code> <code>SessionMetadata</code>
	 */
	public HashMap<String, SessionMetadata> convertToMap(Vector<SessionMetadata> vector_SessionMetadata)
	{
		HashMap<String, SessionMetadata> sessionMap = new HashMap<String, SessionMetadata>();
		
		for(int i = 0; i<vector_SessionMetadata.size(); i++)
		{
			SessionMetadata session_Metadata = vector_SessionMetadata.get(i);
			sessionMap.put(session_Metadata.getSession_id(), session_Metadata);
		}
		
		return sessionMap;
		
	}
}

