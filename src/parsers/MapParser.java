package parsers;

import java.util.HashMap;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;
import metadatas.GridCellMetadata;
import metadatas.MapMetadata;
import utilities.Constants;
import utilities.FileHandlerUtility;

/**
 * Parser file that fetches all the content from the Map File
 * and gets it into a JSON Array form which it is converted into
 * a HashMap for Maps.
 * 
 * @author Tarandeep Singh
 */
public class MapParser
{
	/**
	 * Constant to store the name of the Campaign file.
	 */
	private String filename =  Constants.MAP_FILE_NAME;
	
	/**
	 * Reference variable to <code>FileHandlerUtility</code> for file handling 
	 * operations like reading or writing.
	 */
	private FileHandlerUtility fileHandler = new FileHandlerUtility();

	/**
	 * This reads the content in <code>String</code> format from 
	 * the specified file.
	 * 
	 * @param fileName Name of the input Map file
	 * @return result String content read from the file
	 */
	public String getJSONString(String fileName)
	{
		String result = "";
		try
		{
			result = fileHandler.readAllData(fileName);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Parses the input String from the file into JSONArray and then convert it to 
	 * HashMap to send it to <code>MapModel</code>
	 * @return HashMap of the type <code>map_id</code> <code>MapMetadata</code>
	 */
	public HashMap<String, MapMetadata> parseMap()
	{
		String response = getJSONString(filename);
		Vector<MapMetadata> vector_MapMetadata = new Vector<MapMetadata>();
		MapMetadata mapMetadata = null;
		HashMap<String, MapMetadata> Map = null;

		try 
		{
			JSONArray outerJSON = new JSONArray(response.trim());

			if (outerJSON != null && outerJSON.length() > 0)
			{

				for (int j = 0; j < outerJSON.length(); j++)
				{

					mapMetadata = new MapMetadata();
					JSONObject jsonObject = outerJSON.getJSONObject(j);

					try
					{
						String map_id = jsonObject.getString("map_id");
						mapMetadata.setMap_id(map_id);

						String map_name = jsonObject.getString("map_name");
						mapMetadata.setMap_name(map_name);
						
						String map_size = jsonObject.getString("map_size");
						mapMetadata.setMap_size(map_size);
						
						JSONArray innerJSON = jsonObject.getJSONArray("map_layout");
						Vector<GridCellMetadata> vector_GridCellData = new Vector<GridCellMetadata>();
						
						if (innerJSON != null && innerJSON.length() > 0)
						{

							for (int i = 0; i < innerJSON.length(); i++)
							{
								GridCellMetadata gridCellMetadata = new GridCellMetadata();
								JSONObject innerObject = innerJSON.getJSONObject(i);

								String xPosition = innerObject.getString("xposition");
								gridCellMetadata.setxPosition(Integer.parseInt(xPosition));
								
								String yPosition = innerObject.getString("yposition");
								gridCellMetadata.setyPosition(Integer.parseInt(yPosition));
								
								String value = innerObject.getString("value");
								gridCellMetadata.setValue(value);
								
								String type = innerObject.getString("type");
								gridCellMetadata.setType(type);
								
								vector_GridCellData.add(gridCellMetadata);
								
							}
						}
						
						mapMetadata.setMap_layout(vector_GridCellData);
						
						vector_MapMetadata.add(mapMetadata);
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		Map = convertToMap(vector_MapMetadata);

		return Map;
	}

	/**
	 * Converts the input <code>vector_MapMetadata</code> to the HashMap to send 
	 * it to <code>MapModel</code>
	 * @param vector_MapMetadata Vector that has list of <code>MapMetadata</code>
	 * @return convertToMap HashMap of the type <code>map_id</code> <code>MapMetadata</code>
	 */
	public HashMap<String, MapMetadata> convertToMap(Vector<MapMetadata> vector_MapMetadata)
	{
		HashMap<String, MapMetadata> map = new HashMap<String, MapMetadata>();

		for(int i = 0; i<vector_MapMetadata.size(); i++)
		{

			MapMetadata mapMetadata = vector_MapMetadata.get(i);
			map.put(mapMetadata.getMap_id(), mapMetadata);
		}

		return map;
	}
}