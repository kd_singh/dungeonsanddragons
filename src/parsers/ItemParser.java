package parsers;

import java.util.HashMap;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

import metadatas.ItemListMetaInterface;
import metadatas.ItemListMetadata;
import metadatas.ItemMetadata;
import utilities.Constants;
import utilities.FileHandlerUtility;

/**
 * Parser file that fetches all the content from the Item File
 * and gets it into a JSON Array form which it is converted into
 * a HashMap for Items.
 * 
 * @author Gurvinder Singh
 */
public class ItemParser
{
	/**
	 * Constant to store the name of the Item file.
	 */
	String filename = Constants.ITEM_FILE_NAME;
	
	/**
	 * Reference variable to <code>FileHandlerUtility</code> for file handling 
	 * operations like reading or writing.
	 */
	FileHandlerUtility fileHandler = new FileHandlerUtility();
	
	/**
	 * This reads the content in <code>String</code> format from 
	 * the specified file.
	 * 
	 * @param fileName Name of the input Item file
	 * @return String content read from the file
	 */
	public String getJSONString(String fileName)
	{
		String result = "";
		try
		{
			result = fileHandler.readAllData(fileName);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
	
	/**
	 * Parses the input String from the file into JSONArray and then convert it to 
	 * HashMap to send it to <code>ItemModel</code>
	 * 
	 * @return HashMap of the type <code>item_type</code>, <code>ItemMetadata</code>
	 */
	public HashMap<String, ItemMetadata> parseItem()
	{
		String response = getJSONString(filename);
		Vector<ItemMetadata> vector_itemMetadata = new Vector<ItemMetadata>();
		HashMap<String, ItemMetadata> itemMap = null;
		ItemMetadata itemMetadata;
		try 
		{
			JSONArray outerJSON = new JSONArray(response.trim());

			if (outerJSON != null && outerJSON.length() > 0)
			{

				for (int j = 0; j < outerJSON.length(); j++)
				{

					itemMetadata = new ItemMetadata();
					JSONObject jsonObject = outerJSON.getJSONObject(j);

					try
					{
						String item_type = jsonObject.getString("item_type");
						itemMetadata.setItem_type(item_type);

						JSONArray innerJSON = jsonObject.getJSONArray("item_list"); 
						Vector<ItemListMetaInterface> vectorItemList = new Vector<ItemListMetaInterface>();
						
						if (innerJSON != null && innerJSON.length() > 0)
						{
							for (int k = 0; k < innerJSON.length(); k++)
							{
								ItemListMetadata itemListMetadata = new ItemListMetadata();
								JSONObject innerJsonObject = innerJSON.getJSONObject(k);

								try
								{
									String list_item_id = innerJsonObject.getString("list_item_id");
									itemListMetadata.setList_item_id(list_item_id);

									String list_item_name = innerJsonObject.getString("list_item_name");
									itemListMetadata.setList_item_name(list_item_name);

									String list_item_enchanment = innerJsonObject.getString("list_item_enchanment");
									itemListMetadata.setList_item_enchanment(list_item_enchanment);

									String list_item_bonus = innerJsonObject.getString("list_item_bonus");
									itemListMetadata.setList_item_bonus(list_item_bonus);
									
									String list_item_range = innerJsonObject.getString("list_item_range");
									itemListMetadata.setRange(list_item_range);
									
									String list_item_decorator = innerJsonObject.getString("list_item_decorator");
									itemListMetadata.setDecoraterWeapon(list_item_decorator);
									
									vectorItemList.add(itemListMetadata);
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
							}
						}
						
						itemMetadata.setItemListMetadata(vectorItemList); 
						vector_itemMetadata.add(itemMetadata);
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			
			itemMap = convertToMap(vector_itemMetadata);
		}	
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return itemMap;
	}

	/**
	 * Converts the input <code>vector_itemMetadata</code> to the HashMap to send 
	 * it to <code>ItemModel</code>
	 * 
	 * @param vector_itemMetadata Vector that has list of <code>ItemMetadata</code>
	 * @return HashMap of the type <code>item_type</code>, <code>ItemMetadata</code>
	 */
	public HashMap<String, ItemMetadata> convertToMap(Vector<ItemMetadata> vector_itemMetadata)
	{
		HashMap<String, ItemMetadata> itemMap = new HashMap<String, ItemMetadata>();
		
		for(int i = 0; i<vector_itemMetadata.size(); i++)
		{

			ItemMetadata item_Model = vector_itemMetadata.get(i);
			itemMap.put(item_Model.getItem_type(), item_Model);
		}
		
		return itemMap;
	}

}
