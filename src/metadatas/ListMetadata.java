package metadatas;

import Model.MapModel;

public class ListMetadata 
{
	String item_ID;
	String item_Name;

	public String getItem_ID() 
	{
		return item_ID;
	}
	public void setItem_ID(String item_ID) 
	{
		this.item_ID = item_ID;
	}
	public String getItem_Name() {
		return item_Name;
	}
	public void setItem_Name(String item_name) {
		this.item_Name = item_name;
	}
	
	@Override
	public String toString()
	{
		return item_Name;
	}
	
}
