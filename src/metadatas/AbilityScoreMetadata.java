package metadatas;

/**
 * This class stores the state of character's ability score and constitutes the character
 * metadata.
 * 
 * @author Gurvinder Singh
 * @see CharacterMetadata
 */
public class AbilityScoreMetadata {
	
	/**
	 * It is the strength score for the character.
	 */
	String ab_strength;
	/**
	 * It is the dexterity score for the character.
	 */
	String ab_dexterity;
	/**
	 * It is the constitution score for the character.
	 */
	String ab_constitution;
	/**
	 * It is the intelligence score for the character.
	 */
	String ab_intelligence;
	/**
	 * It is the wisdom score for the character.
	 */
	String ab_wisdom;
	/**
	 * It is the charisma score for the character.
	 */
	String ab_charisma;

	/**
	 * This method is used to convert an ability modifier to JSON format.
	 * 
	 * @param abilityScoreMetadata It is the instance of abilityScoreMetadata containing character scores data.
	 * @return returns a string in JSON format.
	 */
	public String convertToString(AbilityScoreMetadata abilityScoreMetadata)
	{
		String Return="{\n\t";
		Return = Return + "\"ab_strength\":"		+ "\""+ abilityScoreMetadata.getAb_strength() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"ab_dexterity\":"		+ "\""+ abilityScoreMetadata.getAb_dexterity() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"ab_constitution\":"	+ "\""+ abilityScoreMetadata.getAb_constitution() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"ab_intelligence\":"	+ "\""+ abilityScoreMetadata.getAb_intelligence() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"ab_wisdom\":"          + "\""+ abilityScoreMetadata.getAb_wisdom()+"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"ab_charisma\":"		+ "\""+ abilityScoreMetadata.getAb_charisma()  +"\"";
		Return=  Return + "\n}";

		return Return;
	}

	/**
	 * This method is a getter for strength ability. 
	 * @return a value for strength ability. 
	 */
	public String getAb_strength() 
	{
		return ab_strength == null ? "0": ab_strength;
	}
	
	/**
	 * This method is a setter method for strength ability. 
	 * @param ab_strength It is the value of strength ability.
	 */
	public void setAb_strength(String ab_strength) 
	{
		this.ab_strength = ab_strength;

	}
	
	/**
	 * This method is a getter for dexterity ability. 
	 * @return a value for dexterity ability. 
	 */
	public String getAb_dexterity() 
	{
		return ab_dexterity  == null ? "0": ab_dexterity;
	}
	
	/**
	 * This method is a setter method for dexterity ability. 
	 * @param ab_dexterity It is the value of dexterity ability.
	 */
	public void setAb_dexterity(String ab_dexterity) 
	{
		this.ab_dexterity = ab_dexterity;
	}
	
	/**
	 * This method is a getter for constitution ability. 
	 * @return a value for constitution ability. 
	 */
	public String getAb_constitution()
	{
		return ab_constitution == null ? "0": ab_constitution;
	}
	
	/**
	 * This method is a setter method for constitution ability. 
	 * @param ab_constitution It is the value of dexterity ability.
	 */
	public void setAb_constitution(String ab_constitution) 
	{
		this.ab_constitution = ab_constitution;
	}
	
	/**
	 * This method is a getter for intelligence ability. 
	 * @return a value for intelligence ability. 
	 */
	public String getAb_intelligence() 
	{
		return ab_intelligence == null ? "0": ab_intelligence;
	}
	
	/**
	 * This method is a setter method for intelligence ability. 
	 * @param ab_intelligence It is the value of intelligence ability.
	 */
	public void setAb_intelligence(String ab_intelligence) 
	{
		this.ab_intelligence = ab_intelligence;
	}
	
	/**
	 * This method is a getter for wisdom ability. 
	 * @return a value for wisdom ability. 
	 */
	public String getAb_wisdom() 
	{
		return ab_wisdom == null ? "0": ab_wisdom;
	}
	
	/**
	 * This method is a setter method for wisdom ability. 
	 * @param ab_wisdom It is the value of wisdom ability.
	 */
	public void setAb_wisdom(String ab_wisdom) 
	{
		this.ab_wisdom = ab_wisdom;
	}
	
	/**
	 * This method is a getter for charisma ability. 
	 * @return a value for charisma ability. 
	 */
	public String getAb_charisma() 
	{
		return ab_charisma == null ? "0": ab_charisma;
	}
	
	/**
	 * This method is a setter method for charimsa ability. 
	 * @param ab_charisma It is the value of charimsa ability.
	 */
	public void setAb_charisma(String ab_charisma) 
	{
		this.ab_charisma = ab_charisma;
	}



}
