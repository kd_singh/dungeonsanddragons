package metadatas;

import java.util.HashMap;

/**
 * This class stores the state of the Items within the items metadata.
 * @author vijay
 */

public class ItemListMetadata extends ItemListMetaInterface
{
	/**
	 * String attributes for the items list.
	 */
	String list_item_id;
	String list_item_name;
	String list_item_enchanment;
	String list_item_bonus;
	String range;
	String decoraterWeapon;
	HashMap<String,String> weaponEnchantmentMap = new HashMap<>();
	
	/**
	 * This Method converts the Metadata to String format.
	 * @param itemListMetadata
	 * @return Formatted String
	 */
	public String convertToString(ItemListMetaInterface itemListMetadata)
	{
		String Return="{\n\t";
		Return = Return + "\"list_item_id\":"      + "\""+ itemListMetadata.getList_item_id() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"list_item_name\":"      + "\""+ itemListMetadata.getList_item_name() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"list_item_enchanment\":"       + "\""+ itemListMetadata.getList_item_enchanment() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"list_item_bonus\":"       + "\""+ itemListMetadata.getList_item_bonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"list_item_range\":"       + "\""+ itemListMetadata.getRange() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"list_item_decorator\":"      + "\""+ itemListMetadata.getDecoraterWeapon() +"\"";
		Return=  Return + "\n}";	
		
		return Return;
	}
	
	/**
	 * Getter for the List item id
	 * @return item list id
	 */
	public String getList_item_id() 
	{
		return list_item_id;
	}
	
	/**
	 * Setter for the List item id
	 * @param list_item_id id of the list item
	 */
	public void setList_item_id(String list_item_id)
	{
		this.list_item_id = list_item_id;
	}
	
	/**
	 * Getter for the List item name
	 * @return list item name
	 */
	public String getList_item_name() 
	{
		return list_item_name;
	}
	
	/**
	 * Setter for the list item name
	 * @param list_item_name name of the list item
	 */
	public void setList_item_name(String list_item_name) 
	{
		this.list_item_name = list_item_name;
	}
	
	/**
	 * Getter for the list item enchantment
	 * @return list item enchantment
	 */
	public String getList_item_enchanment() 
	{
		return list_item_enchanment;
	}
	
	/**
	 * Setter for the list item enchantment
	 * @param list_item_enchanment list item enchantment attribute
	 */
	public void setList_item_enchanment(String list_item_enchanment) 
	{
		this.list_item_enchanment = list_item_enchanment;
	}
	
	/**
	 * Getter for the list item bonus
	 * @return list item bonus
	 */
	public String getList_item_bonus() 
	{
		return list_item_bonus;
	}
	
	/**
	 * Setter for the list item bonus
	 * @param list_item_bonus list item bonus
	 */
	public void setList_item_bonus(String list_item_bonus) 
	{
		this.list_item_bonus = list_item_bonus;
	}

	/**
	 * Overrides the to String method.
	 */
	public String toString()
	{
		return list_item_name;
	}

	public HashMap<String, String> getWeaponEnchantmentType() {
		return this.weaponEnchantmentMap;
	}
	
	public void setWeaponEnchantmentType(String key, String Value)
	{
		this.weaponEnchantmentMap.put(key, Value);
	}
	
	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getDecoraterWeapon() {
		return decoraterWeapon;
	}

	public void setDecoraterWeapon(String decoraterWeapon) {
		this.decoraterWeapon = decoraterWeapon;
	}

}
