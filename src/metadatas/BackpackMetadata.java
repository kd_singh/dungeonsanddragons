package metadatas;

/**
 * This class stores the state of the character's backpack.	
 * 
 * @author Gurvinder Singh
 */
public class BackpackMetadata 
{	
	/**
	 * It specifies the type of item i.e. Ring,Helmet etc. 
	 */
	String itemType;
	
	/**
	 * It specifies the id of the item present in backpack.
	 */
	String itemId;

	/**
	 * This method is used to convert BackpackMetadata to JSON format.
	 * 
	 * @param backpackMetadata It is a object of backpack metadata containing details about items in backpack.
	 * @return a string in JSON format.
	 */
	public String convertToString(BackpackMetadata backpackMetadata)
	{
		String Return="{\n\t";
		Return = Return + "\"item_type\":"      + "\""+ backpackMetadata.getItemType() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"item_id\":"      + "\""+ backpackMetadata.getItemId() +"\"";
		Return=  Return + "\n}";	
		
		return Return;
	}
	
	/**
	 * It is the getter method for getting the item type of the item present in character's backpack.
	 * 
	 * @return the item type of the item present in backpack.
	 */
	public String getItemType() 
	{
		return itemType;
	}
	
	/**
	 * It is used to set the ItemType of the item present in backpack.
	 * 
	 * @param itemType It specifies whether an item is helmet, belt etc.
	 */
	public void setItemType(String itemType) 
	{
		this.itemType = itemType;
	}
	
	/**
	 * It is used to get the Item Id of the item present in backpack.
	 * @return the item id of the item present in backpack.
	 */
	public String getItemId() 
	{
		return itemId;
	}
	
	/**
	 * It is used to set the Item Id of the item present in backpack.
	 * 
	 * @param itemId it is the item Id used to identify an item.
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	
}
