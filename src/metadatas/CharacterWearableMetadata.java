package metadatas;

/**
 * This class stores the state of character wearable items.
 * 
 * @author Gurvinder Singh
 *
 */
public class CharacterWearableMetadata 
{

	String itm_helmet;
	String itm_armor;
	String itm_shield;
	String itm_ring;
	String itm_belt;
	String itm_boots;
	String itm_weapon;
	
	/**
	 * 
	 * @param characterWearableMetadata
	 * @return Return characterWearableMetadata in string type
	 */
	public String convertToString(CharacterWearableMetadata characterWearableMetadata)
	{
		String Return="{\n\t";
		Return = Return + "\"itm_helmet\":"		+ "\""+ characterWearableMetadata.getItm_helmet() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"itm_armor\":"		+ "\""+ characterWearableMetadata.getItm_armor() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"itm_shield\":"		+ "\""+ characterWearableMetadata.getItm_shield() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"itm_ring\":"		+ "\""+ characterWearableMetadata.getItm_ring() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"itm_belt\":"		+ "\""+ characterWearableMetadata.getItm_belt() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"itm_boots\":"		+ "\""+ characterWearableMetadata.getItm_boots()  +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"itm_weapon\":"		+ "\""+ characterWearableMetadata.getItm_weapon()  +"\"";
		Return=  Return + "\n}";	
		
		return Return;
	}
	
	/**
	 * Getter for item helmet
	 * @return itm_helmet
	 */
	public String getItm_helmet() 
	{
		return itm_helmet == null ? "" : itm_helmet;
	}
	
	/**
	 * Setter for item helmet
	 * @param itm_helmet
	 */
	public void setItm_helmet(String itm_helmet) 
	{
		this.itm_helmet = itm_helmet;
	}
	
	/**
	 * Getter for item armor
	 * @return itm_armor
	 */
	public String getItm_armor() 
	{
		return itm_armor == null ? "" : itm_armor;
	}
	
	/**
	 * Setter for item armor
	 * @param itm_armor
	 */
	public void setItm_armor(String itm_armor) 
	{
		this.itm_armor = itm_armor;
	}
	
	/**
	 * Getter for item shield
	 * @return itm_shield
	 */
	public String getItm_shield() 
	{
		return itm_shield == null ? "" : itm_shield;
	}
	
	/**
	 * Setter for item shield
	 * @param itm_shield
	 */
	public void setItm_shield(String itm_shield) 
	{
		this.itm_shield = itm_shield;
	}
	
	/**
	 * Getter for item ring
	 * @return itm_ring
	 */
	public String getItm_ring()
	{
		return itm_ring == null ? "" : itm_ring;
	}
	
	/**
	 * Setter for Item ring
	 * @param itm_ring
	 */
	public void setItm_ring(String itm_ring) 
	{
		this.itm_ring = itm_ring;
	}
	
	/**
	 * Getter for item belt
	 * @return itm_belt
	 */
	public String getItm_belt() 
	{
		return itm_belt == null ? "" : itm_belt;
	}
	
	/**
	 * Setter for item belt
	 * @param itm_belt
	 */
	public void setItm_belt(String itm_belt) 
	{
		this.itm_belt = itm_belt;
	}
	
	/**
	 * Getter for item boots
	 * @return itm_boots
	 */
	public String getItm_boots() 
	{
		return itm_boots == null ? "" : itm_boots; 
	}
	
	/**
	 * Setter for item boots
	 * @param itm_boots
	 */
	public void setItm_boots(String itm_boots) 
	{
		this.itm_boots = itm_boots;
	}
	
	/**
	 * Getter for item weapon
	 * @return itm_weapon
	 */
	public String getItm_weapon() 
	{
		return itm_weapon == null ? "" : itm_weapon;
	}
	
	/**
	 * Setter for Item weapons
	 * @param itm_weapon
	 */
	public void setItm_weapon(String itm_weapon) 
	{
		this.itm_weapon = itm_weapon;
	}

	
}
