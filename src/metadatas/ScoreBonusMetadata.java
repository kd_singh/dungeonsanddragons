package metadatas;

import View.CharacterCreateView;
import utilities.CharacterUtility;

import org.apache.log4j.*;

/**
 * This Class adapts the Player abilities based on Bonus Scores.
 * Created by Han on 2017-02-25.
 */
/**
 * @author vijay
 *
 */
public class ScoreBonusMetadata {
    public static final Logger log = LogManager.getLogger(ScoreBonusMetadata.class);
    /**
     * Integer values for the Ability bonuses
     */
    int dexBonus, intBonus, conBonus, chaBonus, wisBonus, strBonus;

    /**
     * Base Scores for the Abilities
     */
    int strScore ;
    int dexScore ;
    int intScore ;
    int conScore ;
    int chaScore ;
    int wisScore ;
    
    /**
     * Final added Scores based on bonus and base
     */
    int strResult;
    int dexResult;
    int conResult;
    int chaResult;
    int wisResult;
    int intResult;

    /**
     * Ability Modifier scores
     */
    
    int strModifierResult;
    int dexModifierResult;
    int conModifierResult;
    int chaModifierResult;
    int wisModifierResult;
    int intModifierResult;
    
    
   
    
    /**
     * Constructor for the Class.
     * 
     */
    public ScoreBonusMetadata() 
    {
        this.dexBonus = 0;
        this.intBonus = 0;
        this.conBonus = 0;
        this.chaBonus = 0;
        this.wisBonus = 0;
        this.strBonus = 0;
        this.strScore = 0;
        this.dexScore = 0;
        this.intScore = 0;
        this.conScore = 0;
        this.chaScore = 0;
        this.wisScore = 0;
    }

    /**
     * Getter for the Strength Resultant
     * @return Strength Result
     */
    public int getStrResult() {
        strResult = strScore + strBonus;
//        log.info("strength: " + strScore + " + " + strBonus);
        return strResult;
    }

    /**
     * Getter for the Dexterity Resultant
     * @return Dexterity Result
     */
    public int getDexResult() {
        dexResult = dexScore + dexBonus;
//        log.info("dexterity: " + dexScore + " + " + dexBonus);
        return dexResult;
    }

    /**
     * Getter for the Intelligence Result
     * @return Intelligence Result
     */
    public int getIntResult() {
        intResult = intScore + intBonus;
//        log.info("intelligence: " + intScore + " + " + intBonus);
        return intResult;
    }
    
    /**
     * Getter for the Constitution Result
     * @return Constitution Result
     */
    public int getConResult() {
        conResult = conScore + conBonus;
//        log.info("constitution: " + conScore + " + " + conBonus);
        return conResult;
    }
    
    /**
     * Getter for the Charisma Result
     * @return Charisma Result
     */
    public int getChaResult() {
        chaResult = chaScore + chaBonus;
//        log.info("charisma: " + chaScore + " + " + chaBonus);
        return chaResult;
    }
    
    /**
     * Getter for the Wisdom Result
     * @return Wisdom Result
     */
    public int getWisResult() {
        wisResult = wisScore + wisBonus;
//        log.info("wisdom: " + wisScore + " + " + wisBonus);
        return wisResult;
    }
    
    /**
     * Getter for the Dexterity Bonus
     * @return Dexterity Bonus
     */
    public int getDexBonus() {
        return dexBonus;
    }
    
    /**
     * Setter for the Dexterity Bonus
     * @param dexBonus Dexterity Bonus
     */
    public void setDexBonus(int dexBonus) {
        this.dexBonus = dexBonus;
    }
    
    /**
     * Getter for the Intelligence Bonus
     * @return Intelligence Bonus
     */
    public int getIntBonus() {
        return intBonus;
    }
    
    /**
     * Setter for the Intelligence Bonus
     * @param intBonus Intelligence Bonus
     */
    public void setIntBonus(int intBonus) {
        this.intBonus = intBonus;
    }
    
    /**
     * Getter for the Constitution Bonus
     * @return Constitution Bonus
     */
    public int getConBonus() {
        return conBonus;
    }
    
    /**
     * Setter for the Constitution Bonus
     * @param conBonus Constitution Bonus
     */
    public void setConBonus(int conBonus) {
        this.conBonus = conBonus;
    }
    
    /**
     * Getter for the Charisma Bonus
     * @return Charisma Bonus
     */
    public int getChaBonus() {
        return chaBonus;
    }
    
    /**
     * Setter for the Charisma Bonus
     * @param chaBonus Charisma Bonus
     */
    public void setChaBonus(int chaBonus) {
        this.chaBonus = chaBonus;
    }
    
    /**
     * Getter for the Wisdom Bonus
     * @return Wisdom Bonus
     */
    public int getWisBonus() {
        return wisBonus;
    }
    
    /**
     * Setter for the Wisdom Bonus
     * @param wisBonus Wisdom bonus
     */
    public void setWisBonus(int wisBonus) {
        this.wisBonus = wisBonus;
    }
    
    /**
     * Getter for the Strength Bonus
     * @return Strength Bonus
     */
    public int getStrBonus() {
        return strBonus;
    }
    
    /**
     * Setter for Strength Bonus
     * @param strBonus Strength Bonus
     */
    public void setStrBonus(int strBonus) {
        this.strBonus = strBonus;
    }
    
    /**
     * Getter for the Strength Score
     * @return Strength Score
     */
    public int getStrScore() {
        return strScore;
    }
    
    /**
     * Setter for the Strength Score
     * @param strScore Strength Score
     */
    public void setStrScore(int strScore) {
        this.strScore = strScore;
    }
    
    /**
     * Getter for the Dexterity Score
     * @return Dexterity Score
     */
    public int getDexScore() {
        return dexScore;
    }
    
    /**
     * Setter for the Dexterity Score
     * @param dexScore Dexterity Score
     */
    public void setDexScore(int dexScore) {
        this.dexScore = dexScore;
    }
    
    /**
     * Getter for the Intelligence Score
     * @return Intelligence Score
     */
    public int getIntScore() {
        return intScore;
    }
    
    /**
     * Setter for the Intelligence Score
     * @param intScore Intelligence Score
     */
    public void setIntScore(int intScore) {
        this.intScore = intScore;
    }
    
    /**
     * Getter for the Constitution Score
     * @return Constitution Score
     */
    public int getConScore() {
        return conScore;
    }
    
    /**
     * Setter for the Constitution Score
     * @param conScore Constitution Score
     */
    public void setConScore(int conScore) {
        this.conScore = conScore;
    }
    
    /**
     * Getter for the Charisma Score
     * @return Charisma Score
     */
    public int getChaScore() {
        return chaScore;
    }
    
    /**
     * Setter for the Charisma Score
     * @param chaScore Charisma Score
     */
    public void setChaScore(int chaScore) {
        this.chaScore = chaScore;
    }
    
    /**
     * Getter for the Wisdom Score
     * @return Wisdom Score
     */
    public int getWisScore() {
        return wisScore;
    }
    
    /**
     * Setter for the Wisdom Score
     * @param wisScore Wisdom Score
     */
    public void setWisScore(int wisScore) {
        this.wisScore = wisScore;
    }
    
    public int getStrModifier()
    {
    	return scoreToModifier(this.getStrResult());
    }
    public int getDexModifier()
    {
    	return scoreToModifier(this.getDexResult());
    }
    public int getConModifier()
    {
    	return scoreToModifier(this.getConResult());
    }
    public int getChaModifier()
    {
    	return scoreToModifier(this.getChaResult());
    }
    public int getWisModifier()
    {
    	return scoreToModifier(this.getWisResult());
    }
    public int getIntModifier()
    {
    	return scoreToModifier(this.getIntResult());
    }
    
    
    public int scoreToModifier(int score){
        int modifier = (score- 10)/2;
        if (modifier < 0) {
            modifier = 0;
            log.warn("Modifier is less then 0, reset to 0");
        }
        return modifier;
    }
}
