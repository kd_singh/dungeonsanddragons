package metadatas;

import java.util.Vector;

/**
 * This class stores the state of the Item Lists.
 * @author vijay
 */
public class ItemMetadata 
{	
	/**
	 * String item type
	 */
	String item_type;
	/**
	 * Vector for item list metadata
	 */
	Vector<ItemListMetaInterface> itemListMetadata;
	
	/**
	 * Getter for the Item type
	 * @return the type of the item
	 */
	public String getItem_type() 
	{
		return item_type;
	}
	
	/**
	 * Setter for the item type
	 * @param item_type type of the item
	 */
	public void setItem_type(String item_type) 
	{
		this.item_type = item_type;
	}
	
	/**
	 * Getter for the itemlistmetadata
	 * @return the item list metadata
	 */
	public Vector<ItemListMetaInterface> getItemListMetadata() 
	{
		return itemListMetadata;
	}
	
	/**
	 * Setter for the Item List metadata
	 * @param itemListMetadata the item list metadata
	 */
	public void setItemListMetadata(Vector<ItemListMetaInterface> itemListMetadata) 
	{
		this.itemListMetadata = itemListMetadata;
	}

	/**
	 * This method converts the metadata to String
	 * @param itemMetadata the metadata of the item
	 * @return the formatted string for itemmetadata
	 */
	public String convertToString(ItemMetadata itemMetadata)
	{	
		ItemListMetadata itemListMetadata = new ItemListMetadata();
		String Return="{\n\t";
		Return = Return + "\"item_type\":"      + "\""+ itemMetadata.getItem_type() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"item_list\":"        + "[";

		Vector<ItemListMetaInterface> temp_vector_ItemList = itemMetadata.getItemListMetadata();
		
		for(int i = 0 ; i<temp_vector_ItemList.size() ; i++)
		{
			Return=  Return + "\n\t";
			Return = Return + itemListMetadata.convertToString(temp_vector_ItemList.get(i));
			Return=  Return + ",\n";
		}
		
		Return = Return.substring(0, Return.length()-2);
		Return = Return + "\n]";
		Return=  Return + "\n}";	
		
		return Return;
		
	}


}
