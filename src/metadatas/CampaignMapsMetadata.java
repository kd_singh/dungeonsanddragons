package metadatas;

/**
 * This indicates the Meta data for maps which are part of a campaign.
 *
 * @author Tarandeep Singh
 * @version 1.0.0
 * @since 07 February 2017
 * 
 * 
 */

public class CampaignMapsMetadata 
	
{
	
	String map_id;
	String map_name;
	String map_sequence;
	
	/**
	 * Function to convert MapMetaData to String
	 * @param campaignMapsMetadata The meta data corresponding to maps present in campaign
	 * @return Return Map meta data in form of String 
	 */
	public String convertToString(CampaignMapsMetadata campaignMapsMetadata)
	{
		String Return="{\n\t";
		Return = Return + "\"map_id\":"      + "\""+ campaignMapsMetadata.getMap_id() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"map_name\":"      + "\""+ campaignMapsMetadata.getMap_name() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"map_sequence\":"       + "\""+ campaignMapsMetadata.getMap_sequence() +"\"";
		Return=  Return + "\n}";	
		
		return Return;
	}
	
	/**
	 * Function to get map id 
	 * @return map_id Unique id corresponding to a map
	 */
	public String getMap_id() 
	{
		return map_id;
	}
	
	/**
	 * Function to set map id 
	 * @param map_id The map_id fetched above
	 */
	public void setMap_id(String map_id) 
	{
		this.map_id = map_id;
	}
	
	/**
	 * Function to get map name 
	 * @return map_name Name corresponding to the map
	 */
	public String getMap_name() 
	{
		return map_name;
	}
	
	/**
	 * Function to set map name 
	 * @param map_name Name corresponding to the map fetched above
	 */
	public void setMap_name(String map_name) 
	{
		this.map_name = map_name;
	}
	
	/**
	 * Function to get map sequence 
	 * @return map_sequence Sequence of a map in a campaign
	 */
	public String getMap_sequence() 
	{
		return map_sequence;
	}
	
	/**
	 * Function to set map sequence 
	 * @param map_sequence Sequence of a map in a campaign
	 */
	public void setMap_sequence(String map_sequence) 
	{
		this.map_sequence = map_sequence;
	}
}
