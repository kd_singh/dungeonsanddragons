package metadatas;

public class CharacterTypeMetadata 
{
	String Type;
	CharacterMetadata characterMetadata;
	
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public CharacterMetadata getCharacterMetadata() {
		return characterMetadata;
	}
	public void setCharacterMetadata(CharacterMetadata characterMetadata) {
		this.characterMetadata = characterMetadata;
	}

}
