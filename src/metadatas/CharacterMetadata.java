package metadatas;

import java.util.Observable;
import java.util.Vector;

import behavior.Behavior;
import utilities.CharacterType;
import utilities.CharacterUtility;

/**
 * This class is used to store state of a character.
 * All the abilities, ability modifiers are accumulated here.
 * 
 * @author Gurvinder
 */
public class CharacterMetadata extends Observable 
{
	/**
	 * String attributes for character id, name, level and backpack.
	 */
	String character_id;
	String character_name;
	String character_level;
	String character_backpack;
	
	/**
	 * Metadata Objects to store different attributes managed by this class.
	 */
	AbilityScoreMetadata abilityScoreMetadata;
	AbilityModifierMetadata abilityModifierMetadata;
	CharacterWearableMetadata characterWearableMetadata;
	Vector<BackpackMetadata> backpackMetadata;
	BonusMetadata bonusMetadata;
	ItemListMetaInterface weapon;
	
	/**
	 * String attributes for for hitpoints and bonuses.
	 */
	String hitPoints;
	String armorClass;
	String attackBonus;
	String damageBonus;

	Behavior behavior;
    String characterType = CharacterType.Player.toString();
	CharacterMetadata characterMetadata;

	public void setBehaviour(Behavior behavior)
	{
		this.behavior = behavior;
	}
	
	public void executeAttackBehavior(CharacterMetadata opponent)
	{
		this.behavior.executeAttack(this, opponent);
	}
	
	public void executeMoveBehavior(int curr_playerX, int curr_playerY, int opponent_playerX, int opponent_playerY)
	{
		this.behavior.executeMove( curr_playerX, curr_playerY, opponent_playerX, opponent_playerY);
	}
	
	public void executeLootBehavior()
	{
		this.behavior.executeLoot();
	}

	/**
	 * Default constructor for character metadata.
	 * Initializes the Metadata object.
	 */
	public CharacterMetadata()
	{
		this.weapon = new ItemListMetadata();

		this.characterMetadata = this;
		character_level = "1";
		abilityScoreMetadata = new AbilityScoreMetadata();
		abilityModifierMetadata = new AbilityModifierMetadata();
		bonusMetadata = new BonusMetadata();
		characterWearableMetadata = new CharacterWearableMetadata();
	}

	public String getCharacterType() {
		return characterType;
	}

	public void setCharacterType(String characterType) {
		this.characterType = characterType;
	}

	/**
	 * Getter for the Character id
	 * @return the character id
	 */
	public String getCharacter_id()
	{
		return character_id;
	}

	public ItemListMetaInterface getWeapon() {
		return weapon;
	}

	public void setWeapon(ItemListMetaInterface weapon) {
		this.weapon = weapon;
	}

	/**
	 * Setter for the Character id
	 *  
	 * @param character_id It is the character id.
	 */
	public void setCharacter_id(String character_id)
	{
		this.character_id = character_id;
	}
	
	/**
	 * Getter for character name
	 * 
	 * @return the character name
	 */
	public String getCharacter_name() 
	{
		return character_name;
	}
	
	/**
	 * Setter for character name
	 * @param character_name name of the Character
	 */
	public void setCharacter_name(String character_name)
	{
		this.character_name = character_name;
	}
	
	/**
	 * Getter for character level 
	 * @return character_level of the character
	 */
	public String getCharacter_level()
	{
		return character_level;
	}
	
	/**
	 * Setter for character level 
	 * @param character_level the level of the character to set
	 */
	public void setCharacter_level(String character_level) {
		this.character_level = character_level;
	}
	
	/**
	 * Gets the ability score metadata
	 * 
	 * @return the ability score metadata
	 */
	public AbilityScoreMetadata getAbilityScoreMetadata() 
	{
		return abilityScoreMetadata;
	}
	
	/**
	 * This method sets the ability score of the character.
	 * 
	 * @param abilityScoreMetadata It is the state of character's ability score.
	 */
	public void setAbilityScoreMetadata(AbilityScoreMetadata abilityScoreMetadata) 
	{
		this.abilityScoreMetadata = abilityScoreMetadata;
	}
	
	/**
	 * This method returns the ability score for the character.
	 * 
	 * @return the state of character ability score.
	 */
	public AbilityModifierMetadata getAbilityModifierMetadata() 
	{
		return abilityModifierMetadata;
	}
	
	/**
	 * This method sets the ability modifier.
	 * 
	 * @param abilityModifierMetadata It is the state of character modifiers.
	 */
	public void setAbilityModifierMetadata(AbilityModifierMetadata abilityModifierMetadata) 
	{
		this.abilityModifierMetadata = abilityModifierMetadata;
	}
	
	/**
	 * This gets the character wearable metadata.
	 * 
	 * @return an instance of character wearables.
	 */
	public CharacterWearableMetadata getCharacterWearableMetadata() {
		return characterWearableMetadata;
	}
	/**
	 * This method sets the character wearable metadata.e
	 * 
	 * @param characterWearableMetadata It is the state of character wearable.
	 */
	public void setCharacterWearableMetadata(CharacterWearableMetadata characterWearableMetadata) 
	{
		this.characterWearableMetadata = characterWearableMetadata;
	}
	
	/**
	 * This method returns a collection of all the backpack items.
	 * 
	 * @return the collection of items in character backpack.
	 */
	public Vector<BackpackMetadata> getBackpackMetadata() 
	{
		return backpackMetadata;
	}
	
	/**
	 * This method sets the character backpack metadata.
	 * 
	 * @param backpackMetadata It is the state of character backpack.
	 */
	public void setBackpackMetadata(Vector<BackpackMetadata> backpackMetadata) {
		this.backpackMetadata = backpackMetadata;
	}
	
	/**
	 * Getter for armor class
	 * 
	 * @return the armor class of the character.
	 */
	public String getArmorClass() 
	{
		return armorClass;
	}
	
	/**
	 * Setter for Armor Class
	 * 
	 * @param armorClass It is the character's Armor Class.
	 */
	public void setArmorClass(String armorClass)
	{
		this.armorClass = armorClass;
	}
	
	/**
	 * Getter for Attack Bonus
	 * 
	 * @return the attack bonus for the character.
	 */
	public String getAttackBonus() 
	{
		return attackBonus;
	}
	
	/**
	 * Setter for attack bonus.
	 * 
	 * @param attackBonus It is the character attack bonus.
	 */
	public void setAttackBonus(String attackBonus)
	{
		this.attackBonus = attackBonus;
	}
	
	/**
	 * Getter for damage bonus.
	 * 
	 * @return the character damage bonus.
	 */
	public String getDamageBonus()
	{
		return damageBonus;
	}
	
	/**
	 * Setter for Damage bonus
	 * @param damageBonus It is the character bonus.
	 */
	public void setDamageBonus(String damageBonus) 
	{
		this.damageBonus = damageBonus;
	}
	
	/**
	 * Getter for hit points.
	 * 
	 * @return the hit points
	 */
	public String getHitPoints() 
	{
		return hitPoints;
	}
	
	/**
	 * Setter for hit points
	 * 
	 * @param hitPoints hit points for the Character based on abilities
	 */
	public void setHitPoints(String hitPoints)
	{
		this.hitPoints = hitPoints;
	}
	
	/**
	 * Getter for the bonusmetadata of the Character
	 * @return the bonus metadata of the character
	 */
	public BonusMetadata getBonusMetadata() 
	{
		return bonusMetadata;
	}
	
	/**
	 * Setter for the bonusmetadata
	 * @param bonusMetadata metadata for the bonus modifiers of the character
	 */
	public void setBonusMetadata(BonusMetadata bonusMetadata) 
	{
		this.bonusMetadata = bonusMetadata;
	}
	
	/**
	 * This class converts the data to JSON format.
	 * 
	 * @param characterMetadata It is the character state.
	 * @return a JSON string of formatted Metadata.
	 */
	public String convertToString(CharacterMetadata characterMetadata)
	{

		AbilityScoreMetadata abilityScoreMetadata = new AbilityScoreMetadata();
		AbilityModifierMetadata abilityModifierMetadata = new AbilityModifierMetadata();
		CharacterWearableMetadata characterWearableMetadata = new CharacterWearableMetadata();
		BackpackMetadata backpackMetadata = new BackpackMetadata();
		BonusMetadata bonusMetadata = new BonusMetadata();

		String Return="{\n\t";
		Return = Return + "\"character_id\":"					+ "\""+  characterMetadata.getCharacter_id() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"character_name\":"					+ "\""+ characterMetadata.getCharacter_name() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"character_level\":"				+ "\""+ characterMetadata.getCharacter_level() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"ability_scores\":"			+ ""+ abilityScoreMetadata.convertToString(characterMetadata.getAbilityScoreMetadata()) +",";
		Return=  Return + "\n\t";
		Return = Return + "\"ability_modifier\":" 		+ ""+ abilityModifierMetadata.convertToString(characterMetadata.getAbilityModifierMetadata())  +",";
		Return=  Return + "\n\t";
		Return = Return + "\"character_wearable\":"			+ ""+ characterWearableMetadata.convertToString(characterMetadata.getCharacterWearableMetadata()) +",";
		Return=  Return + "\n\t";
		Return = Return + "\"item_bonus\":"			+ ""+ bonusMetadata.convertToString(characterMetadata.getBonusMetadata()) +",";
		Return=  Return + "\n\t";
		Return = Return + "\"armor_class\":"			+ "\""+ characterMetadata.getArmorClass() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"attack_bonus\":"			+ "\""+ characterMetadata.getAttackBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"damage_bonus\":"			+ "\""+ characterMetadata.getDamageBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"hit_points\":"			+ "\""+ characterMetadata.getHitPoints() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"backpack_items\":"        + "[";

		if(characterMetadata.getBackpackMetadata() == null)
		{
			Return = Return + "\n]";
		}
		else
		{	
			if(!(characterMetadata.getBackpackMetadata().size()>0))
			{
				Return = Return + "\n]";
			}
			else
			{
				Vector<BackpackMetadata> temp_vector_BackpackItems = characterMetadata.getBackpackMetadata();

				for(int i = 0 ; i<temp_vector_BackpackItems.size() ; i++)
				{
					Return=  Return + "\n\t";
					Return = Return + backpackMetadata.convertToString(temp_vector_BackpackItems.get(i));
					Return=  Return + ",\n";
				}

				Return = Return.substring(0, Return.length()-2);
				Return = Return + "\n]";
			}
		}
		Return=  Return + "\n}";

		return Return;

	}
	
	/**
	 * This method removes item from the backpack.
	 * 
	 * @param backpackMetadata It is the state of character's backpack
	 * @return a true if an item is removed from the backpack.
	 */
	public boolean actionRemovefromBackpack(BackpackMetadata backpackMetadata)
	{
		Vector<BackpackMetadata> vector_backpack = characterMetadata.getBackpackMetadata();
		if(vector_backpack == null)
		{
			return false;
		}
		else
		{
			for(int i=0;i<vector_backpack.size();i++)
			{
				BackpackMetadata exiting_backpackMetadata = vector_backpack.get(i);

				if(backpackMetadata.getItemId().equals(exiting_backpackMetadata.itemId)&& 
						backpackMetadata.getItemType().equals(exiting_backpackMetadata.getItemType()))
				{
					vector_backpack.remove(i);
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * This method moves the item back to backpack.
	 * 
	 * @param backpackMetadata state of the backpack metadata.
	 */
	public void actionMoveToBackPack(BackpackMetadata backpackMetadata)
	{
		Vector<BackpackMetadata> vector_backPack = characterMetadata.getBackpackMetadata();
		if(vector_backPack == null)
		{
			vector_backPack = new Vector<>();
			vector_backPack.add(backpackMetadata);
			characterMetadata.setBackpackMetadata(vector_backPack);
		}
		else
		{
			vector_backPack.add(backpackMetadata);
		}
		String itemType = backpackMetadata.getItemType();

		if(itemType.equals("HELMET"))
		{
			if(characterWearableMetadata.getItm_helmet() != null)
			{
				characterWearableMetadata.setItm_helmet(null);
			}
		}

		if(itemType.equals("ARMOR"))
		{
			if(characterWearableMetadata.getItm_armor() != null)
			{
				characterWearableMetadata.setItm_armor(null);
			}
		}

		if(itemType.equals("RING"))
		{
			if(characterWearableMetadata.getItm_ring() != null)
			{
				characterWearableMetadata.setItm_ring(null);
			}
		}

		if(itemType.equals("BOOTS"))
		{
			if(characterWearableMetadata.getItm_boots() != null)
			{
				characterWearableMetadata.setItm_boots(null);
			}
		}

		if(itemType.equals("BELT"))
		{
			if(characterWearableMetadata.getItm_belt() != null)
			{
				characterWearableMetadata.setItm_belt(null);
			}
		}

		if(itemType.equals("SHIELD"))
		{
			if(characterWearableMetadata.getItm_shield() != null)
			{
				characterWearableMetadata.setItm_shield(null);
			}
		}

		if(itemType.equals("WEAPON"))
		{
			if(characterWearableMetadata.getItm_weapon() != null)
			{
				characterWearableMetadata.setItm_weapon(null);;
			}
		}



		sendNotification();
	}

	/**
	 * This method moves an item to wearable.
	 * Calls <code>sendNotification</code> method
	 * @param backpackMetadata it is the state of character backpack.
	 */
	public void actionMoveToWearables(BackpackMetadata backpackMetadata)
	{
		Vector<BackpackMetadata> vector_backPack = characterMetadata.getBackpackMetadata();

		for (int i = 0; i < vector_backPack.size(); i++) 
		{
			if(vector_backPack.get(i).getItemId().equals(backpackMetadata.getItemId()) && 
					vector_backPack.get(i).getItemType().equals(backpackMetadata.getItemType()))
			{

				vector_backPack.remove(i);
			}
		}

		String itemType = backpackMetadata.getItemType();
		String itemID = backpackMetadata.getItemId();

		if(itemType.equals("HELMET"))
		{
				characterWearableMetadata.setItm_helmet(itemID);
		}

		if(itemType.equals("ARMOR"))
		{
				characterWearableMetadata.setItm_armor(itemID);
		}

		if(itemType.equals("RING"))
		{
				characterWearableMetadata.setItm_ring(itemID);
		}

		if(itemType.equals("BOOTS"))
		{
				characterWearableMetadata.setItm_boots(itemID);
		}

		if(itemType.equals("BELT"))
		{
				characterWearableMetadata.setItm_belt(itemID);
		}

		if(itemType.equals("SHIELD"))
		{
				characterWearableMetadata.setItm_shield(itemID);
		}

		if(itemType.equals("WEAPON"))
		{
				characterWearableMetadata.setItm_weapon(itemID);;
		}

		sendNotification();
	}

	public boolean actionSwitchToWearables(BackpackMetadata friendsBackPackMetadata)
	{

		String itemType = friendsBackPackMetadata.getItemType();
		String itemID = friendsBackPackMetadata.getItemId();
		Boolean setBonusflag = false;
		
		if(itemType.equals("HELMET"))
		{
			if(characterWearableMetadata.getItm_helmet() == null)
			{
				characterWearableMetadata.setItm_helmet(itemID);
				setBonusflag = true;
			}
			else
			{
				actionAddtoBackPack(friendsBackPackMetadata);
			}
		}

		if(itemType.equals("ARMOR"))
		{
			if(characterWearableMetadata.getItm_armor() == null)
			{
				characterWearableMetadata.setItm_armor(itemID);
				setBonusflag = true;

			}
			else
			{
				actionAddtoBackPack(friendsBackPackMetadata);
			}
		}

		if(itemType.equals("RING"))
		{
			if(characterWearableMetadata.getItm_ring() == null)
			{
				characterWearableMetadata.setItm_ring(itemID);
				setBonusflag = true;

			}
			else
			{
				actionAddtoBackPack(friendsBackPackMetadata);
			}
		}

		if(itemType.equals("BOOTS"))
		{
			if(characterWearableMetadata.getItm_boots() == null)
			{
				characterWearableMetadata.setItm_boots(itemID);
				setBonusflag = true;

			}
			else
			{
				actionAddtoBackPack(friendsBackPackMetadata);
			}
		}

		if(itemType.equals("BELT"))
		{
			if(characterWearableMetadata.getItm_belt() == null)
			{
				characterWearableMetadata.setItm_belt(itemID);
				setBonusflag = true;

			}
			else
			{
				actionAddtoBackPack(friendsBackPackMetadata);
			}
		}

		if(itemType.equals("SHIELD"))
		{
			if(characterWearableMetadata.getItm_shield() == null)
			{
				characterWearableMetadata.setItm_shield(itemID);
				setBonusflag = true;
			}
			else
			{
				actionAddtoBackPack(friendsBackPackMetadata);
			}
		}

		if(itemType.equals("WEAPON"))
		{
			if(characterWearableMetadata.getItm_weapon() == null)
			{
				characterWearableMetadata.setItm_weapon(itemID);
				setBonusflag = true;
			}
			else
			{
				actionAddtoBackPack(friendsBackPackMetadata);
			}
		}

		sendNotification();
		return setBonusflag;
	}

	public void removeItemFromWearables(BackpackMetadata backPackMetadata)
	{

		String itemType = backPackMetadata.getItemType();

		if(itemType.equals("HELMET"))
		{
			if(characterWearableMetadata.getItm_helmet() != null)
			{
				characterWearableMetadata.setItm_helmet(null);
			}
		}

		if(itemType.equals("ARMOR"))
		{
			if(characterWearableMetadata.getItm_armor() != null)
			{
				characterWearableMetadata.setItm_armor(null);
			}
		}

		if(itemType.equals("RING"))
		{
			if(characterWearableMetadata.getItm_ring() != null)
			{
				characterWearableMetadata.setItm_ring(null);
			}
		}

		if(itemType.equals("BOOTS"))
		{
			if(characterWearableMetadata.getItm_boots() != null)
			{
				characterWearableMetadata.setItm_boots(null);
			}
		}

		if(itemType.equals("BELT"))
		{
			if(characterWearableMetadata.getItm_belt() != null)
			{
				characterWearableMetadata.setItm_belt(null);
			}
		}

		if(itemType.equals("SHIELD"))
		{
			if(characterWearableMetadata.getItm_shield() != null)
			{
				characterWearableMetadata.setItm_shield(null);
			}
		}

		if(itemType.equals("WEAPON"))
		{
			if(characterWearableMetadata.getItm_weapon() != null)
			{
				characterWearableMetadata.setItm_weapon(null);;
			}
		}

		sendNotification();
	}

	public void actionAddtoBackPack(BackpackMetadata backpackMetadata)
	{
		Vector<BackpackMetadata> vector_backPack = characterMetadata.getBackpackMetadata();
		if(vector_backPack == null)
		{
			vector_backPack = new Vector<>();
			vector_backPack.add(backpackMetadata);
			characterMetadata.setBackpackMetadata(vector_backPack);
		}
		else
		{
			vector_backPack.add(backpackMetadata);
		}
	}
	/**
	 * Sends the notification to Observer.
	 * 
	 */
	public void sendNotification() 
	{
		setChanged();
		notifyObservers(this);
	} 

	/**
	 * This Method Sets the characteristic of the user.
	 */
	public void setCharacteristic()
	{
		CharacterUtility util = new CharacterUtility();
		BonusMetadata bonusMetadata = characterMetadata.getBonusMetadata();

		int armorScore, hitpoint, level, strength = 0;
		int dexterity = 0, constitution = 0, attackBonus = 0, damageBonus = 0;
		int armorClassBonus = 0, attackClassBonus = 0, damageClassBonus = 0;

		level = Integer.parseInt(characterMetadata.getCharacter_level());

		if(characterMetadata.abilityModifierMetadata.getAbm_dexterity() != null)
		{
			dexterity = Integer.parseInt(characterMetadata.abilityModifierMetadata.getAbm_dexterity());
		}
		if(!bonusMetadata.getArmorClassBonus().equals(""))
		{
			armorClassBonus = Integer.parseInt(bonusMetadata.getArmorClassBonus());
		}
		armorScore = util.calulateArmorClass(dexterity, armorClassBonus);
		characterMetadata.setArmorClass(String.valueOf(armorScore));

		if(characterMetadata.abilityModifierMetadata.getAbm_constitution() != null)
		{
			constitution = Integer.parseInt(characterMetadata.abilityModifierMetadata.getAbm_constitution());
		}
		hitpoint = util.calculateHitPoint(level, constitution);
		characterMetadata.setHitPoints(String.valueOf(hitpoint));

		if(!bonusMetadata.getAttackBonus().equals(""))
		{
			attackClassBonus = Integer.parseInt(bonusMetadata.getAttackBonus());
		}
		attackBonus = util.calculateAttackBonus(level, attackClassBonus);
		characterMetadata.setAttackBonus(String.valueOf(attackBonus));


		if(characterMetadata.getAbilityModifierMetadata().getAbm_strength() != null)
		{
			strength = Integer.parseInt(characterMetadata.getAbilityModifierMetadata().getAbm_strength());
		}
		if(!bonusMetadata.getDamageBonus().equals(""))
		{
			damageClassBonus = Integer.parseInt(bonusMetadata.getDamageBonus());
		}
		damageBonus = util.calculateDamageBonus(strength, damageClassBonus);
		characterMetadata.setDamageBonus(String.valueOf(damageBonus));

	}

	public void attack(CharacterMetadata opponentMetadata) {
		int opponentAC = Integer.parseInt(opponentMetadata.getArmorClass());

	}

	
}
