package metadatas;

/**
 * This Class is the Container for the Grid Cells Metadata to be stored in the Map.
 * @author Karan Deep Singh
 */
public class GridCellMetadata 
{

	public int xPosition;
	public int yPosition;
	public String value;
	public String type;
	
	/**
	 * getter for the X position
	 * @return x position
	 */
	public int getxPosition() 
	{
		return xPosition;
	}
	
	/**
	 * Setter for the X position
	 * @param xPosition integer value of the x position
	 */
	public void setxPosition(int xPosition) 
	{
		this.xPosition = xPosition;
	}
	
	/**
	 * Getter for the Y position on cell
	 * @return Y position of the cell
	 */
	public int getyPosition() 
	{
		return yPosition;
	}
	
	/** Setter for the Y position of the cell
	 * @param yPosition integer value of the Y postion of the Cell
	 */
	public void setyPosition(int yPosition) 
	{
		this.yPosition = yPosition;
	}
	
	/**
	 * getter for the Value of Cell
	 * @return value stored in cell data
	 */
	public String getValue() 
	{
		return value;
	}
	
	/**
	 * Setter for the value of Cell
	 * @param value String value for Cell
	 */
	public void setValue(String value) 
	{
		this.value = value;
	}
	
	/**
	 * Getter for the Type attribute of Cell
	 * @return String for type
	 */
	public String getType() 
	{
		return type;
	}
	
	/**
	 * Setter for the Type attribute of Cell
	 * @param type String for type of Cell
	 */
	public void setType(String type) 
	{
		this.type = type;
	}
	
	/**
	 * This Function Converts the Metadata to String to Write in Maps file.
	 * @param gridCellMetadata Metadata for the Cell
	 * @return String for the Cell data
	 */
	public String convertToString(GridCellMetadata gridCellMetadata)
	{
		String Return="{\n\t";
		Return = Return + "\"xposition\":"      + "\""+ gridCellMetadata.getxPosition() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"yposition\":"      + "\""+ gridCellMetadata.getyPosition() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"value\":"       + "\""+ gridCellMetadata.getValue() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"type\":"      + "\""+ gridCellMetadata.getType() +"\",";
		Return=  Return + "\n}";	
		
		return Return;
	}
}
