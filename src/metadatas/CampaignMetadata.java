package metadatas;

import java.util.Observable;
import java.util.Vector;

/**
 * This stores the Meta data related to Campaign
 * 
 *
 * @author Tarandeep Singh
 * @version 1.0.0
 * @since 07 February 2017
 * @see campaignMetadata
 * @see Observable
 */

public class CampaignMetadata extends Observable
{
	
	String campaign_id;
	String campaign_name;
	String campaign_size;
	Vector<CampaignMapsMetadata> vector_campaignMap;
	CampaignMetadata campaignMetadata;
	
	/**
	 * Constructor initializing CampaignMetadata
	 * 
	 */
	
	public CampaignMetadata() 
	{
		this.campaignMetadata = this;
	}
	
	/**
	 * Function to get campaign ID
	 * @return campaign_id Corresponding id to a campaign
	 */
	public String getCampaign_id() 
	{
		return campaign_id;
	}
	
	/**
	 * Function to set campaign ID
	 * @param campaign_id As fetched above
	 */
	public void setCampaign_id(String campaign_id) 
	{
		this.campaign_id = campaign_id;
	}
	
	/**
	 * Function to get campaign name
	 * @return campaign_name Corresponding name to a campaign
	 */
	public String getCampaign_name() 
	{
		return campaign_name;
	}
	
	/**
	 * Function to set campaign name
	 * @param campaign_name The name corresponding to a Campaign
	 */
	public void setCampaign_name(String campaign_name) 
	{
		this.campaign_name = campaign_name;
	}
	
	/**
	 * Function to get campaign size
	 * @return campaign_size Corresponding size of a campaign
	 */
	public String getCampaign_size() 
	{
		return campaign_size;
	}
	
	/**
	 * Function to set campaign size
	 * @param campaign_size Corresponding size of a campaign fetched above
	 */
	public void setCampaign_size(String campaign_size) 
	{
		this.campaign_size = campaign_size;
	}
	
	/**
	 * Function to get Vector containing Campaign maps data
	 * @return vector_campaignMap
	 */
	public Vector<CampaignMapsMetadata> getVector_campaignMap() 
	{
		return vector_campaignMap;
	}
	
	/**
	 * Function to set Vector containing Campaign maps data
	 *  @param vector_campaignMap The map data stored in a vector
	 */
	public void setVector_campaignMap(Vector<CampaignMapsMetadata> vector_campaignMap) 
	{
		this.vector_campaignMap = vector_campaignMap;
	}
	
	/**
	 * Function to convert the CampaignMetadata to String
	 * @param campaignMetadata The meta data corresponding to the campaigns
	 * @return Return Campaign meta data in form of string 
	 */
	public String convertToString(CampaignMetadata campaignMetadata)
	{
			CampaignMapsMetadata campaignMapsMetadata = new CampaignMapsMetadata();
		
			String Return="{\n\t";
			Return = Return + "\"campaign_id\":"      + "\""+ campaignMetadata.getCampaign_id() +"\",";
			Return=  Return + "\n\t";
			Return = Return + "\"campaign_name\":"      + "\""+ campaignMetadata.getCampaign_name() +"\",";
			Return=  Return + "\n\t";
			Return = Return + "\"campaign_size\":"       + "\""+ campaignMetadata.getCampaign_size() +"\",";
			Return=  Return + "\n\t";
			Return = Return + "\"campaign_maps\":"        + "[";

			Vector<CampaignMapsMetadata> temp_vector_campaignMap = campaignMetadata.getVector_campaignMap();
			
			for(int i = 0 ; i<temp_vector_campaignMap.size() ; i++)
			{
				Return=  Return + "\n\t";
				Return = Return + campaignMapsMetadata.convertToString(temp_vector_campaignMap.get(i));
				Return=  Return + ",\n";
			}
			
			Return = Return.substring(0, Return.length()-2);
			Return = Return + "\n]";
			Return=  Return + "\n}";	
			
			return Return;
	}
	
	/**
	 * Function to add maps to the Campaign
	 * @param mapMetadata The meta data corresponding to the maps in a campaign
	 */
	public void actionAddMaps(MapMetadata mapMetadata)
	{
		Vector<CampaignMapsMetadata> vector_mapsMetadata;
		
		if(campaignMetadata.getVector_campaignMap() == null)
		{
			vector_mapsMetadata = new Vector<CampaignMapsMetadata>();
		}
		else
		{
			vector_mapsMetadata = campaignMetadata.getVector_campaignMap();
		}
		
		String newMapSequence = String.valueOf(vector_mapsMetadata.size() + 1);
		campaignMetadata.setCampaign_size(newMapSequence);
		
		CampaignMapsMetadata newMap = new CampaignMapsMetadata();
		
		newMap.setMap_id(mapMetadata.getMap_id());
		newMap.setMap_name(mapMetadata.getMap_name());
		newMap.setMap_sequence(newMapSequence);
		
		vector_mapsMetadata.addElement(newMap);
		campaignMetadata.setVector_campaignMap(vector_mapsMetadata);
		
		setChanged();
		notifyObservers();
		
	}

	/**
	 * Function to delete maps from the Campaign
	 * @param mapMetadata The meta data corresponding to the maps in campaign
	 */
	public void actionDeleteMaps(MapMetadata mapMetadata)
	{
		Vector<CampaignMapsMetadata> vector_mapsMetadata;
		
		if(campaignMetadata.getVector_campaignMap() != null && campaignMetadata.getVector_campaignMap().size() > 0)
		{
			vector_mapsMetadata = campaignMetadata.getVector_campaignMap();
		
			String newMapSequence = String.valueOf(vector_mapsMetadata.size() - 1);
			campaignMetadata.setCampaign_size(newMapSequence);
			
			vector_mapsMetadata.remove(vector_mapsMetadata.size() - 1);
			campaignMetadata.setVector_campaignMap(vector_mapsMetadata);
			
			setChanged();
			notifyObservers();
		}
		
	}

}
