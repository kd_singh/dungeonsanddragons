package metadatas;

import java.util.Observable;

/**
 * This class is used to store the modifier data for all the abilities that a character can possess.
 * 
 * @author Gurvinder Singh
 * @see CharacterMetadata
 */
public class AbilityModifierMetadata extends Observable {
	
	/**
	 * It is strength modifier for a character
	 */
	String abm_strength;
	/**
	 * It is dexterity modifier for a character
	 */
	String abm_dexterity;
	/**
	 * It is constitution modifier for a character
	 */
	String abm_constitution;
	/**
	 * It is intelligence modifier for a character
	 */
	String abm_intelligence;
	/**
	 * It is wisdom modifier for a character
	 */
	String abm_wisdom;
	/**
	 * It is charisma modifier for a character
	 */
	String abm_charisma;
	
	/**
	 * This method is used to convert an ability modifier to JSON format.
	 * 
	 * @param abilityModifierMetadata It is the instance of abilityModifier containing data.
	 * @return returns a string in JSON format.
	 */
	public String convertToString(AbilityModifierMetadata abilityModifierMetadata)
	{
		String Return="{\n\t";
		Return = Return + "\"abm_strength\":"      + "\""+ abilityModifierMetadata.getAbm_strength() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"abm_dexterity\":"      + "\""+ abilityModifierMetadata.getAbm_dexterity() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"abm_constitution\":"       + "\""+ abilityModifierMetadata.getAbm_constitution() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"abm_intelligence\":"        + "\""+ abilityModifierMetadata.getAbm_intelligence() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"abm_wisdom\":"          + "\""+ abilityModifierMetadata.getAbm_wisdom()+"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"abm_charisma\":" + "\""+ abilityModifierMetadata.getAbm_charisma()  +"\"";
		Return=  Return + "\n}";	
		
		return Return;
	}
	
	/**
	 * This method is a getter for strength ability modifier. 
	 * @return a value for strength ability modifier. 
	 */
	public String getAbm_strength() 
	{
		return abm_strength == null ? "": abm_strength;
	}
	
	/**
	 * This method is a setter method for strength ability modifier. 
	 * @param abm_strength It is the value of strength ability modifier.
	 */
	public void setAbm_strength(String abm_strength) 
	{
		this.abm_strength = abm_strength;
		setChanged();
		notifyObservers(this);
	}
	
	/**
	 * This method is a getter for dexterity ability modifier. 
	 * @return a value for dexterity ability modifier. 
	 */
	public String getAbm_dexterity() 
	{
		return abm_dexterity == null ? "": abm_dexterity;
	}
	
	/**
	 * This method is a setter method for dexterity ability modifier. 
	 * @param abm_dexterity It is the value of dexterity ability modifier.
	 */
	public void setAbm_dexterity(String abm_dexterity) 
	{
		this.abm_dexterity = abm_dexterity;
		setChanged();
		notifyObservers(this);
	}
	
	/**
	 * This method is a getter for constitution ability modifier. 
	 * @return a value for constitution ability modifier. 
	 */
	public String getAbm_constitution() 
	{
		return abm_constitution == null ? "": abm_constitution;
	}
	
	/**
	 * This method is a setter method for constitution ability modifier. 
	 * @param abm_constitution It is the value of dexterity ability modifier.
	 */
	public void setAbm_constitution(String abm_constitution) 
	{
		this.abm_constitution = abm_constitution;
		setChanged();
		notifyObservers(this);
	}
	
	/**
	 * This method is a getter for intelligence ability modifier. 
	 * @return a value for intelligence ability modifier. 
	 */
	public String getAbm_intelligence() 
	{
		return abm_intelligence == null ? "": abm_intelligence;
	}
	
	/**
	 * This method is a setter method for intelligence ability modifier. 
	 * @param abm_intelligence It is the value of intelligence ability modifier.
	 */
	public void setAbm_intelligence(String abm_intelligence) 
	{
		this.abm_intelligence = abm_intelligence;
		setChanged();
		notifyObservers(this);
	}
	
	/**
	 * This method is a getter for wisdom ability modifier. 
	 * @return a value for wisdom ability modifier. 
	 */
	public String getAbm_wisdom() 
	{
		return abm_wisdom == null ? "": abm_wisdom;
	}
	
	/**
	 * This method is a setter method for wisdom ability modifier. 
	 * @param abm_wisdom It is the value of wisdom ability modifier.
	 */
	public void setAbm_wisdom(String abm_wisdom) 
	{
		this.abm_wisdom = abm_wisdom;
		setChanged();
		notifyObservers(this);
	}
	
	/**
	 * This method is a getter for charisma ability modifier. 
	 * @return a value for charisma ability modifier. 
	 */
	public String getAbm_charisma() 
	{
		return abm_charisma == null ? "": abm_charisma;
	}
	
	/**
	 * This method is a setter method for charimsa ability modifier. 
	 * 
	 * @param abm_charisma It is the value of charimsa ability modifier.
	 */
	public void setAbm_charisma(String abm_charisma) 
	{
		this.abm_charisma = abm_charisma;
		setChanged();
		notifyObservers(this);
	}
	


}
