package metadatas;

/**
 * BonusMetadata is used to set the bonus based on the item selected by the user as wearable item.
 * 
 * @author Gurvinder Singh
 * @see CharacterMetadata
 */
public class BonusMetadata 
{
	
	/**
	 * It is the dexterity bonus.
	 */
	String dexBonus;
	
	/**
	 * It is the intelligence bonus.
	 */
	String intBonus;
	
	/**
	 * It is the strength bonus.
	 */
	String strBonus;
	
	/**
	 * It is the wisdom bonus.
	 */
	String wisBonus;
	
	/**
	 * It is the constitution bonus.
	 */
	String conBonus;
	
	/**
	 * It is the character bonus.
	 */
	String chaBonus;
	
	/**
	 * It is the armor class bonus.
	 */
	String armorClassBonus;
	
	/**
	 * It is the damage bonus.
	 */
	String damageBonus;
	
	/**
	 * It is the attack bonus.
	 */
	String attackBonus;

	/**
	 * It is the constructor for the bonus metadata.
	 */
	
	public BonusMetadata() 
	{
		this.dexBonus = "";
		this.intBonus = "";
		this.strBonus = "";
		this.wisBonus = "";
		this.conBonus = "";
		this.chaBonus = "";
		this.armorClassBonus = "";
		this.damageBonus = "";
		this.attackBonus = "";
	}

	public String convertToString(BonusMetadata bonusMetadata)
	{
		String Return="{\n\t";
		Return = Return + "\"dex_bonus\":"      + "\""+ bonusMetadata.getDexBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"int_bonus\":"      + "\""+ bonusMetadata.getIntBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"str_bonus\":"      + "\""+ bonusMetadata.getStrBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"wis_bonus\":"      + "\""+ bonusMetadata.getWisBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"con_bonus\":"      + "\""+ bonusMetadata.getConBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"cha_bonus\":"      + "\""+ bonusMetadata.getChaBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"itm_armor_bonus\":"      + "\""+ bonusMetadata.getArmorClassBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"itm_attack_bonus\":"      + "\""+ bonusMetadata.getAttackBonus() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"itm_damage_bonus\":"      + "\""+ bonusMetadata.getDamageBonus() +"\"";
		Return=  Return + "\n}";	
		
		return Return;
	}
	
	
	public String getDexBonus() {
		return dexBonus;
	}
	public void setDexBonus(String dexBonus) {
		this.dexBonus = dexBonus;
	}
	public String getIntBonus() {
		return intBonus;
	}
	public void setIntBonus(String intBonus) {
		this.intBonus = intBonus;
	}
	public String getStrBonus() {
		return strBonus;
	}
	public void setStrBonus(String strBonus) {
		this.strBonus = strBonus;
	}
	public String getWisBonus() {
		return wisBonus;
	}
	public void setWisBonus(String wisBonus) {
		this.wisBonus = wisBonus;
	}
	public String getConBonus() {
		return conBonus;
	}
	public void setConBonus(String conBonus) {
		this.conBonus = conBonus;
	}
	public String getChaBonus() {
		return chaBonus;
	}
	public void setChaBonus(String chaBonus) {
		this.chaBonus = chaBonus;
	}
	public String getArmorClassBonus() {
		return armorClassBonus;
	}
	public void setArmorClassBonus(String armorClassBonus) {
		this.armorClassBonus = armorClassBonus;
	}
	public String getDamageBonus() {
		return damageBonus;
	}
	public void setDamageBonus(String damageBonus) {
		this.damageBonus = damageBonus;
	}
	public String getAttackBonus() {
		return attackBonus;
	}
	public void setAttackBonus(String attackBonus) {
		this.attackBonus = attackBonus;
	}
	
}
