package metadatas;

import java.util.HashMap;

public class ItemListMetadataDecorator extends ItemListMetaInterface
{
    public final ItemListMetaInterface decoratedItemListMetadata;
    
    String list_item_id;
	String list_item_name;
	String list_item_enchanment;
	String list_item_bonus;
	HashMap<String,String> weaponEnchantmentMap;
	
    public ItemListMetadataDecorator(ItemListMetaInterface decoratedItemListMetadata)
    {
    	this.decoratedItemListMetadata = decoratedItemListMetadata;
    }
    
    public HashMap<String,String> getWeaponEnchantmentType()
    {
    	return this.decoratedItemListMetadata.getWeaponEnchantmentType();
    }
    
	public void setWeaponEnchantmentType(String key, String value) {
		this.decoratedItemListMetadata.setWeaponEnchantmentType(key, value);
	}
    
    /**
	 * Setter for the List item id
	 * @param list_item_id id of the list item
	 */
	public void setList_item_id(String list_item_id)
	{
		this.decoratedItemListMetadata.setList_item_id(list_item_id);
	}

    
    /**
	 * Setter for the list item name
	 * @param list_item_name name of the list item
	 */
	public void setList_item_name(String list_item_name) 
	{
		this.decoratedItemListMetadata.setList_item_name(list_item_name);
	}

	/**
	 * Setter for the list item enchantment
	 * @param list_item_enchanment list item enchantment attribute
	 */
	public void setList_item_enchanment(String list_item_enchanment) 
	{
		this.decoratedItemListMetadata.setList_item_enchanment(list_item_enchanment);
	}

	/**
	 * Setter for the list item bonus
	 * @param list_item_bonus list item bonus
	 */
	public void setList_item_bonus(String list_item_bonus) 
	{
		this.decoratedItemListMetadata.setList_item_bonus(list_item_bonus);
	}

	/**
	 * Getter for the List item id
	 * @return item list id
	 */
	public String getList_item_id() 
	{
		return this.decoratedItemListMetadata.getList_item_id();
	}

	/**
	 * Getter for the List item name
	 * @return list item name
	 */
	public String getList_item_name() 
	{
		return this.decoratedItemListMetadata.getList_item_name();
	}

	/**
	 * Getter for the list item bonus
	 * @return list item bonus
	 */
	public String getList_item_bonus() 
	{
		return this.decoratedItemListMetadata.getList_item_bonus();
	}

	/**
	 * Getter for the list item enchantment
	 * @return list item enchantment
	 */
	public String getList_item_enchanment() 
	{
		return this.decoratedItemListMetadata.getList_item_enchanment();
	}

	public void setRange(String range)
	{
		this.decoratedItemListMetadata.setRange(range);
	}

	public void setDecoraterWeapon(String decoraterWeapon)
	{
		this.decoratedItemListMetadata.setDecoraterWeapon(decoraterWeapon);
	}

	public String getRange() 
	{
		return this.decoratedItemListMetadata.getRange();
	}

	public String getDecoraterWeapon() 
	{
		return this.decoratedItemListMetadata.getDecoraterWeapon();
	}

	

}
