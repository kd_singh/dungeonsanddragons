package metadatas;

import java.util.Vector;


/**
 * Base Class to Handle the Map Metadata.
 * @author Vijay Karan Singh
 *
 */
public class MapMetadata {
	
	/**
	 * String for Map ID
	 */
	String map_id; 
	
	/**
	 * String for Map Name
	 */
	String map_name;
	
	/**
	 * String for Map Size
	 */
	String map_size;
	
	/**
	 * Vector for Map Cells layout
	 */
	Vector<GridCellMetadata> map_layout;
	
	/**
	 * Getter for the Map id associated in MapMetaData
	 * @return id of the map
	 */
	public String getMap_id() 
	{
		return map_id;
	}
	
	/**
	 * Setter for the map Id in MetaData
	 * @param map_id id of the map to store
	 */
	public void setMap_id(String map_id) 
	{
		this.map_id = map_id;
	}
	
	
	/**
	 * Getter for the Map Name
	 * @return name of the map
	 */
	public String getMap_name() 
	{
		return map_name;
	}
	
	/**
	 * Setter for the Map Name
	 * @param map_name Name of the map to save
	 */
	public void setMap_name(String map_name) 
	{
		this.map_name = map_name;
	}
	
	/**
	 * Getter for the Map Size
	 * @return size of the Map
	 */
	public String getMap_size() 
	{
		return map_size;
	}
	
	
	/**
	 * Setter for the Map Size.
	 * @param map_size
	 */
	public void setMap_size(String map_size) 
	{
		this.map_size = map_size;
	}
	
	
	/**
	 * Getter for the MapLayout Vector
	 * @return vector of map cells layout with data
	 */
	public Vector<GridCellMetadata> getMap_layout() 
	{
		return map_layout;
	}
	
	
	/**
	 * Setter for the Map Layout Vector
	 * @param map_layout Vector containing the Map Layout MetaData
	 */
	public void setMap_layout(Vector<GridCellMetadata> map_layout) 
	{
		this.map_layout = map_layout;
	}
	
	/**
	 * Method that converts the MetaData to String to Push to maps File
	 * @param mapMetadata map MetaData containing cells informations
	 * @return String to save as Map Layout in Maps File
	 */
	public String convertToString(MapMetadata mapMetadata)
	{	
		GridCellMetadata gridCellMetadata = new GridCellMetadata();
		String Return="{\n\t";
		Return = Return + "\"map_id\":"      + "\""+ mapMetadata.getMap_id() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"map_name\":"      + "\""+ mapMetadata.getMap_name() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"map_size\":"      + "\""+ mapMetadata.getMap_size() +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"map_layout\":"        + "[";

		Vector<GridCellMetadata> temp_vector_GridCell = mapMetadata.getMap_layout();
		
		for(int i = 0 ; i<temp_vector_GridCell.size() ; i++)
		{
			Return=  Return + "\n\t";
			Return = Return + gridCellMetadata.convertToString(temp_vector_GridCell.get(i));
			Return=  Return + ",\n";
		}
		
		Return = Return.substring(0, Return.length()-2);
		Return = Return + "\n]";
		Return=  Return + "\n}";	
		
		return Return;
		
	}
}
