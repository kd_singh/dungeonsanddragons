package metadatas;

import java.util.HashMap;

public abstract class ItemListMetaInterface 
{
	HashMap<String,String> weaponEnchantmentMap = new HashMap<>();
	public abstract HashMap<String, String> getWeaponEnchantmentType();
	public abstract void setWeaponEnchantmentType(String key, String value);
	public abstract void setList_item_name(String itemName);
	public abstract void setList_item_enchanment(String itemEnchantment);
	public abstract void setList_item_bonus(String itemBonus);
	public abstract void setRange(String range);
	public abstract void setList_item_id(String newItemID);
	public abstract void setDecoraterWeapon(String decoraterWeapon);
	
//	public abstract String convertToString(ItemListMetaInterface itemListMetaInterface);
	public abstract String getList_item_id();
	public abstract String getList_item_name();
	public abstract String getList_item_bonus();
	public abstract String getList_item_enchanment();
	public abstract String getRange();
	public abstract String getDecoraterWeapon();



}
