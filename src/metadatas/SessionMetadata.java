package metadatas;

import java.util.Vector;

public class SessionMetadata {

	String session_id;
	String session_name;
	String campaign_id;
	String progress_seq;
	Vector<CharacterTypeMetadata> vector_characterTypeMetadata;
	MapMetadata mapMetadata;
	
	SessionMetadata sessionMetadata;
	
	/**
	 * Constructor initializing SessionMetadata
	 * 
	 */
	public SessionMetadata(){
		
		this.sessionMetadata = this;
	}
		
	/**
	 * Function to get session ID
	 * @return session_id Corresponding id to a campaign
	 */
	public String getSession_id() 
	{
		return session_id;
	}
	
	public void setSession_id(String session_id) 
	{
		this.session_id = session_id;
	}
	
	/**
	 * Function to get session name
	 * @return SessionName Corresponding name to a session
	 */
	public String getSession_name() 
	{
		return session_name;
	}
	
	/**
	 * Function to set campaign name
	 * @param campaign_name The name corresponding to a Campaign
	 */
	public void setSession_name(String session_name) 
	{
		this.session_name = session_name;
	}
	
	/**
	 * Function to get campaign id
	 * @return campaign id Corresponding campaign id to a session
	 */
	public String getCampaig_Id() 
	{
		return campaign_id;
	}
	
	/**
	 * Function to set campaign id
	 * @param campaign_id The id corresponding to a Campaign
	 */
	public void setCampaign_id(String campaign_id) 
	{
		this.campaign_id = campaign_id;
	}
	
	/**
	 * Function to get progress sequence
	 * @return sequence Corresponding sequence of a map in campaign
	 */
	public String getProgress_seq() 
	{
		return progress_seq;
	}
	
	/**
	 * Function to set progress sequence
	 * @return sequence Corresponding sequence of a map in campaign
	 */
	public void setProgress_seq(String progress_seq) 
	{
		this.progress_seq = progress_seq;
	}

	public Vector<CharacterTypeMetadata> getVector_characterMetadata() {
		return vector_characterTypeMetadata;
	}

	public void setVector_characterMetadata(Vector<CharacterTypeMetadata> vector_characterMetadata) {
		this.vector_characterTypeMetadata = vector_characterMetadata;
	}

	public MapMetadata getMapMetadata() {
		return mapMetadata;
	}

	public void setMapMetadata(MapMetadata mapMetadata) {
		this.mapMetadata = mapMetadata;
	}

	/**
	 * This class converts the data to JSON format.
	 * 
	 * @param characterMetadata It is the character state.
	 * @return a JSON string of formatted Metadata.
	 */
	public String convertToString(SessionMetadata sessionMetadata)
	{
		String Return="{\n\t";
		Return = Return + "\"session_id\":"      + "\""+ session_id +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"session_name\":"					+ "\""+ session_name +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"campaign_id\":"					+ "\""+ campaign_id +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"progress_seq\":"					+ "\""+ progress_seq +"\",";
		Return=  Return + "\n\t";
		Return = Return + "\"map_info\":"					    + mapMetadata.convertToString(mapMetadata) +",";
		Return=  Return + "\n\t";
		Return = Return + "\"character_info\":"        + getString();
		
//		if(vector_characterTypeMetadata.isEmpty())
//		{
//			Return = Return + "\n]";
//		}
//		
//		else
//		{
//
//			for(int i = 0 ; i<vector_characterTypeMetadata.size() ; i++)
//			{
//				Return=  Return + "\n\t";
////				Return = Return + vector_characterMetadata.elementAt(i).convertToString(vector_characterMetadata.get(i));
//				Return=  Return + ",\n";
//			}
//
//			Return = Return.substring(0, Return.length()-2);
//			Return = Return + "\n]";
//		}
	
		
		Return=  Return + "\n}";

		return Return;
	}
	
	public String getString()
	{
		String Return="[\n\t";
		
		for(int i = 0; i<vector_characterTypeMetadata.size(); i++)
		{
			CharacterTypeMetadata tempMetadata = vector_characterTypeMetadata.get(i);
			CharacterMetadata characterMetadata = new CharacterMetadata();
			
		    Return = Return + "{\n\t";
			Return = Return + "\"char_type\":"      + "\""+ tempMetadata.getType() +"\",";
			Return=  Return + "\n\t";
			Return = Return + "\"char_data\":"      + characterMetadata.convertToString(tempMetadata.getCharacterMetadata());
			
			Return=  Return + "\n}";
			
			if(i != vector_characterTypeMetadata.size() - 1)
			{
				Return=  Return + ",";
			}
			
		}
		
		Return = Return + "]\n\t";

		return Return;
	}
}
