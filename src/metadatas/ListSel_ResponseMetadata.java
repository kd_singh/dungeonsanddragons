package metadatas;

/*
 * This class is a representation of data on selection of JList
 */

public class ListSel_ResponseMetadata {
	
	
	String selected_name;
	String selected_id;
	
	/**
	 * This method returns the name of item selected on a List
	 * @return selected_name
	 */
	public String getSelected_name() {
		return selected_name;
	}
	
	/**
	 * This method returns the name of the item selected in list.
	 * @param selected_name
	 */
	public void setSelected_name(String selected_name) {
		this.selected_name = selected_name;
	}
	
	/**
	 * This method returns the id of the item selected in list.
	 * @return selected_id
	 */
	public String getSelected_id() {
		return selected_id;
	}
	
	/**
	 * This method sets the id of the selected item in List
	 * @param selected_id
	 */
	public void setSelected_id(String selected_id) {
		this.selected_id = selected_id;
	}
	
	
}
