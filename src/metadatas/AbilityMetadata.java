package metadatas;

/**
 * This class is used to store the ability score and ability modifier metadatas.
 */
public class AbilityMetadata 
{
    AbilityScoreMetadata abilityScoreMetadata;
    AbilityModifierMetadata abilityModifierMetadata;

    /**
     * It is the constructor for AbilityMetadata.
     * 
     * @param abilityScoreMetadata It is an instance of AbilityScoreMetadata.
     * @param abilityModifierMetadata It is an instance of AbilityModifierMetadata.
     */
    public AbilityMetadata(AbilityScoreMetadata abilityScoreMetadata, AbilityModifierMetadata abilityModifierMetadata) 
    {
        this.abilityScoreMetadata = abilityScoreMetadata;
        this.abilityModifierMetadata = abilityModifierMetadata;
    }

    /**
     * Getter method for AbilityScoreMetadata
     *  
     * @return an instance of AbilityScoreMetadata
     */
    public AbilityScoreMetadata getAbilityScoreMetadata() 
    {
        return abilityScoreMetadata;
    }

    /**
     * Setter method for Ability score metadata.
     * 
     * @param abilityScoreMetadata It is an instance of AbilityScoreMetadata.
     */
    public void setAbilityScoreMetadata(AbilityScoreMetadata abilityScoreMetadata)
    {
        this.abilityScoreMetadata = abilityScoreMetadata;
    }
    
    /**
     * Getter method for AbilityModifierMetadata
     * 
     * @return an instance of AbilitityModifierMetadata.
     */
    public AbilityModifierMetadata getAbilityModifierMetadata() 
    {
        return abilityModifierMetadata;
    }

    /**
     * Setter method for AbilityModifierMetadata
     * 
     * @param abilityModifierMetadata an instance of AbilityModifierMetadata.
     */
    public void setAbilityModifierMetadata(AbilityModifierMetadata abilityModifierMetadata) 
    {
        this.abilityModifierMetadata = abilityModifierMetadata;
    }
}
